# Description
APL@Voro is a program designed to aid in the analysis of lipid bilayer simulations carried out by gromacs.
It calculates the area per lipid and the membrane thickness even for mixed bilayers. Colored Voronoi diagrams
and different types of plots are presented in an interactive environment. When you use APL@Voro in your research please cite:

* Gunther Lukat, Jens Kr�ger, Bj�rn Sommer: [APL@Voro: A Voronoi-Based Membrane Analysis Tool for GROMACS Trajectories](https://pubs.acs.org/doi/10.1021/ci400172g), J. Chem. Inf. Model.201353112908-2925
# Homepage
[http://aplvoro.com](http://aplvoro.com)

# Installation
## Debian
https://bitbucket.org/martin-kern/apl-voro/raw/3e9d37f6499160e15281571fb95dbecceca362af/aplvoro_3.3.3_x64.deb  
sha256 sum: 8a67d0ad148b755c4a351e2d81fc80806d882f890815acec8a22b8a08fe92a9f

## Red-Hat / Fedora
https://bitbucket.org/martin-kern/apl-voro/raw/3e9d37f6499160e15281571fb95dbecceca362af/aplvoro-3.3.3-1.x86_64.rpm  
sha256 sum: a8cd0a8c068f5c62fb12cdd0cc4da9b07aca0cfa7d1e40b8fb5240285a08a556

## Install from source

### Linux

**Requirements:**

* Qt 5.15.2 or higher
* g++ 7.4.0 or higher
* Eigen3

Download the source code and unpack the archive. Use following console commands to install APL@Voro in `<path>`.
By default APL@Voro will be installed in `/usr/`.

```shell
    qmake aplvoro.pro PREFIX=<path>
    make
    make install
```

# Team
Maintainer since 2018:

* Martin Kern, Uni. of Konstanz

Original Developer until 2018:

* Gunther Lukat, Bielefeld Uni.

Supervisors:

* Bj�rn Sommer, Royal College of Art
* Jens Kr�ger, Uni. of T�bingen

# Changelog 

## Version 3.3.3
* Fixed xvg export which was broken in 3.3.2

## Version 3.3.2
* Re-enabled the command line functionality
* Fixed man page for the deb package
* When opening a new simulation the option "detect by CRYST_1 record" is now selected by default
* Items in the list of open simulations are no longer selectable

## Version 3.3.1
* Fixed a multi-threading issue when loading trajectories
* Fixed a bug that can cause a crash when removing a curve from a 2D plot
* Fixed an off by one error where one more frame than desired is loaded
* Changed the dropdown menu for frame selection in the frame export dialog to spin boxes
* Fixed some typos in the manual

## Version 3.3.0
* The Voronoi view has been overhauled.
    * The tool bar was removed and is replaced by a context menu.
    * Hold the middle mouse buttonm to pan the view.
    * The information from properties table was moved into the selection tree.
    * Hovering the cursor over a Voronoi cell will display details on that cell.
    * Image export includes the legend.
* A position-based algorithm for leaflet detection was added
* Data for 2D plots can be restricted to a selection


## Version 3.2.1
### New features
* Load multiple simulations and compare them in one session
* Synchronize simulations along specific frames.

### Known issues
* Massive memory leaks
* Image export of the Voronoi view will not include the legend.

## Version 3.1.0
### New features
* New color scales for visualization
* Select first frame, last frame and stride for trajectories

### Bug fixes
* Crash when clicking the middle mose button
* Crash while playing the simulation with some trajectories
* Manual definition of the box size would always create a cube with all sides having the length of the X dimension

### Other changes
* Changed the frame counter from a dropdown list to a spin box
* Manual will be opened in a webbrowser