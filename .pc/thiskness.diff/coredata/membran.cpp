//**************************************************************
//                  membrane.cpp
//           Copyright (c) 2012 - 2013 by Gunther Lukat
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************
/*!
 * ... text ...
 */
#include "membran.h"
#include "molecule.h"
#include "layer.h"
#include "atom.h"
#include <string>
#include <QPolygonF>

Membran::Membran()
{

    upside = new Layer();


    downside = new Layer();

    maxX = 0;
    maxY = 0;
    framenr =0;
    time = 0;

}




void Membran::makeVoronoi(int &a, int mode, bool vdwcheck, double upright, double downright)
{
    double X = this->maxX;
    double Y = this->maxY;
    upside->graphit(a,X,Y,protein, vdwcheck, upright, downright);
    downside->graphit(a,X,Y,protein,vdwcheck, upright, downright);
    protein.clear();
    calcThickness(mode);

}
Layer& Membran::getUP()
{
    return *upside;
}
Layer& Membran::getDOWN()
{
    return *downside;
}



void Membran::calcThickness(int mode)
{


    upside->Minthick = 100000000;
    upside->Maxthick = 0;
    downside->Minthick = 100000000;
    downside->Maxthick = 0;
    QHash<int,Atom>::iterator it;
    QHash<int,Atom>::iterator itd;
    for(it = upside->QHside.begin(); it != upside->QHside.end(); it++)
    {
           Atom *b= &it.value();
           QPointF2D p(b->getX(),b->getY());
           Triangle tri;
           tri = downside->t->TriangleContainsPoint(p);

           double f1,f2,f3;
           if(!(tri.A.number < 0||tri.B.number <0||tri.C.number <0))
           {
               if(IsPointInTriangle(p,tri))
               {
                   double f1z =downside->QHside.find(tri.A.number)->getZ();
                   double f2z =downside->QHside.find(tri.B.number)->getZ();
                   double f3z =downside->QHside.find(tri.C.number)->getZ();


                   if(mode ==1) //use Pandit
                   {   double x1;
                       double d1 = distanceEuclidean(p,tri.A);
                       double d2 = distanceEuclidean(p,tri.B);
                       double d3 = distanceEuclidean(p,tri.C);
                       if (d1 <= d2)
                       {
                           if (d1 <= d3)
                               {
                               x1 = b->getZ()-f1z;
                                       x1 < 0? b->setThick(x1+maxZ) : b->setThick(x1);
                              // d1 is smallest;
                               }
                             else
                              {
                               x1 = b->getZ()-f3z;
                               x1 < 0? b->setThick(x1+maxZ) : b->setThick(x1);//d3 is smallest;
                           }
                       }
                       else
                       {
                           if  (d2 <= d3)
                             {
                               x1 = b->getZ()-f2z;
                               x1 < 0? b->setThick(x1+maxZ) : b->setThick(x1);// d2 is smallest
                           }
                           else
                            {
                               x1 = b->getZ()-f3z;
                              x1 < 0? b->setThick(x1+maxZ) : b->setThick(x1);//  d3 smallest
                           }
                       }


                   }
                   else //use Lukat
                   {
                       float x1;
                       x1= b->getZ()-f1z;
                       x1 < 0? f1 = x1+maxZ : f1 = x1;


                       x1 = b->getZ()-f2z;
                       x1 < 0 ? f2 = x1+maxZ: f2 = x1;

                       x1 = b->getZ()-f3z;
                       x1 < 0 ? f3 = x1+maxZ: f3 = x1;


                       b->setThick(interpTriangle(b->getX(),b->getY(),tri.A.x(),tri.A.y(),f1,tri.B.x(),tri.B.y(),f2,tri.C.x(),tri.C.y(),f3)/10);
                   }

               }
           }
       }
        for(itd = downside->QHside.begin(); itd != downside->QHside.end(); itd++)
        {
            Atom *b= &itd.value();
            QPointF2D p(b->getX(),b->getY());
            p.number = b->getNumber();
            Triangle tri;
            tri = upside->t->getTriangleCCW(p);
            double f1,f2,f3;
            if(!(tri.A.number < 0||tri.B.number <0||tri.C.number <0))
            {
                if(IsPointInTriangle(p,tri))
                {
                   double f1z =upside->QHside.find(tri.A.number)->getZ();
                    double f2z =upside->QHside.find(tri.B.number)->getZ();
                    double f3z =upside->QHside.find(tri.C.number)->getZ();
                    if(mode ==1) //use Pandit
                    {
                        double x1;
                        double d1 = distanceEuclidean(p,tri.A);
                        double d2 = distanceEuclidean(p,tri.B);
                        double d3 = distanceEuclidean(p,tri.C);
                        if(d1 <= d2)
                            {
                             if(d1 <= d3)
                                 {
                                  x1 = f1z-b->getZ();
                                  x1 < 0? b->setThick(x1+maxZ) : b->setThick(x1);;// d1 is smallest;
                                  }
                             else
                                 {
                                  x1 = f3z-b->getZ();
                                  x1 < 0? b->setThick(x1+maxZ) : b->setThick(x1);;//d3 is smallest;
                                  }
                            }
                             else
                                 {
                                  if(d2 <= d3)
                                     {
                                       x1 = f2z-b->getZ();
                                       x1 < 0? b->setThick(x1+maxZ) : b->setThick(x1);;// d2 is smallest
                                      }
                                  else
                                       {
                                        x1 = f3z-b->getZ();
                                        x1 < 0? b->setThick(x1+maxZ) : b->setThick(x1);;//  d3 smallest
                                        }
                                 }
                    }
                    else //use Lukat
                    {
                        float x1;
                        x1=f1z-b->getZ();
                        x1 < 0? f1 = x1+maxZ : f1 = x1;
                        x1 = f2z-b->getZ();
                        x1 < 0? f2 = x1+maxZ : f2 = x1;
                        x1 = f3z-b->getZ();
                        x1 < 0? f3 = x1+maxZ : f3 = x1;
                        b->setThick(interpTriangle(b->getX(),b->getY(),tri.A.x(),tri.A.y(),f1,tri.B.x(),tri.B.y(),f2,tri.C.x(),tri.C.y(),f3)/10.0);
                    }

                }
            }

        }



for(it = downside->QHside.begin(); it != downside->QHside.end(); it++)
{    double dist = it->getThick();
    if(dist < downside->Minthick)
        downside->Minthick=dist;
    if(dist >downside->Maxthick)
        downside->Maxthick=dist;

}
for(it = upside->QHside.begin(); it != upside->QHside.end(); it++)
{
    double dist = it->getThick();
      if(dist < upside->Minthick)
          upside->Minthick=dist;
      if(dist >upside->Maxthick)
          upside->Maxthick=dist;

}

}

int Membran::compare_doub(const void* a, const void* b)
{
    double* arg1 = (double*) a;
    double* arg2 = (double*) b;
    if( *arg1 < *arg2 ) return -1;
    else if( *arg1 == *arg2 ) return 0;
    else return 1;
}

void Membran::calculate3dplots()
{
    upside->precalc3dplots(maxX,maxY);
    downside->precalc3dplots(maxX,maxY);

}



