#include <QApplication>
#include "gui/mainwindow.h"
#include "coredata/core.h"
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <algorithm>
#include "io/apl/conf_parser.hpp"
#include "io/pdb/pdbreader.h"
#include "io/ndx/index_parser.hpp"
#include "io/xvg/xvg_printer.h"
using namespace std;

class CLParser
{
public:

    CLParser(int argc_, char * argv_[],bool switches_on_=false);
    ~CLParser(){}

    string get_arg(int i);
    string get_arg(string s);

private:

    int argc;
    vector<string> argv;

    bool switches_on;
    map<string,string> switch_map;
};

CLParser::CLParser(int argc_, char * argv_[],bool switches_on_)
{
    argc=argc_;
    argv.resize(argc);
    copy(argv_,argv_+argc,argv.begin());
    switches_on=switches_on_;

    //map the switches to the actual
    //arguments if necessary
    if (switches_on)
    {
        vector<string>::iterator it1,it2;
        it1=argv.begin();
        it2=it1+1;

        while (true)
        {
            if (it1==argv.end()) break;
            if (it2==argv.end()) break;

            if ((*it1)[0]=='-')
                switch_map[*it1]=*(it2);

            it1++;
            it2++;
        }
    }
}

string CLParser::get_arg(int i)
{
    if (i>=0&&i<argc)
        return argv[i];

    return "";
}

string CLParser::get_arg(string s)
{
    if (!switches_on) return "";

    if (switch_map.find(s)!=switch_map.end())
        return switch_map[s];

    return "";
}




int main(int argc, char *argv[])
{


    QApplication a(argc, argv);
    a.setGraphicsSystem("native");
    a.setAttribute(Qt::AA_DontCreateNativeWidgetSiblings);
    a.setQuitOnLastWindowClosed(true);
     std::vector<Atom> atomlist;
    core *mycore = new core;
    QStringList areaplot_lipids,thickplot_lipids,protein_groups;
    QStringList args = a.arguments();
    QString pdb = "NONE";
    QString xtc = "NONE";
    QString index = "NONE";
    QString conf = "NONE";
    QString selectionindex ="NONE";
    QString areaplots  ="Area";
    QString thickplots ="Thick";
    QString framefile ="NONE";
    QString framefileselect ="NONE";
    float vdw = -1.0;
    bool gui = 1;
    double box_x,box_y,box_z;
    box_x=0.0;
    box_y=0.0;
    box_z=0.0;
    int pbc = 20;
    int mode = 1;
    int from = -1;
    int till = -1;
    bool xtc_mode = 0;
    bool printframes= false;
    bool printframes_select= false;
    QFile pdbfile, xtcfile,conffile,indexfile,selectionindexfile;

    /*PARSING CMD LINE OPTIONS*/
    if (args.count() >= 2)
    {
        CLParser cmd_line(argc,argv,true);
        string temp;
        temp=cmd_line.get_arg("-g");

        if (temp!="")
        {
            QString useUI;
            useUI = QString::fromStdString(temp);
            useUI = useUI.simplified();
            useUI.replace(" ","");
            if(useUI =="false"||useUI =="0")
               gui = 0;

        }

        temp=cmd_line.get_arg("-f");
        if (temp!="")
        {
            pdb = QString::fromStdString(temp);

        }

        temp=cmd_line.get_arg("-c");
        if (temp!="")
        {
            conf = QString::fromStdString(temp);
        }
        temp=cmd_line.get_arg("-x");
        if (temp!="")
        {
            xtc =QString::fromStdString(temp);

        }
        temp=cmd_line.get_arg("-i");
        if (temp!="")
        {
           index = QString::fromStdString(temp);
        }

        temp=cmd_line.get_arg("-si");
        if (temp!="")
        {
           selectionindex = QString::fromStdString(temp);
        }

        temp=cmd_line.get_arg("-pbc");
        if (temp!="")
        {  QString pbcstring;
           pbcstring = QString::fromStdString(temp);
           pbc = pbcstring.toInt();

        }
        temp=cmd_line.get_arg("-box");
        if (temp!="")
        {  QString boxstring;
           boxstring = QString::fromStdString(temp);
           QStringList trunc = boxstring.split(QRegExp("\\s+"));
           box_x=trunc[0].toFloat();
           box_y=trunc[1].toFloat();
           box_z=trunc[2].toFloat();
        }
        temp=cmd_line.get_arg("-vdw");
        if (temp!="")
        {  QString vdwstring;
           vdwstring = QString::fromStdString(temp);
           vdw = vdwstring.toFloat();
        }
        temp=cmd_line.get_arg("-mode");
        if (temp!="")
        {
          QString modestring;
          modestring = QString::fromStdString(temp);
          mode = modestring.toInt();

        }
        temp=cmd_line.get_arg("-oa");
        if (temp!="")
        {
            areaplots=QString::fromStdString(temp);
            areaplots = areaplots.simplified();
            areaplots.replace(" ","");
            areaplots += "_";
        }
        temp=cmd_line.get_arg("-ot");
        if (temp!="")
        {

           thickplots=QString::fromStdString(temp);
           thickplots = thickplots.simplified();
           thickplots.replace(" ","");
           thickplots += "_";
        }
        temp=cmd_line.get_arg("-of");
        if (temp!="")
        {
           printframes = true;
           framefile=QString::fromStdString(temp);
           framefile = framefile.simplified();
           framefile.replace(" ","");
           framefile += "_";
        }
        temp=cmd_line.get_arg("-ofs");
        if (temp!="")
        {
           printframes_select = true;
           framefileselect=QString::fromStdString(temp);
           framefileselect = framefileselect.simplified();
           framefileselect.replace(" ","");
           framefileselect += "_";
        }
        temp=cmd_line.get_arg("-from");
        if (temp!="")
        {

           from=QString::fromStdString(temp).toInt();

        }
        temp=cmd_line.get_arg("-till");
        if (temp!="")
        {

           till=QString::fromStdString(temp).toInt();

        }


        printf("\n");
        printf("\n");
        printf("************* APL@Voro Version 3.0 **************\n");
        printf("\n");
        printf("         :) Written By Gunther Lukat :(          \n");
        printf("\n");
        printf("\n");
        printf("This program is free software; you can redistribute it and/or\n");
        printf(" modify it under the terms of the GNU General Public License\n");
        printf(" as published by the Free Software Foundation; either version 3\n");
        printf("   of the License, or (at your option) any later version.\n");

        printf("\n");
        printf("\n");
        printf("Option     Filename  Type         Description\n");
        printf(" ------------------------------------------------------------\n");
        printf("  -f       %s           Input, Opt.   Structure:  pdb\n",pdb.toStdString().c_str());
        printf("  -c       %s           Input, Opt.   Configure file: apl\n",conf.toStdString().c_str());
        printf("  -x       %s           Input, Opt.   Trajectory: xtc trr\n",xtc.toStdString().c_str());
        printf("  -i       %s           Input, Opt.   Index file: ndx\n",index.toStdString().c_str());
        printf("  -si      %s           Input, Opt.   selection Index file: ndx\n",selectionindex.toStdString().c_str());
        printf("  -oa      %s           Output, Opt.  XVG file: Average area over frames\n",areaplots.toStdString().c_str());
        printf("  -ot      %s           Output, Opt.  XVG file: Average thickness over frames\n",thickplots.toStdString().c_str());
        printf("  -of      %s           Output, Opt.  txt file: Print frames to txt \n",framefile.toStdString().c_str());
        printf("  -ofs     %s           Output, Opt.  txt file: Print selection in frames to txt \n",framefileselect.toStdString().c_str());

        printf("\n");
        printf("\n");
        printf("Option     Type     Value         Description\n");
        printf(" ------------------------------------------------------------\n");
        printf("  -pbc     Num          %d        Pbc-offset for Voronoi diagrams\n",pbc);
        printf("  -box     Vector       %d %d %d     Box dimensions of Structure file in nm\n",(int)box_x,(int)box_y,(int)box_z);
        printf("                                  (default : 0 0 0 means detect by CRYST RECORD) \n");
        printf("  -mode    Num          %d        Algorithm for thickness calculation : multi = 1 , single = 2\n",mode);
        printf("  -vdw     Num          %d        offset for TPIM: -1 = by vdw\n",(int)vdw);
        printf("  -from    Num          %d        Print frames to txt starting from this framenumber (-1 = start from first frame (default))\n",from);
        printf("  -till    Num          %d        Print frames to txt till this framenumber (-1 = till last frame (default)\n",till);
        printf("  -g       Bool         %d        Display GUI: 1=true, 0=false\n",gui);
        printf("\n");
        printf("\n");

    }


        /*END PARSING CMD LINE OPTIONS*/


        /*WITHOUT UI*/



        if(!gui)
        {



            if((pdb == "NONE" || conf == "NONE") )
            {
                printf("Error: Running without GUI requires option -f and option -c\n");
                return 0;

            }
            if((pdb == "NONE" && conf == "NONE") )
            {
                printf("Error: Running without GUI requires option -f and option -c\n");
                return 0;

            }
            if(pdb !="NONE")
            {
               pdbfile.setFileName(pdb);
               if(!pdbfile.exists())
               {
                   printf("Error: No such file for option -f %s \n",pdb.toStdString().c_str());
                   return 0;
               }
            }
            if(conf !="NONE")
            {
               conffile.setFileName(conf);
               if(!conffile.exists())
               {
                   printf("Error: No such file for option -c %s \n",conf.toStdString().c_str());
                   return 0;
               }
            }

            /*PDB and CONF was given*/
            if(xtc !="NONE")
            {
                xtcfile.setFileName(xtc);
                if(!xtcfile.exists())
                {
                    printf("Error: No such file for option -x %s \n",xtc.toStdString().c_str());
                    return 0;
                }
                xtc_mode = 1;
                mycore->setPathTrr(xtc);
            }

            if(index !="NONE")
            {
                indexfile.setFileName(index);
                if(!indexfile.exists())
                {
                    printf("Error: No such file for option -i %s \n",index.toStdString().c_str());
                    return 0;
                }

            }

            if(selectionindex !="NONE")
            {
                selectionindexfile.setFileName(selectionindex);
                if(!selectionindexfile.exists())
                {
                    printf("Error: No such file for option -si %s \n",selectionindex.toStdString().c_str());
                    return 0;
                }

            }
            if(from >=0 && till >=0)
            {

                if(from > till)
                {
                    printf("Error:Framenumbers for option -from must <= option -till \n");
                    return 0;
                }

            }

            if(selectionindex =="NONE" && printframes_select==true)
            {

                    printf("Error: No such file for option -si %s \n",selectionindex.toStdString().c_str());
                    return 0;


            }
            /*ALL RELEVANT OPTIONS ARE VALID*/
            /*START PARSING CONF FILE*/
            conf_parser *cparse = new conf_parser(conffile);

            if (!cparse->get_Lipids())
               {
                printf("Error: No lipid names from %s\n",conf.toStdString().c_str());
                  return 0;
               }
            if (! cparse->get_Lipid_atomnames(&atomlist))
            {
             printf("Error:: No lipid atom names from %s\n",conf.toStdString().c_str());
               return 0;
            }
            if (! cparse->get_Area_plotnames(&areaplot_lipids))
            {
             printf("No lipid names found for plotting the area!\n");

            }
            if (! cparse->get_Thick_plotnames(&thickplot_lipids))
            {
             printf("No lipid names found for plotting the thickness!\n");

            }
            if(index !="NONE")
            {
                if (!cparse->get_Protein_index_group(&protein_groups))
               {
               printf("Error: No protein group names found in %s\n",conf.toStdString().c_str());
                  return 0;
               }

            }
            else
             {
                 index_parser *ip = new index_parser(indexfile);
                 QHash<QString,QList<int> > *intlist = &(mycore->peptides);
                 for (int i = 0 ; i < protein_groups.size(); i++)
                   {

                     ip->get_content_section(intlist,protein_groups[i]);
                     if(!mycore->peptides.contains(protein_groups[i]))
                     {
                     printf("Error: Protein group %s not found in %s\n",protein_groups[i].toStdString().c_str(),index.toStdString().c_str());
                        return 0;
                     }
                     if(mycore->peptides.contains(protein_groups[i]) && mycore->peptides[protein_groups[i]].size() < 1 )
                     {
                         printf("Error: Protein group %s is empty in %s\n",protein_groups[i].toStdString().c_str(),index.toStdString().c_str());
                            return 0;
                     }

                   }

                 delete(ip);
             }
            if(selectionindex !="NONE")
            {


                 index_parser *ip = new index_parser(selectionindexfile);
                 QHash<int,QList<int> > *intlist = &(mycore->selectionIndex);

                     ip->get_index_selection(intlist,selectionindexfile,mycore->framemembrans.size());
                     delete(ip);


             }


            mycore->setPath(pdb);
            mycore->thickmode = mode;
            mycore->offset = pbc;

            if(!atomlist.size() >0 )
            {
                printf("Error: Defined atoms not found in %s\n",pdb.toStdString().c_str());
                return 0;
            }
            else//atoms in conf are valid
            {
                if(box_x == box_y == box_z == 0.0)
                {
                    Pdbreader *pread = new Pdbreader();
                    pread->setPath(pdb);

                    pread->getBox(box_x,box_y,box_z);

                    delete(pread);

                }

                /*CALC VORONOI*/
                if(!xtc_mode)
                  {

                    mycore->makeWorkingSet(&atomlist,box_x*10,box_y*10,box_z*10) ;
                  }
                else{
                      printf("Reading Trajectory file....\n");
                      mycore->makeWorkingSetTrr(&atomlist,box_x*10,box_y*10,box_z*10) ;
                      }

                int size = mycore->framemembrans.size();
                printf("Calculating Voronoi diagrams for %d frames.....\n",size);
                #pragma omp parallel for
                   for(int i = 0 ; i< size; i++)
                    {
                     mycore->framemembrans[i].makeVoronoi(mycore->offset,mycore->thickmode, mycore->vdwcheck, mycore->uprighttpm, mycore->downrighttpm);
                    }


                   if(till == -1)
                       till = mycore->framemembrans.size();
                   if(from == -1)
                       from = 0;
                /*Now print plots if defined*/
                QVector <double> up_result;
                QVector <double> down_result;
                QString lipid;
                QString file_name;
                xvg_printer *xprint = new xvg_printer();
                if(areaplot_lipids.size()>0)
                {

                  printf("Generating %d plots of the average lateral area.....\n",areaplot_lipids.size()*2);
                  for(int i = 0 ; i < areaplot_lipids.size(); i++)
                     {
                      lipid =areaplot_lipids[i];
                      for (int j = 0 ; j <  mycore->framemembrans.size();j++)
                          {
                          if(lipid == "select")
                            {
                              up_result.push_back(mycore->framemembrans[j].upside->getAreaForIndexList(mycore->selectionIndex[j]));
                              down_result.push_back(mycore->framemembrans[j].downside->getAreaForIndexList(mycore->selectionIndex[j]));
                             }
                          else{
                          up_result.push_back(mycore->framemembrans[j].upside->getAreaForResidue(lipid));
                          down_result.push_back(mycore->framemembrans[j].downside->getAreaForResidue(lipid));
                           }
                          }
                      file_name = xprint->setPath(areaplots,lipid,"upper_layer");
                      xprint->print_file(up_result,1);
                      printf("Saved lateral average area for '%s' in the upper leaflet to file %s\n",lipid.toStdString().c_str(),file_name.toStdString().c_str());
                      file_name = xprint->setPath(areaplots,lipid,"lower_layer");
                      xprint->print_file(down_result,1);
                      printf("Saved lateral average area for '%s' in the lower leaflet to file %s\n",lipid.toStdString().c_str(),file_name.toStdString().c_str());
                      up_result.clear();
                      down_result.clear();
                     }

                }

                if(thickplot_lipids.size()>0)
                {

                  printf("Generating %d plots of the average thickness.....\n",thickplot_lipids.size()*2);
                  for(int i = 0 ; i < thickplot_lipids.size(); i++)
                     {
                      lipid = thickplot_lipids[i];

                      for (int j = 0 ; j <  mycore->framemembrans.size();j++)
                          {
                          if(lipid == "select")
                            {
                            up_result.push_back(mycore->framemembrans[j].upside->getThickForIndexList(mycore->selectionIndex[j]));
                            down_result.push_back(mycore->framemembrans[j].downside->getThickForIndexList(mycore->selectionIndex[j]));
                            }
                            else
                            {
                             up_result.push_back(mycore->framemembrans[j].upside->getThickForResidue(lipid));
                             down_result.push_back(mycore->framemembrans[j].downside->getThickForResidue(lipid));
                            }
                         }

                      file_name = xprint->setPath(thickplots,lipid,"upper_layer");
                      xprint->print_file(up_result,0);
                      printf("Saved average thickness for %s in the upper leaflet to file %s\n",lipid.toStdString().c_str(),file_name.toStdString().c_str());
                      file_name = xprint->setPath(thickplots,lipid,"lower_layer");
                      xprint->print_file(down_result,0);
                      printf("Saved average thickness for %s in the lower leaflet to file %s\n",lipid.toStdString().c_str(),file_name.toStdString().c_str());
                      up_result.clear();
                      down_result.clear();
                     }

                }

                if(printframes)
                {

                   QString framefile2 = framefile + "upper_layer.txt";
                    QFile file(framefile2);
                    if (!file.open(QFile::WriteOnly | QFile::Text))
                    {

                            return 0;
                    }
                    QHash<int,Atom>::iterator it;

                    QTextStream out(&file);
                    printf("Printing %d frames to file %s for the upper leaflet \n",(till-from),framefile2.toStdString().c_str());
                    out <<"#VLayers"<<"\n";
                     out << "#Number"<<"  "<<"Name"<<"  "<<"Residue"<<"  "<<"ResidueID"<<"  "<<"X"<<"  "<<"Y"<<"  "<<"Z"<<"  "<<"Time"<<"  "<<"Area"<<"  "<<"Thick"<<"\n";


                                            for(int l = till; l <from;l++)
                                            {
                                                out <<"#Framenumber"<<" "<<l<<"\n";




                                                 for(it = mycore->framemembrans[l].upside->QHside.begin(); it != mycore->framemembrans[l].upside->QHside.end(); it++)
                                                   {
                                                    out <<it.value().getNumber()<<"  "<<it.value().getName()<<"  "<<it.value().getResname()<<"  "<<it.value().getResNumber()<<"  "<<it.value().getX() <<"  "<<it.value().getY() <<"  "<<it.value().getZ() <<"  "<<it.value().getTime()<<"  "<<it.value().getArea()<<"  "<<it.value().getThick()<<"\n";
                                                   }



                                            }


                   framefile2 = framefile + "lower_layer.txt";
                   printf("Printing %d frames to file %s for the upper leaflet \n",(till-from),framefile2.toStdString().c_str());
                   QFile file2(framefile2);
                    if (!file2.open(QFile::WriteOnly | QFile::Text))
                        {

                           return 0;
                        }


                        QTextStream out2(&file2);
                        out2 <<"#VLayers"<<"\n";
                        out2 << "#Number"<<"  "<<"Name"<<"  "<<"Residue"<<"  "<<"ResidueID"<<"  "<<"X"<<"  "<<"Y"<<"  "<<"Z"<<"  "<<"Time"<<"  "<<"Area"<<"  "<<"Thick"<<"\n";


                        for(int l = from; l < till;l++)
                             {
                              out2 <<"#Framenumber"<<" "<<l<<"\n";




                              for(it = mycore->framemembrans[l].downside->QHside.begin(); it != mycore->framemembrans[l].downside->QHside.end(); it++)
                                   {
                                    out2 <<it.value().getNumber()<<"  "<<it.value().getName()<<"  "<<it.value().getResname()<<"  "<<it.value().getResNumber()<<"  "<<it.value().getX() <<"  "<<it.value().getY() <<"  "<<it.value().getZ() <<"  "<<it.value().getTime()<<"  "<<it.value().getArea()<<"  "<<it.value().getThick()<<"\n";
                                   }


                             }




                }

                if(printframes_select)
                {

                   QString framefile2_select = framefileselect + "UPPER_LAYER.txt";
                    QFile file2(framefile2_select);
                    if (!file2.open(QFile::WriteOnly | QFile::Text))
                    {

                            return 0;
                    }
                    QHash<int,Atom>::iterator it;

                    QTextStream out(&file2);
                    printf("Printing %d frames to file %s for selected indices in the upper leaflet \n",(till-from),framefile2_select.toStdString().c_str());
                    out <<"#VLayers"<<"\n";
                     out << "#Number"<<"  "<<"Name"<<"  "<<"Residue"<<"  "<<"ResidueID"<<"  "<<"X"<<"  "<<"Y"<<"  "<<"Z"<<"  "<<"Time"<<"  "<<"Area"<<"  "<<"Thick"<<"\n";


                      for(int l = from; l < till;l++)
                                            {
                                                out <<"#Framenumber"<<" "<<l<<"\n";



                                                    for(it = mycore->framemembrans[l].upside->QHside.begin(); it != mycore->framemembrans[l].upside->QHside.end(); it++)
                                                    {
                                                        if(mycore->selectionIndex[l].contains(it.value().getNumber()))
                                                       out <<it.value().getNumber()<<"  "<<it.value().getName()<<"  "<<it.value().getResname()<<"  "<<it.value().getResNumber()<<"  "<<it.value().getX() <<"  "<<it.value().getY() <<"  "<<it.value().getZ() <<"  "<<it.value().getTime()<<"  "<<it.value().getArea()<<"  "<<it.value().getThick()<<"\n";
                                                      }




                                            }


                   framefile2_select = framefileselect + "LOWER_LAYER.txt";
                   printf("Printing %d frames to file %s for selected indices in the lower leaflet \n",(till-from),framefile2_select.toStdString().c_str());
                   QFile file3(framefile2_select);
                    if (!file3.open(QFile::WriteOnly | QFile::Text))
                        {

                           return 0;
                        }


                        QTextStream out2(&file3);
                        out2 <<"#VLayers"<<"\n";
                        out2 << "#Number"<<"  "<<"Name"<<"  "<<"Residue"<<"  "<<"ResidueID"<<"  "<<"X"<<"  "<<"Y"<<"  "<<"Z"<<"  "<<"Time"<<"  "<<"Area"<<"  "<<"Thick"<<"\n";


                        for(int l = from; l < till;l++)
                             {
                              out2 <<"#Framenumber"<<" "<<l<<"\n";

                               for(it = mycore->framemembrans[l].downside->QHside.begin(); it != mycore->framemembrans[l].downside->QHside.end(); it++)
                                  {
                                      if(mycore->selectionIndex[l].contains(it.value().getNumber()))
                                     out2 <<it.value().getNumber()<<"  "<<it.value().getName()<<"  "<<it.value().getResname()<<"  "<<it.value().getResNumber()<<"  "<<it.value().getX() <<"  "<<it.value().getY() <<"  "<<it.value().getZ() <<"  "<<it.value().getTime()<<"  "<<it.value().getArea()<<"  "<<it.value().getThick()<<"\n";
                                    }




                             }




            }







          }
        }





        else/*WITH UI*/
        {
            MainWindow w;
            w.show();
            w.setcore(*mycore);
            //printf("GUI MODE: Ignoring output files and dynamic index!");



            if((pdb == "NONE" && conf != "NONE") )
            {
                printf("Error: Option -c requires option -f\n");
                return 0;

            }
            if((pdb != "NONE" && conf == "NONE") )
            {
                printf("Error: Option -f requires option -c\n");
                return 0;

            }
            if(pdb !="NONE")
            {
               pdbfile.setFileName(pdb);
               if(!pdbfile.exists())
               {
                   printf("Error: No such file for option -f %s \n",pdb.toStdString().c_str());
                   return 0;
               }
            }
            if(conf !="NONE")
            {
               conffile.setFileName(conf);
               if(!conffile.exists())
               {
                   printf("Error: No such file for option -c %s \n",conf.toStdString().c_str());
                   return 0;
               }

            }

            /*PDB and CONF was given*/
            if(xtc !="NONE")
            {
                xtcfile.setFileName(xtc);
                if(!xtcfile.exists())
                {
                    printf("Error: No such file for option -x %s \n",xtc.toStdString().c_str());
                    return 0;
                }
                xtc_mode = 1;
                mycore->setPathTrr(xtc);
            }

            if(index !="NONE")
            {
                indexfile.setFileName(index);
                if(!indexfile.exists())
                {
                    printf("Error: No such file for option -i %s \n",index.toStdString().c_str());
                    return 0;
                }

            }
            if(pdb != "NONE" && conf != "NONE")
            {

            /*ALL RELEVANT OPTIONS ARE VALID*/
            /*START PARSING CONF FILE*/

            conf_parser *cparse = new conf_parser(conffile);

            if (!cparse->get_Lipids())
               {
                printf("Error: No lipid names from %s\n",conf.toStdString().c_str());
                  return 0;
               }
            if (! cparse->get_Lipid_atomnames(&atomlist))
            {
             printf("Error:: No lipid atom names from %s\n",conf.toStdString().c_str());
               return 0;
            }
            if (! cparse->get_Area_plotnames(&areaplot_lipids))
            {
             printf("Warning: No lipid names found for plotting!");

            }
            if (! cparse->get_Thick_plotnames(&thickplot_lipids))
            {
             printf("Warning: No lipid names found for plotting!");

            }
            if(index !="NONE")
            {
                if (!cparse->get_Protein_index_group(&protein_groups))
               {
               printf("Error: No protein group names found in %s\n",conf.toStdString().c_str());
                  return 0;
               }
              else
                {
                 index_parser *ip = new index_parser(indexfile);
                 QHash<QString,QList<int> > *intlist = &(mycore->peptides);
                 for (int i = 0 ; i < protein_groups.size(); i++)
                   {

                     ip->get_content_section(intlist,protein_groups[i]);
                     if(!mycore->peptides.contains(protein_groups[i]))
                     {
                     printf("Error: Protein group %s not found in %s\n",protein_groups[i].toStdString().c_str(),index.toStdString().c_str());
                        return 0;
                     }
                     if(mycore->peptides.contains(protein_groups[i]) && mycore->peptides[protein_groups[i]].size() < 1 )
                     {
                         printf("Error: Protein group %s is empty in %s\n",protein_groups[i].toStdString().c_str(),index.toStdString().c_str());
                            return 0;
                     }

                   }


             }
            }
            mycore->setPath(pdb);
            mycore->thickmode = mode;
            mycore->offset = pbc;
            if(!atomlist.size() >0 )
               {
                        printf("Error: Defined atoms not found in %s\n",pdb.toStdString().c_str());
                           return 0;
                 }
            else
            {
                if(box_x == box_y == box_z == 0.0)
                {
                    Pdbreader *pread = new Pdbreader();
                    pread->setPath(pdb);

                    pread->getBox(box_x,box_y,box_z);

                    delete(pread);

                }


                /*Now set UI and CALC VORONOI*/

                if(!xtc_mode)
                  {
                    w.display_fromTerminal(atomlist,box_x*10,box_y*10,box_z*10, 0);
                  }
                else{
                    w.display_fromTerminal(atomlist,box_x*10,box_y*10,box_z*10, 1);
                    }
/*
                QVector <double> up_result;
                QVector <double> down_result;
                xvg_printer *xprint = new xvg_printer();

                if(areaplot_lipids.size()>0)
                {


                  for(int i = 0 ; i < areaplot_lipids.size(); i++)
                     {
                      for (int j = 0 ; j <  mycore->framemembrans.size();j++)
                          {
                          up_result.push_back(mycore->framemembrans[j].upside->getAreaForResidue(areaplot_lipids[i]));
                          down_result.push_back(mycore->framemembrans[j].downside->getAreaForResidue(areaplot_lipids[i]));
                         }
                      xprint->setPath(areaplots,areaplot_lipids[i],"upper_layer");
                      xprint->print_file(up_result,1);
                      xprint->setPath(areaplots,areaplot_lipids[i],"lower_layer");
                      xprint->print_file(down_result,1);
                      up_result.clear();
                      down_result.clear();
                     }

                }
                if(thickplot_lipids.size()>0)
                {


                  for(int i = 0 ; i < thickplot_lipids.size(); i++)
                     {
                      for (int j = 0 ; j <  mycore->framemembrans.size();j++)
                          {
                          up_result.push_back(mycore->framemembrans[j].upside->getThickForResidue(thickplot_lipids[i]));
                          down_result.push_back(mycore->framemembrans[j].downside->getThickForResidue(thickplot_lipids[i]));
                         }
                      xprint->setPath(thickplots,thickplot_lipids[i],"upper_layer");
                      xprint->print_file(up_result,0);
                      xprint->setPath(thickplots,thickplot_lipids[i],"lower_layer");
                      xprint->print_file(down_result,0);
                      up_result.clear();
                      down_result.clear();
                     }

                }

*/
           }

            }

            return a.exec();
        }//WITH GUI




}

