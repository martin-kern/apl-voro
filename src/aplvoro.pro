#-------------------------------------------------
#
# Project created by QtCreator 2011-10-06T20:58:20
#
#-------------------------------------------------



include(coredata/core.pri)
include(data/data.pri)
include(logic/logic.pri)
include(geom/geom.pri)
include(io/pdb/io_pdb.pri)
include(io/xdr/io_xdr.pri)
include(io/xvg/io_xvg.pri)
include(io/ndx/io_ndx.pri)
include(io/apl/io_apl.pri)
include(gui/gui.pri)
include(gui/plot2d/gui_plot2d.pri)
include(gui/graphicobjects/gui_objects.pri)
include(gui/datamodels/gui_models.pri)
include(qwt/qwt.pri)



isEmpty(PREFIX) {
 PREFIX = /usr
}



QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS += -std=c++14 -Ofast
QMAKE_CC = gcc#-4.8
QMAKE_CXX = g++#-4.8
macx {
QMAKE_CC = gcc-4.7
QMAKE_CXX = g++-4.7
}
SOURCES += main.cpp \
    commandlineparser.cpp



QT += xml

contains(QT_CONFIG, opengl):QT += opengl

TEMPLATE = app


QMAKE_CXXFLAGS += -fopenmp
QMAKE_LFLAGS += -fopenmp

LIBS += -lgomp

TARGET = aplvoro

RESOURCES     = qrc_images.qrc \
    text.qrc




macx {
OSX = $$OSX_BUNDLE
isEmpty(OSX_BUNDLE) {
 OSX = bundle
}


equals(OSX, bundle){
message(The project will be build as .app bundle.)

CONFIG +=app_bundle
QMAKE_LFLAGS += -static-libgcc -static-libstdc++


manualIndex.files = manual/manual/
manualIndex.path = Contents/Resources/manual
QMAKE_BUNDLE_DATA += manualIndex


license.files = license/
license.path = Contents/License
QMAKE_BUNDLE_DATA += license
ICON = aplvoro.icns
}



equals(OSX, unix) {
   message(The project will installed in $$PREFIX (not a bundle))
   CONFIG -=app_bundle
   target.path = $$PREFIX/bin

   icon.path =$$PREFIX/share/pixmaps
   icon.files = pixmaps/64x64/aplvoro.png

   icon16.path = $$PREFIX/share//aplvoro/icons/hicolor/16x16/apps
   icon16.files = pixmaps/16x16/aplvoro.png

   icon24.path = $$PREFIX/share/aplvoro/icons/hicolor/24x24/apps
   icon24.files = pixmaps/24x24/aplvoro.png

   icon32.path = $$PREFIX/share/aplvoro/icons/hicolor/32x32/apps
   icon32.files = pixmaps/32x32/aplvoro.png

   icon48.path = $$PREFIX/share/aplvoro/icons/hicolor/48x48/apps
   icon48.files = pixmaps/48x48/aplvoro.png

   icon64.path = $$PREFIX/share/aplvoro/icons/hicolor/64x64/apps
   icon64.files = pixmaps/64x64/aplvoro.png

   desktop.path = $$PREFIX/share/applications
   desktop.files = aplvoro.desktop

   manual.path = $$PREFIX/share/aplvoro/manual
   manual.files = manual/*

   manualcss.path = $$PREFIX/share/aplvoro/manual/css
   manualcss.files = manual/css/*

   manualjs.path = $$PREFIX/share/aplvoro/manual/js
   manualjs.files = manual/js/*

   manualimages.path = $$PREFIX/share/aplvoro/manual/images
   manualimages.files = manual/images/*

   manuallessons.path = $$PREFIX/share/aplvoro/manual/lessons
   manuallessons.files = manual/lessons/*

   manpage.path = $$PREFIX/share/man/man1
   manpage.files = manpage/*


INSTALLS += target
INSTALLS += icon
INSTALLS += icon16
INSTALLS += icon24
INSTALLS += icon32
INSTALLS += icon48
INSTALLS += icon64
INSTALLS += desktop
INSTALLS += manual
INSTALLS += manualjs
INSTALLS += manualcss
INSTALLS += manualimages
INSTALLS += manuallessons
INSTALLS += manpage
}
}



unix:!macx{
message(The project will be installed in $$PREFIX)


LIBS += -lGLU
LIBS += -lrt


target.path =$$PREFIX/bin

icon.path =$$PREFIX/share/pixmaps
icon.files = pixmaps/64x64/aplvoro.png

icon16.path =$$PREFIX/share/aplvoro/icons/hicolor/16x16/apps
icon16.files = pixmaps/16x16/aplvoro.png

icon24.path =$$PREFIX/share/aplvoro/icons/hicolor/24x24/apps
icon24.files = pixmaps/24x24/aplvoro.png

icon32.path = $$PREFIX/share/aplvoro/icons/hicolor/32x32/apps
icon32.files = pixmaps/32x32/aplvoro.png

icon48.path =$$PREFIX/share/aplvoro/icons/hicolor/48x48/apps
icon48.files = pixmaps/48x48/aplvoro.png

icon64.path =$$PREFIX/share/aplvoro/icons/hicolor/64x64/apps
icon64.files = pixmaps/64x64/aplvoro.png

desktop.path =$$PREFIX/share/applications
desktop.files = aplvoro.desktop

manual.path =$$PREFIX/share/aplvoro/manual
manual.files = manual/*

manualcss.path =$$PREFIX/share/aplvoro/manual/css
manualcss.files = manual/css/*

manualjs.path =$$PREFIX/share/aplvoro/manual/js
manualjs.files = manual/js/*

manualimages.path = $$PREFIX/share/aplvoro/manual/images
manualimages.files = manual/images/*

manuallessons.path = $$PREFIX/share/aplvoro/manual/lessons
manuallessons.files = manual/lessons/*

manpage.path = $$PREFIX/share/man/man1
manpage.files = manpage/*


INSTALLS += target
INSTALLS += icon
INSTALLS += icon16
INSTALLS += icon24
INSTALLS += icon32
INSTALLS += icon48
INSTALLS += icon64
INSTALLS += desktop
INSTALLS += manual
INSTALLS += manualjs
INSTALLS += manualcss
INSTALLS += manualimages
INSTALLS += manuallessons
INSTALLS += manpage

}



HEADERS += \
    commandlineparser.hpp

