#include <QApplication>
#include <time.h>
#include <iostream>
#include "data/constants.hpp"
#include "gui/mainwindow.h"
#include "coredata/core.h"
#include "io/apl/conf_parser.hpp"
#include "io/pdb/pdbreader.h"
#include "io/ndx/index_parser.hpp"
#include "io/xvg/xvg_printer.h"
#include "data/setup_parameters.hpp"
#include "commandlineparser.hpp"


int main(int argc, char *argv[])
{
    CommandLineParser parser;
    QApplication apl_voro_app(argc, argv);
    apl_voro_app.setAttribute(Qt::AA_DontCreateNativeWidgetSiblings);
    apl_voro_app.setQuitOnLastWindowClosed(true);
    QStringList args = apl_voro_app.arguments();

    if (!parser.parse(apl_voro_app.arguments()))
        return 1;

    if (parser.no_args_given())  // Empty GUI.
    {
        MainWindow apl_voro_main_window;
        apl_voro_main_window.showMaximized();
        return apl_voro_app.exec();
    }
    else if (parser.with_gui())  // GUI with one project.
    {
        MainWindow apl_voro_main_window;
        apl_voro_main_window.showMaximized();
        apl_voro_main_window.create_project(parser.setup_params());
        return apl_voro_app.exec();
    }
    else  // Pure console application.
    {
        time_t start, end;
        time(&start);
        std::cout << APP_NAME << " Version " << APP_VER << "\n";
        core::command_line_evaluation(parser);
        time(&end);
        std::cout << "Finished in " << difftime(end, start) << " seconds\n";
        return 0;
    }
}
