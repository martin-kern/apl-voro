#ifndef REGRESSION_HPP
#define REGRESSION_HPP

#include <vector>
#include <eigen3/Eigen/Dense>
#include <vector>
#include <QList>
#include <math.h>
#include "coredata/atom.h"
#include "coredata/molecule.h"


using namespace std;
using namespace Eigen;

vector<float> polynomial_regression(vector<Molecule> &molecules, unsigned int degree);

/**
 * @brief n_parameters: Calculate the number of parameters for a 2D polynomial for a certain degree
 * @param degree:
 * @return
 */
int n_parameters(int degree);

float polyval(vector<float> parameters, float x, float y, unsigned int degree);
#endif // REGRESSION_HPP
