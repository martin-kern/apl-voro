#include "regression.hpp"


int n_parameters(int degree)
{
    int n = degree - 1;
    return (int(pow(n, 2)) + n) / 2;
}

vector<float> polynomial_regression(vector<Molecule> &molecules, unsigned int degree)
{
    int n_params = int((pow(degree + 1, 2) + degree + 1) / 2);
    vector<float> parameters;
    unsigned int n_atoms = 0;
    for (unsigned int i = 0; i < molecules.size(); i++)
        n_atoms = n_atoms + unsigned(molecules[i].list.size());

    // Fill matrices
    MatrixXd X(n_atoms, n_params);
    MatrixXd Z(n_atoms, 1);
    unsigned int line = 0;
    for (unsigned int i = 0; i < molecules.size(); i++)
    {
        QList<int> atom_indices = molecules[i].list.keys();
        for (int j = 0; j < atom_indices.size(); j++)
        {
            double x = molecules[i].list.value(atom_indices[j]).getX();
            double y = molecules[i].list.value(atom_indices[j]).getY();
//            double x = molecules[i].list[atom_indices[j]].getX();
//            double y = molecules[i].list[atom_indices[j]].getY();
            Z (line, 0) = molecules[i].list[atom_indices[j]].getZ();
            unsigned int col = 0;
            for (int k = signed(degree); k >= 0; k--)
            {
                for (int l = 0; l < k + 1; l++)
                {
                    X (line, col) = pow(x, double(k - l)) * pow(y, double(l));
                    col++;
                }
            }
            line++;
        }
    }
    // Calculater polynomial parameters
    MatrixXd p = X.bdcSvd(ComputeThinU | ComputeThinV).solve(Z);
    // Turn parameters from a matrix to a vector
    for (int i = 0; i < n_params; i++)
        parameters.push_back(float(p(i)));

    return parameters;
}


float polyval(vector<float> parameters, float x, float y, unsigned int degree)
{
    float val = 0;
    unsigned int ind = 0;
    for (int i = signed(degree); i >= 0; i--)
    {
        for (int j = 0; j < i + 1 ; j++)
        {
            val += parameters[ind] * float(pow(x, i - j) * pow(y, j));
            ind++;
        }
    }
    return val;
}
