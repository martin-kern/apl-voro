#include "commandlineparser.hpp"


CommandLineParser::CommandLineParser()
{
    g_checked = false;
    no_params = true;
    selection_index_ = "";
    area_xvg_ = "";
    thickness_xvg_ = "";
    frames_txt_ = "";
    sel_frames_txt_ = "";
}


CommandLineParser::~CommandLineParser(){}


bool CommandLineParser::parse(QStringList args)
{
    // Boolean variables to see which options have been used.
    bool f_checked = false;
    bool c_checked = false;
    bool x_checked = false;
    bool n_checked = false;
    bool sn_checked = false;
    bool oa_checked = false;
    bool ot_checked = false;
    bool of_checked = false;
    bool ofs_checked = false;
    bool pbc_checked = false;
    bool box_checked = false;
    bool mode_checked = false;
    bool vdw_checked = false;
    bool ld_check = false;
    bool pd_check = false;
    bool b_checked = false;
    bool skip_checked = false;
    bool e_checked = false;
    bool name_checked = false;
    g_checked = false;
    no_params = true;
    selection_index_ = "";

    QString arg;
    for (int i = 0; i < args.length(); i++)
    {
        arg = args[i];

        // ===================
        // | Check -f option |
        // ===================
        if (arg == "-f")
        {
            i++;
            QString pdb = args[i];
            if (!file_exists(pdb))
            {
                qInfo() << "Error. File " << pdb << " does not exist or is not accessible.";
                return false;
            }
            if (!pdb.endsWith(".pdb"))
            {
                qInfo() << "Error. File given with the -f option must be a pdb file.\n";
                return false;
            }
            f_checked = true;
            no_params = false;
            setup_params_.pdb = pdb;
        }

        // ===================
        // | Check -c option |
        // ===================
        if (arg == "-c")
        {
            i++;
            QString conf = args[i];
            if (!file_exists(conf))
            {
                qInfo() << "Error. File " << conf << " does not exist or is not accessible.";
                return false;
            }
            if (!conf.endsWith(".apl"))
            {
                qInfo() << "Error. File given with the -c option must be a apl file.\n";
                return false;
            }
            QFile conf_file;
            conf_file.setFileName(conf);
            conf_parser *c_parser = new conf_parser(conf_file);
            if (!c_parser->parse())
                return false;
            c_checked = true;
            no_params = false;
            setup_params_.key_atoms = c_parser->get_key_atoms();
            setup_params_.protein_names = c_parser->get_protein();
            area_lipids_ = c_parser->get_area_lipids();
            thickness_lipids_ = c_parser->get_thick_lipids();
            delete c_parser;
        }

        // ===================
        // | Check -x option |
        // ===================
        if (arg == "-x")
        {
            i++;
            QString trj = args[i];
            if (!file_exists(trj))
            {
                qInfo() << "Error. File " << trj << " does not exist or is not accessible.";
                return false;
            }
            if (!trj.endsWith(".xtc") && !trj.endsWith(".trr"))
            {
                qInfo() << "Error. File given with the -x option must be a xtc or trr file.\n";
                return false;
            }
            x_checked = true;
            no_params = false;
            setup_params_.trj = trj;
        }

        // ===================
        // | Check -n option |
        // ===================
        if (arg == "-n")
        {
            i++;
            QString ndx = args[i];
            if (!file_exists(ndx))
            {
                qInfo() << "Error. File " << ndx << " does not exist or is not accessible.";
                return false;
            }
            if (!ndx.endsWith(".ndx"))
            {
                qInfo() << "Error. File given with the -i option must be an ndx file.\n";
                return false;
            }
            n_checked = true;
            no_params = false;
            setup_params_.index = ndx;
        }

        // ====================
        // | Check -sn option |
        // ====================
        if (arg == "-sn")
        {
            i++;
            selection_index_ = args[i];
            if (!file_exists(selection_index_))
            {
                qInfo() << "Error. File " << selection_index_ << " does not exist or is not accessible.";
                return false;
            }
            if (!selection_index_.endsWith(".ndx"))
            {
                qInfo() << "Error. File given with the -sn option must be an ndx file.\n";
                return false;
            }
            sn_checked = true;
            no_params = false;
        }

        // ====================
        // | Check -oa option |
        // ====================
        if (arg == "-oa")
        {
            i++;
            area_xvg_ = args[i];
            if (!directory_exists(area_xvg_))
            {
                qInfo() << "Error. Directory " << area_xvg_ << " does not exist or is not accessible.";
                return false;
            }
            oa_checked = true;
            no_params = false;
        }

        // ====================
        // | Check -ot option |
        // ====================
        if (arg == "-ot")
        {
            i++;
            thickness_xvg_ = args[i];
            if (!directory_exists(thickness_xvg_))
            {
                qInfo() << "Error. Directory " << thickness_xvg_ << " does not exist or is not accessible.";
                return false;
            }
            ot_checked = true;
            no_params = false;
        }

        // ====================
        // | Check -of option |
        // ====================
        if (arg == "-of")
        {
            i++;
            frames_txt_ = args[i];
            if (!directory_exists(frames_txt_))
            {
                qInfo() << "Error. Directory " << frames_txt_ << " does not exist or is not accessible.";
                return false;
            }
            of_checked = true;
            no_params = false;
        }

        // =====================
        // | Check -ofs option |
        // =====================
        if (arg == "-ofs")
        {
            i++;
            sel_frames_txt_ = args[i];
            if (!directory_exists(sel_frames_txt_))
            {
                qInfo() << "Error. Directory " << sel_frames_txt_ << " does not exist or is not accessible.";
                return false;
            }
            ofs_checked = true;
            no_params = false;
        }

        // =====================
        // | Check -pbc option |
        // =====================
        if (arg == "-pbc")
        {
            i++;
            bool ok = false;
            int pbc = args[i].toInt(&ok);
            if (!ok)
            {
                qInfo() << "Error. -pbc optoon requires an integer argument.";
                return false;
            }

            if (pbc < 1 || pbc > 100)
            {
                qInfo() << "Error. -pbc option out of range. Value must be between 1 and 100.";
                return false;
            }
            pbc_checked = true;
            no_params = false;
            setup_params_.pbc_cutoff = pbc;
        }

        // =====================
        // | Check -box option |
        // =====================
        if (arg == "-box")
        {
            i++;
            bool ok1 = false;
            bool ok2 = false;
            bool ok3 = false;
            double x = args[i].toDouble(&ok1);
            i++;
            double y = args[i].toDouble(&ok2);
            i++;
            double z = args[i].toDouble(&ok3);

            if (!ok1 || !ok2 || !ok3)
            {
                qInfo() << "Error. -box option requires three double values separated by spaces.";
                return false;
            }

            if (x <= 0 || y <= 0 || z <= 0)
            {
                qInfo() << "Error. All three box dimensions must be larger than 0.";
                return false;
            }
            box_checked = true;
            no_params = false;
            setup_params_.x_box = x;
            setup_params_.y_box = y;
            setup_params_.z_box = z;
        }

        // =====================
        // | Check -mode option |
        // =====================
        if (arg == "-mode")
        {
            i++;
            QString mode = args[i];

            if (mode == "multi")
                setup_params_.modus = 0;
            else if (mode == "single")
                setup_params_.modus = 0;
            else
            {
                qInfo() << "Error. mode option can either be 'single' or 'multi'.";
                return false;
            }
            mode_checked = true;
            no_params = false;
        }

        // =====================
        // | Check -vdw option |
        // =====================
        if (arg == "-vdw")
        {
            i++;
            bool ok = false;
            double vdw = args[i].toDouble(&ok);
            if (!ok)
            {
                qInfo() << "Error. -vdw option requires a single integer value.";
                return false;
            }
            if (vdw < 0)
            {
                qInfo() << "Error. -vdw must be at least 0.";
                return false;
            }
            vdw_checked = true;
            no_params = false;
            setup_params_.vdw_check = 1;
            setup_params_.upright = vdw;
        }

        // ====================
        // | Check -ld option |
        // ====================
        if (arg == "-ld")
        {
            i++;
            bool ok = false;
            int ld = args[i].toInt(&ok);
            if (!ok)
            {
                qInfo() << "Error. -ld option requires a single integer value. Either 0 or 1.";
                return false;
            }
            if (ld < 0 || ld > 1)
            {
                qInfo() << "Error. -ld option must be either 0 or 1.";
                return false;
            }
            no_params = false;
            ld_check = true;
            setup_params_.leaflet_detection = ld;
        }

        // ====================
        // | Check -pd option |
        // ====================
        if (arg == "-pd")
        {
            i++;
            bool ok = false;
            int pd = args[i].toInt(&ok);
            if (!ok)
            {
                qInfo() << "Error. -pd option requires a single integer value.";
                return false;
            }
            if (pd < 0)
            {
                qInfo() << "Error. -pd option must be at least 0.";
                return false;
            }
            no_params = false;
            pd_check = true;
            setup_params_.p_degree = pd;
        }

        // =====================
        // | Check -b option |
        // =====================
        if (arg == "-b")
        {
            i++;
            bool ok = false;
            int first_frame = args[i].toInt(&ok);
            if (!ok)
            {
                qInfo() << "Error. -b option requires a single integer value.";
                return false;
            }
            if (first_frame < 0)
            {
                qInfo() << "Error. -b must be at least 0.";
                return false;
            }
            b_checked = true;
            no_params = false;
            setup_params_.first_frame = first_frame;
        }

        // =====================
        // | Check -skip option |
        // =====================
        if (arg == "-skip")
        {
            i++;
            bool ok = false;
            int stride = args[i].toInt(&ok);
            if (!ok)
            {
                qInfo() << "Error. -skip option requires a single integer value.";
                return false;
            }
            if (stride < 1)
            {
                qInfo() << "Error. -skip must be at least 1.";
                return false;
            }
            skip_checked = true;
            no_params = false;
            setup_params_.stride = stride;
        }

        // =====================
        // | Check -e option |
        // =====================
        if (arg == "-e")
        {
            i++;
            bool ok = false;
            int last_frame = args[i].toInt(&ok);
            if (!ok)
            {
                qInfo() << "Error. -e option requires a single integer value.";
                return false;
            }
            if (last_frame < -1)
            {
                qInfo() << "Error. -skip must be at least -1.";
                return false;
            }
            e_checked = true;
            no_params = false;
            setup_params_.last_frame = last_frame;
        }

        // ===================
        // | Check -g option |
        // ===================
        if (arg == "-g")
        {
            g_checked = true;
            no_params = false;
        }

        // ======================
        // | Check -name option |
        // ======================
        if (arg == "-name")
        {
            i++;
            setup_params_.name = arg[i];
            name_checked = true;
            no_params = false;
        }

        // ===================
        // | Check -h option |
        // ===================
        if (arg == "-h")
        {
            print_help();
            return false;
        }
    }

    // Set default parameters where applicable
    if (!pbc_checked)
        setup_params_.pbc_cutoff = 20;
    if (!box_checked && f_checked)
    {
        Pdbreader *reader = new Pdbreader(setup_params_.pdb);
        double x_cryst, y_cryst, z_cryst;
        reader->getBox(x_cryst, y_cryst, z_cryst);
        setup_params_.x_box = x_cryst;
        setup_params_.y_box = y_cryst;
        setup_params_.z_box = z_cryst;
        delete  reader;
    }
    if (!mode_checked)
        setup_params_.modus = 0;
    if (!vdw_checked)
    {
        setup_params_.vdw_check = 0;
        setup_params_.upright = 0;
    }
    if (!b_checked)
        setup_params_.first_frame = 0;
    if (!skip_checked)
        setup_params_.stride = 1;
    if (!e_checked)
        setup_params_.last_frame = -1;
    if (!name_checked)
    {
        QString n = setup_params_.pdb.split("/").last();
        setup_params_.name = n.split(".")[0];
    }
    if (!ld_check)
        setup_params_.leaflet_detection = 0;
    if (!pd_check)
        setup_params_.p_degree = 4;

    // Validate given arguments
    if ((f_checked || x_checked || n_checked) && !c_checked)
    {
        qInfo() << "Error. If you want to open a model you need to specify a config file.";
        return false;
    }
    if (c_checked && !f_checked)
    {
        qInfo() << "Error. You need to at least specify a pdb file in conjunction with a config file.";
        return false;
    }

    if (setup_params_.last_frame != -1 && setup_params_.first_frame > setup_params_.last_frame)
    {
        qInfo() << "Error. The last frame must come after the first frame.";
        return false;
    }
    if (!g_checked && !oa_checked && !ot_checked && !of_checked && !ofs_checked && !no_params)
    {
        qInfo() << "Error. No output file was specified.";
        return false;
    }
    if (ofs_checked && !sn_checked)
    {
        qInfo() << "Error. You must specify an index file for the selection using the -sn option.";
        return false;
    }
    if (oa_checked && area_lipids_.isEmpty())
    {
        qInfo() << "Error. When using the -oa otion you need to specify the area parameter in the configuration file";
        return false;
    }
    if (ot_checked && thickness_lipids_.isEmpty())
    {
        qInfo() << "Error. When using the -ot otion you need to specify the thick parameter in the configuration file";
        return false;
    }
    return true;
}


void CommandLineParser::print_help()
{
    QFile help_file(":text/help.txt");
    if (!help_file.open(QIODevice::ReadOnly))
        printf("Failed to open help.txt\n");
    else
    {
        QString data = help_file.readAll();
        QTextStream out(stdout);
        out << data << Qt::endl;
    }
}


bool CommandLineParser::file_exists(QString path)
{
    QFileInfo check_file(path);
    if (check_file.exists() && check_file.isFile())
        return true;
    else
        return false;
}


bool CommandLineParser::directory_exists(QString path)
{
    if (!path.contains("/"))  // working directory is used
        return true;
    QStringList dir_elements =  path.split("/");
    QString path_only = "";
    for (int i = 0; i < dir_elements.length() - 1; i++)
        path_only += (dir_elements[i] + "/");
    QFileInfo check_directory(path_only);
    if (check_directory.exists() && check_directory.isDir())
        return true;
    else
        return false;
}
