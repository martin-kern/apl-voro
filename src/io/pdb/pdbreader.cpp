//**************************************************************
//
//           Copyright (c) 2012 - 2020 by Gunther Lukat, Martin Kern
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************

#include "io/pdb/pdbreader.h"


Pdbreader::Pdbreader()
{
    mymolnumber = -1;
    linestartA="ATOM";
    linestartB="HETATOM";
}


Pdbreader::Pdbreader(QString pathline)
{

        mymolnumber = -1;
        mymol = new Molecule();
        path = pathline;
        linestartA="ATOM";
        linestartB="HETATOM";

}


bool Pdbreader::getBox(double &maxX, double &maxY, double &maxZ)
{
    std::string line = "x";
    QFile file(path);

    char maxXchars[9];
    char maxYchars[9];
    char maxZchars[9];
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return false;

    QTextStream in(&file);
    while (!in.atEnd())
    {
        QString newline = in.readLine();
        std::string line = newline.toStdString();
        if (line.find("CRYST1") !=std::string::npos)
        {
            size_t length;
            length= line.copy(maxXchars,8,8);
            maxXchars[length]='\0';
            maxX = atof(maxXchars)/10.0;
            length= line.copy(maxYchars,8,17);
            maxYchars[length]='\0';
            maxY = atof(maxYchars)/10.0;
            length= line.copy(maxZchars,8,26);
            maxZchars[length]='\0';
            maxZ = atof(maxZchars)/10.0;
            break;
        }
    }
    return true;
}


void Pdbreader::setPath(QString pathline)
{
    path= pathline;
}


void Pdbreader::get_single_molecules(vector<molecule_atoms> &molecule_list)
{
    /* Create a list of all unique molecules. Each element contains
     * the molecule name and a list of all atom names. This data is
     * used to set up the lipid selection page of the setup wizard.
     */
    QFile file(path);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    QTextStream in(&file);
    QStringList seen_molecules;
    QString line = in.readLine();
    bool line_read = true;
    while (!in.atEnd())
    {
        if(line.startsWith("ATOM") || line.startsWith("HETATM"))
        {
            QString molecule_name = line.mid(17, 3).trimmed();
            if (!seen_molecules.contains(molecule_name))
            {
                seen_molecules.append(molecule_name);
                QString residue_number = line.mid(22, 4).trimmed();
                molecule_atoms molecule_atms;
                molecule_atms.molecule_name = molecule_name;
                while (line.mid(22, 4).trimmed() == residue_number &&
                       !in.atEnd())
                {
                    line_read = false;
                    molecule_atms.atom_names.append(line.mid(12, 4).trimmed());
                    line = in.readLine();
                }
                molecule_list.push_back(molecule_atms);
            }
        }
        if (line_read)
            line = in.readLine();
        line_read = true;
    }
}


bool Pdbreader::known(std::vector<Molecule> &list,Molecule &m)
{
    for (unsigned int i = 0 ; i< list.size();i++)
    {
        if(list[i].getMolname().compare(m.getMolname())==0)
            return true;
    }
    return false;
}


void Pdbreader::getIndexedProtein(QString protein,
                                  QList<int> numbers,
                                  QHash<int, Atom> &atomlist,
                                  const QHash<QString, double> &vdwradii)
{
    QFile file(path);
    int atomnumber = 0;

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    QTextStream in(&file);
    while (!in.atEnd())
    {
        QString line = in.readLine();
        if(line.startsWith("ATOM"))
        {
            atomnumber +=1;
            if(numbers.contains(atomnumber))
            {
                QString rname=protein;
                QString atomname=line.mid(12,4).trimmed();
                int molnumber = line.mid(22,4).trimmed().toInt();
                double xwert= line.mid(30,8).trimmed().toDouble();
                double ywert = line.mid(38,8).trimmed().toDouble();
                double zwert = line.mid(46,8).trimmed().toDouble();
                double vdwr = 1.5;
                QString astart = atomname.left(1).toUpper();
                if(vdwradii.find(astart) != vdwradii.end())
                   vdwr = vdwradii.find(astart).value();

                Atom na;
//                na.meta_info = new atom_meta;
//                na.meta_info->atom_number = atomnumber;
//                na.meta_info->molnumber = molnumber;
//                na.meta_info->prot = true;
//                na.meta_info->vdw = float(vdwr);
//                na.meta_info->name = atomname;
//                na.meta_info->resname = rname;
//                atom_meta_data.push_back(am);


                na.setX(float(xwert));
                na.setY(float(ywert));
                na.setZ(float(zwert));
                na.setThick(100.0);
                na.setNumber(atomnumber);

                na.setVdw(vdwr);
                na.setProt(true);
                na.setName(atomname);
                na.setResname(rname);
                na.setResNumber(molnumber);
                atomlist.insert(atomnumber,na);
            }
        }
    }
}


void Pdbreader::getResidues(std::vector<Atom> &atoms,
                            std::vector<Molecule> &tlist,
                            const QHash<QString, double> &vdwradii)
{
    mymolnumber = -1;
    mymol = new Molecule();
    int count = 0;
    int atomnumber = 0;
    QFile file(path);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    QTextStream in(&file);
    while (!in.atEnd())
    {
        QString line = in.readLine();
        if(line.startsWith("ATOM"))
        {
            atomnumber++;
            QString atomname = line.mid(12,4).trimmed();
            QString residuename = line.mid(17,3).trimmed();
            if(inreslist(atoms, residuename))
            {
                double vdwr;
                int molnumber = line.mid(22,4).trimmed().toInt();
                double xwert= line.mid(30,8).trimmed().toDouble();
                double ywert = line.mid(38,8).trimmed().toDouble();
                double zwert = line.mid(46,8).trimmed().toDouble();
                QString astart = atomname.left(1).toUpper();
                if(vdwradii.find(astart) != vdwradii.end())
                    vdwr = vdwradii.find(astart).value();
                else
                    vdwr = 1.5;

                Atom na;
//                na.meta_info = new atom_meta;
//                na.meta_info->atom_number = atomnumber;
//                na.meta_info->molnumber = molnumber;
//                na.meta_info->prot = false;
//                na.meta_info->vdw = float(vdwr);
//                na.meta_info->name = atomname;
//                na.meta_info->resname = residuename;
//                atom_meta_data.push_back(am);


                na.setX(float(xwert));
                na.setY(float(ywert));
                na.setZ(float(zwert));
                na.setThick(100.0);

                na.setName(atomname);
                na.setVdw(vdwr);
                na.setNumber(atomnumber);
                na.setResname(residuename);
                na.setResNumber(molnumber);

                if(molnumber == mymolnumber)
                    mymol->addAtom(na);
                else
                {
                    if(count != 0)
                    {
                        count++;
                        tlist.push_back(*mymol);
                        delete(mymol);
                        mymol = new Molecule(residuename, molnumber, na);
                        mymolnumber = molnumber;
                    }
                    else
                    {
                        count++;
                        delete(mymol);
                        mymol = new Molecule(residuename, molnumber, na);
                        mymolnumber = molnumber;
                    }
                }
            }
        }
    }
    tlist.push_back(*mymol);
    delete(mymol);
}


bool Pdbreader::inreslist(std::vector<Atom> &atoms, QString resname)
{
    for (unsigned int i = 0 ; i < atoms.size(); i++)
    {
        QString nm(atoms[i].getResname());
        if(nm.compare(resname) == 0)
            return true;
    }
    return false;
}


bool Pdbreader::inProtlist(QHash<int, QVector<Atom> > &proteins, QString resname)
{
    QHash<int, QVector<Atom> >::iterator it;
    for (it = proteins.begin(); it != proteins.end(); ++it)
    {
        for (int i = 0 ; i <it.value().size(); i++)
        {
           QString rnm = it.value()[i].getResname();
           if(rnm.compare(resname) == 0)
               return true;
        }
    }
    return false;
}
