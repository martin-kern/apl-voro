//**************************************************************
//
//           Copyright (c) 2012 - 2020 by Gunther Lukat, Martin Kern
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************

#ifndef Pdbreader_h
#define Pdbreader_h 1
#include <string>
#include "coredata/atom.h"
#include "data/setup_parameters.hpp"
//#include "data/simulation.hpp"
#include <vector>
#include "coredata/membran.h"
#include "coredata/molecule.h"
#include "io/ndx/index_parser.hpp"
#include "data/atom_meta.hpp"
#include <iostream>
#include <fstream>
#include <QFile>
#include <QString>
#include <QStringList>
#include <QTextStream>

using namespace std;

struct molecule_atoms
{
    QString molecule_name;
    QStringList atom_names;
};

//struct AtomMeta;
//struct SimulationFrame;

class Pdbreader{

public:
    int mymolnumber;
    Molecule *mymol;
    QString moleculenamenew, moleculenameold;

    Pdbreader();
    Pdbreader(QString path);


    bool getBox(double &maxX, double &maxY,double &maxZ);
    void setPath(QString path);
    bool getList(std::vector<Molecule> &mlist, QList<int> numbers);
    void getWorkingset(std::vector<Atom> &sets, vector<Molecule> &mem, const QHash<QString, double> &vdwradii);
    bool known(std::vector<Molecule> &list,Molecule &m);

    void getResidues(std::vector<Atom> &atoms,
                     std::vector<Molecule> &tlist,
                     const QHash<QString, double> &vdwradii);

    void getIndexedProtein(QString protein,
                           QList<int> numbers,
                           QHash<int, Atom> &atomlist,
                           const QHash<QString, double> &vdwradii);
    void get_single_molecules(vector<molecule_atoms> &molecule_list);

private:
    std::string linestartA;  //= "ATOM"
    std::string linestartB;  // = "HETATOM"
    QString path;
    bool inreslist(vector<Atom> &atoms,QString resname);
    bool inProtlist(QHash<int,QVector<Atom> > &proteins,QString resname);


};
#endif
