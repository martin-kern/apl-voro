#include "aplreadwrite.hpp"

AplReadWrite::AplReadWrite()
{

}

void AplReadWrite::write(QString file_name, QHash<unsigned int, core *> *core_hash_list)
{
    /* Write an open session into a file.
     */
    QHash<unsigned int, core *>::iterator it;
    QFile file(file_name);

    QJsonArray core_list;
    for (it = core_hash_list->begin(); it != core_hash_list->end(); it++)
    {
        setup_parameters sp = it.value()->setup_params();
        QJsonObject core_object;
        core_object["name"] = sp.name;
        core_object["pdb"] = sp.pdb;
        core_object["trj"] = sp.trj;
        core_object["index"] = sp.index;
        core_object["pbc_cutoff"] = sp.pbc_cutoff;
        core_object["polynomial_degree"] = signed(sp.p_degree);
        core_object["mode"] = sp.modus;
        core_object["leaflet_detection"] = sp.leaflet_detection;
        core_object["first_frame"] = sp.first_frame;
        core_object["stride"] = sp.stride;
        core_object["last_frame"] = sp.last_frame;
        core_object["vdw_check"] = sp.vdw_check;
        core_object["upright"] = sp.upright;
        core_object["x_box"] = sp.x_box;
        core_object["y_box"] = sp.y_box;
        core_object["z_box"] = sp.z_box;

        QJsonArray protein_array, key_atom_array;
        for (int i = 0; i < sp.protein_names.length(); i++)
            protein_array.append(sp.protein_names[i]);
        for (int i = 0; i < sp.key_atoms.length(); i++)
            key_atom_array.append(sp.key_atoms[i]);
        core_object["protein_names"] = protein_array;
        core_object["key_atoms"] = key_atom_array;
        core_list.append(core_object);
    }
    QJsonDocument write_document(core_list);
    file.open(QIODevice::WriteOnly);
    file.write(write_document.toJson());
}


QList<setup_parameters> AplReadWrite::read(QString file_name)
{
    /* Load a session from a file. No validation of file is done.
     */
    QList<setup_parameters> setup_list;
    QFile file(file_name);
    file.open(QIODevice::ReadOnly);
    QJsonDocument load_document(QJsonDocument::fromJson(file.readAll()));
    QJsonArray core_list = load_document.array();
    for (int i = 0; i < core_list.size(); i++)
    {
        QJsonObject core_object = core_list[i].toObject();
        setup_parameters sp;
        sp.name = core_object["name"].toString();
        sp.pdb = core_object["pdb"].toString();
        sp.trj = core_object["trj"].toString();
        sp.index = core_object["index"].toString();
        sp.pbc_cutoff = core_object["pbc_cutoff"].toInt();
        sp.p_degree = unsigned(core_object["polynomial_degree"].toInt());
        sp.modus = core_object["mode"].toInt();
        sp.leaflet_detection = core_object["leaflet_detection"].toInt();
        sp.first_frame = core_object["first_frame"].toInt();
        sp.stride = core_object["stride"].toInt();
        sp.last_frame = core_object["last_frame"].toInt();
        sp.vdw_check = core_object["vdw_check"].toInt();
        sp.upright = core_object["upright"].toDouble();
        sp.x_box = core_object["x_box"].toDouble();
        sp.y_box = core_object["y_box"].toDouble();
        sp.z_box = core_object["z_box"].toDouble();
        QJsonArray protein_array = core_object["protein_names"].toArray();
        for (int j = 0; j < protein_array.size(); j++)
            sp.protein_names.append(protein_array[j].toString());
        QJsonArray atom_list_array = core_object["key_atoms"].toArray();
        for (int j = 0; j < atom_list_array.size(); j++)
            sp.key_atoms.append(atom_list_array[j].toString());
        setup_list.append(sp);
    }
    return setup_list;
}
