//**************************************************************
//
//           Copyright (c) 2012 - 2013 by Gunther Lukat
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************


#include "conf_parser.hpp"


conf_parser::conf_parser(QFile &file)
{
    conf_file = &file;
}

bool conf_parser::parse()
{
    QString line;
    QStringList trunc;
    if (!conf_file->open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qInfo() << "Configuration Error. Configuration file could not be opened.";
        return false;
    }
    QTextStream in(conf_file);
    while(!in.atEnd())
    {
        line = in.readLine().simplified();
        line.replace( " ", "" );

        // remove comments
        if (line.contains("#"))
        {
            int index = line.lastIndexOf("#");
            line = line.remove(index,line.size());
        }

        // Parse lipids. The "Lipid" tag is no longer needed. But it is handled for comatibilty reasons.
        if (line.startsWith("Lipids")){}
        // Parse lipids for area measurement.
        else if (line.startsWith("Area"))
        {
            trunc = line.split("=", Qt::SkipEmptyParts);
            if(trunc.size() != 2)
            {
                conf_file->close();
                qInfo() << "Configuration Error. Area option is not valid.";
                return false;
            }
            else
            {
                line = trunc[1];
                area_plot_lipids = line.split(",", Qt::SkipEmptyParts);
            }
        }
        // Parse lipids for thickness measurement.
        else if (line.startsWith("Thick"))
        {
            trunc=line.split("=", Qt::SkipEmptyParts);
            if(trunc.size() != 2)
            {
                conf_file->close();
                qInfo() << "Configuration Error. Thick option is not valid.";
                return false;
            }
            else
            {
                line = trunc[1];
                thickness_plot_lipids = line.split(",", Qt::SkipEmptyParts);
            }
        }
        // Parse index groups for protein insertion.
        else if (line.startsWith("Index_group"))
        {
            trunc=line.split("=", Qt::SkipEmptyParts);
            if(trunc.size() != 2)
            {
                qInfo() << "Configuration Error. Index_group option is not valid.";
                return false;
            }
            else
            {
                line = trunc[1];
                protein_groups = line.split(",", Qt::SkipEmptyParts);
            }
        }
        // Parse lipids and key atoms.
        else if(line.contains("=") && line.split("=").size() == 2)
        {
            QStringList s1 = line.split("=");
            QStringList s2 = s1[1].split(",");
            for (int i = 0; i < s2.size(); i++) {
                key_atoms.append(s1[0].trimmed() + " " + s2[i].trimmed());
            }
        }
    }

    if (key_atoms.isEmpty())
    {
        qInfo() << "Configuration Error. No key atoms for lipids given..";
        return false;
    }
    return true;
}


QStringList conf_parser::get_key_atoms()
{
    return key_atoms;
}


QStringList conf_parser::get_area_lipids()
{
    return area_plot_lipids;
}


QStringList conf_parser::get_thick_lipids()
{
    return thickness_plot_lipids;
}


QStringList conf_parser::get_protein()
{
    return protein_groups;
}


//bool conf_parser::get_Lipids()
//{
//    QString line;
//    if (!conf_file->open(QIODevice::ReadOnly | QIODevice::Text))
//          return false;
//    QTextStream in(conf_file);
//    while(!in.atEnd())
//    {
//        QStringList trunc;
//        line = in.readLine();
//        line= line.simplified();
//        line.replace( " ", "" );
//        if(line.contains("#"))
//        {
//            int index = line.lastIndexOf("#");
//            line = line.remove(index,line.size());
//        }
//        if(line.startsWith("Lipids") )
//        {
//            trunc=line.split("=",QString::SkipEmptyParts);
//            if(trunc.size() != 2)
//            {
//                conf_file->close();
//                return false;
//            }
//            else
//            {
//                line = trunc[1];
//                lipid_names = line.split(",",QString::SkipEmptyParts);
//            }
//        }
//    }
//    conf_file->close();
//    return true;
//}


//bool conf_parser::get_Lipid_atomnames(std::vector<Atom> *atomlist)
//{
//    QString line,resname;
//    QStringList trunc;
//    if (!conf_file->open(QIODevice::ReadOnly | QIODevice::Text))
//          return false;
//    QTextStream in(conf_file);

//    while(!in.atEnd())
//    {
//        line = in.readLine();
//        line= line.simplified();
//        line.replace( " ", "" );
//        if(line.contains("#"))
//        {
//            int index = line.lastIndexOf("#");
//            line = line.remove(index,line.size());
//        }
//        if(line.contains("="))
//        {
//            for (int i = 0; i < lipid_names.size(); i++)
//            {
//                if(line.startsWith(lipid_names[i]))
//                {
//                    resname = lipid_names[i];
//                    line.remove(resname);
//                    line.remove("=");
//                    trunc = line.split(",",QString::SkipEmptyParts);
//                    for (int j = 0 ; j < trunc.size(); j++)
//                    {
//                        Atom mya;
//                        mya.setResname(resname);
//                        trunc[j] = trunc[j].simplified();
//                        trunc[j].replace( " ", "" );
//                        mya.setName(trunc[j]);
//                        atomlist->push_back(mya);
//                    }
//                }
//            }
//        }
//    }
//    conf_file->close();
//    return true;
//}


//bool conf_parser::get_Area_plotnames(QStringList *areaplot_lipids)
//{
//    QString line;
//    QStringList trunc;
//    if (!conf_file->open(QIODevice::ReadOnly | QIODevice::Text))
//          return false;
//    QTextStream in(conf_file);
//    while(!in.atEnd())
//    {
//        line = in.readLine();
//        line= line.simplified();
//        line.replace( " ", "" );
//        if(line.contains("#"))
//        {
//            int index = line.lastIndexOf("#");
//            line = line.remove(index,line.size());
//        }
//            if(line.startsWith("Area") )
//            {
//                trunc=line.split("=", QString::SkipEmptyParts);
//                if(trunc.size() != 2)
//                {
//                    conf_file->close();
//                    return false;
//                }
//                else
//                {
//                    line = trunc[1];
//                    *areaplot_lipids = line.split(",",QString::SkipEmptyParts);

//                    //trunc now holds lipid names
//                }

//            }
//    }
//    conf_file->close();
//	return true;
//}


//bool conf_parser::get_Thick_plotnames(QStringList *thickplot_lipids)
//{
//    QString line;
//    QStringList trunc;
//    if (!conf_file->open(QIODevice::ReadOnly | QIODevice::Text))
//        return false;

//    QTextStream in(conf_file);
//    while(!in.atEnd())
//    {
//        line = in.readLine();
//        line= line.simplified();
//        line.replace( " ", "" );
//        if(line.contains("#"))
//        {
//            int index = line.lastIndexOf("#");
//            line = line.remove(index,line.size());
//        }
//        if(line.startsWith("Thick") )
//        {
//            trunc=line.split("=",QString::SkipEmptyParts);
//            if(trunc.size() != 2)
//            {
//                conf_file->close();
//                return false;
//            }
//            else
//            {
//                line = trunc[1];
//                *thickplot_lipids = line.split(",",QString::SkipEmptyParts);
//                //trunc now holds lipid names
//            }

//        }
//    }
//    conf_file->close();
//    return true;
//}


//bool conf_parser::get_Protein_index_group(QStringList *protein_goup)
//{
//    QString line;
//    QStringList trunc;
//    if (!conf_file->open(QIODevice::ReadOnly | QIODevice::Text))
//      return false;

//    QTextStream in(conf_file);
//    while(!in.atEnd())
//    {
//        line = in.readLine();
//        line= line.simplified();
//        line.replace( " ", "" );
//        if(line.contains("#"))
//        {
//            int index = line.lastIndexOf("#");
//            line = line.remove(index,line.size());
//        }
//        if(line.startsWith("Index_group") )
//        {
//            trunc=line.split("=",QString::SkipEmptyParts);
//            if(trunc.size() != 2)
//                return false;
//            else
//            {
//                line = trunc[1];
//                *protein_goup = line.split(",",QString::SkipEmptyParts);
//            }
//        }
//    }
//    conf_file->close();
//    return true;
//}
