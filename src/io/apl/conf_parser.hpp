//**************************************************************
//
//           Copyright (c) 2012 - 2013 by Gunther Lukat
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************

#ifndef CONF_PARSER_HPP
#define CONF_PARSER_HPP
#include<QFile>
#include<QString>
#include<QStringList>
#include<QTextStream>
#include <QDebug>
#include "coredata/atom.h"
class conf_parser
{
public:
    conf_parser(QFile &file);

    bool parse();
    QStringList get_key_atoms();
    QStringList get_area_lipids();
    QStringList get_thick_lipids();
    QStringList get_protein();

//    bool get_Lipids();
//    bool get_Lipid_atomnames(std::vector<Atom> *atomlist);
//    bool get_Area_plotnames(QStringList *areaplot_lipids);
//    bool get_Thick_plotnames(QStringList *thickplot_lipids);
//    bool get_Protein_index_group(QStringList *protein_goup);
private:
    QFile *conf_file;
//    QStringList lipid_names;

    QStringList key_atoms;
    QStringList area_plot_lipids;
    QStringList thickness_plot_lipids;
    QStringList protein_groups;
};

#endif // CONF_PARSER_HPP
