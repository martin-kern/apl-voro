#ifndef APLREADWRITE_HPP
#define APLREADWRITE_HPP

#include <QString>
#include <QHash>
#include <QDataStream>
#include <QFile>
#include <QList>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include "coredata/core.h"

class AplReadWrite
{
public:
    AplReadWrite();
    static void write(QString file, QHash<unsigned int, core*> *core_hash_list);
    static QList<setup_parameters> read(QString file);
};

#endif // APLREADWRITE_HPP
