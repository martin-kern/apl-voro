#ifndef THREADSAVE_HASH_MAP_HPP
#define THREADSAVE_HASH_MAP_HPP
#include <QHash>
#include <QMutex>

/**
 * An implementation of QHash where multiple threads can safely insert using the thread_safe_insert function
 */
template <typename Key, typename T>
class ThreadSaveHashMap : public QHash<Key, T>
{
public:
    ThreadSaveHashMap() : QHash<Key, T>()
    {}

    void thread_safe_insert(const Key &k, const T &v)
    {
        mutex.lock();
        this->insert(k, v);
        mutex.unlock();
    }

private:
    QMutex mutex;
};

#endif // THREADSAVE_HASH_MAP_HPP
