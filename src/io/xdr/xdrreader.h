#ifndef XDRREADER_H
#define XDRREADER_H
#include <QtGlobal>

#include <mutex>
#include <thread>
#include <condition_variable>
#include <memory>
#include <iostream>
#include <vector>
//#include <omp.h>
#include <math.h>
#include <queue>
#include <QHash>
#include <QVector3D>
#include <QFileInfo>

#include "io/xdr/xdrfile.h"
#include "io/xdr/xdrfile_trr.h"
#include "io/xdr/xdrfile_xtc.h"
#include "coredata/core.h"
#include "coredata/atom.h"
#include "coredata/molecule.h"
#include "coredata/membran.h"
#include "io/xdr/ThreadQueue.h"
#include "logic/leafletdetector.hpp"
#include "logic/regression.hpp"
#include "data/setup_parameters.hpp"
#include "io/xdr/threadsave_hash_map.hpp"
#include "io/xdr/ThreadQueue.h"

using namespace std;

struct Frame
{
    Membran mem;
    QVector<QVector3D> vec;
    int nr;
    std::vector<Molecule> molecules;
    int res;
    qreal getX(int num)
    {
        return double(vec[num-1].x());
    }
    qreal getY(int num)
    {
        return double(vec[num-1].y());
    }
    qreal getZ(int num)
    {
        return double(vec[num-1].z());
    }
};


class Xdrreader
{
public:
    Xdrreader(char *pathline, setup_parameters *sp);

//    static bool read(QString xdr_path, Simulation *simulation);


    void read(ThreadSaveHashMap<int,Membran> *_tlist, std::vector<Molecule> *_molecules, QHash<QString, QHash<int, Atom> > *_protein);
    void read_for_command_line(threadsafe_queue<Membran> *out_queue,
                               std::vector<Molecule> *_molecules,
                               QHash<QString, QHash<int, Atom> > *_protein,
                               CommandLineParser parser);
    int result;
    int natoms;
    int nframes;
    float time;
    XDRFILE *xd;
    rvec *x;
    int step;
    float lambda;
    matrix box;
    QHash<QString,QHash< int, Atom> > *protein;
    std::vector<Molecule> *molecules;

private:
    char *path;
    setup_parameters *setup_params;
    int framenumber;
    int notified;
    void producer(threadsafe_queue<Frame> &queue, QString ending);
    void consumer(threadsafe_queue<Frame> &queue, ThreadSaveHashMap<int,Membran> &out);
    void consumer_for_cl(threadsafe_queue<Frame> &queue, threadsafe_queue<Membran> &out);
    void processdata(Frame &frame, ThreadSaveHashMap<int,Membran> &out);
    void process_for_cl(Frame &frame, threadsafe_queue<Membran> &out);
    void process_frame(Frame &frame);
    double Round(double Zahl, int Stellen);
};
#endif // XDRREADER_H
