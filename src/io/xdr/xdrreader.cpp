#include "io/xdr/xdrreader.h"


Xdrreader::Xdrreader(char* pathline, setup_parameters *sp)
{
    path = pathline;
    result = 0;
    natoms = 0;
    nframes = 0;
    setup_params = sp;
}


double Xdrreader::Round(double Zahl, int Stellen)
{
    double v[] = { 1, 10, 1e2, 1e3, 1e4, 1e5, 1e6, 1e7, 1e8 };
    return floor(Zahl * v[Stellen] + 0.5) / v[Stellen];
}



void Xdrreader::read(ThreadSaveHashMap<int, Membran> *_tlist, std::vector<Molecule> *_molecules, QHash< QString, QHash< int, Atom> > *_protein)
{
    molecules = _molecules;
    protein = _protein;
    QFileInfo fi(path);
    QString ending = fi.suffix();
    threadsafe_queue<Frame> queue;

    unsigned int num = thread::hardware_concurrency() - 1;
    if (num <= 0)
         num = 1;
    vector<thread> ts;

    for (unsigned int i = 0; i < num; i++)
        ts.push_back(thread(&Xdrreader::consumer, this ,ref(queue), ref(*_tlist)));

    thread producer_tread(&Xdrreader::producer, this, ref(queue), ending);
    for (unsigned int i = 0; i < num ; i++)
        ts[i].join();

    producer_tread.join();
}


void Xdrreader::read_for_command_line(threadsafe_queue<Membran> *_tlist,
                                      std::vector<Molecule> *_molecules,
                                      QHash< QString, QHash< int, Atom> > *_protein,
                                      CommandLineParser parser)
{
    molecules = _molecules;
    protein = _protein;
    QFileInfo fi(path);
    QString ending = fi.suffix();
    threadsafe_queue<Frame> queue;

    unsigned int num = thread::hardware_concurrency() - 2;
    if (num <= 0)
         num = 1;
    vector<thread> ts;

    thread reader_thread(&Xdrreader::producer, this, ref(queue), ending);
    thread process_thread(&Xdrreader::consumer_for_cl, this ,ref(queue), ref(*_tlist));
    thread writer_thread(&core::cl_writer, ref(_tlist), ref(parser));
    reader_thread.join();
    process_thread.join();
    writer_thread.join();

}


void Xdrreader::producer(threadsafe_queue<Frame> &queue, QString ending)
{
    xd = xdrfile_open(path, "r");
    float prec = 1000;
    
    if (ending.compare("xtc") == 0)
        result = read_xtc_natoms(path,&natoms);
   
    if (ending.compare("trr") == 0)
        result = read_trr_natoms(path, &natoms);
    
    x = (float(*)[3])calloc(natoms, sizeof(x[0]));

    int currentFrame = 0;

    while (result == 0 && (setup_params->last_frame == -1 || currentFrame < setup_params->last_frame))
    {
        if (ending.compare("xtc") == 0)
            result = read_xtc(xd, natoms, &step, &time, box, x, &prec);

        if (ending.compare("trr") == 0)
            result = read_trr(xd, natoms, &step, &time, &lambda, box, x, nullptr, nullptr);

        //Only load frame if it is within the specified range and account for stride
        if (currentFrame >= setup_params->first_frame && ((currentFrame - setup_params->first_frame) % setup_params->stride) == 0)
        {
            Membran *mem = new Membran();
            nframes ++;
            double boxx = double(box[0][0]);
            boxx = boxx*10;

            double boxx4 = double(box[1][1]);
            boxx4 = boxx4*10;

            double boxx5 = double(box[2][2]);
            boxx5 = boxx5*10;
            mem->maxX = Round(boxx,3);
            mem->maxY = Round(boxx4,3);
            mem->maxZ = Round(boxx5,3);
            Frame frame;
            mem->framenr = nframes;

            frame.nr = nframes;

            for (int i = 0; i< natoms; i++)
            {
                double px = double(x[i][0]) * 10;
                double py = double(x[i][1]) * 10;
                double pz = double(x[i][2]) * 10;
                QVector3D pkt(float(Round(px,3)), float(Round(py,3)), float(Round(pz,3)));
                frame.vec.push_back(pkt);
            }

            frame.molecules = *molecules;
            mem->protein = *protein;
            frame.mem = *mem;
            frame.mem.time = time;
            frame.res = result;

            queue.push(frame);
            delete  mem;
        }
        currentFrame++;
    }
    queue.notified = true;
    queue.notifyall();
}


void  Xdrreader::consumer(threadsafe_queue<Frame> &queue, ThreadSaveHashMap<int,Membran> &out)
{
    while(true)
    {
        Frame data;
        if(queue.wait_and_pop(data))
            processdata(data, out);
        else
            break;
    }
}


void  Xdrreader::consumer_for_cl(threadsafe_queue<Frame> &queue, threadsafe_queue<Membran>  &out)
{
    while(true)
    {
        Frame data;
        if(queue.wait_and_pop(data))
            process_for_cl(data, out);
        else {
            out.notified = true;
            out.notifyall();
            break;
        }
    }
}


void Xdrreader::processdata(Frame &frame, ThreadSaveHashMap<int,Membran> &out)
{
    process_frame(frame);
    out.thread_safe_insert(frame.nr, frame.mem);
}


void Xdrreader::process_for_cl(Frame &frame, threadsafe_queue<Membran> &out)
{
    process_frame(frame);
    out.push(frame.mem);
}

void Xdrreader::process_frame(Frame &frame)
{
    frame.mem.polynomial = polynomial_regression(frame.molecules, setup_params->p_degree);
    QHash<QString,QHash<int,Atom> >::iterator it;
    QHash<int,Atom>::iterator it2;

    // Protein atoms
    for (it = frame.mem.protein.begin() ; it != frame.mem.protein.end(); it++)
    {
        for (it2 = it.value().begin(); it2 != it.value().end(); ++it2 )
        {
            Atom a = it2.value();
            int num = a.getNumber();

            a.setX(frame.vec[num-1].x());
            a.setY(frame.vec[num-1].y());
            a.setZ(frame.vec[num-1].z());
            a.setThick(100);

            frame.mem.protein[it.key()][num]=a;
        }
    }


    for (unsigned int i=0; i<frame.molecules.size(); i++)
    {
        Molecule mol;
        QList<int> keys = frame.molecules[i].list.keys();

        for (int j = 0; j < keys.size(); j++)
        {
            int num = keys[j];
            Atom a = frame.molecules[i].list[num];
            a.setX(frame.vec[num-1].x());
            a.setY(frame.vec[num-1].y());
            a.setZ(frame.vec[num-1].z());
            a.setThick(100);
            mol.list[num] = a;
        }

        // Assign molecule to leaflet

        if (setup_params->leaflet_detection == 0)
        {
            if (mol.detectSide())
            {
                for (int k = 0 ; k < frame.molecules[i].included.size();k++)
                {
                    int number = frame.molecules[i].included[k];
                    frame.mem.leaflets["upside"]->QHside.insert(number,mol.list[number]);
                    frame.mem.leaflets["upside"]->QHside[number].setSide(1);
                }
            }
            else
            {
                for (int k = 0 ; k < frame.molecules[i].included.size();k++)
                {
                    frame.mem.leaflets["downside"]->QHside[frame.molecules[i].included[k]] = mol.getAtom(frame.molecules[i].included[k]);
                    frame.mem.leaflets["downside"]->QHside[frame.molecules[i].included[k]].setSide(0);
                }
            }
        }
        else if (setup_params->leaflet_detection == 1)
        {
            vector<float> pos = frame.molecules[i].ref_pos();
            float poly_z = polyval(frame.mem.polynomial, pos[0], pos[1], unsigned(setup_params->p_degree));
            bool side = pos[2] - poly_z > 0;  // true: upper leaflet, false: lower leaflet
            if (side)
            {
                for (int k = 0 ; k < frame.molecules[i].included.size();k++)
                {
                    int number = frame.molecules[i].included[k];
                    frame.mem.leaflets["upside"]->QHside.insert(number,mol.list[number]);
                    frame.mem.leaflets["upside"]->QHside[number].setSide(1);
                }
            }
            else
            {
                for (int k = 0 ; k < frame.molecules[i].included.size();k++)
                {
                    frame.mem.leaflets["downside"]->QHside[frame.molecules[i].included[k]] = mol.getAtom(frame.molecules[i].included[k]);
                    frame.mem.leaflets["downside"]->QHside[frame.molecules[i].included[k]].setSide(0);
                }
            }
        }
    }
}
