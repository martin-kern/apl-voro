/*
 *  xvg_printer.cpp
 *  testsets
 *
 *  Created by Merlin on 19.08.11.
 *  Copyright 2011 _Gunther Lukat_ All rights reserved.
 *
 */

#include "io/xvg/xvg_printer.h"

xvg_printer::xvg_printer()
{

}
QString xvg_printer::setPath(QString path, QString l_name, QString side)
{   xvg_file = new QFile();
    QString name;
    name = path;
    name +="_";
    name +=side;
    name +="_";
    name +=l_name;
    name +=".xvg";

    xvg_file->setFileName(name);

    int c = 1;
    while(xvg_file->exists())
          {
        QString num;
        name = path;
        name +="_";
        name +=side;
        name +="_";
        name +=l_name;
        name +="_";
        name +=num.setNum(c,10);
        name +=".xvg";
         xvg_file->setFileName(name);
        c +=1;

          }
    return name;

}
bool xvg_printer::print_file(QVector<double> values,int mode)
{

         if (!xvg_file->open(QIODevice::WriteOnly | QIODevice::Text))
             return false;

         QTextStream out(xvg_file);
         out << "@type xy " <<"\n";
         if(mode ==1)
         {
         out << "@xaxis  label Time (frames)" <<"\n";
         out <<  "@yaxis  label Area nm ^2" <<"\n";
         }
         if(mode ==0)
         {
         out << "@xaxis  label Time (frames)" <<"\n";
         out <<  "@yaxis  label Thickness nm" <<"\n";
         }



   for (int j = 0; j < values.size(); j++)
   {
       out << j <<"   "<< values[j]<<"\n";
   }

   xvg_file->close();
return true;
}
