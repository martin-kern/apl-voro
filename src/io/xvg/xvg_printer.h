/*
 *  xvg_printer.h
 *  testsets
 *
 *  Created by Merlin on 19.08.11.
 *  Copyright 2011 __Gunther Lukat__. All rights reserved.
 *
 */


#ifndef xvg_printer_h
#define xvg_printer_h 1
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <QFile>
#include <QTextStream>
#include "coredata/membran.h"
#include "coredata/molecule.h"
#include "coredata/atom.h"




/**
 * @brief
 *
 */
/*!
 \brief

*/
class xvg_printer{

public:
        xvg_printer();
        bool print_file(QVector<double> values, int mode);
        QString setPath(QString path,QString l_name,QString side);

        QFile *xvg_file;

};
#endif
