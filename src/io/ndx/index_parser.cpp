//**************************************************************
//
//           Copyright (c) 2012 - 2013 by Gunther Lukat
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************


#include "index_parser.hpp"


index_parser::index_parser(QFile &file)
{
    index_file = &file;
}


bool index_parser::index_list(QString ndx_path, QStringList sections, QList<int> *ndx_list)
{
    QFile ndx_file(ndx_path);
    bool read = false;
    if (!ndx_file.exists())
        return false;
    QTextStream ndx_stream(&ndx_file);
    while (!ndx_stream.atEnd())
    {
        QString line = ndx_stream.readLine();
        if (line.trimmed().startsWith("["))
        {
            int line_length = line.trimmed().length();
            QString section_name = line.trimmed().mid(1, line_length - 2).trimmed();
            if (sections.contains(section_name))
                read = true;
            else
                read = false;
        }

        if (!line.trimmed().startsWith("[") && read)
        {
            QStringList indices = line.trimmed().split(" ", Qt::SkipEmptyParts);
            for (int i = 0; i < indices.length(); i++)
                ndx_list->append(indices[i].toInt());
        }
    }
    return true;
}


bool index_parser::get_content(QStringList *list)
{
    if (!index_file->open(QIODevice::ReadOnly | QIODevice::Text))
        return false;

    QTextStream in(index_file);
    while (!in.atEnd())
    {
        QString line = in.readLine();
        if(line.startsWith("["))
        {
            int s = line.size();
            QString center = line.mid(1,s-2).trimmed();
            list->push_back(center);
        }
    }
    index_file->close();
    return true;
}


bool index_parser::get_content_section(QHash <QString, QList <int>> *intlist,
                                       QString section)
{
    bool alpha = false;
    QList <int> tlist;
    if (!index_file->open(QIODevice::ReadOnly | QIODevice::Text))
        return false;

    QTextStream in(index_file);
    while (!in.atEnd())
    {
        QString line = in.readLine();
        if(line.startsWith("["))
        {
            alpha = false;
            if(isSection(section, line))
                alpha = true;
        }
        if(alpha && !isSection(section, line))
        {
            QStringList list2 = line.split(" ", Qt::SkipEmptyParts);
            if(intlist->contains(section))
                tlist = (*intlist)[section];
            else
                intlist->insert(section,tlist);
            for(int i = 0 ; i < list2.size(); i++)
            {
                int xxx = list2[i].toInt();
                (*intlist)[section].push_back(xxx);
            }
        }
    }
    index_file->close();
    return true;
}


bool index_parser::isSection(QString section, QString line)
{
    if(line.startsWith("["))
    {
        int s = line.size();
        QString foundsec = line.mid(1,s-2).trimmed();
        if(foundsec.compare(section)==0)
            return true;
    }
    return false;
}


/**
 * @brief index_parser::get_index_selection
 * @param ndx_file_path
 * @return
 */
QList <int> index_parser::get_index_selection(QString ndx_file_path)
{
    QList <int> index_list;
    QFile *ndx_file = new QFile(ndx_file_path);
    if (!ndx_file->open(QIODevice::ReadOnly | QIODevice::Text))
        return index_list;

    QTextStream in(ndx_file);
    while (!in.atEnd())
    {
        QString line = in.readLine();
        if (!line.startsWith("[")) {
            QStringList string_list = line.split(" ", Qt::SkipEmptyParts);
            for(int i = 0 ; i < string_list.size(); i++)
                index_list.append(string_list[i].toInt());
        }
    }
    ndx_file->close();
    return index_list;
}
