//**************************************************************
//
//           Copyright (c) 2012 - 2013 by Gunther Lukat
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************

#ifndef INDEX_PARSER_HPP
#define INDEX_PARSER_HPP
#include<QString>
#include<QStringList>
#include<QTextStream>
#include <QFile>


class index_parser
{
public:
    index_parser(QFile &file);
    static bool index_list(QString ndx_path, QStringList sections, QList<int> *ndx_list);
    bool get_content(QStringList *list);
    bool get_content_section(QHash < QString, QList <int>  > *intlist, QString section);
    bool isSection(QString section, QString line);

    /**
     * @brief read a GROMACS ndx file and return a list of the contained indices. Categories will be ignored.
     * @param ndx_file_path path to an ndx file
     * @return
     */
    static QList <int> get_index_selection(QString ndx_file_path);
    QFile *index_file;
};

#endif // INDEX_PARSER_HPP
