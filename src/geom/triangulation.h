//**************************************************************
//
//           Copyright (c) 2012 - 2013 by Gunther Lukat
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************


#ifndef TRIANGULATION_H
#define TRIANGULATION_H

#include "edge.h"
#include"geomalgorithm.h"
#include <QDataStream>


/*!
 \brief

*/
class Triangulation
{
public:


    Edge *startingEdge; /**< TODO */ /*!< TODO */


/*!
 \brief

 \param a
 \param b
 \param c
*/
   Triangulation(const QPointF2D &a, const QPointF2D &b, const QPointF2D &c);

   // Returns an edge e, s.t. either x is on e, or e is an edge of
   // a triangle containing x.
   //The search starts from startingEdge
   // and proceeds in the general direction of x. Based on the
   // pseudocode in Guibas and Stolfi (1985) p.121.


   /*!
    \brief

    \param x
    \return Edge
   */
   Edge* Locate(const QPointF2D& x);


   // Inserts a new point into a subdivision representing a Delaunay triangulation, and fixes the affected edges so that the result
   // is still a Delaunay triangulation. This is based on the
   // pseudocode from Guibas and Stolfi (1985) p.120,


   /*!
    \brief

    \param x
    \param QVector<QPair<int
    \param doubles
   */
   void InsertSite(const QPointF2D& x, QVector<QPair<int,int> >&doubles);



   /*!
    \brief

    \param x
    \return Triangle
   */
   Triangle TriangleContainsPoint(const QPointF2D& x);



    /*!
     \brief

     \param x
     \return Triangle
    */
    Triangle getTriangleCCW(const QPointF2D& x);


    /*!
     \brief

     \param x
     \param points
     \return QVector<QPointF2D>
    */
    QVector<QPointF2D>& getTriangleCCW_nonVirtual(const QPointF2D& x, QVector<QPointF2D> &points);



    /*!
     \brief

     \param x
     \return Triangle
    */
    Triangle getTriangleCW(const QPointF2D& x);



    /*!
     \brief

     \param x
     \param poly
     \return QPolygonF
    */
    QPolygonF &getCCW_VCellFor(const QPointF2D& x, QPolygonF* poly);


    /*!
     \brief

     \param x
     \param poly
     \return QPolygonF
    */
    QPolygonF &getCW_VCellFor(const QPointF2D& x, QPolygonF* poly);




    /*!
     \brief

     \param x
     \param poly
     \return QList<QPointF2D>
    */
    QList<QPointF2D> &getNBLIST(const QPointF2D& x, QList<QPointF2D> *poly);


   // Add a new edge e connecting the destination of a to the
    // origin of b, in such a way that all three have the same
    // left face after the connection is complete.
   // Additionally, the data pointers of the new edge are set.


   /*!
    \brief

    \param a
    \param b
    \return Edge
   */
   Edge* Connect(Edge* a, Edge* b);

   // Essentially turns edge e counterclockwise inside its enclosing // quadrilateral. The data pointers are modified accordingly.


   /*!
    \brief

    \param e
   */
   void Swap(Edge* e);


   // Returns twice the area of the oriented triangle (a, b, c), i.e., the // area is positive if the triangle is oriented counterclockwise.
 //  inline double TriArea(const QPointF2D& a, const QPointF2D& b, const QPointF2D& c);


   // Returns TRUE if the point d is inside the circle defined by the // points a, b, c. See Guibas and Stolfi (1985) p.107
   //int InCircle(const QPointF2D& a, const QPointF2D& b, const QPointF2D& c, const QPointF2D& d);


    // Returns TRUE if the points a, b, c are in a counterclockwise order
   //int ccw(const QPointF2D& a, const QPointF2D& b, const QPointF2D& c);




   /*!
    \brief

    \param x
    \param e
    \return int
   */
   int RightOf(const QPointF2D& x, Edge* e);



   /*!
    \brief

    \param x
    \param e
    \return int
   */
   int LeftOf(const QPointF2D& x, Edge* e);


   // A predicate that determines if the point x is on the edge e.
   // The point is considered on if it is in the EPS-neighborhood
   // of the edge.


   /*!
    \brief

    \param x
    \param e
    \return int
   */
   int OnEdge(const QPointF2D& x, Edge* e);




};

#endif // TRIANGULATION_H
