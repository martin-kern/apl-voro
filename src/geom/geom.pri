SOURCES += geom/edge.cpp \
           geom/triangulation.cpp

HEADERS +=  geom/edge.h \
            geom/triangulation.h \
            geom/qpointf2d.h \
            geom/geomalgorithm.h
