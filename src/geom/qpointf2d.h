//**************************************************************
//
//           Copyright (c) 2012 - 2013 by Gunther Lukat
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************


#ifndef QPOINTF2D_H
#define QPOINTF2D_H
#include <math.h>
#include <iostream>
#include <QPointF>
#include <QDataStream>

#ifndef ABS
#define ABS(a)	((a) >= 0 ? (a) : -(a))
#endif

#ifndef MAX
#define MAX(a, b)	((a) >= (b) ? (a) : (b))
#define MIN(a, b)	((a) <= (b) ? (a) : (b))
#endif

#ifndef TRUE
#define FALSE 0
#define TRUE  1
#endif

#define EPS 1e-6




/*!
 \brief

*/
class  QPointF2D
{
public:

    /*!
     \brief

     \param stream
     \param c
     \return QDataStream &operator
    */
    friend QDataStream &operator<<(QDataStream &stream, const QPointF2D &c)
    {

        stream << c.xp;
        stream << c.yp;
        stream << c.virt;
        stream << c.number;

        return stream;
    }

    /*!
     \brief

     \param stream
     \param c
     \return QDataStream &operator >>
    */
    friend QDataStream &operator>>(QDataStream &stream, QPointF2D &c)
    {


        stream >> c.xp >> c.yp >> c.virt >> c.number;

        return stream;

    }

/*!
 \brief

*/
    QPointF2D();

/*!
 \brief

 \param p
*/
    QPointF2D(const QPointF &p);

/*!
 \brief

 \param p
*/
    QPointF2D(const QPoint &p);

/*!
 \brief

 \param xpos
 \param ypos
*/
    QPointF2D(qreal xpos, qreal ypos);

    /*!
     \brief

     \return qreal
    */
    inline qreal norm() const;
    bool virt; /**< TODO */ /*!< TODO */

    /*!
     \brief

     \return qreal
    */
    qreal manhattanLength() const;


    /*!
     \brief

     \return bool
    */
    bool isNull() const;

    /*!
     \brief

     \return qreal
    */
    qreal x() const;

    /*!
     \brief

     \return qreal
    */
    qreal y() const;

    /*!
     \brief

     \param x
    */
    void setX(qreal x);

    /*!
     \brief

     \param y
    */
    void setY(qreal y);

    /*!
     \brief

     \return qreal
    */
    qreal &rx();

    /*!
     \brief

     \return qreal
    */
    qreal &ry();


    /*!
     \brief

     \param p
     \return QPointF2D &operator
    */
    QPointF2D &operator+=(const QPointF2D &p);

    /*!
     \brief

     \param p
     \return QPointF2D &operator
    */
    QPointF2D &operator-=(const QPointF2D &p);

    /*!
     \brief

     \param c
     \return QPointF2D &operator
    */
    QPointF2D &operator*=(qreal c);

    /*!
     \brief

     \param c
     \return QPointF2D &operator
    */
    QPointF2D &operator/=(qreal c);


    /*!
     \brief

     \param
     \param
     \return bool operator
    */
    friend inline bool operator==(const QPointF2D &, const QPointF2D &);

    /*!
     \brief

     \param
     \param
     \return bool operator
    */
    friend inline bool operator!=(const QPointF2D &, const QPointF2D &);

    /*!
     \brief

     \param
     \param
     \return const QPointF2D operator
    */
    friend inline const QPointF2D operator+(const QPointF2D &, const QPointF2D&);

    /*!
     \brief

     \param
     \param
     \return const QPointF2D operator
    */
    friend inline const QPointF2D operator-(const QPointF2D &, const QPointF2D &);

    /*!
     \brief

     \param qreal
     \param
     \return const QPointF2D operator
    */
    friend inline const QPointF2D operator*(qreal, const QPointF2D &);

    /*!
     \brief

     \param
     \param qreal
     \return const QPointF2D operator
    */
    friend inline const QPointF2D operator*(const QPointF2D &, qreal);

    /*!
     \brief

     \param
     \return const QPointF2D operator
    */
    friend inline const QPointF2D operator-(const QPointF2D &);

    /*!
     \brief

     \param
     \param qreal
     \return const QPointF2D operator
    */
    friend inline const QPointF2D operator/(const QPointF2D &, qreal);


    /*!
     \brief

     \return QPoint
    */
    QPoint toPoint() const;

    /*!
     \brief

     \return QPointF
    */
    QPointF toPointF() const;
    int number; /*!< TODO */

    double xp; /*!< TODO */
    double yp;  /*!< TODO */
private:
    friend class QMatrix;
    friend class QTransform;


};


/*!
 \brief

*/
inline QPointF2D::QPointF2D() : xp(0), yp(0) {  number = 0; virt = true; }


/*!
 \brief

 \param xpos
 \param ypos
*/
inline QPointF2D::QPointF2D(qreal xpos, qreal ypos) : xp(xpos), yp(ypos) { number = 0; virt = true;}

/*!
 \brief

 \param p
*/
inline QPointF2D::QPointF2D(const QPoint &p) : xp(p.x()), yp(p.y()) { number = 0; virt = true;}

/*!
 \brief

 \return bool
*/
inline bool QPointF2D::isNull() const
{
    return qIsNull(xp) && qIsNull(yp);
}

/*!
 \brief

 \return qreal
*/
inline qreal QPointF2D::x() const
{
    return xp;
}


/*!
 \brief

 \return qreal
*/
inline qreal QPointF2D::y() const
{
    return yp;
}

/*!
 \brief

 \param xpos
*/
inline void QPointF2D::setX(qreal xpos)
{
    xp = xpos;
}


/*!
 \brief

 \param ypos
*/
inline void QPointF2D::setY(qreal ypos)
{
    yp = ypos;
}

/*!
 \brief

 \return qreal
*/
inline qreal &QPointF2D::rx()
{
    return xp;
}


/*!
 \brief

 \return qreal
*/
inline qreal &QPointF2D::ry()
{
    return yp;
}

/*!
 \brief

 \return qreal
*/
inline qreal QPointF2D::norm() const
{
    return sqrt(this->x() * this->x() + this->y() * this->y());
}

/*!
 \brief

 \param p
 \return QPointF2D &QPointF2D::operator
*/
inline QPointF2D &QPointF2D::operator+=(const QPointF2D &p)
{
    xp+=p.xp;
    yp+=p.yp;
    return *this;
}


/*!
 \brief

 \param p
 \return QPointF2D &QPointF2D::operator
*/
inline QPointF2D &QPointF2D::operator-=(const QPointF2D &p)
{
    xp-=p.xp; yp-=p.yp; return *this;
}

/*!
 \brief

 \param c
 \return QPointF2D &QPointF2D::operator
*/
inline QPointF2D &QPointF2D::operator*=(qreal c)
{
    xp*=c; yp*=c; return *this;
}

/*!
 \brief

 \param p1
 \param p2
 \return bool operator
*/
inline bool operator==(const QPointF2D &p1, const QPointF2D &p2)
{
    return qFuzzyIsNull(p1.xp - p2.xp) && qFuzzyIsNull(p1.yp - p2.yp);
}

/*!
 \brief

 \param p1
 \param p2
 \return bool operator
*/
inline bool operator!=(const QPointF2D &p1, const QPointF2D &p2)
{
    return !qFuzzyIsNull(p1.xp - p2.xp) || !qFuzzyIsNull(p1.yp - p2.yp);
}

/*!
 \brief

 \param p1
 \param p2
 \return const QPointF2D operator
*/
inline const QPointF2D operator+(const QPointF2D &p1, const QPointF2D &p2)
{
    return QPointF2D(p1.xp+p2.xp, p1.yp+p2.yp);
}


/*!
 \brief

 \param p1
 \param p2
 \return const QPointF2D operator
*/
inline const QPointF2D operator-(const QPointF2D &p1, const QPointF2D &p2)
{
    return QPointF2D(p1.xp-p2.xp, p1.yp-p2.yp);
}

/*!
 \brief

 \param p
 \param c
 \return const QPointF2D operator
*/
inline const QPointF2D operator*(const QPointF2D &p, qreal c)
{
    return QPointF2D(p.xp*c, p.yp*c);
}


/*!
 \brief

 \param c
 \param p
 \return const QPointF2D operator
*/
inline const QPointF2D operator*(qreal c, const QPointF2D &p)
{
    return QPointF2D(p.xp*c, p.yp*c);
}

/*!
 \brief

 \param p
 \return const QPointF2D operator
*/
inline const QPointF2D operator-(const QPointF2D &p)
{
    return QPointF2D(-p.xp, -p.yp);
}

/*!
 \brief

 \param c
 \return QPointF2D &QPointF2D::operator
*/
inline QPointF2D &QPointF2D::operator/=(qreal c)
{
    xp/=c;
    yp/=c;
    return *this;
}

/*!
 \brief

 \param p
 \param c
 \return const QPointF2D operator
*/
inline const QPointF2D operator/(const QPointF2D &p, qreal c)
{
    return QPointF(p.xp/c, p.yp/c);
}

/*!
 \brief

 \return QPoint
*/
inline QPoint QPointF2D::toPoint() const
{
    return QPoint(qRound(xp), qRound(yp));
}

/*!
 \brief

 \return QPointF
*/
inline QPointF QPointF2D::toPointF() const
{
    return QPointF(xp, yp);
}


/*!
 \brief

*/
class Line {
public:

/*!
 \brief

*/
    Line()	{}

/*!
 \brief

 \param
 \param
*/
    Line(const QPointF2D&, const QPointF2D&);

    /*!
     \brief

     \param
     \return double
    */
    double eval(const QPointF2D&) const;

    /*!
     \brief

     \param
     \return int
    */
    int classify(const QPointF2D&) const;
private:
    double a, b, c; /**< TODO */ /*!< TODO */
};



/*!
 \brief

 \param p
 \param q
*/
inline Line::Line(const QPointF2D& p, const QPointF2D& q)
// Computes the normalized line equation through the
// points p and q.
{
    QPointF2D t = q - p;
   double len = t.norm();
   a =   t.y() / len;
   b = - t.x() / len;
   c = -(a*p.x() + b*p.y());
}


/*!
 \brief

 \param p
 \return double
*/
inline double Line::eval(const QPointF2D& p) const
// Plugs point p into the line equation.
{
    return (a * p.x() + b* p.y() + c);
}

/*!
 \brief

 \param p
 \return int
*/
inline int Line::classify(const QPointF2D& p) const
// Returns -1, 0, or 1, if p is to the left of, on,
// or right of the line, respectively.
{
   double d = eval(p);
    return (d < -EPS) ? -1 : (d > EPS ? 1 : 0);
}

/*!
 \brief

*/
class Triangle {
public:

/*!
 \brief

*/
    Triangle()	{}

/*!
 \brief

 \param
 \param
 \param
*/
    Triangle(const QPointF2D&, const QPointF2D&, const QPointF2D&);

    /*!
     \brief

     \return int
    */
    int clockwise();

    /*!
     \brief

     \param d
     \return int
    */
    int InCircle(const QPointF2D& d);

    /*!
     \brief

     \return QPointF2D
    */
    inline  QPointF2D circumCircleCenter();
    QPointF2D A, B, C; /**< TODO */ /*!< TODO */

private:

    /*!
     \brief

     \param a
     \param b
     \param c
     \return double
    */
    double TriArea(const QPointF2D& a, const QPointF2D& b, const QPointF2D& c);
};


/*!
 \brief

 \param a
 \param b
 \param c
*/
inline Triangle::Triangle(const QPointF2D& a, const QPointF2D& b, const QPointF2D& c)
 {
     A = a;
     B = b;
     C = c;
 }


 /*!
  \brief

  \param a
  \param b
  \param c
  \return double
 */
 inline double Triangle::TriArea(const QPointF2D& a, const QPointF2D& b, const QPointF2D& c)
 {
     return (b.x() - a.x())*(c.y() - a.y()) - (b.y() - a.y())*(c.x() - a.x());
 }


/*!
 \brief

 \return int
*/
inline int Triangle::clockwise()
{
    return (TriArea(A, B, C) > 0);
}

/*!
 \brief

 \param d
 \return int
*/
inline int Triangle::InCircle(const QPointF2D& d)
{
    return (   (A.x()*A.x() + A.y()*A.y()) * TriArea(B, C, d)
             - (B.x()*B.x() + B.y()*B.y()) * TriArea(A, C, d)
             + (C.x()*C.x() + C.y()*C.y()) * TriArea(A, B, d)
             - (d.x()*d.x() + d.y()*d.y()) * TriArea(A, B, C) > 0
            );
}


/*!
 \brief

 \return QPointF2D
*/
inline  QPointF2D Triangle::circumCircleCenter()
{
     double D = (A.x() - C.x()) * (B.y() - C.y()) - (B.x() - C.x()) * (A.y() - C.y());

   double p_0 = (((A.x() - C.x()) * (A.x() + C.x()) + (A.y() - C.y()) * (A.y() + C.y())) / 2 * (B.y() - C.y())
        -  ((B.x() - C.x()) * (B.x() + C.x()) + (B.y() - C.y()) * (B.y() + C.y())) / 2 * (A.y() - C.y()))
        / D;

   double p_1 = (((B.x() - C.x()) * (B.x() + C.x()) + (B.y() - C.y()) * (B.y() + C.y())) / 2 * (A.x() - C.x())
        -  ((A.x() - C.x()) * (A.x() + C.x()) + (A.y() - C.y()) * (A.y() + C.y())) / 2 * (B.x() - C.x()))
        / D;

   return(QPointF2D(p_0,p_1));
}


#endif // QPOINTF2D_H
