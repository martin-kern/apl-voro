//**************************************************************
//
//           Copyright (c) 2012 - 2013 by Gunther Lukat
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************



#ifndef EDGE_H
#define EDGE_H
#include "qpointf2d.h"
#include <QVector>
#include <QPointF>
#include <QtGui/QPolygonF>
#include <QDebug>
#include "geom/geomalgorithm.h"


class QuadEdge;


/* ----------------------------------------------------------------------------
 * Edge
 * ------------------------------------------------------------------------- */

/**
 * @brief A directed edge from one vertex to another, adjacent to two faces.
 * Based on Dani Lischinski's code from Graphics Gems IV.
 * Original quad-edge data structure due to Guibas and Stolfi (1985).
 *
 * ID     the ID number assigned to the edge;
 *        positive
 * data   generic data attached to the edge by the client
 * Org    the vertex of origin for the edge;
 *        null if currently unknown
 * Dest   the destination vertex for the edge;
 *        null if currently unknown
 * Left   the left face of the edge;
 *        null if currently unknown
 * Right  the right face of the edge;
 *        null if currently unknown
 */

class Edge
{

  /* -- public class methods ----------------------------------------------- */

  public:



  /*!
   \brief Return a new, unconnected edge.

   \return Edge
  */
  static Edge *make();

  /*!
   \brief  * Add a new edge e connecting the destination of a to the origin of b,
   * in such a way that all three have the same  left face after the connection is complete.
   * Additionally, the data pointers of the new edge are set.

   \param a
   \param b
   \return Edge
  */
  static Edge *connect(Edge *a, Edge *b);

  /*!
   \brief Release the storage occupied by a given edge.
   * edge -> the edge to kill;
   *         must be nonnull

   \param edge
  */
  static void kill(Edge *edge);

  /*
   * Splice a given pair of edges.
   * a, b -> the edges to splice;
   *         must be nonnull
   *
   * This operator affects the two edge rings around the origins of a and b,
   * and, independently, the two edge rings around the left faces of a and b.
   * In each case, (i) if the two rings are distinct, Splice will combine
   * them into one; (ii) if the two are the same ring, Splice will break it
   * into two separate pieces.
   * Thus, Splice can be used both to attach the two edges together, and
   * to break them apart. See Guibas and Stolfi (1985) p.96 for more details
   * and illustrations.
   */
  /**
   * @brief
   *
   * @param a
   * @param b
   */
  /*!
   \brief

   \param a
   \param b
  */
  static void splice(Edge *a, Edge *b);


  public:


  /*!
   \brief

   \return unsigned int
  */
  unsigned int getID();


  /*!
   \brief

   \param num
  */
  void setID(unsigned int num);


  /*!
   \brief

   \return QPointF2D
  */
  QPointF2D *Org();


  /*!
   \brief

   \return QPointF2D
  */
  QPointF2D *Dest();


  /*!
   \brief

   \param org
  */
  void setOrg(QPointF2D *org);


  /*!
   \brief

   \param dest
  */
  void setDest(QPointF2D *dest);


  /*!
   \brief

   \return Triangle
  */
  Triangle Left();


 /*!
  \brief

  \return Triangle
 */
 Triangle Left_nonV();



    /*!
     \brief

     \param poly
    */
    void VCell_CW(QPolygonF* poly);

  /*!
   \brief

   \return Triangle
  */
  Triangle Right();




  /*!
   \brief

   \return Edge
  */
  Edge *Rot();


  /*!
   \brief

   \return Edge
  */
  Edge *InvRot();


  /*!
   \brief

   \return Edge
  */
  Edge *Sym();


  /*!
   \brief

   \return Edge
  */
  Edge *Onext();


  /*!
   \brief

   \return Edge
  */
  Edge *Oprev();


  /*!
   \brief

   \return Edge
  */
  Edge *Dnext();


  /*!
   \brief

   \return Edge
  */
  Edge* Dprev();


  /*!
   \brief

   \return Edge
  */
  Edge* Lnext();

  /*!
   \brief

   \return Edge
  */
  Edge* Lprev();


  /*!
   \brief

   \return Edge
  */
  Edge* Rnext();

  /*!
   \brief

   \return Edge
  */
  Edge* Rprev();


 /*!
  \brief

  \param orig
  \param dest
 */
 void EndPoints(QPointF2D* orig, QPointF2D* dest);


 /*!
  \brief

  \return const QPointF2D
 */
 const QPointF2D& Org2d() const;


 /*!
  \brief

  \return const QPointF2D
 */
 const QPointF2D& Dest2d() const;

  protected:

/*!
 \brief

*/
  Edge();


  /*!
   \brief

  */
  ~Edge();



  private:



  Edge *next;  /*!< TODO */


  unsigned int num;  /*!< TODO */

public :
  QPointF2D *data;  /*!< TODO */



  friend class QuadEdge;


  /*!
   \brief

   \return QuadEdge
  */
  QuadEdge* Qedge()
  {
      return (QuadEdge *)(this - num);
  }


};


/*!
 \brief

 \return unsigned int
*/
inline unsigned int Edge::getID()
{
  return num;
}


/*!
 \brief

 \return Edge
*/
inline Edge* Edge::Rot()
{
  return (num<3) ? this+1 : this-3;
}


/*!
 \brief

 \return Edge
*/
inline Edge* Edge::InvRot()
{
  return num>0 ? this-1 : this+3;
}


/*!
 \brief

 \return Edge
*/
inline Edge* Edge::Sym()
{
  return num<2 ? this+2 : this-2;
}


/*!
 \brief

 \return Edge
*/
inline Edge* Edge::Onext()
{
  return next;
}

/*!
 \brief

 \return Edge
*/
inline Edge* Edge::Oprev()
{
  return Rot()->Onext()->Rot();
}

/*!
 \brief

 \return Edge
*/
inline Edge* Edge::Dnext()
{
  return Sym()->Onext()->Sym();
}

/*!
 \brief

 \return Edge
*/
inline Edge* Edge::Dprev()
{
  return InvRot()->Onext()->InvRot();
}

/*!
 \brief

 \return Edge
*/
inline Edge* Edge::Lnext()
{
  return InvRot()->Onext()->Rot();
}

/*!
 \brief

 \return Edge
*/
inline Edge* Edge::Lprev()
{
  return Onext()->Sym();
}


/*!
 \brief

 \return Edge
*/
inline Edge* Edge::Rnext()
{
  return Rot()->Onext()->InvRot();
}

/*!
 \brief

 \return Edge
*/
inline Edge* Edge::Rprev()
{
  return Sym()->Onext();
}

/*!
 \brief

 \return QPointF2D
*/
inline QPointF2D *Edge::Org()
{
  return data;
}

/*!
 \brief

 \return QPointF2D
*/
inline QPointF2D* Edge::Dest()
{
  return Sym()->data;
}

/*!
 \brief

 \return Triangle
*/
inline Triangle Edge::Left()
{
    QPointF2D A = Org2d();
    QPointF2D B = Dest2d();
    QPointF2D C = next->Dest2d();
    Triangle t(A,B,C);
    return t;


}


/*!
 \brief

 \return Triangle
*/
inline Triangle Edge::Left_nonV()
{
    QPointF2D A = Org2d();
    Edge *e = Onext();
    QPointF2D B = Dest2d();
    QPointF2D C = Onext()->Dest2d();

    while(B.virt)
    {   e = e->Onext();
        B = e->Dest2d();
    }
    while(C.virt)
     {
        e = e->Onext();
        C = e->Dest2d();
     }
    Triangle t(A,B,C);
    return t;
}



/*!
 \brief

 \param poly
*/
inline void Edge::VCell_CW(QPolygonF* poly)
{

    QPointF2D st= Org2d();
    QPointF2D st2;
    Edge *e = Dnext();
    QPointF one = Dnext()->Right().circumCircleCenter().toPointF();
    while(1)
    {
        if(st2 != st)
        {
            st2 = e->Dnext()->Org2d();
           // Triangle t = e->Onext()->Left();
            poly->push_back(e->Dnext()->Right().circumCircleCenter().toPointF());
            e = e->Dnext();
        }else
        {   poly->push_back(one);
            return;
        }
    }
  return;

}


/*!
 \brief

 \return Triangle
*/
inline Triangle Edge::Right()
{
    QPointF2D A = Org2d();
    QPointF2D B = Dest2d();
    QPointF2D C = Oprev()->Dest2d();
    Triangle t(A,B,C);
    return t;
}


/*!
 \brief

 \param orig
 \param dest
*/
inline void Edge::EndPoints(QPointF2D* orig, QPointF2D* dest)
{
    data = orig;
    Sym()->data = dest;
}

/*!
 \brief

 \return const QPointF2D
*/
inline const QPointF2D& Edge::Org2d() const
{
    return *data;
}

/*!
 \brief

 \return const QPointF2D
*/
inline const QPointF2D& Edge::Dest2d() const
{
return (num < 2) ? *((this + 2)->data) : *((this - 2)->data);
}



#endif /* #ifndef edgeINCLUDED */
