//**************************************************************
//
//           Copyright (c) 2012 - 2013 by Gunther Lukat
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************


#ifndef GEOMALGORITHM_H
#define GEOMALGORITHM_H
#include "qpointf2d.h"
#include <QVector>
#include <QtGui/QPolygonF>
#include <QtCore/qmath.h>


/*!
 \brief

 \param v
 \return double
*/
inline double polygonArea(QVector<QPointF> v)
{
    int i,j,N;
    N = v.size();
       double area = 0;

       for (i=0;i<N;i++) {
          j = (i + 1) % N;
          area += v[i].x() * v[j].y();
          area -= v[i].y() * v[j].x();
       }

       area /= 2;
       return(area < 0 ? -area : area);
}

/*!
 \brief

 \param v
 \return double
*/
inline double polygonArea(QPolygonF v)
{
    int i,j,N;
    N = v.size();
       double area = 0;

       for (i=0;i<N;i++) {
          j = (i + 1) % N;
          area += v[i].x() * v[j].y();
          area -= v[i].y() * v[j].x();
       }

       area /= 2;
       return(area < 0 ? -area : area);
}


/*!
 \brief Returns twice the area of the oriented triangle (a, b, c), i.e., the area is positive if the triangle is oriented counterclockwise.

 \param a
 \param b
 \param c
 \return double
*/
inline double TriArea(const QPointF& a, const QPointF& b, const QPointF& c)
{
     return (b.x() - a.x())*(c.y() - a.y()) - (b.y() - a.y())*(c.x() - a.x());
}



/*!
 \brief Returns TRUE if the point d is inside the circle defined by the points a, b, c. See Guibas and Stolfi (1985) p.107

 \param a
 \param b
 \param c
 \param d
 \return int
*/
inline int InCircle(const QPointF& a, const QPointF& b, const QPointF& c, const QPointF& d)
{


    return (   (a.x()*a.x() + a.y()*a.y()) * TriArea(b, c, d)
             - (b.x()*b.x() + b.y()*b.y()) * TriArea(a, c, d)
             + (c.x()*c.x() + c.y()*c.y()) * TriArea(a, b, d)
             - (d.x()*d.x() + d.y()*d.y()) * TriArea(a, b, c) > 0
            );
}



/*!
 \brief  Returns TRUE if the points a, b, c are in a counterclockwise order

 \param a
 \param b
 \param c
 \return int
*/
inline int ccw(const QPointF& a, const QPointF& b, const QPointF& c)
{
    return (TriArea(a, b, c) > 0);
}



/*!
 \brief

 \param p1
 \param p2
 \param p3
 \return float
*/
inline float cross(QPointF p1, QPointF p2, QPointF p3)
{
    return (p1.x() - p3.x()) * (p2.y() - p3.y()) - (p2.x() - p3.x()) * (p1.y() - p3.y());
}


/*!
 \brief

 \param pt
 \param v1
 \param v2
 \param v3
 \return bool
*/
inline bool IsPointInTriangle(QPointF pt, QPointF v1, QPointF v2, QPointF v3)
{
  bool b1, b2, b3;

  b1 = cross(pt, v1, v2) < 0.0f;
  b2 = cross(pt, v2, v3) < 0.0f;
  b3 = cross(pt, v3, v1) < 0.0f;

  return ((b1 == b2) && (b2 == b3));
}




/*!
 \brief

 \param a
 \param b
 \param c
 \return double
*/
inline double TriArea(const QPointF2D& a, const QPointF2D& b, const QPointF2D& c)
{
     return (b.x() - a.x())*(c.y() - a.y()) - (b.y() - a.y())*(c.x() - a.x());
}



/*!
 \brief

 \param a
 \param b
 \param c
 \param d
 \return int
*/
inline int InCircle(const QPointF2D& a, const QPointF2D& b, const QPointF2D& c, const QPointF2D& d)
{


    return (   (a.x()*a.x() + a.y()*a.y()) * TriArea(b, c, d)
             - (b.x()*b.x() + b.y()*b.y()) * TriArea(a, c, d)
             + (c.x()*c.x() + c.y()*c.y()) * TriArea(a, b, d)
             - (d.x()*d.x() + d.y()*d.y()) * TriArea(a, b, c) > 0
            );
}


/*!
 \brief

 \param a
 \param b
 \param c
 \return int
*/
inline int ccw(const QPointF2D& a, const QPointF2D& b, const QPointF2D& c)
{
    return (TriArea(a, b, c) > 0);
}



/*!
 \brief

 \param p1
 \param p2
 \param p3
 \return double
*/
inline double cross(QPointF2D p1, QPointF2D p2, QPointF2D p3)
{
    return (p1.x() - p3.x()) * (p2.y() - p3.y()) - (p2.x() - p3.x()) * (p1.y() - p3.y());
}







/*!
 \brief

 \param pt
 \param t
 \return bool
*/
inline bool IsPointInTriangle(QPointF2D pt, Triangle t)
{
  bool b1, b2, b3;
  if(t.A == pt || t.B == pt || t.C == pt)
      return true;

  b1 = cross(pt, t.A, t.B) < 0.0f;
  b2 = cross(pt, t.B, t.C) < 0.0f;
  b3 = cross(pt, t.C, t.A) < 0.0f;

  return ((b1 == b2) && (b2 == b3));
}


/*!
 \brief

 \param pt
 \param v1
 \param v2
 \param v3
 \return bool
*/
inline bool IsPointInTriangle(QPointF2D pt, QPointF2D v1, QPointF2D v2, QPointF2D v3)
{
  bool b1, b2, b3;

  b1 = cross(pt, v1, v2) < 0.0f;
  b2 = cross(pt, v2, v3) < 0.0f;
  b3 = cross(pt, v3, v1) < 0.0f;

  return ((b1 == b2) && (b2 == b3));
}


/*!
 \brief

 \param x0
 \param y0
 \param x1
 \param y1
 \param fp1
 \param x2
 \param y2
 \param fp2
 \param x3
 \param y3
 \param fp3
 \return double
*/
inline double interpTriangle(double x0,double y0,double x1, double y1, double fp1, double x2, double y2, double fp2, double x3, double y3, double fp3 )
{
   double b0 =  1/((x2 - x1) * (y3 - y1) - (x3 - x1) * (y2 - y1));
   double b1 = ((x2 - x0) * (y3 - y0) - (x3 - x0) * (y2 - y0)) * b0;
   double b2 = ((x3 - x0) * (y1 - y0) - (x1 - x0) * (y3 - y0)) * b0;
   double b3 = ((x1 - x0) * (y2 - y0) - (x2 - x0) * (y1 - y0)) * b0;
   if(!(b1 <0 || b2 < 0 || b3 <0))
   {
       return ((b1*fp1) +(b2*fp2) + (b3*fp3));
   }
   else return 0.0;
}


/*!
 \brief

 \param x0
 \param y0
 \param x1
 \param y1
 \return double
*/
inline double distanceEuclidean(double x0, double y0, double x1, double y1)
{
   return sqrt(pow(x0-x1,2)+pow(y0-y1,2));
}

/*!
 \brief

 \param a
 \param b
 \return double
*/
inline double distanceEuclidean(QPointF a, QPointF b)
{
   return sqrt(pow(a.x()-b.x(),2)+pow(a.y()-b.y(),2));
}

/*!
 \brief

 \param a
 \param b
 \return double
*/
inline double distanceEuclidean(QPointF2D a, QPointF2D b)
{
   return sqrt(pow(a.x()-b.x(),2)+pow(a.y()-b.y(),2));
}



/*!
 \brief

 \param x0
 \param y0
 \param z0
 \param x1
 \param y1
 \param z1
 \return double
*/
inline double distanceEuclidean3D(double x0, double y0, double z0, double x1,double y1, double z1)
{
   return sqrt(pow(x0-x1,2)+pow(y0-y1,2)+pow(z0-z1,2));
}


/*!
 \brief

 \param pt1
 \param pt2
 \return double
*/
inline double dotproduct(QPointF pt1, QPointF pt2)
{
     return (pt2.x()-pt1.x())/qSqrt((pt2.x()-pt1.x())*(pt2.x()-pt1.x()) + (pt2.y()-pt1.y())*(pt2.y()-pt1.y()));
}


/*!
 \brief

 \param m_points
 \param qhullpoints
*/
inline void ConvexHull(QList<QPointF>m_points ,QPolygonF &qhullpoints)
{
       QVector<QPointF> points; // My number of points
       points.resize(m_points.size()+1);
       QPointF pt_temp(m_points[0]);
        qreal angle_temp= 0;
        int k_temp = 0;
        //fill points and sort
        for (int i = 1; i < m_points.size(); ++i)
            {
                if (m_points[i].y() < pt_temp.y())
                {
                    points[i+1] = pt_temp;
                    pt_temp = m_points[i];
                }
                else
                {
                    points[i+1] = m_points[i];
                }
            }
            points[1] = pt_temp;

            for (int i = 2; i <= m_points.size(); ++i)
                {
                    pt_temp = points[i];
                    angle_temp = dotproduct(points[1], pt_temp);
                    k_temp = i;
                    for (int j= 1; j <= m_points.size()-i; ++j)
                    {
                        if (dotproduct(points[1], points[i+j]) > angle_temp)
                        {
                            pt_temp = points[i+j];
                            angle_temp = dotproduct(points[1], points[i+j]);
                            k_temp = i+j;
                        }
                    }
                    points[k_temp] = points[i];
                    points[i] = pt_temp;
                }
                points[0] = points[m_points.size()];
               int m_M = 1; // Number of points on the convex hull.

                    for (int i(2); i <= m_points.size(); ++i)
                    {
                        while (ccw(points[m_M-1], points[m_M], points[i]) <= 0)
                        {
                            if (m_M > 1)
                            {
                                m_M -= 1;
                            }
                            else if (i == m_points.size())
                            {
                                break;
                            }
                            else
                            {
                                i += 1;
                            }
                        }
                        m_M += 1;
                        pt_temp = points[m_M];
                        points[m_M] = points[i];
                        points[i] = points[m_M];
                    }
                    for (int i(0); i < m_M; ++i)
                       {
                           qhullpoints.push_back(points[i+1]);
                       }
                       qhullpoints.push_back(points[1]);

}
#endif // GEOMALGORITHM_H
