//**************************************************************
//
//           Copyright (c) 2012 - 2013 by Gunther Lukat
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************


#include "triangulation.h"
#include "qpointf2d.h"
#include <QLineF>

Triangulation::Triangulation(const QPointF2D& a, const QPointF2D& b, const QPointF2D& c) // Initialize a subdivision to the triangle defined by the points a, b, c.
{
QPointF2D *da, *db, *dc;
da = new QPointF2D(a);
db = new QPointF2D(b);
dc = new QPointF2D(c);
Edge* ea = Edge::make();


ea->EndPoints(da, db);
Edge* eb = Edge::make();
Edge::splice(ea->Sym(), eb);
eb->EndPoints(db, dc);
Edge* ec = Edge::make();
Edge::splice(eb->Sym(), ec);
ec->EndPoints(dc, da);
Edge::splice(ec->Sym(), ea);
startingEdge = ea;
}





// Add a new edge e connecting the destination of a to the // origin of b, in such a way that all three have the same // left face after the connection is complete.
// Additionally, the data pointers of the new edge are set.
Edge* Triangulation::Connect(Edge* a, Edge* b)
{
Edge* e = Edge::make();
Edge::splice(e, a->Lnext());
Edge::splice(e->Sym(), b);
e->EndPoints(a->Dest(), b->Org());
return e;
}

// Essentially turns edge e counterclockwise inside its enclosing // quadrilateral. The data pointers are modified accordingly.
void Triangulation::Swap(Edge* e)
{
Edge* a = e->Oprev();
Edge* b = e->Sym()->Oprev();
Edge::splice(e, a);
Edge::splice(e->Sym(), b);
Edge::splice(e, a->Lnext());
Edge::splice(e->Sym(), b->Lnext());
e->EndPoints(a->Dest(), b->Dest());
}

// Returns twice the area of the oriented triangle (a, b, c), i.e., the // area is positive if the triangle is oriented counterclockwise.
/*
inline double Triangulation::TriArea(const QPointF2D& a, const QPointF2D& b, const QPointF2D& c)
{
    return (b.x() - a.x())*(c.y() - a.y()) - (b.y() - a.y())*(c.x() - a.x());
}




// Returns TRUE if the point d is inside the circle defined by the // points a, b, c. See Guibas and Stolfi (1985) p.107.
int Triangulation::InCircle(const QPointF2D& a, const QPointF2D& b, const QPointF2D& c, const QPointF2D& d)
{

    return (   (a.x()*a.x() + a.y()*a.y()) * TriArea(b, c, d)
             - (b.x()*b.x() + b.y()*b.y()) * TriArea(a, c, d)
             + (c.x()*c.x() + c.y()*c.y()) * TriArea(a, b, d)
             - (d.x()*d.x() + d.y()*d.y()) * TriArea(a, b, c) > 0
            );
}

// Returns TRUE if the points a, b, c are in a counterclockwise order
int Triangulation::ccw(const QPointF2D& a, const QPointF2D& b, const QPointF2D& c)
{
return (TriArea(a, b, c) > 0);
}
*/
int Triangulation::RightOf(const QPointF2D& x, Edge* e)
{
return ccw(x, e->Dest2d(), e->Org2d());
}


int Triangulation::LeftOf(const QPointF2D& x, Edge* e)
{
return ccw(x, e->Org2d(), e->Dest2d());
}


Triangle Triangulation::TriangleContainsPoint(const QPointF2D& x)
{
    Triangle t;
    Edge *e = Locate(x);
    if(e->Org2d()!=x)
        e->Sym();
    t = e->Left();
    if(IsPointInTriangle(x,t))
        return e->Left();
    else
        return e->Right();
}

QVector<QPointF2D>& Triangulation::getTriangleCCW_nonVirtual(const QPointF2D& x, QVector<QPointF2D> &points)
{

   Edge *e = Locate(x);
   if(e->Dest2d() == x)
       e = e->Sym();
   QPointF2D pp = e->Dest2d();
   while(pp.virt)
   {
       e = e->Onext();
       pp=e->Dest2d();
   }
   points.push_back(pp);
   e = e->Onext();
   while(e->Dest2d() != pp)
   {
       if(!e->Dest2d().virt)
           points.push_back(e->Dest2d());

       e=e->Onext();
   }
   return points;

}



Triangle Triangulation::getTriangleCCW(const QPointF2D& x)
{
   Edge *e = Locate(x);

   Triangle t = e->Left();
   return t;
}


Triangle Triangulation::getTriangleCW(const QPointF2D& x)
{
    Edge *e = Locate(x);
    if(e->Org2d()!=x)
        e->Sym();
    Triangle t = e->Right();
    return t;
}



QList<QPointF2D>& Triangulation::getNBLIST(const QPointF2D& x, QList<QPointF2D> *poly)
{
    Edge *e = Locate(x);
    if(e->Org2d()!=x)
      e =  e->Sym();

    QPointF2D st2 = e->Dest2d();
    poly->push_back(st2);



    while(1)
    {
        QPointF2D p = e->Onext()->Dest2d();
        if(p != st2)
        {
           // st2 = e->Onext()->Org2d();

            poly->push_back(p);
            e = e->Onext();
        }else
        {   //poly->push_back(one);
            return *poly;
        }
    }


}





QPolygonF& Triangulation::getCCW_VCellFor(const QPointF2D& x, QPolygonF* poly)
{
    Edge *e = Locate(x);
    if(e->Org2d()!=x)
      e =  e->Sym();

    QPointF2D st2 = e->Dest2d();

    Triangle t = e->Left();
    QPointF one =t.circumCircleCenter().toPointF();
    poly->push_back(one);

    while(1)
    {
        QPointF2D p = e->Onext()->Dest2d();
        if(p != st2)
        {
           // st2 = e->Onext()->Org2d();
             t = e->Onext()->Left();
            QPointF mypoint = t.circumCircleCenter().toPointF();
            poly->push_back(mypoint);
            e = e->Onext();
        }else
        {   poly->push_back(one);
            return *poly;
        }
    }


}


QPolygonF&  Triangulation::getCW_VCellFor(const QPointF2D& x, QPolygonF* poly)
{
    Edge *e = Locate(x);
    if(e->Org2d()!=x)
      e =  e->Sym();
    QPointF2D st2 = e->Dest2d();

    Triangle t = e->Right();
    QPointF one =t.circumCircleCenter().toPointF();
    poly->push_back(one);

    while(1)
    {
        QPointF2D p = e->Oprev()->Dest2d();
        if(p != st2)
        {
           // st2 = e->Onext()->Org2d();
             t = e->Dnext()->Right();
            QPointF mypoint = t.circumCircleCenter().toPointF();
            poly->push_back(mypoint);
            e = e->Dnext();
        }else
        {   poly->push_back(one);
            return *poly;
        }
    }


}





   // A predicate that determines if the point x is on the edge e. // The point is considered on if it is in the EPS-neighborhood // of the edge.
int Triangulation::OnEdge(const QPointF2D& x, Edge* e)
   {
       double t1,t2,t3;
       t1 = (x - e->Org2d()).norm();
       t2 = (x - e->Dest2d()).norm();

       if (t1 < EPS || t2 < EPS)
       return TRUE;

       t3 = (e->Org2d() - e->Dest2d()).norm();

       if (t1 > t3 || t2 > t3)
       return FALSE;

       Line line(e->Org2d(), e->Dest2d());
       return (fabs(line.eval(x)) < EPS);

 }



// Returns an edge e, s.t. either x is on e, or e is an edge of // a triangle containing x. The search starts from startingEdge // and proceeds in the general direction of x. Based on the
// pseudocode in Guibas and Stolfi (1985) p.121.
Edge* Triangulation::Locate(const QPointF2D& x)
{
    Edge* e = startingEdge;
    while (TRUE)
    {
        if (x == e->Org2d()|| x == e->Dest2d())
            return e;
        else if (RightOf(x, e))
            e = e->Sym();
        else if (!RightOf(x, e->Onext()))
            e = e->Onext();
        else if (!RightOf(x, e->Dprev()))
            e = e->Dprev();
        else
            return e;
    }
}


// Inserts a new point into a subdivision representing a Delaunay // triangulation, and fixes the affected edges so that the result // is still a Delaunay triangulation. This is based on the
// pseudocode from Guibas and Stolfi (1985) p.120, with slight
// modifications and a bug fix.
void Triangulation::InsertSite(const QPointF2D& x, QVector<QPair<int, int> > &doubles)
{
   Edge* e = Locate(x);

   if ((x == e->Org2d()) || (x == e->Dest2d())) //point already in
   {
       if(e->Org2d() == x)
          doubles.push_back(qMakePair(x.number,e->Org2d().number));
       if(e->Dest2d() == x)
          doubles.push_back(qMakePair(x.number,e->Dest2d().number));
    return;
   }
    else if (OnEdge(x, e))
         {
          e = e->Oprev();
          Edge::kill(e->Onext());
            }
// Connect the new point to
// triangle (or quadrilateral, if the new point fell on an // existing edge.)

Edge* base = Edge::make();
base->EndPoints(e->Org(), new QPointF2D(x));
Edge::splice(base, e);
startingEdge = base;
do {
     base = Connect(e, base->Sym());
     e = base->Oprev();
   }
   while (e->Lnext() != startingEdge);
// Examine suspect edges to ensure that the Delaunay condition // is satisfied.
    do {
       Edge* t = e->Oprev();
      if (RightOf(t->Dest2d(), e) && InCircle(e->Org2d(), t->Dest2d(), e->Dest2d(), x))
        {
            Swap(e);
            e = e->Oprev();
        }
        else if (e->Onext() == startingEdge) // no more suspect edges
        return;
       else  // pop a suspect edge
        e = e->Onext()->Lprev();
      }while (TRUE);
}
/*****************************************************************************/

