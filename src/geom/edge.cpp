//**************************************************************
//
//           Copyright (c) 2012 - 2013 by Gunther Lukat
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************

/* ============================================================================
 * p2/cell/edge.cc
 * ========================================================================= */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <QDataStream>
#include "edge.h"


/* ----------------------------------------------------------------------------
 * QuadEdge
 * ------------------------------------------------------------------------- */

class QuadEdge
{

  /* -- public instance methods -------------------------------------------- */


public:


  /*
   * Initialize the edges of this quad edge with no connections.
   */
  QuadEdge()
  {
    edges[0].num = 0;
    edges[1].num = 1;
    edges[2].num = 2;
    edges[3].num = 3;

    edges[0].next = edges+0;
    edges[1].next = edges+3;
    edges[2].next = edges+2;
    edges[3].next = edges+1;


  }

  /* -- public instance variables ------------------------------------------ */

  public:

  /*
   * The edges of this quad edge.
   */
  Edge edges[4];

};

/* ----------------------------------------------------------------------------
 * Edge
 * ------------------------------------------------------------------------- */

/* -- public class methods ------------------------------------------------- */

Edge *Edge::make()
{
  return (new QuadEdge())->edges;
}

Edge *Edge::connect(Edge *a, Edge *b)
{
    Edge* e = make();
    splice(e, a->Lnext());
    splice(e->Sym(), b);
    e->EndPoints(a->Dest(), b->Org());
    return e;
}

void Edge::kill(Edge *edge)
{
 splice(edge, edge->Oprev());
 splice(edge->Sym(), edge->Sym()->Oprev());
 delete edge->Qedge();
}

void Edge::splice(Edge *a, Edge *b)
{
  assert(a!=0);
  assert(b!=0);

  // see Guibas and Stolfi

  Edge* alpha = a->Onext()->Rot();
  Edge* beta  = b->Onext()->Rot();

  Edge* t1 = b->Onext();
  Edge* t2 = a->Onext();
  Edge* t3 = beta->Onext();
  Edge* t4 = alpha->Onext();

  a->next     = t1;
  b->next     = t2;
  alpha->next = t3;
  beta->next  = t4;
}

/* -- public instance methods ---------------------------------------------- */

void Edge::setID(unsigned int num)
{
  assert(num>0);

  this->num = num;
}

void Edge::setOrg(QPointF2D *org)
{
  // add this edge to the (vertex) orbit of _org_

  data = org;

//  org->addEdge(this);
}

void Edge::setDest(QPointF2D *dest)
{
  // add this edge to the (vertex) orbit of _dest_

  Sym()->data = dest;


}

/* -- protected instance methods ------------------------------------------- */

Edge::Edge()
{
  // _index_ is initialized by QuadEdge
  // _next_ is initialized by QuadEdge
  // _id_ is initialized by QuadEdge

  data   = 0;
  //vertex = 0;
  //face   = 0;
}

Edge::~Edge()
{
}




