#ifndef ATOM_META_HPP
#define ATOM_META_HPP
#include <QString>

/**
 * @brief The atom_meta struct
 * All atom data that does not change between simulation frames.
 */

struct atom_meta
{
    int atom_number;
    int molnumber;
    bool prot;
    float vdw;
    QString name;
    QString resname;
};


#endif // ATOM_META_HPP
