#ifndef SETUP_PARAMETERS_HPP
#define SETUP_PARAMETERS_HPP
#include <QString>
#include <QStringList>


struct setup_parameters
{
    QString name;
    QString pdb, trj, index;
    int pbc_cutoff;
    int modus;
    unsigned int p_degree;  // polynomial degree used for surface fit
    int leaflet_detection;  // 0: orientation-based, 1: position-based
    int first_frame;
    int stride;
    int last_frame;
    int vdw_check;
    double upright;
    double x_box, y_box, z_box;
    QStringList protein_names, key_atoms;
};

#endif // SETUP_PARAMETERS_HPP
