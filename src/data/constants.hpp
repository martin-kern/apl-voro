#ifndef CONSTANTS_HPP
#define CONSTANTS_HPP

/* This file contains some constants that are used throughout the
 * application.
 */

#define APP_NAME "APL@Voro"  // Name of the application
#define APP_VER  "3.3.3"  // Current version

// VDW radii for a variety of atoms
#define VDW_H 1.1
#define VDW_B 1.92
#define VDW_C 1.7
#define VDW_N 1.55
#define VDW_O 1.52
#define VDW_F 1.47
#define VDW_S 1.8
#define VDW_P 1.8
#define VDW_I 1.98
#define VDW_NA 2.27
#define VDW_CA 2.31


// Page IDs of the project setup wizard.
enum {project_setup, protein_selection, lipid_selection};

#endif // CONSTANTS_HPP
