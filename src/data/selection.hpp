#ifndef SELECTION_HPP
#define SELECTION_HPP
#include <QString>
#include <QList>

struct selection
{
    QString leaflet;
    unsigned int mem_id, view_id;
    QList<int> selection_indices;
};

#endif // SELECTION_HPP
