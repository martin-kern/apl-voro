//**************************************************************
//                  Core.cpp
//           Copyright (c) 2012 - 2013 by Gunther Lukat
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************

#include "coredata/core.h"
#include "coredata/atom.h"
#include "coredata/membran.h"
#include "io/pdb/pdbreader.h"
#include "io/xdr/xdrreader.h"
#include <sstream>
#include <set>
#include <QFile>
#include<QProgressBar>


core::core(setup_parameters setup_params)
{
    _setup_params = setup_params;
    leaflet_names.push_back("upside");
    leaflet_names.push_back("downside");

    framemembrans.clear();
    ALLupsideAnumbers.reserve(1);
    // VDW radii in angstrom
    vdwradii.insert("H",1.1);
    vdwradii.insert("B",1.92);
    vdwradii.insert("C",1.70);
    vdwradii.insert("N",1.55);
    vdwradii.insert("O",1.52);
    vdwradii.insert("F",1.47);
    vdwradii.insert("S",1.80);
    vdwradii.insert("P",1.80);
    vdwradii.insert("I",1.98);
    vdwradii.insert("Na",2.27);
    vdwradii.insert("Ca",2.31);
    sync_frames[0] = 0;
    sync_frames[1] = 0;

    if (!setup_params.index.isEmpty())
    {
        QFile index_file;
        index_file.setFileName(setup_params.index);
        index_parser *ndx_parser = new index_parser(index_file);
        for (int i = 0; i < setup_params.protein_names.length(); i++)
            ndx_parser->get_content_section(&peptides, setup_params.protein_names[i]);
        delete  ndx_parser;
        ndx_parser = nullptr;
    }

    // Convert box dimensions from nm to angstrom
    double x_box = setup_params.x_box * 10;
    double y_box = setup_params.y_box * 10;
    double z_box = setup_params.z_box * 10;

    criterialist = new QHash<QString, Critall>();

    std::vector<Atom> atoms;
    for (int i = 0; i < setup_params.key_atoms.length(); i++)
    {
        QString residue_name = setup_params.key_atoms[i].split(" ")[0];
        QString atom_name = setup_params.key_atoms[i].split(" ")[1];

        Atom key_atom;

        key_atom.setResname(residue_name);
        key_atom.setName(atom_name);
        atoms.push_back(key_atom);
    }

    makeWorkingSetTrr(&atoms, x_box, y_box, z_box);

    int size = framemembrans.size();
    sync_frames[1] = unsigned(size - 1);

    #pragma omp parallel for
    for(int i = 0; i < size; i++)
    {
        framemembrans[i].makeVoronoi(setup_params.pbc_cutoff,
                                     _setup_params.modus,
                                     setup_params.vdw_check,
                                     setup_params.upright,
                                     setup_params.upright);
    }

    QSet<QString> stringset, stringlip;
    for (int i = 0; i < leaflet_names.length(); i++)
    {
         for (QHash<int, Atom> ::iterator it =
              framemembrans[0].leaflets[leaflet_names[i]]->QHside.begin();
              it!=  framemembrans[0].leaflets[leaflet_names[i]]->QHside.end();
              it++)
         {
             QString newname((it.value().getResname()));
             stringset.insert(newname);
             if(!it.value().isProt())
                 stringlip.insert(newname);
         }
         residues[leaflet_names[i]] = stringset.values();
         lipids[leaflet_names[i]] = stringlip.values();
         stringset.clear();
         stringlip.clear();
    }
    peptidenames = protein.keys();

    core::set_leaflet_globals("upside");
    core::set_leaflet_globals("downside");
    // Remove duplicates?
    QSet<int> setup = QSet<int>::fromList(framemembrans[0].leaflets["upside"]->QHside.keys());
    QSet<int> setdown = QSet<int>::fromList(framemembrans[0].leaflets["downside"]->QHside.keys());
    ALLupsideAnumbers = setup.toList();
    ALLdownsideAnumbers = setdown.toList();
}


core::~core()
{
    framemembrans.clear();
    for (int i = 0; i < leaflet_names.length(); i++)
        residues[leaflet_names[i]].clear();
}





void core::makeWorkingSetTrr(std::vector<Atom> *atoms,
                             double _maxX,
                             double _maxY,
                             double _maxZ)
{
    std::vector<Molecule> molecules;
    Pdbreader *reader = new Pdbreader();
    reader->setPath(_setup_params.pdb);
    reader->getResidues(*atoms, molecules, vdwradii);
    Membran *mem = new Membran();


    if(peptides.size()>0)
    {
        QHash<QString, QList<int> > :: iterator it;
        for(it=peptides.begin(); it != peptides.end(); it++)
        {
            QHash<int,Atom>atomlist;
            reader->getIndexedProtein(it.key(), it.value(), atomlist, vdwradii);
            mem->protein.insert(it.key(),atomlist);
            protein.insert(it.key(),atomlist);
        }
   }
   mem->maxX = _maxX;
   mem->maxY = _maxY;
   mem->maxZ = _maxZ;

   fillLayers(*atoms, molecules, *mem, _setup_params);
   framemembrans.insert(0, *mem);

   if (!_setup_params.trj.isEmpty())
   {
       std::string tpath = _setup_params.trj.toStdString();
       char *trrpath = new char[tpath.length() + 1];
       strcpy(trrpath, tpath.c_str());
       Xdrreader *trrread = new Xdrreader(trrpath, &_setup_params);
       trrread->read(&framemembrans, &molecules, &mem->protein);
       delete(trrread);
   }

   delete mem;
   delete reader;
   reader = nullptr;
}


void core::fillLayers(std::vector<Atom> &atoms, std::vector<Molecule> &molecules, Membran &mem, setup_parameters setup_params)
{
    /* Decide wether Atom is on the Upperside or Downside of the
     * Membrane and Fill the HASHMAP and NUMBERLISTS. Decision is made
     * based on lipid orientation.
     */


    // Add key atom information
    for(unsigned int i = 0; i<molecules.size(); i++ )
    {
        QList<int> keys = molecules[i].list.keys();
        qSort(keys.begin(), keys.end());
        for(int i2 = 0; i2< keys.size(); i2++)
        {
            for(unsigned int i3 = 0; i3 < atoms.size(); i3++)
            {
                QString molname1(((molecules[i].list[keys[i2]]).getResname()));
                QString molname2(((atoms)[i3].getResname()));
                QString aname1((molecules[i].list[keys[i2]]).getName());
                QString aname2(((atoms)[i3]).getName());
                if (molname1.compare(molname2) == 0 && aname1.compare(aname2)== 0)
                {
                    Atom a = molecules[i].list[keys[i2]];
                    molecules[i].included.push_back(a.getNumber());
                }
            }
        }
    }

    mem.polynomial = polynomial_regression(molecules, setup_params.p_degree);
    bool sideindex; // false = downside, true = upside
    for(unsigned int i = 0; i<molecules.size(); i++ )
    {
        vector<float> pos = molecules[i].ref_pos();
        if (setup_params.leaflet_detection == 1)
        {
            sideindex = (pos[2] - polyval(mem.polynomial, pos[0], pos[1], setup_params.p_degree)) > 0;
        }
        else
            sideindex = molecules[i].detectSide();

        for (int j = 0; j < molecules[i].included.size(); j++)
        {
            Atom a = molecules[i].list[molecules[i].included[j]];
            if (sideindex)
            {
                a.setSide(1);
                mem.leaflets["upside"]->QHside[a.getNumber()] = a;
            }
            else
            {
                a.setSide(0);
                mem.leaflets["downside"]->QHside[a.getNumber()] = a;
            }
        }
    }
}


void core::set_leaflet_globals(QString leaflet_name)
{
    double min_area = framemembrans[0].leaflets[leaflet_name]->Minarea;
    double max_area = framemembrans[0].leaflets[leaflet_name]->Maxarea;
    double min_thickness = framemembrans[0].leaflets[leaflet_name]->Minthick;
    double max_thickness = framemembrans[0].leaflets[leaflet_name]->Maxthick;
    QSet<int> setup = QSet<int>::fromList(framemembrans[0].leaflets[leaflet_name]->QHside.keys());
    for(int i = 0; i < framemembrans.size(); i++)
    {
        QSet<int>tset = QSet<int>::fromList(framemembrans[i].leaflets[leaflet_name]->QHside.keys());
        setup.unite(tset);
        if(framemembrans[i].leaflets[leaflet_name]->Minarea < min_area)
            min_area = framemembrans[i].leaflets[leaflet_name]->Minarea;
        if(framemembrans[i].leaflets[leaflet_name]->Minthick < min_thickness)
            min_thickness = framemembrans[i].leaflets[leaflet_name]->Minthick;
        if(framemembrans[i].leaflets[leaflet_name]->Maxarea > max_area)
            max_area = framemembrans[i].leaflets[leaflet_name]->Maxarea;
        if(framemembrans[i].leaflets[leaflet_name]->Maxthick > max_thickness)
            max_thickness = framemembrans[i].leaflets[leaflet_name]->Maxthick;
    }
    this->max_thickness[leaflet_name] = max_thickness;
    this->min_thickness[leaflet_name] = min_thickness;
    this->max_area[leaflet_name] = max_area;
    this->min_area[leaflet_name] = min_area;
}


void core::add_selection(QString name, Critall selection)
{
    criterialist->insert(name, selection);
}


unsigned int* core::get_sync_frames()
{
    return sync_frames;
}


void core::set_sync_frame_1(unsigned int frame)
{
    sync_frames[0] = frame;
}


void core::set_sync_frame_2(unsigned int frame)
{
    sync_frames[1] = frame;
}


QStringList core::get_leaflet_names()
{
    return leaflet_names;
}


QList<int> core::selection(QString crit_name)
{
    Critall critlist = criterialist->find(crit_name).value();
    QList<int> id_list;

}

void core::command_line_evaluation(CommandLineParser parser)
{
    setup_parameters setup_params = parser.setup_params();
    std::string tpath = setup_params.trj.toStdString();
    char *trrpath = new char[tpath.length() + 1];
    strcpy(trrpath, tpath.c_str());
    Xdrreader *xdr_reader = new Xdrreader(trrpath, &setup_params);
    threadsafe_queue<Membran> *out_queue = new threadsafe_queue<Membran>();
    std::vector<Atom> atoms;
    for (int i = 0; i < setup_params.key_atoms.length(); i++)
    {
        QString residue_name = setup_params.key_atoms[i].split(" ")[0];
        QString atom_name = setup_params.key_atoms[i].split(" ")[1];

        Atom key_atom;

        key_atom.setResname(residue_name);
        key_atom.setName(atom_name);
        atoms.push_back(key_atom);
    }

    QHash<QString, double> vdw_radii;
    vdw_radii.insert("H",1.1);
    vdw_radii.insert("B",1.92);
    vdw_radii.insert("C",1.70);
    vdw_radii.insert("N",1.55);
    vdw_radii.insert("O",1.52);
    vdw_radii.insert("F",1.47);
    vdw_radii.insert("S",1.80);
    vdw_radii.insert("P",1.80);
    vdw_radii.insert("I",1.98);
    vdw_radii.insert("Na",2.27);
    vdw_radii.insert("Ca",2.31);

    std::vector<Molecule> molecules;
    Pdbreader *pdb_reader = new Pdbreader();
    pdb_reader->setPath(parser.setup_params().pdb);
    pdb_reader->getResidues(atoms, molecules, vdw_radii);

    Membran *mem = new Membran();
    mem->maxX = setup_params.x_box * 10;
    mem->maxY = setup_params.y_box * 10;
    mem->maxZ = setup_params.z_box * 10;

    QHash<QString, QList<int>> peptides;
    if (!setup_params.index.isEmpty())
    {
        QFile index_file;
        index_file.setFileName(setup_params.index);
        index_parser *ndx_parser = new index_parser(index_file);
        for (int i = 0; i < setup_params.protein_names.length(); i++)
            ndx_parser->get_content_section(&peptides, setup_params.protein_names[i]);
        delete  ndx_parser;
        ndx_parser = nullptr;
    }
    if(peptides.size()>0)
    {
        QHash<QString, QList<int> > :: iterator it;
        for(it=peptides.begin(); it != peptides.end(); it++)
        {
            QHash<int,Atom>atomlist;
            pdb_reader->getIndexedProtein(it.key(), it.value(), atomlist, vdw_radii);
            mem->protein.insert(it.key(),atomlist);
        }
   }


    fillLayers(atoms,  molecules, *mem, setup_params);
    xdr_reader->read_for_command_line(out_queue, &molecules, &mem->protein, parser);

    delete[] trrpath;
    delete xdr_reader;
    delete pdb_reader;
    delete mem;

}

void core::cl_writer(threadsafe_queue<Membran> *membranes, CommandLineParser parser)
{
    setup_parameters setup_params = parser.setup_params();

    QString area_path = parser.area_xvg();
    if (area_path.isEmpty())
        area_path = "apl";
    QString thickness_path = parser.thickness_xvg();
    if (thickness_path.isEmpty())
        thickness_path = "thickness";
    QString frames_path = parser.frames_txt();
    QString selected_frames_path = parser.sel_frames_txt();
    QString selection_index_path = parser.selection_index();
    QStringList area_lipids = parser.area_lipids();
    QStringList thickness_lipids = parser.thickness_lipids();

    QFile frames_up, selected_frames;
    QList<QFile*> files;
    QHash<QString, QTextStream*> apl_streams;
    QHash<QString, QTextStream*> thickness_streams;
    QHash<QString, QTextStream*> frame_streams;
    QHash<QString, QTextStream*> selection_frame_steams;
    QStringList leaflets;
    leaflets.push_back("upside");
    leaflets.push_back("downside");
    QList<int> selection;

    // initialize apl files
    for (int i = 0; i < area_lipids.size(); i++) {
        for (int j = 0; j < leaflets.size(); j++) {
            QFile *file = new QFile(area_path + "_" + leaflets[j] + "_" + area_lipids[i] + ".xvg");
            file->open(QIODevice::WriteOnly | QIODevice::Text);
            QTextStream *stream = new QTextStream(file);
            *stream << "type xy\n@xaxis label Time (frames)\n@yaxis label Area nm ^2\n";
            apl_streams.insert(area_lipids[i] + leaflets[j], stream);
            files.append(file);
        }
    }
    // initialize thickness files
    for (int i = 0; i < thickness_lipids.size(); i++) {
        for (int j = 0; j < leaflets.size(); j++) {
            QFile *file = new QFile(thickness_path + "_" + leaflets[j] + "_" + thickness_lipids[i] + ".xvg");
            file->open(QIODevice::WriteOnly | QIODevice::Text);
            QTextStream *stream = new QTextStream(file);
            *stream << "type xy\n@xaxis label Time (frames)\n@yaxis label Thickness nm\n";
            thickness_streams.insert(thickness_lipids[i] + leaflets[j], stream);
            files.append(file);
        }
    }
    // initiallize frames textfile
    if (!frames_path.isEmpty()) {
        for (int j = 0; j < leaflets.size(); j++) {
            QFile *file = new QFile(frames_path + "_" + leaflets[j] + ".txt");
            file->open(QIODevice::WriteOnly | QIODevice::Text);
            QTextStream *stream = new QTextStream(file);
            *stream <<"#VLayers\n";
            *stream << "#Number  Name  Residue  ResidueID  X  Y  Z  Time  Area  Thick\n";
            frame_streams.insert(leaflets[j], stream);
            files.append(file);
        }
    }
    // initiallize selection frames textfile
    if (!selected_frames_path.isEmpty()) {
        selection = index_parser::get_index_selection(selection_index_path);
        for (int j = 0; j < leaflets.size(); j++) {
            QFile *file = new QFile(selected_frames_path + "_" + leaflets[j] + ".txt");
            file->open(QIODevice::WriteOnly | QIODevice::Text);
            QTextStream *stream = new QTextStream(file);
            *stream <<"#VLayers\n";
            *stream << "#Number  Name  Residue  ResidueID  X  Y  Z  Time  Area  Thick\n";
            selection_frame_steams.insert(leaflets[j], stream);
            files.append(file);
        }
    }
    int frame_counter = 1;
    for (int i = 0; i < files.size(); i++)
        files[i]->flush();

    while (true)
    {
        Membran mem;
        if (membranes->wait_and_pop(mem))
        {

            std::cout << "\rProcessing frame " << frame_counter;
            std::cout.flush();

            double time = mem.time;
            mem.makeVoronoi(setup_params.pbc_cutoff, setup_params.modus, setup_params.vdw_check, setup_params.upright, setup_params.upright);
            for (int i = 0; i < area_lipids.size(); i++) {
                for (int j = 0; j < leaflets.size(); j++) {
                    if (area_lipids[i] == "select" && !selected_frames_path.isEmpty())
                        *apl_streams[area_lipids[i] + leaflets[j]] << time << "   " << mem.leaflets[leaflets[j]]->getAreaForIndexList(selection) <<  "\n";
                    else {
                        *apl_streams[area_lipids[i] + leaflets[j]] << time << "   " << mem.leaflets[leaflets[j]]->getAreaForResidue(area_lipids[i]) <<  "\n";
                    }
                }
            }
            for (int i = 0; i < thickness_lipids.size(); i++) {
                for (int j = 0; j < leaflets.size(); j++) {
                    if (thickness_lipids[i] == "select" && !selected_frames_path.isEmpty())
                        *thickness_streams[thickness_lipids[i] + leaflets[j]] << time << "   " << mem.leaflets[leaflets[j]]->getThickForIndexList(selection)  << "\n";
                    else {
                        *thickness_streams[thickness_lipids[i] + leaflets[j]] << time << "   " << mem.leaflets[leaflets[j]]->getThickForResidue(thickness_lipids[i])  << "\n";
                    }
                }
            }
            if (!frames_path.isEmpty()) {
                for (int j = 0; j < leaflets.size(); j++) {
                    *frame_streams[leaflets[j]] << "#Framenumber"<< " " << frame_counter << " Time " << time << "\n";
                    for(QHash<int,Atom>::iterator it = mem.leaflets[leaflets[j]]->QHside.begin(); it != mem.leaflets[leaflets[j]]->QHside.end(); it++) {
                        *frame_streams[leaflets[j]] <<it.value().getNumber()<<"  "<<it.value().getName()<<"  "<<it.value().getResname()<<"  "<<it.value().getResNumber()<<"  "<<it.value().getX() <<"  "<<it.value().getY() <<"  "<<it.value().getZ() <<"  "<<it.value().getArea()<<"  "<<it.value().getThick()<<"\n";
                    }
                }
            }
            if (!selected_frames_path.isEmpty()) {
                for (int j = 0; j < leaflets.size(); j++) {
                    *selection_frame_steams[leaflets[j]] << "#Framenumber"<< " " << frame_counter << " Time " << time << "\n";
                    for(QHash<int,Atom>::iterator it = mem.leaflets[leaflets[j]]->QHside.begin(); it != mem.leaflets[leaflets[j]]->QHside.end(); it++) {
                        if (selection.contains(it.value().getNumber()))
                            *selection_frame_steams[leaflets[j]] <<it.value().getNumber()<<"  "<<it.value().getName()<<"  "<<it.value().getResname()<<"  "<<it.value().getResNumber()<<"  "<<it.value().getX() <<"  "<<it.value().getY() <<"  "<<it.value().getZ() <<"  "<<it.value().getArea()<<"  "<<it.value().getThick()<<"\n";
                    }
                }
            }
            frame_counter++;
            for (int i = 0; i < files.size(); i++)
                files[i]->flush();
        }
        else {
            for (int i = 0; i < files.size(); i++)
                files[i]->close();
            std::cout << "\n";
            return;
        }
    }
}

