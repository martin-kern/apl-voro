//**************************************************************
//                  Molecule.h
//           Copyright (c) 2012 - 2013 by Gunther Lukat
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************

#include "atom.h"
#include <vector>
#include <string>
#include <QHash>
#ifndef Molecule_h
#define Molecule_h 1

using namespace std;

/*!
 \brief

*/
class Molecule{

private:

public:
    Molecule();
    Molecule(QString _name, int num, Atom x);
    Molecule(std::string name, std::vector<Atom> atomlist, int a);
    virtual ~Molecule();

    QHash<int,Atom> list;
    QString molname;
    QVector<int>included;
    int number;
    int sideindex;
    virtual void addAtom(Atom x);
    void setName(QString name);
    QString getName();
    virtual int getSize();
    virtual QString getMolname();
    virtual int getMolnumber();
    virtual Atom& getAtom(int n);
    virtual Atom& getFirst();
    virtual Atom& getLast();
    bool detectSide();
    vector<float> ref_pos();
};
#endif
