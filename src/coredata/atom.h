//**************************************************************
//                  Atom.h
//           Copyright (c) 2012 - 2013 by Gunther Lukat
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************


#ifndef Atom_h
#define Atom_h 1


#include <string>

#include <QSet>
#include <QPointF>
#include <QPolygonF>
#include <QPair>
#include <iostream>
#include <QDataStream>
#include <QVector3D>
#include "data/atom_meta.hpp"



/*!

 \brief This structure is helper structure for queries made in the Voronoi views.

 */
struct Criterion
{
    bool isnot; /*!<  True if the criteria shoud be negated */
    bool name, area, neighb, thick, select;  /*!< Identifier which kind of criterias schould be matched */
    QString resname; /*!<  residue name (Atom.getresname()) that should be matched */
    double d_val;  /*!< Thickness or area that shoud be matched */
    int intval;  /*!< number of neighbors that should be matched */
    bool boolval;
    Criterion() : isnot(false), name(false), area(false), neighb(false), thick(false), select(false){}
    QSet<int> preselection;
};

/*!
 \brief  This structure is helper structure for queries made in the Voronoi views.

 */
struct Critall
{
   bool and_or;  /*!< True if all criteria in the list schould be matched. False if only one needs to be matched. */
   QList<Criterion> critlist; /*!< TODO */
};

/*!

 \brief This structure is helper structure for generating periodic images of atoms.

 */
struct PBCA
{
    class QPointF p; /*!< The x/y position of an atom */
    class Atom *a; /*!< pointer to an atom the periodic image was created for */
    int number; /*!< atomnumber of the atom. */
    bool virtu; /*!< True if the position p is a periodic image */
};

/*!
  \brief This class stores all information available for an atom.
 */
class Atom
{

public:
    Atom();
    Atom(const double &x_coord,const double &y_coord);
    Atom(double x_coord, double y_coord, double z_coord, QString atom_name, int atom_number, int mol_number, QString resname);
    Atom(const float &x_coord,const float &y_coord);
    ~Atom();


    QVector3D position() {return QVector3D(x, y, z);}


    double getX() const;
    void setX(const float &a);
    double getY() const;
    void setY(const float &a);
    double getZ() const;
    void setZ(const float &a);
    double getArea() const;
    void setArea(const double &t);
    double getThick() const;
    void setThick(const double &t);
    int getNumber() const;
    void setNumber(const int &a);
    int getSide() const;
    void setSide(const int &a);
    const  QVector<QPointF> &getVcell()const;
    void setVcell(const QVector< QPointF > &_cell);
    int getNeighbors() const;


    double getVdw() const;
    void setVdw(const double &t);
    const QString &getResname() const;
    void setResname(const QString &_resname);
    int getResNumber() const;
    void setResNumber(const int &a);
    const QString &getName()const;
    void setName(const QString &_name);
    const bool &isProt()const;
    void setProt(const bool &_protein);




    /*!
     \brief  For Convenience

     \param qpoly
    */
    void makeQpolygon(QPolygonF &qpoly);

    /*!
     \brief used for debugging

    */
    void printAtom();


    /*!
     \brief  Will return true if this atom matches the given criterion

     \param crit
     \return bool
    */
    bool matchCriterion(const Criterion crit);

    /*!
     \brief  Will return true if this atom matches ALL the given criteria.

     \param critlist
     \return bool
    */
    bool matchCriterion_list_all(const QList<Criterion> critlist);

    /*!
     \brief Will return true if this atom matches ONE of the given criteria.

     \param critlist
     \return bool
    */
    bool matchCriterion_list_single(const QList<Criterion> critlist);

    // remove?
    bool preselect;
    void setpreselect(bool a);

//    atom_meta *meta_info;

private:
    float x;  /*!< X Position */
    float y;  /*!< Y Position */
    float z;  /*!< Z Poition */
    QVector< QPointF > poly; /*!< the Polygon/Voronoi cell created for this atom */
    double thick;  /*!< The calculated thickness */
    int side; /*!< 0 = bottom, 1 = top */
    double area;  /*!< the calculated area of this Atom */


    int atomnumber;  /*!< The number of this atom as it appears in the PDB file */
    int molnumber; /*!< The moleculen number of this atom as it appears in the PDB file */
    int id;  /*!< TODO */
    bool prot; /*!< TODO */
    double vdw; /*!< Van der Waals radius of this atom */
    QString name;  /*!< Name  (C, P, N, ....)  */
    QString resname;  /*!< Name of the molecule this Atom belongs to */


    void toQpoint(QPointF &a);
    void constructthepolygon();
    void polyArea();
    double distance(Atom &a);
};
#endif
