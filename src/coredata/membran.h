//**************************************************************
//                  membrane.h
//           Copyright (c) 2012 - 2013 by Gunther Lukat
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************

#ifndef MEMBRAN_H
#define MEMBRAN_H
#include "atom.h"
#include "molecule.h"
#include "layer.h"
#include <QDataStream>
#include <cmath>
#include <vector>

using std::vector;


class Membran
{
public:

    Membran();
    ~Membran();

    QHash<QString, Layer*> leaflets;
    QHash<QString, QHash<int, Atom>> protein;
    double maxX, maxY, maxZ;
    int framenr;
    double time;
    vector<float> polynomial;

    void addSites(std::vector<Atom> alist, int &a);
    void makeVoronoi(int a,int mode, bool vdwcheck, double upright, double downright);
    void calcThickness(int mode, Layer *upside, Layer *downside);
    void calcLayerThickness(int mode, Layer *layer_1, Layer *layer_2, bool up);
    double BinPolygonsOfA(Atom *a, Atom *b);
    void calculate3dplots();
    Layer& getUP();
    Layer& getDOWN();
};
#endif
