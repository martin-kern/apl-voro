//**************************************************************
//                  Core.h
//           Copyright (c) 2012 - 2013 by Gunther Lukat
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************


#ifndef CORE_H
#define CORE_H
#include <iostream>
#include <QDataStream>
#include <QVector>
//#include <QProgressBar>
#include <QFile>
#include "atom.h"
#include "io/pdb/pdbreader.h"
#include "io/ndx/index_parser.hpp"
#include "molecule.h"
#include "membran.h"
#include "data/setup_parameters.hpp"
#include "data/atom_meta.hpp"
#include "io/xdr/threadsave_hash_map.hpp"
#include "io/xdr/ThreadQueue.h"
#include "commandlineparser.hpp"


/*!
 \brief The Core of APLVoro: The frames are stored here.

*/
class core
{
public:
    core(setup_parameters setup_params);
    ~core();

    QString name() const {return _setup_params.name;}
    setup_parameters setup_params() const {return _setup_params;}
    Critall get_criteria(QString name) {return criterialist->find(name).value();}
    QHash<QString, Critall>* get_crit_list() {return criterialist;}


    ThreadSaveHashMap<int, Membran> framemembrans;

    QList<int> ALLupsideAnumbers;
    QList<int> ALLdownsideAnumbers;
    QHash<QString, QStringList> residues;
    QHash<QString, QStringList> lipids;

    QStringList peptidenames;
    QHash<QString,QList<int> > peptides;
    QHash<QString, QHash<int, Atom> >protein;


    // Have max and min area and thickness readily available.
    QHash<QString, double> max_thickness,
                           min_thickness,
                           max_area,
                           min_area;


    unsigned int* get_sync_frames();
    void set_sync_frame_1(unsigned int sync_frame);
    void set_sync_frame_2(unsigned int sync_frame);
    QStringList get_leaflet_names();
    float average_area();
    float average_thickness();
    void add_selection(QString name, Critall selection);
    QList<int> selection(QString crit_name);

    static void command_line_evaluation(CommandLineParser parser);
    static void fillLayers(std::vector<Atom> &atoms, std::vector<Molecule> &molecules, Membran &mem, setup_parameters setup_params);
    static void cl_writer(threadsafe_queue<Membran> *membranes, CommandLineParser parser);

private:
    QHash<QString, double> vdwradii;
    setup_parameters _setup_params;
    unsigned int sync_frames[2];
    QStringList leaflet_names;
    vector<atom_meta> atom_meta_data;

    void makeWorkingSetTrr(std::vector<Atom> *atomnames,
                           double _maxX,
                           double _maxY,
                           double _maxZ);


    void set_leaflet_globals(QString leaflet_name);
    QHash<QString, Critall> *criterialist;
};


#endif
