//**************************************************************
//
//           Copyright (c) 2012 - 2013 by Gunther Lukat
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************


#include "coredata/layer.h"
#include "coredata/atom.h"
//#include <omp.h>
#include <algorithm>
#include <QDebug>
#include "geom/geomalgorithm.h"
#include "geom/triangulation.h"


Layer::Layer()
{
    Maxarea = 0;
    Minarea = 10000000;
    Maxthick = 0;
    Minthick = 10000000;
    t = nullptr;
    thickplot = nullptr;
    Areaplot = nullptr;
//    framenr=0;
    _avg_apl = 0;
    _avg_thickness = 0;
}


Layer::~Layer()
{
    if (t != nullptr)
        delete t;
    if (thickplot != nullptr)
        delete thickplot;
    if (Areaplot != nullptr)
        delete Areaplot;
}


QPointF2D Layer::pbcExtendSIDEX(const Atom &atom, double maxX)
{
    double Xoffset = (maxX/100.0)*offset;

    QPointF2D neu;
    neu.number = -1;

    if(atom.getX() <  Xoffset )
    {
        neu.rx() = (atom.getX()+maxX);
        neu.ry() = (atom.getY());
        neu.number = atom.getNumber();
        return neu;
    }
    if(atom.getX() > maxX-Xoffset )
    {
        neu.rx() = (atom.getX()-maxX);
        neu.ry() = (atom.getY());
        neu.number = atom.getNumber();
        return neu;
    }
    return neu;
}


QPointF2D Layer::pbcExtendSIDEY(const Atom &atom, double maxY)
{
    double Yoffset = (maxY/100.0)*offset;
    QPointF2D neu;
    neu.number = -1;

    if(atom.getY() < Yoffset )
    {
        neu.rx() = (atom.getX());
        neu.ry() = (atom.getY() + maxY);
        neu.number = atom.getNumber();
        return neu;
    }
    if(atom.getY() > maxY-Yoffset  )
    {
        neu.rx() = (atom.getX());
        neu.ry() = (atom.getY() - maxY);
        neu.number = atom.getNumber();
        return neu;
    }

    return neu;
}


void Layer::pbcpCorrectFrom(Atom &atom, double maxX, double maxY)
{
    double minX = 0;
    double minY = 0;
    double fx = atom.getX();
    double fy = atom.getY();

    if(fx < minX && fy < minY)
    {
        while(fx<minX)
            fx = fx+maxX;
        while(fy <minY)
            fy = fy+maxY;

        atom.setX(float(fx));
        atom.setY(float(fy));
    }
    else if(fx < minX && fy > maxY)
    {
        while(fx<minX)
            fx = fx+maxX;
        while(fy >maxY)
            fy = fy-maxY;

        atom.setX(float(fx));
        atom.setY(float(fy));
    }
    else if(fx > maxX && fy < minY)
    {
        while(fx>maxX)
            fx = fx-maxX;
        while(fy < minY)
            fy = fy+maxY;
        atom.setX(float(fx));
        atom.setY(float(fy));
    }
    else if(fx > maxX && fy > maxY)
    {
        while(fx > maxX)
            fx = fx-maxX;
        while(fy > maxY)
            fy = fy-maxY;
        atom.setX(float(fx));
        atom.setY(float(fy));
    }
    else if(fx < minX)
    {
        while(fx < minX)
            fx = fx+maxX;
        atom.setX(float(fx));
    }
    else if(fx > maxX)
    {
        while(fx > maxX)
            fx = fx-maxX;
        atom.setX(float(fx));
    }
    else if(fy > maxY)
    {
        while(fy > maxY)
            fy = fy-maxY;
        atom.setY(float(fy));
    }
    else if(fy < minY)
    {
        while(fy < minY)
            fy = fy+maxY;
        atom.setY(float(fy));
    }
}


void Layer::graphit(int a,
                    double maxXBorder,
                    double maxYBorder,
                    QHash<QString, QHash<int, Atom>> &protein,
                    int vdwcheck,
                    double upright,
                    double downright)
{
    double f1z = 0;
    double f2z = 0;
    double f3z = 0;
    double minf1z = 0;
    double minf2z = 0;
    double minf3z =0;
    //set the periodic boundary
    offset = a;
    double Xoffset = (maxXBorder/100.0)*offset;
    double Yoffset = (maxYBorder/100.0)*offset;

    //set the supertriangle
    QPointF2D p_0(0-Xoffset-10,0-Yoffset-10);
    p_0.number = -1;

    QPointF2D p_1((maxXBorder+Xoffset)*2.5,0-Yoffset-10);
    p_1.number = -1;

    QPointF2D p_2(0-Xoffset-10,(maxYBorder+Yoffset)*2.5);
    p_2.number = -1;

    //generate the Quadedge
    if(t != nullptr)
        delete(t);

     t = new Triangulation(p_0, p_1, p_2);

    // iterativ insert points in the quadedge
    QHash<int,Atom>::iterator it;
    for (it = QHside.begin(); it != QHside.end(); ++it )
    {
        QPointF2D pp;
        pp.number = -1;

        //correct pbc conditions from xtc file
        pbcpCorrectFrom(it.value(), maxXBorder, maxYBorder);

        pp.rx() = it->getX();
        pp.ry() = it->getY();

        pp.number = it->getNumber();
        pp.virt = false;
        t->InsertSite(pp,doubles);
        // pbc corner left down
        if(it->getY() < Yoffset && it->getX() < Xoffset)
        {
            pp.rx() = (it->getX() + maxXBorder);
            pp.ry() = (it->getY() + maxYBorder);
            pp.number = it->getNumber();
            pp.virt = true;
            t->InsertSite(pp,doubles);
        }
        // pbc corner left up
        if(it->getY() < Yoffset && it->getX() > maxXBorder-Xoffset  )
        {
            pp.rx() = (it->getX() - maxXBorder);
            pp.ry() = (it->getY() + maxYBorder);
            pp.number = it->getNumber();
            pp.virt = true;
            t->InsertSite(pp,doubles);
        }
        // pbc corner right down
        if(it->getY() > maxYBorder-Yoffset && it->getX() < Xoffset  )
        {
            pp.rx() = (it->getX() + maxXBorder);
            pp.ry() = (it->getY() - maxYBorder);
            pp.number = it->getNumber();
            pp.virt = true;
            t->InsertSite(pp,doubles);
        }
        // pbc corner right up
        if(it->getY() > maxYBorder-Yoffset && it->getX() > maxXBorder-Xoffset  )
        {
            pp.rx() = (it->getX() - maxXBorder);
            pp.ry() = (it->getY() - maxYBorder);
            pp.number = it->getNumber();
            pp.virt = true;
            t->InsertSite(pp,doubles);
        }

        // pbc side up and down
        pp = pbcExtendSIDEX(it.value(), maxXBorder);

        if(pp.number >0)
        {
            pp.virt = true;
            t->InsertSite(pp,doubles);
        }
        // pbc side left  and right
        pp = pbcExtendSIDEY(it.value(), maxYBorder);

        if(pp.number >0)
        {
            pp.virt = true;
            t->InsertSite(pp,doubles);
        }
    }

    // proteininsert
    QHash<QString,QHash<int,Atom> >::Iterator it2;

    QHash<QString,QList<int> > prots;
    for (it2 = protein.begin(); it2 != protein.end(); it2++)
    {
        QList<int> alist;
        for (it = it2.value().begin(); it != it2.value().end(); it++ )
        {
            QPointF2D pp;
            //correct pbc conditions from xtc file
            pbcpCorrectFrom(it.value(), maxXBorder, maxYBorder);

            pp.rx() = it->getX();
            pp.ry() = it->getY();
            double zv = it->getZ();


            pp.number = it->getNumber();
            pp.virt = false;

            Triangle tri;
            tri = t->TriangleContainsPoint(pp);
            if(!(tri.A.number < 0||tri.B.number <0||tri.C.number <0) && !(it2.value().contains(tri.A.number) && it2.value().contains(tri.B.number) && it2.value().contains(tri.C.number)) )
            {
                if(IsPointInTriangle(pp,tri))
                {
                    if(vdwcheck == 0)
                    {
                        f1z = QHside.find(tri.A.number)->getZ() + QHside.find(tri.A.number)->getVdw();
                        f2z = QHside.find(tri.B.number)->getZ() + QHside.find(tri.B.number)->getVdw();
                        f3z = QHside.find(tri.C.number)->getZ() + QHside.find(tri.C.number)->getVdw();
                        minf1z = QHside.find(tri.A.number)->getZ() - QHside.find(tri.A.number)->getVdw();
                        minf2z = QHside.find(tri.B.number)->getZ() - QHside.find(tri.B.number)->getVdw();
                        minf3z = QHside.find(tri.C.number)->getZ() - QHside.find(tri.C.number)->getVdw();
                    }
                    else if(vdwcheck == 1 || vdwcheck == 2 )
                    {
                        f1z = QHside.find(tri.A.number)->getZ() + upright;
                        f2z = QHside.find(tri.B.number)->getZ() + upright;
                        f3z = QHside.find(tri.C.number)->getZ() + upright;
                        minf1z = QHside.find(tri.A.number)->getZ() - downright;
                        minf2z = QHside.find(tri.B.number)->getZ() - downright;
                        minf3z = QHside.find(tri.C.number)->getZ() - downright;
                    }
                    if((f1z >= zv ||f2z >= zv || f3z >= zv) && (minf1z <= zv ||minf2z <= zv|| minf3z <= zv))
                    {
                        alist.push_back(it.value().getNumber());
                        QHside.insert(it.value().getNumber(),it.value());
                    }
                }
            }
        }
        prots.insert(it2.key(),alist);
    }

    for (QHash<QString,QList<int> >::iterator itp = prots.begin(); itp != prots.end(); itp++)
    {
        for(int i = 0 ; i < itp.value().size(); i++)
        {
            QPointF2D pp;
            it  = QHside.find(itp.value()[i]);// it.value();

            //correct pbc conditions from xtc file
            pbcpCorrectFrom(it.value(), maxXBorder, maxYBorder);

            pp.rx() = it->getX();
            pp.ry() = it->getY();
            pp.number = it->getNumber();
            pp.virt = false;

            t->InsertSite(pp,doubles);

            if(it->getY() < Yoffset && it->getX() < Xoffset)
            {
                pp.rx() = (it->getX() + maxXBorder);
                pp.ry() = (it->getY() + maxYBorder);
                pp.number = it->getNumber();
                pp.virt = true;
                t->InsertSite(pp,doubles);
            }
            // pbc corner left up
            if(it->getY() < Yoffset && it->getX() > maxXBorder-Xoffset  )
            {
                pp.rx() = (it->getX() - maxXBorder);
                pp.ry() = (it->getY() + maxYBorder);
                pp.number = it->getNumber();
                pp.virt = true;
                t->InsertSite(pp,doubles);
            }
            // pbc corner right down
            if(it->getY() > maxYBorder-Yoffset && it->getX() < Xoffset  )
            {
                pp.rx() = (it->getX() + maxXBorder);
                pp.ry() = (it->getY() - maxYBorder);
                pp.number = it->getNumber();
                pp.virt = true;
                t->InsertSite(pp,doubles);
            }
            //pbc corner right up
            if(it->getY() > maxYBorder-Yoffset && it->getX() > maxXBorder-Xoffset  )
            {
                pp.rx() = (it->getX() - maxXBorder);
                pp.ry() = (it->getY() - maxYBorder);
                pp.number = it->getNumber();
                pp.virt = true;
                t->InsertSite(pp,doubles);
            }

            //pbc side up and down
            pp = pbcExtendSIDEX(it.value(), maxXBorder);

            if(pp.number >0)
            {
                pp.virt = true;
                t->InsertSite(pp,doubles);
            }
            //pbc side left  and right
            pp = pbcExtendSIDEY(it.value(), maxYBorder);

            if(pp.number >0)
            {
                pp.virt = true;
                t->InsertSite(pp,doubles);
            }
        }
    }

    Minarea = 100000;
    Maxarea = -1;
    minimumx = QHside.begin().value().getX();
    maximumx = QHside.begin().value().getX();
    minimumy = QHside.begin().value().getY();
    maximumy = QHside.begin().value().getY();

    for (it = QHside.begin(); it != QHside.end(); ++it )
    {
        QPointF2D p(it->getX(),it->getY());
        QPolygonF mypoly;
        mypoly =  t->getCCW_VCellFor(p,&mypoly);
        it->setVcell(mypoly);
        it->setArea(polygonArea(mypoly)/10);
        _avg_apl = _avg_apl + it->getArea();

        if(it->getArea() < Minarea && !it->isProt())
            Minarea = it->getArea();
        if(it->getArea() > Maxarea && !it->isProt())
            Maxarea = it->getArea();
        if(it->getX()<minimumx && !it->isProt())
            minimumx = it->getX();
        if(it->getX()>maximumx && !it->isProt())
            maximumx = it->getX();
        if(it->getY()<minimumy && !it->isProt())
            minimumy = it->getY();
        if(it->getY()>maximumy && !it->isProt())
            maximumy = it->getY();
    }
    _avg_apl = _avg_apl / QHside.size();

    for (int i = 0 ; i < doubles.size(); i++)
    {
        Atom *x = &(QHside[doubles[i].first]);
        Atom *y =  &(QHside[doubles[i].second]);
        if(x->getNeighbors()<=0)
            x->setVcell(y->getVcell());
        else if(y->getNeighbors()<=0)
            y->setVcell(x->getVcell());
    }
    doubles.clear();
}


bool pbcSort(const PBCA &a, const PBCA &b)
{
    if(a.p.y() < b.p.y() )
        return 1;
    if(a.p.y()  > b.p.y())
        return 0;
    if((a.p.y() - b.p.y() < 0.002) && (a.p.x() > b.p.x()) )
        return 0;
    if((a.p.y() - b.p.y() < 0.002) && (a.p.x() < b.p.x()) )
        return 1;
    if((a.p.y() == b.p.y() && a.p.x() == b.p.x()) && a.number != b.number)
        return 0;

    return 0;
}


void Layer::precalc3dplots(int columns, int rows)
{
    Areaplot = new double* [rows];
    thickplot = new double* [rows];
    double fp1Area, fp1Thick, fp2Area, fp2Thick, fp3Area, fp3Thick;

    for (int x = 0; x < rows; x++)
    {
        Areaplot[x] = new double[columns];
        thickplot[x] = new double[columns];

        for (int y = 0; y < columns; y++)
        {
            QPointF2D point(x,y);
            Triangle triangle;
            triangle = t->getTriangleCCW(point);
            fp1Area = QHside.find(triangle.A.number).value().getArea();
            fp1Thick = QHside.find(triangle.A.number).value().getThick();

            fp2Area = QHside.find(triangle.B.number).value().getArea();
            fp2Thick = QHside.find(triangle.B.number).value().getThick();

            fp3Area = QHside.find(triangle.C.number).value().getArea();
            fp3Thick = QHside.find(triangle.C.number).value().getThick();
            Areaplot[x][y] = interpTriangle(x,y,triangle.A.x(),triangle.A.y(),fp1Area,triangle.B.x(),triangle.B.y(),fp2Area,triangle.C.x(),triangle.C.y(),fp3Area);
            thickplot[x][y] = interpTriangle(x,y,triangle.A.x(),triangle.A.y(),fp1Thick,triangle.B.x(),triangle.B.y(),fp2Thick,triangle.C.x(),triangle.C.y(),fp3Thick);
        }
    }
}



void Layer::matchCriterion_List(QList<Criterion> critlist, QList<int> &found, bool and_or)
{
    for(QHash<int,Atom> ::iterator it = QHside.begin(), end = QHside.end(); it != end; it++)
    {
        if(and_or)
        {
            if(it.value().matchCriterion_list_all(critlist))
                found.push_back(it.key());
        }
        else
        {
            if(it.value().matchCriterion_list_single(critlist))
                found.push_back(it.key());
        }
    }
}


double Layer::getAreaForResidue(QString Rname)
{
    double result = 0.0;
    int count = 0;
    for(QHash<int,Atom>:: iterator it = QHside.begin(); it != QHside.end(); it++)
    {
        if(it.value().getResname().compare(Rname)==0 )
        {
            count ++;
            result += it.value().getArea();
        }
    }
    return result/(count);
}


double Layer::getSumAreaForResidue(QString Rname)
{
    double result = 0.0;
    for(QHash<int,Atom>:: iterator it = QHside.begin(); it != QHside.end(); it++)
    {
        if(it.value().getResname().compare(Rname) == 0)
            result += it.value().getArea();
    }
    return result;
}


double Layer::getThickForResidue(QString Rname)
{
    double result = 0.0;
    int count = 0;
    for(QHash<int,Atom>:: iterator it = QHside.begin(); it != QHside.end(); it++)
    {
        if(it.value().getResname().compare(Rname)==0 )
        {
            count ++;
            result += it.value().getThick();
        }
    }
    return result / count;
}


int Layer::getNBForResidue(QString Rname)
{
    int result = 0.0;
    int count = 0;
    for(QHash<int,Atom>:: iterator it = QHside.begin(); it != QHside.end(); it++)
    {
        if(it.value().getResname().compare(Rname)==0)
        {
            count++;
            result += it.value().getNeighbors();
        }
    }
    if (count == 0)
        return 0;
    return result / count;
}


double Layer::getAreaForList(QList<int> list)
{
    double result = 0.0;
    int count = 0;
    for(int i = 0; i < list.size(); i++)
    {
        if (!QHside[list[i]].isProt())
        {
            count++;
            result += QHside[list[i]].getArea();
        }
    }
    return result / count;
}


double Layer::getAreaForIndexList(QList<int> list)
{
    QHash<int, Atom>::iterator it;
    double result = 0.0;
    int count = 0;
    for(int i = 0; i < list.size(); i++)
    {
        it = QHside.find(list[i]);
        if(it != QHside.end() && !QHside[list[i]].isProt())
        {
                count ++;
                result += QHside[list[i]].getArea();
        }
    }
    return result/(count);
}


double Layer::getThickForIndexList(QList<int> list)
{
    QHash<int, Atom>::iterator it;
    double result = 0.0;
    int count = 0;
    for(int i = 0; i < list.size(); i++)
    {
        it = QHside.find(list[i]);
        if(it != QHside.end() && !QHside[list[i]].isProt())
        {
            count ++;
            result += QHside[list[i]].getArea();
        }
    }
    return result / count;
}


double Layer::getSumAreaForList(QList<int> list)
{
    double result = 0.0;
    for(int i = 0; i < list.size(); i++)
    {
        if (!QHside[list[i]].isProt())
            result += QHside[list[i]].getArea();
    }
    return result;
}


double Layer::getThickForList(QList<int> list)
{
    double result = 0.0;
    int count = 0;
    for(int i = 0; i < list.size(); i++)
    {
        if (!QHside[list[i]].isProt())
        {
            count ++;
            result += QHside[list[i]].getThick();
        }
    }
    return result/count;
}


int Layer::getNBForList(QList<int> list)
{
    int result = 0.0;
    int count = 0;
    for(int i = 0; i < list.size(); i++)
    {
        count ++;
        result += QHside[list[i]].getNeighbors();
    }
    if (count == 0)
        return result;
    return result/count;
}


void Layer::calculate_avg_thickness()
{
    _avg_thickness = 0;
    int num_of_lipids = 0;
    for (QHash<int,Atom>::iterator it = QHside.begin(); it != QHside.end(); ++it )
    {
        if (!it.value().isProt())
        {
            _avg_thickness = _avg_thickness + it->getThick();
            num_of_lipids++;
        }
    }
    _avg_thickness = _avg_thickness / num_of_lipids;
}


double Layer::area_for_selection(Critall crit)
{
    QHashIterator<int, Atom> atom_iterator(QHside);
    double sum_area = 0;
    int selection_count = 0;

    if (crit.and_or)
    {
        while (atom_iterator.hasNext())
        {
            atom_iterator.next();
            Atom a = atom_iterator.value();
            if (a.matchCriterion_list_all(crit.critlist))
            {
                sum_area = sum_area + a.getArea();
                selection_count++;
            }
        }
    }
    else
    {
        while (atom_iterator.hasNext())
        {
            atom_iterator.next();
            Atom a = atom_iterator.value();
            if (a.matchCriterion_list_single(crit.critlist))
            {
                sum_area = sum_area + a.getArea();
                selection_count++;
            }
        }
    }

//    while (atom_iterator.hasNext())
//    {
//        atom_iterator.next();
//        Atom a = atom_iterator.value();
//        if (crit.and_or)
//        {
//            if (a.matchCriterion_list_all(crit.critlist))
//            {
//                sum_area = sum_area + a.getArea();
//                selection_count++;
//            }
//        }
//        else
//        {
//            if (a.matchCriterion_list_single(crit.critlist))
//            {
//                sum_area = sum_area + a.getArea();
//                selection_count++;
//            }
//        }
//    }
    if (selection_count == 0)
        return -1;
    return sum_area / selection_count;
}


double Layer::sum_area_for_selection(Critall crit)
{
    QHashIterator<int, Atom> atom_iterator(QHside);
    double sum_area = 0;

    if (crit.and_or)
    {
        while (atom_iterator.hasNext())
        {
            atom_iterator.next();
            Atom a = atom_iterator.value();
            if (a.matchCriterion_list_all(crit.critlist))
                sum_area = sum_area + a.getArea();
        }
    }
    else
    {
        while (atom_iterator.hasNext())
        {
            atom_iterator.next();
            Atom a = atom_iterator.value();
            if (a.matchCriterion_list_single(crit.critlist))
                sum_area = sum_area + a.getArea();

        }
    }

    return sum_area;
}


double Layer::thickness_for_selection(Critall crit)
{
    QHashIterator<int, Atom> atom_iterator(QHside);
    double sum_thickness = 0;
    int selection_count = 0;

    if (crit.and_or)
    {
        while (atom_iterator.hasNext())
        {
            atom_iterator.next();
            Atom a = atom_iterator.value();
            if (a.matchCriterion_list_all(crit.critlist))
            {
                sum_thickness = sum_thickness + a.getThick();
                selection_count++;
            }
        }
    }
    else
    {
        while (atom_iterator.hasNext())
        {
            atom_iterator.next();
            Atom a = atom_iterator.value();
            if (a.matchCriterion_list_single(crit.critlist))
            {
                sum_thickness = sum_thickness + a.getThick();
                selection_count++;
            }
        }
    }
    if (selection_count == 0)
        return -1;
    return sum_thickness / selection_count;
}

