//**************************************************************
//                  Molecule.cpp
//           Copyright (c) 2012 - 2013 by Gunther Lukat
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************
/*!
 * ... text ...
 */

#include "molecule.h"
#include <iostream>
#include <vector>
#include <string>
#include <QtAlgorithms>
#include<QDebug>
using namespace std;

Molecule::Molecule()
{
  molname = "name";
  number = 0;
}


Molecule::Molecule(QString _name,int num,Atom x)
{
    molname = _name ;//name.erase(index+1);
    //cout << molname;
    number = num;
// cout << number<<"\n";
    list[x.getNumber()]=x;
}

Molecule::Molecule(std::string name, std::vector<Atom> atomlist,int a)
{
    molname = QString::fromStdString(name);
    list.clear();
    for (unsigned int i =0 ; i < atomlist.size(); i++)
        list[atomlist[i].getNumber()]=atomlist[i];
    sideindex = a;
}

Molecule::~Molecule()
{

}
void Molecule::setName(QString name)
{
    molname = name;
}

QString Molecule::getName()
{
    return molname;
}
void Molecule::addAtom(Atom x){
        list.insert(x.getNumber(),x);


}
int Molecule::getSize(){
    int a =(int)list.size();
    return a;
}

QString Molecule::getMolname()
{
    return molname;
}
int Molecule::getMolnumber(){
    int num = number;
   // cout <<"diese Nummer"<< num<<"\n";
    return num;
}

Atom& Molecule::getAtom(int n)
{
    return list.find(n).value();
}

Atom& Molecule::getFirst()
{
   QList<int> keys = list.keys();
   std::sort(keys.begin(), keys.end());
   return(list.find(keys[0]).value());
   // return list[0];
}
 Atom& Molecule::getLast()
{
     QList<int> keys = list.keys();
     std::sort(keys.begin(), keys.end());
     return(list.find(keys.back()).value());
 // return list.end()-1;
}

bool Molecule::detectSide()
{
    QList<int> keys = list.keys();
    std::sort(keys.begin(), keys.end());
    int a = 0;
    for (int i = 1 ;i < keys.size() ;i++)
    {
        if(list[keys[0]].getZ() < list[keys[i]].getZ())
            a-=1;
        if(list[keys[0]].getZ() > list[keys[i]].getZ())
            a+=1;
    }
    return a > 0;
}

vector<float> Molecule::ref_pos()
{
    vector<float> pos;
    pos.reserve(3);
    int n_included = included.size();
    float x = 0.0f, y = 0.0f, z = 0.0f;
    for (int i = 0; i < n_included; i++)
    {
        x += float(list[included[i]].getX());
        y += float(list[included[i]].getY());
        z += float(list[included[i]].getZ());
    }
    pos.push_back(x / n_included);
    pos.push_back(y / n_included);
    pos.push_back(z / n_included);
    return pos;
}
