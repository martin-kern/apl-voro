//**************************************************************
//                  vt.h
//           Copyright (c) 2012 - 2013 by Gunther Lukat
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************

#ifndef VT_H
#define VT_H
#include <QTreeWidgetItem>
#include "gui/graphicobjects/vcell.h"
#include "coredata/atom.h"
#include "gui/colorgradients.hpp"

/*!
 \brief
 Data structure to combine voronoi cells
*/
class VT
{
public:
    VT();

    bool selected;
    QTreeWidgetItem *item;
    Vcell *cell;
    Atom *atom;
    QColor Residuecolor;
    QColor Areacolor;
    QColor NcColor;
    QColor Thickcolor;


//       void  CalcColors(QColor rescolor,
//                        double globalmaxarea,
//                        double globalminarea,
//                        double maxarea,
//                        double minarea,
//                        double globalmaxthick,
//                        double globalminthick,
//                        double maxthick,
//                        double minthick,
//                        bool useglobminArea,
//                        bool useglobmaxArea,
//                        bool useglobminThick,
//                        bool useglobmaxThick);

       void  calc_colors(QColor rescolor,
                         double maxarea,
                         double minarea,
                         double maxthick,
                         double minthick);
};

#endif // VT_H
