//**************************************************************
//                  Atom.cpp
//           Copyright (c) 2012 - 2013 by Gunther Lukat
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************
/*!
 * Atom class
 * This class stores all information available for an atom.
 * For every atom that is read from a file an instance is created.
 */


#include <string>
#include "atom.h"
#include <QPointF>
#include <Qt>
#include <math.h>
#include <algorithm>
#include <cstring>
#include <QSet>
#include <set>
#include <QtAlgorithms>
#include <QDebug>

bool compQPointFforSort(const QPointF &s1, const QPointF &s2)
 {
    return (s1==s2);
 }
Atom::Atom()
{
    area = 0.0;
    prot = false;
    preselect=false;
    id = 0;
}


Atom::Atom(double x_coord, double y_coord, double z_coord, QString atom_name, int atom_number, int mol_number, QString resname)
{
    x = float(x_coord);
    y = float(y_coord);
    z = float(z_coord);
    name = atom_name;
    atomnumber = atom_number;
    molnumber = mol_number;
    this->resname = resname;
}


Atom::~Atom()
{

}

Atom::Atom(const float &x_coord, const float &y_coord)
{
    id = 0;
    x = x_coord;
    y = y_coord;
    area = 0.0;
    thick = 0.0;
    prot = false;
    preselect = false;
}

Atom::Atom(const double &x_coord, const double &y_coord)
{
    id = 0;
    x = float(x_coord);
    y = float(y_coord);
    area = 0.0;
    thick = 0.0;
    prot = false;
    preselect = false;
}

double Atom::getX() const
{
    return x;
};

double Atom::getY() const
{
    return y;
}

double Atom::getZ() const
{
    return z;
}
void Atom::setX(const float &a)
{
    x = a;
};

void Atom::setY(const float &a)
{
    y = a;
};


void Atom::setZ(const float &a)
{
    z = a;
}

double Atom::getArea() const
{
    return area/10.0;
}
void Atom::setArea(const double &a)
{
    area = a;
}
 double Atom::getThick() const
{
    return thick/10.0;
}
void Atom::setThick(const double &a)
{
    thick = a;
}



int Atom::getNumber() const
{
    return atomnumber;
//    return meta_info->atom_number;
}

int Atom::getResNumber() const
{
    return molnumber;
//    return meta_info->molnumber;
}

double Atom::getVdw() const
{
    return vdw;
//    return double(meta_info->vdw);
}

const QString &Atom::getName() const
{
    return name;
//    return meta_info->name;
};

const QString &Atom::getResname() const
{
  return resname;
//    return meta_info->resname;
}

const bool &Atom::isProt()const
{
    return prot;
//    return meta_info->prot;
}




// MARKED FOR DELETION
void Atom::setResNumber(const int &a)
{
    molnumber = a;
}
void Atom::setVdw(const double &a)
{
    vdw = a;
}
void Atom::setNumber(const int &a)
{
    atomnumber = a;
}
void Atom::setName(const QString &_name)
{
 name = _name;
}
void Atom::setResname(const QString &_resname)
{
    resname = _resname;
}
void Atom::setProt(const bool &_protein)
{
    prot = _protein;
}



 /*!
  * Getter function:
  * returns the side of the lipid the atom belongs to.
  * 0 = bottom; 1 = top;
  */
int Atom::getSide() const
{
    return side;
}

/*!
* Setter function:
* Sets Side of the lipid the atom belongs to.
* 0 = bottom; 1 = top;
*/
void Atom::setSide(const int &a)
{
    side = a;
}


/*!
 * Getter function:
 * returns the VoronoiCell of the atom.
 */
const  QVector<QPointF> &Atom::getVcell()const
{
    return poly;
}

/*!
 * Setter function:
 * sets the  VoronoiCell of the atom.
 */
 void Atom::setVcell(const QVector< QPointF > &_cell)
 {
     poly = _cell;
 }

 /*!
  * Getter function:
  * returns the nukber of direct neighbors od the atom.
  *
  */
int Atom::getNeighbors() const
{
return poly.size() -1;
}



/*
 * Calculate the distance for two Atoms
 * Euclidean Distance
 *
 */
double Atom::distance(Atom &a)
{
    double x1 = this->getX();
    double y1 = this->getY();
    double x2 = a.getX();
    double y2 = a.getY();
    double diffx =0;
    diffx = x1 - x2;
    double diffy =0;
    diffy = y1 - y2;
    double diffx_sqr = pow(diffx,2);
    double diffy_sqr = pow(diffy,2);
    double distance = sqrt(diffx_sqr + diffy_sqr);
    return distance;

}


void Atom::makeQpolygon(QPolygonF &qpoly)
{
    qpoly = poly;
}



/*
 * Type conversion from Atom to QPoint
 *
 *
 */
void Atom::toQpoint(QPointF &a)
{
    a.setX(x);
    a.setY(y);
}


bool Atom::matchCriterion(const Criterion crit)
{
    if( crit.name)
    {
        if(crit.isnot)
        {
            if(!crit.resname.compare(this->getResname()) == false)
                return true;
        }
        else
        {
            if(crit.resname.compare(this->getResname())==0)
                return true;
        }
    }
    else if( crit.area)
    {
        if(crit.isnot)
        {
            if( crit.d_val > (this->getArea()))
                return true;
            else
                return false;
        }
        else
        {
            if( crit.d_val < (this->getArea()))
                return true;
            else
                return false;
        }

     }
     else if( crit.neighb)
     {
        if(crit.isnot)
        {
            if(crit.intval > (this->getNeighbors()))
                return true;
            else
                return false;
        }
        else
        {
            if( crit.intval < (this->getNeighbors()))
                return true;
            else
                return false;
        }
    }
    else if( crit.select)
    {
        if (crit.isnot)
            return !crit.preselection.contains(this->atomnumber);
        else
            return crit.preselection.contains(this->atomnumber);
//        if(crit.isnot)
//        {
//            if(crit.boolval != (this->preselect))
//                return true;
//            else
//                return false;
//        }
//        else
//        {
//            if( crit.boolval == (this->preselect))
//                return true;
//            else
//                return false;
//        }
    }
    else if(crit.thick)
    {
        if(crit.isnot)
        {
            if( crit.d_val > (this->getThick()))
                return true;
            else
                return false;
        }
        else
        {
            if( crit.d_val < (this->getThick()))
                return true;
            else
                return false;
        }
    }
    return false;
}




bool Atom::matchCriterion_list_all(const QList<Criterion> critlist)
 {
   int counter = 0;
   int matchcount = critlist.size();
    for(int i = 0 ; i < matchcount; i++)
    {
        if(matchCriterion(critlist[i]))
            counter++;
    }
    return(counter == matchcount);
 }

bool Atom::matchCriterion_list_single(QList<Criterion> critlist)
 {

    for(int i = 0 ; i < critlist.size(); i++)
    {
        if(matchCriterion(critlist[i]))
            return true;
    }
    return false;
 }

void Atom::setpreselect(bool a)
{
    preselect = a;
}
