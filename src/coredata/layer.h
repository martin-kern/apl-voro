//**************************************************************
//                  Layer.h
//           Copyright (c) 2012 - 2013 by Gunther Lukat
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************

#ifndef LAYER_H
#define LAYER_H
#include "coredata/molecule.h"
#include "coredata/atom.h"
#include "geom/triangulation.h"
#include <QHash>
#include <QDataStream>
#include <QSet>



/*!
 \brief A Layer represents a top or bottom side of a bilayer. All atoms present in the layer are stored in a QHashMap.

*/
class Layer
{
public:

    Layer();
    ~Layer();

    QHash<int,Atom> QHside; /*!< key = Atom number (int) and value = Atom */
    double minimumx, minimumy, maximumx, maximumy, MinZ, MaxZ;  /*!< The minimum and maximum x/y/z coordinates of all atoms present in the QHside. */
    int offset;  /*!< The periodic boundary offset used to creatre periodic images of atoms. */
    double Minarea,Maxarea;  /*!< The smallest and highest calculated projected area of all atoms present in QHside */
    double Minthick, Maxthick; /*!< The smallest and highest interpolated local thickness of all atoms present in QHside */
    double **thickplot;  /*!< Array of doubles holding the data used to display a 3D plot of the layer. */
    double **Areaplot; /*!< Array of doubles holding the data used to display a 3D plot of the layer.*/

    bool side;  /*!< Indicator for the side of this layer */
    QVector< QPair<int, int> > doubles; /*!< Helper field to close Voronoi cells of atoms on the periodic image boundary. */
    QPointF2D pbcExtendSIDEX(const Atom &atom, double maxxX);
    QPointF2D pbcExtendSIDEY(const Atom &atom, double maxY);
    void pbcpCorrectFrom(Atom &atom, double maxX, double maxY);
    void graphit(int a, double maxXBorder, double maxYBorder, QHash< QString, QHash<int, Atom> > &protein, int vdwcheck, double upright, double downright);
    void precalc3dplots(int columns, int rows);
    void matchCriterion_List(QList<Criterion> critlist , QList<int> &found, bool and_or);
    double getAreaForResidue(QString Rname);
    double getSumAreaForResidue(QString Rname);
    double getThickForResidue(QString Rname);
    int getNBForResidue(QString Rname);
    double getAreaForList(QList<int> list);
    double getAreaForIndexList(QList<int> list);
    double getThickForList(QList<int> list);
    double getThickForIndexList(QList<int> list);
    int getNBForList(QList<int> list);
    double getSumAreaForList(QList<int> list);
    Triangulation *t;
    void calculate_avg_thickness();
    double avg_apl(){return _avg_apl;}
    double avg_thickness(){return _avg_thickness;}

    double area_for_selection(Critall crit);
    double sum_area_for_selection(Critall crit);
    double thickness_for_selection(Critall crit);

private:
    double _avg_apl, _avg_thickness;
};

 bool pbcSort(const PBCA &a,const PBCA &b );
#endif
