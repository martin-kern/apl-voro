******************** APL@Voro Version 3.2.0 Beta****************

  :) Written By Gunther Lukat and Martin Kern :(


This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.


Options  Type  Default  Description
 ------------------------------------------------------------
  -f    <.pdb>           Structure file in pdb format.
  -c    <.apl>           APL@Voro configuration file.
  -x    <.xtc/.trr>      GROMACS Trajectory file.
  -n    <.ndx>           GROMACS index file for protein selection.
  -sn   <.ndx>           GROMACS index file for lipid selection.
  -oa   <.xvg>           Output xvg file containing average area over frames.
  -ot   <.xvg>           Output xvg file containing average thickness over
                         frames.
  -of   <.txt>           Print frames to text file.
  -ofs  <.txt>           Print selection in frames to text file.
  -pbc  <int> (20)       Pbc-offset for Voronoi diagrams in percent.
  -box  <vector>         Comma separated Box dimensions of Structure file in nm.
                         (default means detect by CRYST RECORD)
  -mode <enum> (multi)   Algorithm for thickness calculation : multi, single
  -vdw  <float>          Offset for TPIM. Use vdw radii if option is not set.
  -ld   <int> (0)        Leaflet detection algorithm. 0: orientation-based, 1: position-based
  -pd   <int> (4)        Polynomial degree used for surface fit.
  -b    <int> (0)        First frame to be considered.
  -skip <int> (1)        Only consider every nr-th frame.
  -e    <int> (-1)       Last frame to be considered.
  -g          (no)       Use APL@Voro with GUI.
  -name                  Name of the simulation. The pdb file name will be used by default.
  -h                     Display help.
