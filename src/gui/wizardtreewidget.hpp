#ifndef WIZARDTREEWIDGET_HPP
#define WIZARDTREEWIDGET_HPP

#include <QtWidgets>
#include <vector>
#include "coredata/atom.h"

class WizardTreeWidget : public QTreeWidget
{
    Q_OBJECT
    Q_PROPERTY(QStringList atoms READ get_atoms)
    Q_PROPERTY(QStringList protein_names READ get_protein_names)

public:
    WizardTreeWidget(QWidget *parent);
    QStringList get_atoms();
    QStringList get_protein_names();
};

#endif // WIZARDTREEWIDGET_HPP
