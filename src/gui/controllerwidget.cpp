//**************************************************************
//
//           Copyright (c) 2012 - 2013 by Gunther Lukat
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************

#include "controllerwidget.h"

controllerWidget::controllerWidget(QWidget *parent)
    :QFrame (parent)
{
    setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
    setLineWidth(3);
    setMidLineWidth(3);
    setMaximumHeight(80);
    setMinimumHeight(80);
    maxframes = 0;
    time = 5001;

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(SliderUpdate()));

    frameSlider = new QSlider;
    frameSlider->setFocusPolicy(Qt::StrongFocus);
    frameSlider->setOrientation(Qt::Horizontal);
    frameSlider->setMinimum(0);
    frameSlider->setMaximum(200);
    frameSlider->setValue(0);
    frameSlider->setTickPosition(QSlider::TicksBelow);
    frameSlider->setTickInterval(1);
    frameSlider->setDisabled(true);

    framechooser = new QSpinBox;
    framechooser->setFocusPolicy(Qt::StrongFocus);
    framechooser->setMinimum(0);
    framechooser->setValue(0);
    framechooser->setDisabled(true);

    play = new QToolButton(this);
    play->setIconSize(QSize(30, 30));
    play->setFixedSize(32, 32);

    play->setObjectName(QString::fromUtf8("play"));
    play->setIcon(QIcon(QPixmap(":img/play.png")));
    play->setFocusPolicy(Qt::StrongFocus);
    play->setStyleSheet("QToolButton { border: none }");
    play->setDisabled(true);

    pause = new QToolButton(this);
    pause->setIconSize(QSize(30,30));
    pause->setFixedSize(32,32);
    pause->setFocusPolicy(Qt::StrongFocus);
    pause->setDisabled(true);
    pause->setStyleSheet("QToolButton { border: none }");

    pause->setObjectName(QString::fromUtf8("pause"));
    pause->setIcon(QIcon(QPixmap(":img/pause.png")));

    speedSlider = new QSlider;
    speedSlider->setOrientation(Qt::Horizontal);
    speedSlider->setFocusPolicy(Qt::StrongFocus);
    speedSlider->setMinimum(4000);
    speedSlider->setMaximum(5000);
    speedSlider->setTickInterval(100);
    speedSlider->setValue(2000);

    QGridLayout *controllerLayout = new QGridLayout(this);
    controllerLayout->addWidget(play, 0, 0);
    controllerLayout->addWidget(pause, 0, 1);
    controllerLayout->addWidget(frameSlider, 0, 2, 1, 4, nullptr);
    controllerLayout->addWidget(framechooser,0,6);
    controllerLayout->addWidget(speedSlider, 1, 3, 1, 2, nullptr);
    setLayout(controllerLayout);

    connect(frameSlider,SIGNAL(valueChanged(int)),this,SLOT(setChooser()));
    connect(framechooser,SIGNAL(valueChanged(int)),this,SLOT(setScene()));
    connect(play,SIGNAL(clicked()),this,SLOT(startSlider()));
    connect(pause,SIGNAL(clicked()),this,SLOT(stopSlider()));
}


int controllerWidget::getValue()
{
    return framechooser->value();
}


void controllerWidget::setValue(int a)
{
    framechooser->setValue(a);
}

void controllerWidget::setChooser()
{
    framechooser->setValue(frameSlider->value());
}

void controllerWidget::setScene()
{
    frameSlider->blockSignals(true);
    frameSlider->setValue(framechooser->value());
    framechooser->setValue(frameSlider->value());
    emit needredraw();
    frameSlider->blockSignals(false);

}


void controllerWidget::startSlider()
{
    timer->start(time-speedSlider->value());
}


void controllerWidget::stopSlider()
{
    timer->stop();
}


void controllerWidget::SliderUpdate()
{
    if(frameSlider->value() == maxframes)
        timer->stop();

    else
    {
        frameSlider->setValue(frameSlider->value()+1);
        timer->start(time-speedSlider->value());
    }
}
