#ifndef CRITERIONWIDGET_H
#define CRITERIONWIDGET_H
#include "criterionselector.h"
#include "coredata/atom.h"
#include "coredata/core.h"
#if QT_VERSION >= 0x050000
#include <QtWidgets>
#endif

class CriterionWidget: public QWidget
{
     Q_OBJECT
public:
    explicit CriterionWidget(QStringList wordlist, core *related_core, QSet<int> *preselection, QWidget *parent = nullptr);
//    QHash<QString, Critall> *criterialist;
    QStringList residues;
    QLineEdit *criterionName;

private:
    QLabel *descr1, *descr2;
    QPushButton *ok;
    QComboBox *and_or;
    QHBoxLayout *layouttop;
    CriterionSelector *choose1;
    QVBoxLayout *centrallayout;
    QVBoxLayout *layout;
    QHash<int, CriterionSelector*> wlist;
    int counter;
    core *related_core;
    QSet<int> *preselection;

private slots:
    void insertchooser();
    void remchooser(int a);
    void getCriterias();

signals:
    void  criteriacreated(QString a);
};

#endif // CRITERIONWIDGET_H
