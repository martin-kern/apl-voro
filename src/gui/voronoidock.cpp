#include "voronoidock.hpp"

VoronoiDock::VoronoiDock(unsigned int memID,
                         QHash<unsigned int, core*> *cores,
                         QString window_title,
                         int controller_frame,
                         int max_frame,
                         QWidget *parent)
    :QDockWidget(parent)
{
    // set variables
    this->memID = memID;
    side = 0;
    area_mode = 0;
    thickness_mode = 0;
    criterias = new QHash<QString, Critall>;
    vcores = cores;
    active_leaflet_ = "upside";
    simulation = (*cores)[memID];
    maxarea = 3;
    minarea = 0;
    maxthick = 5;
    minthick = 0;
    currentframe = 0;
    metric = 2;  // area

    linecolor = Qt::black;
    selectcolor = Qt::blue;
    bgcolor = Qt::white;

    // UI
    setWindowTitle(window_title);
    setContextMenuPolicy(Qt::NoContextMenu);

    lipid_properties = new QTreeWidget();
    QStringList property_headers;
    property_headers << "Types" << "Atomnumber" << "Residuenumber" << "Area (nm²)" << "Neighb."
                     << "Thick (nm)" << "Key Atom" << "X" << "Y" << "Z";
    lipid_properties->setHeaderLabels(property_headers);
    lipid_properties->setSelectionMode(QAbstractItemView::ExtendedSelection);
    lipid_properties->viewport()->installEventFilter(this);


    makeParents(&(*vcores)[memID]->lipids["upside"], &(*vcores)[memID]->peptidenames);
    voronoi_view = new VoronoiGraphicsView(this);

    // Selection tool, lipid properties and averages
    criteriaselection = new QComboBox;
    criteriaselection->addItem("No criteria");
    QList<QString> crit_names = simulation->get_crit_list()->keys();
    for (int i = 0; i < crit_names.length(); i++)
        criteriaselection->addItem(crit_names[i]);

    QPushButton *makeCriteria = new QPushButton;
    makeCriteria->setText("New selection model");
    QHBoxLayout *criterialayout = new QHBoxLayout;
    criterialayout->addWidget(criteriaselection);
    criterialayout->addWidget(makeCriteria);
    QVBoxLayout *treerun = new QVBoxLayout;
    treerun->addLayout(criterialayout);
    treerun->addWidget(lipid_properties);
    averages_table = new QTableWidget(4, 2, this);
    QString avg_char = QChar(0x2300);
    QString sigma = QChar(0x03C3);
    QStringList h_header_labels;
    h_header_labels << "total" << "selection";
    QStringList v_header_labels;
    v_header_labels << avg_char + " Area" << sigma << avg_char + " Thickness" << sigma;
    averages_table->setHorizontalHeaderLabels(h_header_labels);
    averages_table->setVerticalHeaderLabels(v_header_labels);
    averages_table->setMaximumHeight(200);

    treerun->addWidget(averages_table);
    QWidget *selection_tool = new QWidget(this);
    selection_tool->setLayout(treerun);

    QSplitter *sdown = new QSplitter(Qt::Horizontal);
    sdown->addWidget(selection_tool);
    sdown->addWidget(voronoi_view);
    sdown->setStretchFactor(1, 1);

    QVBoxLayout *viewLayout = new QVBoxLayout;
    viewLayout->addWidget(sdown);

    QMainWindow *voro_widget = new QMainWindow(parent);
    voro_widget->setCentralWidget(sdown);
    setWidget(voro_widget);
    view_preferences = new ViewPreferences(*voronoi_view, 2);

    connect(lipid_properties, &QTreeWidget::itemChanged, this, &VoronoiDock::TreeSelectionsUpdate);
    connect(lipid_properties, &QTreeWidget::itemSelectionChanged, this, &VoronoiDock::setCheck);
    connect(voronoi_view->scene(), &QGraphicsScene::selectionChanged, this, &VoronoiDock::setViewSelect);
    connect(makeCriteria, &QPushButton::clicked, this, &VoronoiDock::showCriteriamaker);

    connect(criteriaselection, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &VoronoiDock::selectCriteria);

    update(controller_frame, max_frame);
    show();
}


/**
 * @brief VoronoiDock::eventFilter
 * Apply custom color to voronoi cells using the lipid properties tree
 * @param obj
 * @param event
 * @return
 */
bool VoronoiDock::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::MouseButtonRelease)
    {
        QMouseEvent *mouse_event = static_cast<QMouseEvent *>(event);
        if (mouse_event->button() == Qt::RightButton)
        {
            QTreeWidgetItem *item = lipid_properties->itemAt(mouse_event->pos());
            if (item && item->parent() && ((item->parent()->text(0).compare("Lipids")==0) ||item->parent()->text(0).compare("Proteins")==0))
            {
//                QList<QTreeWidgetItem*> selected_items = lipid_properties->selectedItems();
                QColor cell_color = Qt::white;
                cell_color = QColorDialog::getColor(Qt::white, this);
                item->setBackground(0, cell_color);
                setCellColorsByType();
                return true;
            }
        }
    }
    return QObject::eventFilter(obj, event);
}


void VoronoiDock::update_globals()
{
    /* Update global max and minvalues when a new membrane is loaded.
     */

    globalminarea = 1000000;
    globalmaxarea = 0;
    globalminthick = 1000000;
    globalmaxthick = 0;

    if (area_mode == 0)
    {
        QList<unsigned int> core_keys = vcores->keys();
        for (int i = 0; i < core_keys.length(); i++)
        {
            globalminarea = min(globalminarea, (*vcores)[core_keys[i]]->min_area[active_leaflet_]);
            globalmaxarea = max(globalmaxarea, (*vcores)[core_keys[i]]->max_area[active_leaflet_]);
        }
    }
    else if (area_mode == 1)
    {
        globalminarea = (*vcores)[memID]->framemembrans[currentframe].leaflets[active_leaflet_]->Minarea;
        globalmaxarea = (*vcores)[memID]->framemembrans[currentframe].leaflets[active_leaflet_]->Maxarea;
    }
    else
        view_preferences->custom_area_margins(globalminarea, globalmaxarea);

    if (thickness_mode == 0)
    {
        QList<unsigned int> core_keys = vcores->keys();
        for (int i = 0; i < core_keys.length(); i++)
        {
            globalminthick = min(globalminthick, (*vcores)[core_keys[i]]->min_thickness[active_leaflet_]);
            globalmaxthick = max(globalmaxthick, (*vcores)[core_keys[i]]->max_thickness[active_leaflet_]);
        }
    }
    else if (thickness_mode == 1)
    {
        globalminthick = (*vcores)[memID]->framemembrans[currentframe].leaflets[active_leaflet_]->Minthick;
        globalmaxthick = (*vcores)[memID]->framemembrans[currentframe].leaflets[active_leaflet_]->Maxthick;
    }
    else
        view_preferences->custom_thickness_margins(globalminthick, globalmaxthick);
}


void VoronoiDock::get_tree_selections()
{
    numbers.clear();

    QTreeWidgetItemIterator it(lipid_properties);

    while(*it)
    {
        if((*it)->childCount() == 0 && (*it)->isSelected())
        {
            int molnumber = (*it)->data(1,0).toInt();
            numbers.push_back(molnumber);
        }
        it++;
    }
    std::sort(numbers.begin(), numbers.end());
}


void VoronoiDock::selectCriteria()
{
    Critall actualcrit;
    QString text = criteriaselection->currentText();
    if(text.compare("No criteria") != 0)
    {
        actualcrit = simulation->get_criteria(text);     
        voronoi_view->scene()->blockSignals(true);
        lipid_properties->blockSignals(true);

        for(QHash<int,VT> ::iterator it = cell_tree_map.begin(), end = cell_tree_map.end(); it != end; it++)
        {
            if(actualcrit.and_or)
            {
                if( it.value().atom->matchCriterion_list_all(actualcrit.critlist))
                {
                    it.value().cell->setSelected(true);
                    it.value().item->setSelected(true);
                }
                else
                {
                    it.value().cell->setSelected(false);
                    it.value().item->setSelected(false);
                }
            }
            else
            {
                if(it.value().atom->matchCriterion_list_single(actualcrit.critlist))
                {
                    it.value().cell->setSelected(true);
                    it.value().item->setSelected(true);
                }
                else
                {
                    it.value().cell->setSelected(false);
                    it.value().item->setSelected(false);
                }
            }
        }
        voronoi_view->scene()->blockSignals(false);
        lipid_properties->blockSignals(false);
    }
    fill_averages();
}


void VoronoiDock::fill_averages()
{
    double avg_area = 0, avg_thickness = 0, stdev_area = 0, stdev_thickness = 0, sel_area = 0, sel_thickness = 0, stdev_sel_area = 0, stdev_sel_thickness = 0;
    int total_count = 0, selected_count = 0;
    // averages
    for (QTreeWidgetItemIterator it(lipid_properties, QTreeWidgetItemIterator::NoChildren); *it; it++)
    {
        avg_area = avg_area + (*it)->text(3).toDouble();  // Area
        avg_thickness = avg_thickness + (*it)->text(5).toDouble();  // Thickness
        if ((*it)->isSelected())
        {
            sel_area = sel_area + (*it)->text(3).toDouble();  // Area
            sel_thickness = sel_thickness + (*it)->text(5).toDouble();  // Thickness
            selected_count++;
        }
        total_count++;
    }
    avg_area = avg_area / total_count;
    avg_thickness = avg_thickness / total_count;
    if (selected_count > 0)
    {
        sel_area = sel_area / selected_count;
        sel_thickness = sel_thickness / selected_count;
    }

    // standard deviation
    for (QTreeWidgetItemIterator it(lipid_properties, QTreeWidgetItemIterator::NoChildren); *it; it++)
    {
        stdev_area = stdev_area + pow((*it)->text(3).toDouble() - avg_area, 2);
        stdev_thickness = stdev_thickness + pow((*it)->text(5).toDouble() - avg_thickness, 2);
        if ((*it)->isSelected())
        {
            stdev_sel_area = stdev_sel_area + pow((*it)->text(3).toDouble() - sel_area, 2);
            stdev_sel_thickness = stdev_sel_thickness + pow((*it)->text(5).toDouble() - sel_thickness, 2);
        }
    }
    stdev_area = sqrt(stdev_area / total_count);
    stdev_thickness = sqrt(stdev_thickness / total_count);

    if (selected_count > 0)
    {
        QTableWidgetItem *sel_area_item = new QTableWidgetItem();
        sel_area_item->setData(Qt::DisplayRole, sel_area);
        QTableWidgetItem *sel_thickness_item = new QTableWidgetItem();
        sel_thickness_item->setData(Qt::DisplayRole, sel_thickness);
        averages_table->setItem(0, 1, sel_area_item);
        averages_table->setItem(2, 1, sel_thickness_item);

        stdev_sel_area = sqrt(stdev_sel_area / selected_count);
        stdev_sel_thickness = sqrt(stdev_sel_thickness / selected_count);
        QTableWidgetItem *sel_stdev_area_item = new QTableWidgetItem();
        sel_stdev_area_item->setData(Qt::DisplayRole, stdev_sel_area);
        QTableWidgetItem *sel_stdev_thickness_item = new QTableWidgetItem();
        sel_stdev_thickness_item->setData(Qt::DisplayRole, stdev_sel_thickness);
        averages_table->setItem(1, 1, sel_stdev_area_item);
        averages_table->setItem(3, 1, sel_stdev_thickness_item);
    }
    else
    {
        averages_table->setItem(0, 1, new QTableWidgetItem("N/A", Qt::DisplayRole));
        averages_table->setItem(1, 1, new QTableWidgetItem("N/A", Qt::DisplayRole));
        averages_table->setItem(2, 1, new QTableWidgetItem("N/A", Qt::DisplayRole));
        averages_table->setItem(3, 1, new QTableWidgetItem("N/A", Qt::DisplayRole));
    }

    QTableWidgetItem *area_item = new QTableWidgetItem();
    area_item->setData(Qt::DisplayRole, avg_area);
    QTableWidgetItem *thickness_item = new QTableWidgetItem();
    thickness_item->setData(Qt::DisplayRole, avg_thickness);

    QTableWidgetItem *stdev_area_item = new QTableWidgetItem();
    stdev_area_item->setData(Qt::DisplayRole, stdev_area);
    QTableWidgetItem *stdev_thickness_item = new QTableWidgetItem();
    stdev_thickness_item->setData(Qt::DisplayRole, stdev_thickness);

    averages_table->setItem(0, 0, area_item);
    averages_table->setItem(1, 0, stdev_area_item);
    averages_table->setItem(2, 0, thickness_item);
    averages_table->setItem(3, 0, stdev_thickness_item);
}


void VoronoiDock::setCellColorsByType()
{
    RCList.clear();

    QTreeWidgetItemIterator it(lipid_properties,
                               QTreeWidgetItemIterator::NotSelectable);
    while (*it)
    {
        if((*it)->parent()
                && ((*it)->parent()->text(0).compare("Lipids")==0
                    ||(*it)->parent()->text(0).compare("Proteins")==0))
        {
            QString aa =  (*it)->text(0);
            QBrush ff = (*it)->background(0);
            QColor col = ff.color();
            RCList.push_back(qMakePair(aa,col));
        }
        it++;
    }
    UpdateTreeandView();
}



void VoronoiDock::update(int controller_frame, int max_frame)
{
    int frame = 0;
    unsigned int *sync_frames = (*vcores)[memID]->get_sync_frames();
    if (max_frame != 0)
    {
        frame = (int(sync_frames[0]) + controller_frame * int(sync_frames[1] - sync_frames[0]) / max_frame);
    }
    currentframe = frame;
    UpdateTreeandView();
}


void VoronoiDock::UpdateTreeandView()
{
    // Called upon frame or leaflet change
    voronoi_view->scene()->blockSignals(true);
    lipid_properties->blockSignals(true);

    get_tree_selections();
    update_globals();

    Layer *actuallayer = (*vcores)[memID]->framemembrans[currentframe].leaflets[active_leaflet_];


    QList<int>atomlistkeys = actuallayer->QHside.keys();

    QList<int> oldnumbers = cell_tree_map.keys();
    for (int j = 0 ; j < oldnumbers.size(); j++)
    {
        // Remove lipids that will no longer be part of the layer
        if(!atomlistkeys.contains(oldnumbers[j]))
        {
            voronoi_view->scene()->removeItem(cell_tree_map[oldnumbers[j]].cell);
            delete cell_tree_map.find(oldnumbers[j]).value().cell;
            QTreeWidgetItem *item = cell_tree_map[oldnumbers[j]].item;
            QTreeWidgetItem *parent = item->parent();
            if (parent != nullptr)
                parent->removeChild(item);
            delete cell_tree_map.find(oldnumbers[j]).value().item;
            cell_tree_map.remove(oldnumbers[j]);
        }
    }

    set_margins(globalminarea,
                globalmaxarea,
                globalminthick,
                globalmaxthick);

    for (int i = 0; i < atomlistkeys.size(); i++)
    {
        VT nvt;
        // Cell already exists. Just update cell.
        if(cell_tree_map.find(atomlistkeys[i]) != cell_tree_map.end())
        {
            nvt = cell_tree_map.find(atomlistkeys[i]).value();
            nvt.atom = &actuallayer->QHside.find(atomlistkeys[i]).value();

            nvt.item->setData(2, Qt::DisplayRole, nvt.atom->getResNumber());
            nvt.item->setData(3, Qt::DisplayRole, nvt.atom->getArea());
            nvt.item->setData(4, Qt::DisplayRole, nvt.atom->getNeighbors());
            nvt.item->setData(5, Qt::DisplayRole, nvt.atom->getThick());
            nvt.item->setData(6, Qt::DisplayRole, nvt.atom->getName());
            nvt.item->setData(7, Qt::DisplayRole, nvt.atom->getX());
            nvt.item->setData(8, Qt::DisplayRole, nvt.atom->getY());
            nvt.item->setData(9, Qt::DisplayRole, nvt.atom->getZ());
        }
        else // Create new cell.
        {
            nvt.atom = &actuallayer->QHside.find(atomlistkeys[i]).value();

            /*Create CELL */
            nvt.cell = new Vcell(linecolor, selectcolor, bgcolor, nvt.atom);
            nvt.cell->setData(0, atomlistkeys[i]);
            nvt.cell->setData(1, bgcolor);
            nvt.cell->setData(2, nvt.atom->getResname());

            voronoi_view->scene()->addItem(nvt.cell);
            /*Create ITEM*/

            QString itemname = nvt.atom->getResname();

            nvt.item = new QTreeWidgetItem;
            nvt.item->setFlags(Qt::ItemIsEnabled|Qt::ItemIsSelectable);

            nvt.item->setText(0, itemname);
            nvt.item->setData(1, 0, atomlistkeys[i]);
            nvt.item->setData(4, 0, nvt.atom->getNeighbors());
            nvt.item->setData(2, 0, nvt.atom->getResNumber());
            nvt.item->setData(3, 0, nvt.atom->getArea());
            nvt.item->setData(5, 0, nvt.atom->getThick());
            nvt.item->setData(6, 0, nvt.atom->getName());
            nvt.item->setData(7, 0, nvt.atom->getX());
            nvt.item->setData(8, 0, nvt.atom->getY());
            nvt.item->setData(9, 0, nvt.atom->getZ());

            // Set elements to selected if top level item is checked
            for (int jt =0 ; jt < toplist.size(); jt++)
            {
                QString mname =  toplist[jt]->data(0,0).toString();

                if(mname.compare(itemname)==0)
                {

                    toplist[jt]->addChild(nvt.item);

                    if(toplist[jt]->checkState(1)==Qt::Checked)
                    {
                        nvt.item->setSelected(true);
                        nvt.cell->setSelected(true);
                    }
                    break;
                }
            }

            // Keep previous selection
            if (selection_ids.contains(nvt.atom->getNumber()))
            {
                nvt.item->setSelected(true);
                nvt.cell->setSelected(true);
            }
        }

        nvt.cell->pencolor = linecolor;
        nvt.cell->brushcolor = selectcolor;

        nvt.cell->setData(1, bgcolor);
        QColor rcolor;

        for(int ix = 0; ix < RCList.size(); ix++)
        {
            QString d = RCList[ix].first;
            std::string c = d.toStdString();
            std::string str((nvt.atom->getResname().toStdString()));

            if(c.compare(str)==0)
            {
                rcolor = (RCList[ix].second);
                break;
            }
        }

        nvt.calc_colors(rcolor,
                        globalmaxarea,
                        globalminarea,
                        globalmaxthick,
                        globalminthick);

        switch (metric)
        {
        case 1: //residue
            nvt.cell->setData(1, nvt.Residuecolor);
            break;
        case 2: //area
            nvt.cell->setData(1, nvt.Areacolor);
            nvt.item->setBackground(0, nvt.Areacolor);
            break;
        case 3: //neighbours
            nvt.cell->setData(1, nvt.NcColor);
            break;
        case 4: //thickness
            nvt.cell->setData(1, nvt.Thickcolor);
            break;
        }

        cell_tree_map.insert(nvt.atom->getNumber(), nvt);
        nvt.cell->changedat(nvt.atom);
    }
    selectCriteria();
    voronoi_view->info_update(active_leaflet_,
                              (*vcores)[memID]->framemembrans[currentframe].time,
                              (*vcores)[memID]->framemembrans[currentframe].framenr,
                              (*vcores)[memID]->framemembrans[currentframe].leaflets[active_leaflet_]->avg_apl(),
                              (*vcores)[memID]->framemembrans[currentframe].leaflets[active_leaflet_]->avg_thickness());


    switch (metric) {
    case 2: // area
        voronoi_view->_legend->set_labels(minarea, maxarea);
        break;
    case 3: // neighbours
        voronoi_view->_legend->set_labels(3, 12);
        break;
    case 4: // thickness
        voronoi_view->_legend->set_labels(minthick, maxthick);
        break;
    }
    setColors(metric);

    voronoi_view->scene()->blockSignals(false);
    lipid_properties->blockSignals(false);
}


void VoronoiDock::set_margins(double min_area, double max_area, double min_thickness, double max_thickness)
{
    this->minarea = max(0.0, min_area);
    this->maxarea = max(0.0, max_area);
    this->minthick = max(0.0, min_thickness);
    this->maxthick = max(0.0, max_thickness);
}


void VoronoiDock::makeParents(QStringList *Lipidlist, QStringList *chainnames)
{
    residues.clear();
    residues.append(*Lipidlist);
    residues.append(*chainnames);
    lipid_properties->clear();
    QTreeWidgetItem *top1 = new QTreeWidgetItem;
    top1->setText(0, "Proteins");
    top1->setBackground(0,Qt::white);
    lipid_properties->addTopLevelItem(top1);

    QTreeWidgetItem *top2 = new QTreeWidgetItem;
    top2->setText(0, "Lipids");
    top2->setBackground(0,Qt::white);
    lipid_properties->addTopLevelItem(top2);

    for(int i = 0 ; i < Lipidlist->size(); i++)
    {
        QString mname = (*Lipidlist)[i];
        QTreeWidgetItem *top = new QTreeWidgetItem;
        top->setFlags(Qt::ItemIsEnabled|Qt::ItemIsUserCheckable);
        top->setCheckState(1, Qt::Unchecked);
        top->setText(0, mname);
        top->setBackground(0, Qt::white);
        toplist.push_back(top);
        top2->addChild(top);
    }

    for(int i  = 0 ; i < chainnames->size() ; i++)
    {
        QString mname = (*chainnames)[i];

        QTreeWidgetItem *top = new QTreeWidgetItem;
        top->setFlags(Qt::ItemIsEnabled|Qt::ItemIsUserCheckable);
        top->setCheckState(1, Qt::Unchecked);
        top->setText(0, mname);
        top->setBackground(0, Qt::white);
        toplist.push_back(top);
        top1->addChild(top);
    }
}


void VoronoiDock::addCriteria(QString a)
{
    QStringList allitems;
    for(int i = 0 ; i < criteriaselection->count(); i++)
    {
        allitems.append(criteriaselection->itemText(i));
    }
    if(!allitems.contains(a))
        criteriaselection->addItem(a);
}


void VoronoiDock::showCriteriamaker()
{
    CriterionWidget *critmaker = new CriterionWidget(residues, (*vcores)[memID], &selection_ids);
    connect(critmaker, &CriterionWidget::criteriacreated, this, &VoronoiDock::addCriteria);
    QString name("Criterion ");
    int num = criteriaselection->count();
    QString numstr;
    numstr.setNum(num);
    name += numstr;
    critmaker->criterionName->setText(name);
    critmaker->show();
}


void VoronoiDock::setcoloringMethod(int coloring_id)
{
    metric = coloring_id;
    if(view_preferences->isVisible())
        view_preferences->coloringmethod = coloring_id;

    if (coloring_id == 1)  // residue
        setCellColorsByType();

    UpdateView(coloring_id);
    setColors(coloring_id);
    lipid_properties->blockSignals(false);
}


void VoronoiDock::UpdateView(int coloring_id)
{
    update_globals();
    QHash<int,VT>::iterator it;
    for(it = cell_tree_map.begin(); it != cell_tree_map.end(); it++)
    {
        VT *nvt = &it.value();
        nvt->cell->pencolor = linecolor;
        nvt->cell->brushcolor = selectcolor;

        nvt->cell->setData(1, bgcolor);
        QColor rcolor;


        for(int ix = 0; ix < RCList.size(); ix++)
        {

            QString d = RCList[ix].first;
            std::string c = d.toStdString();
            std::string str((nvt->atom->getResname().toStdString()));


            if(c.compare(str)==0)
            {
                rcolor = (RCList[ix].second);
                break;
            }
        }
        nvt->calc_colors(rcolor,
                         globalmaxarea,
                         globalminarea,
                         globalmaxthick,
                         globalminthick);

        set_margins(globalminarea,
                    globalmaxarea,
                    globalminthick,
                    globalmaxthick);

        switch (coloring_id) {
        case 1:
            nvt->cell->setData(1, nvt->Residuecolor);
            break;
        case 2:
            nvt->cell->setData(1, nvt->Areacolor);
            break;
        case 3:
            if (!nvt->cell->isSelected())
                nvt->cell->setData(1, nvt->NcColor);
            break;
        case 4:
            nvt->cell->setData(1, nvt->Thickcolor);
            break;
        }

        nvt->cell->changedat(nvt->atom);
     }

     selectCriteria();
     switch (metric) {
     case 2: // area
         voronoi_view->_legend->set_labels(minarea, maxarea);
         break;
     case 3: // neighbours
         voronoi_view->_legend->set_labels(3, 12);
         break;
     case 4: // thickness
         voronoi_view->_legend->set_labels(minthick, maxthick);
         break;
     }
}


void VoronoiDock::set_leaflet(QString leaflet)
{
    cell_tree_map.clear();
    toplist.clear();
    active_leaflet_ = leaflet;
    if (leaflet == "upside")
        side = 0;
    else
        side = 1;

    QList<QGraphicsItem *> list = voronoi_view->scene()->items();
    QList<QGraphicsItem *>::Iterator it;
    for ( it = list.begin(); it != list.end(); ++it )
    {
        if ( *it && qgraphicsitem_cast<Vcell*>(*it))  // Only remove voronoi cells
        {
            voronoi_view->scene()->removeItem(*it);
            delete *it;
        }
    }

    makeParents(&(*vcores)[memID]->lipids[active_leaflet_],
                &(*vcores)[memID]->peptidenames);
    UpdateTreeandView();
}


//void VoronoiDock::setValues(double _maxarea,
//                            double _minarea,
//                            double _maxthick,
//                            double _minthick)
//{
//    maxarea = _maxarea;
//    minarea = _minarea;
//    minthick = _minthick;
//    maxthick =_maxthick;
//}


void VoronoiDock::show_color_options()
{
    view_preferences->coloringmethod = metric;
    view_preferences->setWindowFlags(Qt::Window);
    view_preferences->show();
}


void VoronoiDock::TreeSelectionsUpdate(QTreeWidgetItem *item, int)
{
    lipid_properties->blockSignals(true);
    voronoi_view->scene()->blockSignals(true);

    if(item->childCount() != 0)
    {
        if(item->checkState(1)==Qt::Checked )
        {
            for (int i = 0 ; i < item->childCount(); i++)
            {
                item->child(i)->setSelected(true);
                int num = item->child(i)->data(1,0).toInt();
                cell_tree_map[num].cell->setSelected(true);
            }
        }
        else if(item->checkState(1)==Qt::Unchecked )
        {
            for (int i = 0 ; i < item->childCount(); i++)
            {
                item->child(i)->setSelected(false);
                int num = item->child(i)->data(1,0).toInt();
                cell_tree_map[num].cell->setSelected(false);
            }
        }
        fill_averages();
    }

    lipid_properties->blockSignals(false);
    voronoi_view->scene()->blockSignals(false);
}


void VoronoiDock::setCheck()
{
    voronoi_view->scene()->blockSignals(true);
    for(QHash<int,VT>::Iterator it = cell_tree_map.begin();
        it != cell_tree_map.end(); it++)
    {
        if(it->item->isSelected())
            it->cell->setSelected(true);
        else
            it->cell->setSelected(false);
    }
    fill_averages();
    voronoi_view->scene()->blockSignals(false);
}


void VoronoiDock::setViewSelect()
{
    criteriaselection->setCurrentIndex(0);
    voronoi_view->scene()->blockSignals(true);
    lipid_properties->blockSignals(true);

    QList<QGraphicsItem*>items = voronoi_view->scene()->items();
    int x = items.size();

    for(int i = 0; i< x; i++)
    {       
        int cell = items[i]->data(0).toInt();
        if(items[i]->isSelected())
        {
            if (Vcell *voronoi_cell = qgraphicsitem_cast<Vcell*>(items[i]))
                selection_ids.insert(voronoi_cell->a->getNumber());

            if(cell_tree_map.find(cell) != cell_tree_map.end())
            {
                cell_tree_map.find(cell)->selected = true;
                cell_tree_map.find(cell)->item->setSelected(true);
            }
        }
        else if(items[i]->isSelected()==false)
        {
            if (Vcell *voronoi_cell = qgraphicsitem_cast<Vcell*>(items[i]))
                selection_ids.remove(voronoi_cell->a->getNumber());

            if(cell_tree_map.find(cell) != cell_tree_map.end())
            {
                cell_tree_map.find(cell)->selected = false;
                cell_tree_map.find(cell)->item->setSelected(false);
            }
        }
    }
    fill_averages();
    voronoi_view->scene()->blockSignals(false);
    lipid_properties->blockSignals(false);
}


void VoronoiDock::exportImage()
{
    QString fp = QDir::homePath();
    const QList<QByteArray> imageFormats = QImageWriter::supportedImageFormats();
    QStringList filter;

    if ( imageFormats.size() > 0 )
    {
        for ( int i = 0; i < imageFormats.size(); i++ )
        {
            QString imageFilter;
            if ( i > 0 )
                imageFilter += " ";

            imageFilter += "*.";
            imageFilter += imageFormats[i];
            QString up;
            up += imageFormats[i].toUpper();
            up += "(";
            imageFilter += ")";
            up += imageFilter;
            filter += up;
        }
    }

    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Image File Name"),
                                                    fp,
                                                    filter.join(";;"));
    if (!fileName.isEmpty())
    {
        QFileInfo fi(fileName);
        QString ext = fi.suffix();
        const char myChar = *(ext.toStdString().c_str());

        QSize size = voronoi_view->rect().size();
        size.scale(2592, 1944, Qt::KeepAspectRatio);

        QImage img2(size, QImage::Format_ARGB32);
        QPainter p2(&img2);
        p2.setRenderHint(QPainter::Antialiasing);

        voronoi_view->render(&p2);
        p2.end();
        QImage img3 = img2;
        int dpm = int(600.0 / 0.0254); // ~600 DPI
        img3.setDotsPerMeterX(dpm);
        img3.setDotsPerMeterY(dpm);
        img3.save(fileName, &myChar);
    }
}



void VoronoiDock::exportDat()
{
    QStringList allitems;
    for(int i = 0 ; i < criteriaselection->count(); i++)
    {
        allitems.append(criteriaselection->itemText(i));
    }
    get_tree_selections();
    FramePrinterWidget *exportingwidget = new FramePrinterWidget;
    exportingwidget->populate(&allitems,
                              (*vcores)[memID]->framemembrans.size(),
                              currentframe,
                              criterias,
                              &numbers,
                              (*vcores)[memID],
                              side);
    exportingwidget->show();
}


void VoronoiDock::setColors(int method)
{
    QHash<int,VT>::iterator it;
    for(it = cell_tree_map.begin(); it != cell_tree_map.end(); it++)
    {
        switch (method)
        {
        case 0:
            it->item->setBackground(0,Qt::white);
            break;
        case 1:   // residue
            it->item->setBackground(0,it->Residuecolor);
            break;
        case 2:  // area
            it->item->setBackground(0,it->Areacolor);
            break;
        case 3:  // neighbours
            it->item->setBackground(0,it->NcColor);
            break;
        case 4:  // thickness
            it->item->setBackground(0,it->Thickcolor);
            break;
        }
    }
}


//===================================================================================
//===================================================================================
//===================================================================================


VoronoiGraphicsView::VoronoiGraphicsView(VoronoiDock *parent) : QGraphicsView(parent)
{
    this->_parent = parent;
    zoom_level = 0;
    scale_factor = 1;
    _drag = false;
    setScene(new QGraphicsScene());
    setBackgroundBrush(Qt::white);
    setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setRenderHint(QPainter::Antialiasing, true);
    setTransform(QTransform(scale_factor, 0, 0, -scale_factor, 0, 0));
    _grid = new Grid(QColor(0, 0, 0, 100));
    _grid->set_bounds(viewport()->rect());
    scene()->addItem(_grid);
    _sim_info_item = new SimInfoItem((*parent->vcores)[parent->memID]->name());
    scene()->addItem(_sim_info_item);

    _legend = new Legend(0, 10);
    scene()->addItem(_legend);

    QPalette hl_pallete;
    hl_pallete.setColor(QPalette::Window, Qt::white);
    hl_pallete.setColor(QPalette::WindowText, Qt::black);
    _hover_label = new QLabel("", this);
    _hover_label->setAutoFillBackground(true);
    _hover_label->setPalette(hl_pallete);
    _hover_label->hide();

    // context menu
    _context_menu = new QMenu();
    QAction *coloring_action = _context_menu->addAction("Settings");
    connect(coloring_action, &QAction::triggered, parent, &VoronoiDock::show_color_options);
    QMenu *_leaflet_selection = _context_menu->addMenu("Leaflet");
    QStringList leaflets = (*parent->vcores)[parent->memID]->get_leaflet_names();
    for (int i = 0; i < leaflets.size(); i++)
    {
        QAction *leaflet_action = _leaflet_selection->addAction(leaflets[i]);
        connect(leaflet_action, &QAction::triggered, this, [=](){parent->set_leaflet(leaflets[i]);});
    }
    QMenu *_metric_selection = _context_menu->addMenu("Metric");
    QAction *none_action = _metric_selection->addAction("None");
    QAction *residue_action = _metric_selection->addAction("Residue");
    QAction *apl_action = _metric_selection->addAction("Area");
    QAction *neighbor_action = _metric_selection->addAction("Neighbors");
    QAction *thickness_action = _metric_selection->addAction("Thickness");
    connect(none_action, &QAction::triggered, parent, [=](){parent->setcoloringMethod(0);});
    connect(residue_action, &QAction::triggered, parent, [=](){parent->setcoloringMethod(1);});
    connect(apl_action, &QAction::triggered, parent, [=](){parent->setcoloringMethod(2);});
    connect(neighbor_action, &QAction::triggered, parent, [=](){parent->setcoloringMethod(3);});
    connect(thickness_action, &QAction::triggered, parent, [=](){parent->setcoloringMethod(4);});


    QAction *_toggle_grid_action = _context_menu->addAction("Grid");
    _toggle_grid_action->setCheckable(true);
    _toggle_grid_action->setChecked(true);
    QAction *_toggle_legend_action = _context_menu->addAction("Legend");
    _toggle_legend_action->setCheckable(true);
    _toggle_legend_action->setChecked(true);
    QAction *_export_img_action = _context_menu->addAction("Export Image");
    QAction *_export_file_action = _context_menu->addAction("Export txt/xml");
    QAction *center_action = _context_menu->addAction("Center");
    connect(_toggle_grid_action, &QAction::triggered, this, &VoronoiGraphicsView::toggle_grid);
    connect(_export_img_action,  &QAction::triggered, parent, &VoronoiDock::exportImage);
    connect(_export_file_action, &QAction::triggered, parent, &VoronoiDock::exportDat);
    connect(_toggle_legend_action, &QAction::triggered, this, &VoronoiGraphicsView::toggle_legend);
    connect(center_action, &QAction::triggered, this, &VoronoiGraphicsView::_center_voronoi);
    // TODO: get autocomputeSceneSize to work correctly and use it
//    connect(scene(), &QGraphicsScene::changed, this, &VoronoiGraphicsView::autocomputeSceneSize);
}


void VoronoiGraphicsView::info_update(QString leaflet, float time, int frame, double area, double thickness)
{
    _sim_info_item->info_update(leaflet, time, frame, zoom_level, area, thickness);
}


void VoronoiGraphicsView::_center_voronoi()
{
    int frame = _parent->currentframe;
    unsigned int mem_id = _parent->memID;
    double width = (*_parent->vcores)[mem_id]->framemembrans[frame].maxX;
    double height = (*_parent->vcores)[mem_id]->framemembrans[frame].maxY;
    centerOn(width / 2, height / 2);
    scene()->update(mapToScene(rect()).boundingRect());
    _update_grid();
    _update_voro_info();
    _update_legend();
}


void VoronoiGraphicsView::resizeEvent(QResizeEvent *event)
{
    QSize viewport_size = event->size();
    int vp_width = viewport_size.width();
    int vp_height = viewport_size.height();
    QRectF expanded_scene_rect(mapToScene(-vp_width, -vp_height), mapToScene(rect().bottomRight() + QPoint(vp_width, vp_height)));
    setSceneRect(expanded_scene_rect);
    _center_voronoi();
    QGraphicsView::resizeEvent(event);
}


void VoronoiGraphicsView::wheelEvent(QWheelEvent *event)
{
    if(event->angleDelta().y() > 0) // Zoom in
    {
        zoom_level++;
        scale_factor = scale_factor * 1.1;
    }
    else
    {
        zoom_level--;
        scale_factor = scale_factor / 1.1;
    }
    // eliminate errors from floating point arithmetics
    if (zoom_level == 0)
        scale_factor = 1;

    _sim_info_item->zoom_update(zoom_level);
    setTransform(QTransform(scale_factor, 0, 0, -scale_factor, 0, 0));
    _update_grid();
    _update_voro_info();
    _update_legend();
}


void VoronoiGraphicsView::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::MiddleButton)
    {
        QMouseEvent fake(event->type(), event->pos(), Qt::LeftButton, Qt::LeftButton, event->modifiers());
        setDragMode(QGraphicsView::ScrollHandDrag);
        setInteractive(false);
        _drag = true;
        QGraphicsView::mousePressEvent(&fake);
    }
    else if (event->button() == Qt::RightButton)
    {
        _context_menu->popup(mapToGlobal(event->pos()));
    }
    else if (event->button() == Qt::LeftButton)
    {
        setDragMode(QGraphicsView::RubberBandDrag);
        QGraphicsView::mousePressEvent(event);
    }
    else
        QGraphicsView::mousePressEvent(event);
}


void VoronoiGraphicsView::mouseMoveEvent(QMouseEvent* event)
{
    if (_drag)
    {
        scene()->update(mapToScene(rect()).boundingRect());
        QMouseEvent fake(event->type(), event->pos(), Qt::LeftButton, Qt::LeftButton, event->modifiers());
        QGraphicsView::mouseMoveEvent(&fake);
        _hover_label->move(event->x() + 20, event->y());
        _update_grid();
        _update_voro_info();
        _update_legend();
    }
    else
    {
        QList<QGraphicsItem*> scene_items = scene()->items(mapToScene(event->pos()), Qt::IntersectsItemShape, Qt::AscendingOrder);
        if (!scene_items.isEmpty())
        {
            if (Vcell *voronoi_cell = qgraphicsitem_cast<Vcell*>(scene_items[0]))
            {
                QString hl_text = QString("Lipid: %1\nArea: %2\nThickness: %3").arg(voronoi_cell->a->getResname()).arg(voronoi_cell->a->getArea()).arg(voronoi_cell->a->getThick());
                _hover_label->setText(hl_text);
                _hover_label->move(event->x() + 20, event->y());
                _hover_label->show();
            }
            else
                _hover_label->hide();
        }
        else
            _hover_label->hide();

        QGraphicsView::mouseMoveEvent(event);
    }
}


void VoronoiGraphicsView::mouseReleaseEvent(QMouseEvent *event)
{
    if (event->button() == Qt::MiddleButton)
    {
        QMouseEvent fake(event->type(), event->pos(), Qt::LeftButton, Qt::LeftButton, event->modifiers());
        setDragMode(QGraphicsView::NoDrag);
        setInteractive(true);
        _drag = false;
        QGraphicsView::mouseReleaseEvent(&fake);
    }
    else if (event->button() == Qt::LeftButton)
    {
        QGraphicsView::mouseReleaseEvent(event);
        setDragMode(QGraphicsView::NoDrag);
    }
    else
        QGraphicsView::mouseReleaseEvent(event);
}


void VoronoiGraphicsView::autocomputeSceneSize(const QList<QRectF>& /* region */)
{
    // Change the size of scene rect dynamically to enable endless panning
    // Widget viewport recangle
    QRectF widget_rect_in_scene(mapToScene(-500, -500), mapToScene(rect().bottomRight() + QPoint(500, 500)));
    // Copy the new size from the old one
    QPointF new_top_left(sceneRect().topLeft());
    QPointF new_bottom_right(sceneRect().bottomRight());

    // Check that the scene has a bigger limit in the top side
    if (sceneRect().top() > widget_rect_in_scene.top())
        new_top_left.setY(widget_rect_in_scene.top());

    // Check that the scene has a bigger limit in the bottom side
    if (sceneRect().bottom() < widget_rect_in_scene.bottom())
        new_bottom_right.setY(widget_rect_in_scene.bottom());

    // Check that the scene has a bigger limit in the left side
    if (sceneRect().left() > widget_rect_in_scene.left())
        new_top_left.setX(widget_rect_in_scene.left());

    // Check that the scene has a bigger limit in the right side
    if (sceneRect().right() < widget_rect_in_scene.right())
        new_bottom_right.setX(widget_rect_in_scene.right());

    // Set new scene size
    setSceneRect(QRectF(new_top_left, new_bottom_right));
}


void VoronoiGraphicsView::read_selection_file()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open selection index file"),
                                                    "/home",
                                                    tr("INDEX (*.ndx)"));
    if (fileName.isEmpty())
        return;
    QFile index_file(fileName);
    index_parser *ip = new index_parser(index_file);

    delete(ip);
}


void VoronoiGraphicsView::toggle_grid()
{
    _grid->toggle_grid();
    scene()->update();
}


void VoronoiGraphicsView::toggle_legend()
{
    if (_legend->isVisible())
        _legend->setVisible(false);
    else
        _legend->setVisible(true);
    scene()->update();
}


void VoronoiGraphicsView::toggle_info()
{
    if (_sim_info_item->isVisible())
        _sim_info_item->setVisible(false);
    else
        _sim_info_item->setVisible(true);
    scene()->update();
}


void VoronoiGraphicsView::_update_grid()
{
    _grid->scale_factor = scale_factor;
    QRect vp_rect = viewport()->rect();
    QPointF a = mapToScene(vp_rect.topLeft());
    QPointF b = mapToScene(vp_rect.bottomRight());
    _grid->set_bounds(a.x(), b.y(), b.x() - a.x(),  -b.y() + a.y());
//    _grid->set_bounds(vp_rect);
}


void VoronoiGraphicsView::_update_voro_info()
{
    QPointF vp_top_right = mapToScene(viewport()->rect().topRight());
    QPointF pos = QPointF(vp_top_right.x() - _sim_info_item->boundingRect().width() / scale_factor, vp_top_right.y());
    _sim_info_item->setTransform(QTransform(1 / scale_factor, 0, 0, -1 / scale_factor, pos.x(), pos.y()));
}



void VoronoiGraphicsView::_update_legend()
{
    QPointF vp_top_left = mapToScene(viewport()->rect().topLeft());
    QPointF pos = QPointF(vp_top_left.x() + 50 / scale_factor, vp_top_left.y());
    _legend->setTransform(QTransform(1 / scale_factor, 0, 0, -1 / scale_factor, pos.x(), pos.y()));
}


void VoronoiGraphicsView::set_line_color(QColor lc)
{
    _parent->linecolor = lc;
    _parent->UpdateTreeandView();
}


void VoronoiGraphicsView::set_selection_color(QColor sc)
{
    _parent->selectcolor = sc;
    _parent->UpdateTreeandView();
}

//===================================================================================
//===================================================================================
//===================================================================================


ViewPreferences::ViewPreferences(VoronoiGraphicsView &view_1,
                                 int method,
                                 QWidget *parent)
    :QWidget(parent)
{
    int frameStyle = QFrame::Sunken | QFrame::Panel;
    //Labels that display the current color settings.
    backgroundlabel = new QLabel;
    selectionColorlabel = new QLabel;
    linecolorlabel = new QLabel;
    gridcolorlabel = new QLabel;
    QLabel *colorScalePreview = new QLabel;
    QLabel *gridespacing = new QLabel;

    //Set frame styles for each label
    backgroundlabel->setFrameStyle(frameStyle);
    selectionColorlabel->setFrameStyle(frameStyle);
    linecolorlabel->setFrameStyle(frameStyle);
    gridcolorlabel->setFrameStyle(frameStyle);
    gridespacing->setFrameStyle(frameStyle);
    colorScalePreview->setFrameStyle(frameStyle);

    //Buttons for changing color settings and a spin box to change grid spacing
    QPushButton *getbgcolor = new QPushButton;
    QPushButton *getSelectionColor = new QPushButton;
    QPushButton *getlinecolor = new QPushButton;
    gridselector = new QSpinBox;
    gridselector->setMaximum(10000);
    gridselector->setMinimum(1);
    gridselector->setValue(10);
    QPushButton *getgridcolor = new QPushButton;

    //Combo box for color scale selection.
    QStringList colorScaleList;
    colorScaleList.append("LOCS");
    colorScaleList.append("Linear Gray");
    colorScaleList.append("Heated Object");
    colorScaleList.append("Rainbow");
    colorScaleCB = new QComboBox();
    colorScaleCB->addItems(colorScaleList);

    //Add text to the buttons
    getbgcolor->setText("BackgroundColor");
    getSelectionColor->setText("SelectionColor");
    getlinecolor->setText("LineColor");
    getgridcolor->setText("Gridcolor");

    //Obtain background colors for labels
    //TODO: Make color scale preview work
    voronoi_view = &view_1;
    bgcolor = voronoi_view->_parent->bgcolor;
    linecolor = voronoi_view->_parent->linecolor;
    gridcolor = voronoi_view->_parent->gridcolor;
    selectcolor = voronoi_view->_parent->selectcolor;

    //Set label texts. Color scale doesn't get a text.
    linecolorlabel->setText(linecolor.name());
    gridcolorlabel->setText(gridcolor.name());
    backgroundlabel->setText(bgcolor.name());
    selectionColorlabel->setText(selectcolor.name());
    gridespacing->setText("grid spacing in nm");

    //Set label background colors.
    linecolorlabel->setPalette(QPalette(linecolor));
    gridcolorlabel->setPalette(QPalette(gridcolor));
    backgroundlabel->setPalette(QPalette(bgcolor));
    selectionColorlabel->setPalette(QPalette(selectcolor));

    //Apply background colors.
    linecolorlabel->setAutoFillBackground(true);
    gridcolorlabel->setAutoFillBackground(true);
    backgroundlabel->setAutoFillBackground(true);
    colorScalePreview->setAutoFillBackground(true);
    selectionColorlabel->setAutoFillBackground(true);

    //Vertical alignment for all labels.
    QVBoxLayout *labelslayout = new QVBoxLayout;
    labelslayout->addWidget(backgroundlabel);
    labelslayout->addWidget(selectionColorlabel);
    labelslayout->addWidget(linecolorlabel);
    labelslayout->addWidget(gridcolorlabel);
    labelslayout->addWidget(colorScalePreview);
    labelslayout->addWidget(gridespacing);

    //Vertical alignment for all buttons and the spin box.
    QVBoxLayout *buttonslayout = new QVBoxLayout;
    buttonslayout->addWidget(getbgcolor);
    buttonslayout->addWidget(getSelectionColor);
    buttonslayout->addWidget(getlinecolor);
    buttonslayout->addWidget(getgridcolor);
    buttonslayout->addWidget(colorScaleCB);
    buttonslayout->addWidget(gridselector);

    QHBoxLayout *editlayout = new QHBoxLayout;
    editlayout->addLayout(labelslayout);
    editlayout->addLayout(buttonslayout);

    // Color margins

    QLabel *color_margins = new QLabel("Coloring margins:");
    QButtonGroup *area_grp = new QButtonGroup();
    area_global = new QRadioButton("global area margins ()");
    area_local = new QRadioButton("local area margins");
    area_custom = new QRadioButton("custom area margins");
    area_cmax_sb = new QDoubleSpinBox();
    area_cmax_sb->setRange(0, 1000);
    area_cmax_sb->setValue(10);  //TODO
    area_cmax_sb->setDisabled(true);
    area_cmin_sb = new QDoubleSpinBox();
    area_cmin_sb->setRange(0, 1000);
    area_cmin_sb->setDisabled(true);
    QHBoxLayout *area_custom_hbl = new QHBoxLayout();
    area_custom_hbl->addWidget(area_cmin_sb);
    area_custom_hbl->addWidget(area_cmax_sb);
    area_global->setChecked(true);
    area_grp->addButton(area_global);
    area_grp->addButton(area_local);
    area_grp->addButton(area_custom);
    QButtonGroup *thickness_grp = new QButtonGroup();
    thickness_global = new QRadioButton("global thickness margins ()");
    thickness_local = new QRadioButton("local thickness margins");
    thickness_custom = new QRadioButton("custom thickness margins");
    thickness_cmax_sb = new QDoubleSpinBox();
    thickness_cmax_sb->setRange(0, 1000);
    thickness_cmax_sb->setDisabled(true);
    thickness_cmin_sb = new QDoubleSpinBox();
    thickness_cmin_sb->setRange(0, 1000);
    thickness_cmin_sb->setDisabled(true);
    QHBoxLayout *thickness_custom_hbl = new QHBoxLayout();
    thickness_custom_hbl->addWidget(thickness_cmin_sb);
    thickness_custom_hbl->addWidget(thickness_cmax_sb);
    thickness_global->setChecked(true);
    thickness_grp->addButton(thickness_global);
    thickness_grp->addButton(thickness_local);
    thickness_grp->addButton(thickness_custom);
    QVBoxLayout *area_margins_layout = new QVBoxLayout();
    area_margins_layout->addWidget(area_global);
    area_margins_layout->addWidget(area_local);
    area_margins_layout->addWidget(area_custom);
    area_margins_layout->addLayout(area_custom_hbl);
    QVBoxLayout *thickness_margins_layout = new QVBoxLayout();
    thickness_margins_layout->addWidget(thickness_global);
    thickness_margins_layout->addWidget(thickness_local);
    thickness_margins_layout->addWidget(thickness_custom);
    thickness_margins_layout->addLayout(thickness_custom_hbl);
    QHBoxLayout *margins_layout = new QHBoxLayout();
    margins_layout->addLayout(area_margins_layout);
    margins_layout->addLayout(thickness_margins_layout);

    connect(area_grp, SIGNAL(buttonClicked(int)), this, SLOT(change_area_margins(int)));
    connect(thickness_grp, SIGNAL(buttonClicked(int)), this, SLOT(change_thickness_margins(int)));

    connect(area_cmax_sb, SIGNAL(valueChanged(double)), this, SLOT(update_margins(double)));
    connect(area_cmin_sb, SIGNAL(valueChanged(double)), this, SLOT(update_margins(double)));
    connect(thickness_cmax_sb, SIGNAL(valueChanged(double)), this, SLOT(update_margins(double)));
    connect(thickness_cmin_sb, SIGNAL(valueChanged(double)), this, SLOT(update_margins(double)));

    QVBoxLayout *marginsUplayout = new QVBoxLayout;
    marginsUplayout->addWidget(color_margins);
    marginsUplayout->addLayout(margins_layout);

    QFrame *upframe = new QFrame;
    upframe->setFrameStyle(QFrame::StyledPanel);
    upframe->setLayout(marginsUplayout);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addLayout(editlayout);
    layout->addWidget(upframe);

    setLayout(layout);
    coloringmethod = method;

    connect(getbgcolor, SIGNAL(clicked()), this, SLOT(changebgcolor()));
    connect(getgridcolor, SIGNAL(clicked()), this, SLOT(changegridcolor()));
    connect(gridselector, SIGNAL(valueChanged(int)), this, SLOT(changegridsize()));
    connect(colorScaleCB, SIGNAL(currentIndexChanged(int)), this, SLOT(changeColorScale()));
    connect(getlinecolor, SIGNAL(clicked()), this, SLOT(changelinecolor()));
    connect(getSelectionColor, SIGNAL(clicked()), this, SLOT(changeselectcolor()));
}


void ViewPreferences::changeColorScale()
{
    QString currentText = colorScaleCB->currentText();
    if (currentText == "Linear Gray")
        ColorGradients::setColorScale(GRAY);
    else if (currentText == "Heated Object")
        ColorGradients::setColorScale(HEATED);
    else if (currentText == "Rainbow")
        ColorGradients::setColorScale(RAINBOW);
    else
        ColorGradients::setColorScale(LOCS);
    voronoi_view->_legend->set_gradient();
    voronoi_view->_parent->UpdateTreeandView();
}


void ViewPreferences::changelinecolor()
{
    linecolor = QColorDialog::getColor(Qt::black, this);
    if (linecolor.isValid())
    {
        linecolorlabel->setText(linecolor.name());
        linecolorlabel->setPalette(QPalette(linecolor));
        linecolorlabel->setAutoFillBackground(true);
        voronoi_view->set_line_color(linecolor);
    }
}


void ViewPreferences::changebgcolor()
{
    bgcolor = QColorDialog::getColor(Qt::black, this);
    if (bgcolor.isValid())
    {
        backgroundlabel->setText(bgcolor.name());
        backgroundlabel->setPalette(QPalette(bgcolor));
        backgroundlabel->setAutoFillBackground(true);
        voronoi_view->setBackgroundBrush(bgcolor);
    }
}


void ViewPreferences::changeselectcolor()
{
    selectcolor = QColorDialog::getColor(Qt::black, this);
    if (selectcolor.isValid())
    {
        selectionColorlabel->setText(selectcolor.name());
        selectionColorlabel->setPalette(QPalette(selectcolor));
        selectionColorlabel->setAutoFillBackground(true);
        voronoi_view->set_selection_color(selectcolor);
    }
}


void ViewPreferences::changegridcolor()
{
    gridcolor = QColorDialog::getColor(Qt::black, this);
    if (gridcolor.isValid())
    {
        gridcolorlabel->setText(gridcolor.name());
        gridcolorlabel->setPalette(QPalette(gridcolor));
        gridcolorlabel->setAutoFillBackground(true);
        gridcolor.setAlpha(100);
        voronoi_view->_grid->set_color(gridcolor);
    }
}


void ViewPreferences::changegridsize()
{
    voronoi_view->_grid->set_grid_spacing(gridselector->value());
}


void ViewPreferences::change_area_margins(int)
{
    voronoi_view->blockSignals(true);

    if (area_global->isChecked())
    {
        area_cmin_sb->setDisabled(true);
        area_cmax_sb->setDisabled(true);
        voronoi_view->_parent->set_area_mode(0);
    }
    else if (area_local->isChecked())
    {
        area_cmin_sb->setDisabled(true);
        area_cmax_sb->setDisabled(true);
        voronoi_view->_parent->set_area_mode(1);
    }
    else if (area_custom->isChecked())
    {
        area_cmin_sb->setEnabled(true);
        area_cmax_sb->setEnabled(true);
        voronoi_view->_parent->set_area_mode(2);
    }

    voronoi_view->blockSignals(false);
}


void ViewPreferences::change_thickness_margins(int)
{
    voronoi_view->blockSignals(true);

    if (thickness_global->isChecked())
    {
        thickness_cmin_sb->setDisabled(true);
        thickness_cmax_sb->setDisabled(true);
        voronoi_view->_parent->set_thickness_mode(0);
    }
    else if (thickness_local->isChecked())
    {
        thickness_cmin_sb->setDisabled(true);
        thickness_cmax_sb->setDisabled(true);
        voronoi_view->_parent->set_thickness_mode(1);
    }
    else if (thickness_custom->isChecked())
    {
        thickness_cmin_sb->setEnabled(true);
        thickness_cmax_sb->setEnabled(true);
        voronoi_view->_parent->set_thickness_mode(2);
    }

    voronoi_view->blockSignals(false);
}

void ViewPreferences::custom_area_margins(double &min, double &max)
{
    min = area_cmin_sb->value();
    max = area_cmax_sb->value();
}

void ViewPreferences::custom_thickness_margins(double &min, double &max)
{
    min = thickness_cmin_sb->value();
    max = thickness_cmax_sb->value();
}


void ViewPreferences::update_margins(double)
{
    if (thickness_custom->isChecked())
        voronoi_view->_parent->set_thickness_mode(2);
    if (area_custom->isChecked())
        voronoi_view->_parent->set_area_mode(2);
}
