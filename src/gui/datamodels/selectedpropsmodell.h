#ifndef SELECTEDPROPSMODELL_H
#define SELECTEDPROPSMODELL_H
#include <QAbstractTableModel>
#include <QtGui>
#if QT_VERSION >= 0x050000
#include <QtWidgets>
#endif
#include "coredata/membran.h"
#include "coredata/core.h"
struct Tabelline
{
    QString name;
    int count;
    double area;
    double thick;

};
/*!
 \brief

*/
class selectedPropsmodell: public QAbstractTableModel
{
public:


    void clear();


/**
 * @brief
 *
 */
/*!
 \brief

*/
    selectedPropsmodell();
    /**
     * @brief
     *
     */
    /*!
     \brief

    */
    void call();
    /**
     * @brief
     *
     * @param parent
     * @return int
     */
    /*!
     \brief

     \param parent
     \return int
    */
    int rowCount(const QModelIndex& parent) const;
    /**
     * @brief
     *
     * @param parent
     * @return int
     */
    /*!
     \brief

     \param parent
     \return int
    */
    int columnCount(const QModelIndex& parent) const;
    /**
     * @brief
     *
     * @param index
     * @param role
     * @return QVariant
     */
    /*!
     \brief

     \param index
     \param role
     \return QVariant
    */
    QVariant data(const QModelIndex& index, int role) const;
    /**
     * @brief
     *
     * @param section
     * @param orientation
     * @param role
     * @return QVariant
     */
    /*!
     \brief

     \param section
     \param orientation
     \param role
     \return QVariant
    */
    QHash<QString, Tabelline> tables;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    QHash<int,Atom> *mem; /**< TODO */ /*!< TODO */
    QList<int> selection; /**< TODO */ /*!< TODO */
    QStringList residues; /**< TODO */ /*!< TODO */



    QStringList HHeader; /**< TODO */ /*!< TODO */
    QStringList VHeader; /**< TODO */ /*!< TODO */


};


#endif // SELECTEDPOPSMODELL_H
