#include "treecellmodell.h"

TreeCellModell::TreeCellModell(){}


void TreeCellModell::insertAtom(Atom *a)
{
    communicator[a->getNumber()].atom = a;
}


void TreeCellModell::insertVCell(Vcell *a)
{
    int x = a->data(0).toInt();
    communicator[x].cell = a;
}


void TreeCellModell::insertTreeItem(QTreeWidgetItem *a)
{
    int x = a->data(1,0).toInt();
    communicator[x].item = a;
}


void TreeCellModell::deleteEntry(int a)
{
    communicator.remove(a);
}


bool TreeCellModell::getTreeItem(int a, QTreeWidgetItem &item)
{
    if(communicator.find(a)!=communicator.end())
    {
        item = *communicator[a].item;
        return true;
    }
    return false;
}


Vcell& TreeCellModell::getVCell(int a)
{
    return *communicator[a].cell;
}


bool TreeCellModell::valid(int a)
{
    if(communicator.find(a)!=communicator.end())
        return true;

    return false;
}


void TreeCellModell::clear()
{
    communicator.clear();
}


bool TreeCellModell::selectEntry(int x)
{
   if(communicator.find(x) != communicator.end())
   {
       communicator.find(x).value().cell->setSelected(true);
       communicator.find(x).value().item->setSelected(true);
       return true;
   }
   return false;
}


bool TreeCellModell::deSelectEntry(int x)
{
   if(communicator.find(x) != communicator.end())
   {
       communicator.find(x).value().cell->setSelected(false);
       communicator.find(x).value().item->setSelected(false);
       return true;
   }
   return false;
}

