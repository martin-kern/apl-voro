#include "selectedpropsmodell.h"

selectedPropsmodell::selectedPropsmodell()
{

}
void selectedPropsmodell::call()
{

    tables.clear();
    residues.clear();

   HHeader << "Count" << "Area(nm^2)" << "Avg. Area(nm^2)" <<"Avg. Thickness(nm)";


   for (int i2 = 0 ; i2 < selection.size(); i2++)
   {
       QString n = QString((*mem)[selection[i2]].getResname());
       if(tables.find(n) == tables.end())
       {
           Tabelline line;
           line.name = n;
           line.count = 1;
           line.thick = (*mem)[selection[i2]].getThick();
           line.area = (*mem)[selection[i2]].getArea();
           tables.insert(n,line);
       }
       else
       {
           Tabelline *line =  &tables.find(n).value();
           line->area += (*mem)[selection[i2]].getArea();
           line->count +=1;
           line->thick += (*mem)[selection[i2]].getThick();

       }
   }

   QHash<QString,Tabelline>::Iterator it;
   QString name = "Total";
   Tabelline tableline;
   tableline.name = name;
   tableline.count =0;
   tableline.area = 0;
   tableline.thick = 0;
   for(it = tables.begin(); it != tables.end(); it++)
   {    residues.push_back(it.value().name);
        tableline.count +=it.value().count;
        tableline.area += it.value().area;
        tableline.thick += it.value().thick;
   }



residues.push_back(name);
tables.insert(name,tableline);





}
int selectedPropsmodell::rowCount(const QModelIndex& /*unused*/) const
{


    return residues.size();

}
int selectedPropsmodell::columnCount(const QModelIndex& /*unused*/) const
{

    return 4;
}

QVariant selectedPropsmodell::data(const QModelIndex& index, int role) const
{

    if(role == Qt::DisplayRole)
      {
        if(index.column() == 0)
        {
            QString found = residues[index.row()];
            return tables.find(found).value().count;

        }
        if(index.column() == 1)
        {
            QString found = residues[index.row()];
            return (tables.find(found).value().area);
        }

        if(index.column() == 2)
        {
            QString found = residues[index.row()];
            return (tables.find(found).value().area/tables.find(found).value().count);
        }

        if(index.column() == 3)
        {
            QString found = residues[index.row()];
            return (tables.find(found).value().thick/tables.find(found).value().count);
        }

      }

    return QVariant::Invalid;
}

QVariant selectedPropsmodell::headerData(int section, Qt::Orientation orientation, int role) const
{

    if(role == Qt::DisplayRole)
       {

       if(orientation == Qt::Horizontal)
         {

           return HHeader[section];
         }
       else if(orientation == Qt::Vertical)
         {

           return (residues[section]);
         }

       }

     return QVariant::Invalid;
}

void selectedPropsmodell::clear()
{
    emit QAbstractItemModel::layoutChanged();
}

