SOURCES += gui/datamodels/allviewproptablesmodell.cpp \
           gui/datamodels/selectedpropsmodell.cpp \
           gui/datamodels/averageareasmodelldown.cpp \
           gui/datamodels/averageareasmodellup.cpp \
    gui/datamodels/treecellmodell.cpp

           

HEADERS += 		gui/datamodels/allviewproptablesmodell.h \
                           gui/datamodels/selectedpropsmodell.h \
                           gui/datamodels/averageareasmodelldown.h \
                           gui/datamodels/averageareasmodellup.h \
    gui/datamodels/treecellmodell.h
