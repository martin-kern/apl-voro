#ifndef TREECELLMODELL_H
#define TREECELLMODELL_H

#include <QtWidgets>
#include <QTreeWidgetItem>
#include "gui/graphicobjects/vcell.h"
#include "coredata/atom.h"
#include "coredata/vt.h"



class TreeCellModell
{
public:
    TreeCellModell();
    void insertAtom(Atom *a);
    void insertVCell(Vcell *a);
    void insertTreeItem(QTreeWidgetItem *a);
    bool selectEntry(int x);
    bool deSelectEntry(int x);
    void deleteEntry(int a);
    bool getTreeItem(int a, QTreeWidgetItem &item);
    Vcell& getVCell(int a);
    bool valid(int a);
    void clear();
    QHash<int, VT> communicator;
};

#endif // TREECELLMODELL_H
