#include "averageareasmodellup.h"
#include <QAbstractTableModel>
#include <QtGui>


AverageAreasmodellUp::AverageAreasmodellUp(): QAbstractTableModel(){}


int AverageAreasmodellUp::rowCount(const QModelIndex&) const
{
    return Upresidues->size();
}


int AverageAreasmodellUp::columnCount(const QModelIndex&) const
{
    return 3;
}


QVariant AverageAreasmodellUp::data(const QModelIndex& index, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant::Invalid;


    QString actualRes = (*Upresidues)[index.row()];
    int counter = 0;
    double area = 0;
    double area2 = 0;
    if(index.column() == 0)  // Avg. Area/Frames
    {
        for (int j = 0 ; j < mems->size(); j++)
        {
            for(int i = 0 ; i < numbers->size(); i++)
            {
                if((*mems)[j].leaflets["upside"]->QHside.find((*numbers)[i]) != (*mems)[j].leaflets["upside"]->QHside.end())
                {
                    Atom *a =  &(*mems)[j].leaflets["upside"]->QHside[(*numbers)[i]];
                    QString str(a->getResname());
                    if(str.compare(actualRes) == 0)
                    {
                        counter++;
                        area += a->getArea();
                    }
                }
            }
            area2 +=(area/counter);
        }
        return (area2/mems->size());
    }

    if(index.column() == 1)  // Avg. Thickness/Frames
    {
        for (int j = 0 ; j < mems->size(); j++)
        {
            for(int i = 0 ; i < numbers->size(); i++)
            {
                if((*mems)[j].leaflets["upside"]->QHside.find((*numbers)[i]) != (*mems)[j].leaflets["upside"]->QHside.end())
                {
                    Atom *a =  &(*mems)[j].leaflets["upside"]->QHside[(*numbers)[i]];
                    QString str(a->getResname());
                    if(str.compare(actualRes)==0)
                    {
                        counter++;
                        area += a->getThick();
                    }
                }
            }
            area2 +=(area/counter);
        }
        return (area2/mems->size());
    }

    if(index.column() == 2)  // Sum Area/Frames
    {
        for (int j = 0 ; j < mems->size(); j++)
        {
            for(int i = 0 ; i < numbers->size(); i++)
            {
                if((*mems)[j].leaflets["upside"]->QHside.find((*numbers)[i]) != (*mems)[j].leaflets["upside"]->QHside.end())
                {
                    Atom *a =  &(*mems)[j].leaflets["upside"]->QHside[(*numbers)[i]];
                    QString str(a->getResname());
                    if(str.compare(actualRes)==0)
                        area +=a->getArea();
                }
            }
        }
        return (area/mems->size());
    }
    return QVariant::Invalid;
}


QVariant AverageAreasmodellUp::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role == Qt::DisplayRole)
    {
        if(orientation == Qt::Horizontal)
        {
            QString str;

            if(section ==0)
                str = "Avg. Area/Frames";
            if(section ==1)
                str = "Avg. Thickness/Frames";
            if(section ==2)
                str = "Sum. Area/Frames";

            return str;
        }
        else if(orientation == Qt::Vertical)
        {
            QString resname = (*Upresidues)[section];
            return resname;
        }
    }

    return QVariant::Invalid;
}
