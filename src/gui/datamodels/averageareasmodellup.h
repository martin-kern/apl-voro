#ifndef AVERAGEAREASMODELLUP_H
#define AVERAGEAREASMODELLUP_H
#include <QAbstractTableModel>
#include <QtGui>
#if QT_VERSION >= 0x050000
#include <QtWidgets>
#endif
#include "coredata/membran.h"


class AverageAreasmodellUp: public QAbstractTableModel
{
public:
  AverageAreasmodellUp();
  int rowCount(const QModelIndex& parent) const;
  int columnCount(const QModelIndex& parent) const;
  QVariant data(const QModelIndex& index, int role) const;
  QVariant headerData(int section, Qt::Orientation orientation, int role) const;
  QHash<int,Membran> *mems;
  QStringList *Upresidues;
  QList<int> *numbers;
};


#endif // AVERAGEAREASMODELL_H
