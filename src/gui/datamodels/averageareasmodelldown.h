#ifndef AVERAGEAREASMODELLDOWN_H
#define AVERAGEAREASMODELLDOWN_H

#include <QtWidgets>
#include "coredata/membran.h"


class AverageAreasmodellDown: public QAbstractTableModel
{
public:
  AverageAreasmodellDown();
  int rowCount(const QModelIndex& parent) const;
  int columnCount(const QModelIndex& parent) const;
  QVariant data(const QModelIndex& index, int role) const;
  QVariant headerData(int section, Qt::Orientation orientation, int role) const;

  QHash<int,Membran> *mems;
  QStringList *Downresidues;
  QList<int> *numbers;
};

#endif // AVERAGEAREASMODELL_H
