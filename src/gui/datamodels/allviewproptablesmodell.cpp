#include "allviewproptablesmodell.h"


allViewPropTablesmodell::allViewPropTablesmodell()
{
}


int allViewPropTablesmodell::rowCount(const QModelIndex& /*unused*/) const
{
    return mem->size();
}


int allViewPropTablesmodell::columnCount(const QModelIndex& /*unused*/) const
{
    return 6;
}


QVariant allViewPropTablesmodell::data(const QModelIndex& index, int role) const
{
    QHash<int,Atom>::iterator it = mem->begin() + index.row();

    if(role == Qt::DisplayRole)
    {
        if(index.column() == 0)
            return QVariant(QString(((it->getResname()))));
        if(index.column() == 1)
            return QVariant(QString((it->getName())));
        if(index.column() == 2)
            return it->getArea();
        if(index.column() == 3)
            return it->getX();
        if(index.column() == 4)
            return it->getY();
        if(index.column() == 5)
            return it->getThick();
    }
    return QVariant::Invalid;
}


QVariant allViewPropTablesmodell::headerData(int section, Qt::Orientation orientation, int role) const
{
    QHash<int,Atom>::iterator it = mem->begin() + section;

    if(role == Qt::DisplayRole)
    {
        if(orientation == Qt::Horizontal)
        {
            if(section == 0)
                return QString("Residue");
            if(section == 1)
                return QString("Atomname");
            if(section == 2)
                return QString("Area");
            if(section == 3)
                return QString("X");
            if(section == 4)
                return QString("Y");
            if(section == 5)
                return QString("Thick");
        }
        else if(orientation == Qt::Vertical)
        {
            QString str;
            str.setNum(it.key());
            return str;
        }
    }
    return QVariant::Invalid;
}


void allViewPropTablesmodell::clear()
{
    emit QAbstractItemModel::layoutChanged();
}
