#include "averageareasmodelldown.h"


AverageAreasmodellDown::AverageAreasmodellDown(): QAbstractTableModel()
{

}


int AverageAreasmodellDown::rowCount(const QModelIndex& /*unused*/) const
{
    return Downresidues->size();
}


int AverageAreasmodellDown::columnCount(const QModelIndex& /*unused*/) const
{
    return 3;
}


QVariant AverageAreasmodellDown::data(const QModelIndex& index, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant::Invalid;

    QString actualRes = (*Downresidues)[index.row()];
    int counter = 0;
    int counter2 = 0;
    double area = 0;
    double area2 = 0;
    if(index.column() == 0)
    {
        for (int j = 0 ; j < mems->size(); j++)
        {
            counter2++;
            for(int i = 0 ; i < numbers->size(); i++)
            {
                if((*mems)[j].leaflets["downside"]->QHside.find((*numbers)[i]) != (*mems)[j].leaflets["downside"]->QHside.end())
                {
                    Atom *a =  &(*mems)[j].leaflets["downside"]->QHside[(*numbers)[i]];

                    QString str(a->getResname());
                    if(str.compare(actualRes) == 0)
                    {
                        counter +=1;
                        area +=a->getArea();
                    }
                }
            }
            area2 +=(area/counter);
        }
        return (area2/counter2);
    }
    if(index.column() == 1)
    {
        for (int j = 0 ; j < mems->size(); j++)
        {
            for(int i = 0 ; i < numbers->size(); i++)
            {
                if((*mems)[j].leaflets["downside"]->QHside.find((*numbers)[i]) != (*mems)[j].leaflets["downside"]->QHside.end())
                {
                    Atom *a =  &(*mems)[j].leaflets["downside"]->QHside[(*numbers)[i]];
                    QString str(a->getResname());
                    if(str.compare(actualRes)==0)
                    {
                        counter +=1;
                        area += a->getThick();
                    }
                }
            }
            area2 +=(area/counter);
        }
        return (area2/mems->size());
    }
    if(index.column() == 2)
    {
        for (int j = 0 ; j < mems->size(); j++)
        {
            for(int i = 0 ; i < numbers->size(); i++)
            {
                if((*mems)[j].leaflets["downside"]->QHside.find((*numbers)[i]) != (*mems)[j].leaflets["downside"]->QHside.end())
                {
                    Atom *a =  &(*mems)[j].leaflets["downside"]->QHside[(*numbers)[i]];
                    QString str(a->getResname());
                    if(str.compare(actualRes)==0)
                        area +=a->getArea();
                }
            }
        }
        return (area/mems->size());
    }

    return QVariant::Invalid;
}


QVariant AverageAreasmodellDown::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role == Qt::DisplayRole)
    {
        if(orientation == Qt::Horizontal)
        {
            QString str = "";

            if(section == 0)
                str = "Avg. Area/Frames";
            if(section == 1)
                str = "Avg. Thickness/Frames";
            if(section == 2)
                str = "Sum. Area/Frames";
            return str;
         }
        else if(orientation == Qt::Vertical)
        {
            QString resname = (*Downresidues)[section];
            return resname;
        }
    }
    return QVariant::Invalid;
}
