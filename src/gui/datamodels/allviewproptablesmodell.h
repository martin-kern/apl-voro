#ifndef ALLVIEWPROPTABLESMODELL_H
#define ALLVIEWPROPTABLESMODELL_H

#include <QAbstractTableModel>
#include "coredata/membran.h"


class allViewPropTablesmodell: public QAbstractTableModel
{
public:
    allViewPropTablesmodell();
    void clear();
    int rowCount(const QModelIndex& parent) const;
    int columnCount(const QModelIndex& parent) const;
    QVariant data(const QModelIndex& index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    QHash<int, Atom> *mem;
};

#endif // ALLVIEWPROPTABLES_H
