#include "wizardtreewidget.hpp"

WizardTreeWidget::WizardTreeWidget(QWidget *parent) : QTreeWidget(parent)
{

}

QStringList WizardTreeWidget::get_atoms()
{
    /* Creates a string list containing all key atoms.
     * Each string contains residue name and atom name
     * separated by a single space.
     */
    QTreeWidgetItemIterator it(this);
    QStringList atom_list;
    while(*it)
    {
        if((*it)->checkState(1)==Qt::Checked)
        {
            QString key_atom = (*it)->parent()->text(0) + " " + (*it)->text(0);
            atom_list.append(key_atom);
         }
        it++;
    }
    return atom_list;
}


QStringList WizardTreeWidget::get_protein_names()
{
    QTreeWidgetItemIterator it(this);
    QStringList protein_names;
    while(*it)
    {
        if((*it)->checkState(1)==Qt::Checked)
            protein_names.append((*it)->text(0));
        it++;
    }
    return protein_names;
}
