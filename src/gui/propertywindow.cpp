#include "gui/propertywindow.h"

PropertyWindow::PropertyWindow(QWidget *parent)
    : QDockWidget(parent)
{
    setWindowFlags(Qt::Window);
    selectedMol = new selectedprops(this);
    allMol = new AllProperties(this);

    tables = new QTabWidget(this);
    tables->addTab(allMol, "All");
    tables->addTab(selectedMol, "Selected");

    selectUpmodell = new selectedPropsmodell;
    selectDownmodell = new selectedPropsmodell;

    proxyDModel = new QSortFilterProxyModel(this);
    proxyUModel = new QSortFilterProxyModel(this);

    allupmodell = new allViewPropTablesmodell;
    allDownmodell = new allViewPropTablesmodell;

    actualFrame = new QPushButton;
    actualFrame->setText("Update");
    QHBoxLayout *buttons = new QHBoxLayout;
    buttons->addWidget(actualFrame, 1, Qt::AlignLeft);

    QVBoxLayout *treerun = new QVBoxLayout;
    treerun->addWidget(tables);
    treerun->addLayout(buttons);

    QWidget *contents = new QWidget(this);
    contents->setLayout(treerun);
    setWidget(contents);

    connect(actualFrame,SIGNAL(clicked()),this,SLOT(UpdateRequested()));
}


void PropertyWindow::closeEvent(QCloseEvent *event)
{
    emit closing();
    event->accept();
}


void PropertyWindow::updatedat(Membran *mem,
                               QList<int> upsidenumbers,
                               QList<int> downsidenumbers)
{
    allupmodell->clear();
    allDownmodell->clear();

    allupmodell->mem = &(mem->leaflets["upside"]->QHside);
    allDownmodell->mem = &(mem->leaflets["downside"]->QHside);

    proxyUModel = new QSortFilterProxyModel;

    proxyUModel->setSourceModel(allupmodell);
    allMol->UpsideTable->setModel(proxyUModel);

    allMol->UpsideTable->setSortingEnabled(true);

    proxyDModel = new QSortFilterProxyModel;
    proxyDModel->setSourceModel(allDownmodell);
    allMol->DownsideTable->setModel(proxyDModel);
    allMol->DownsideTable->setSortingEnabled(true);

    selectUpmodell->clear();
    selectUpmodell->mem = &(mem->leaflets["upside"]->QHside);
    selectUpmodell->selection = upsidenumbers;
    selectUpmodell->call();
    selectedMol->Upperselected->setModel(selectUpmodell);

    selectDownmodell->clear();
    selectDownmodell->mem = &(mem->leaflets["downside"]->QHside);
    selectDownmodell->selection = downsidenumbers;
    selectDownmodell->call();
    selectedMol->Downselected->setModel(selectDownmodell);
}


void PropertyWindow::UpdateRequested()
{
    emit updateRequest();
}
