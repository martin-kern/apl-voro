#ifndef PROJECTWIZARDPAGE_HPP
#define PROJECTWIZARDPAGE_HPP

#include <QtWidgets>
#include <climits>
#include <cfloat>
#include "io/pdb/pdbreader.h"
#include "data/constants.hpp"

class ProjectWizardPage : public QWizardPage
{
    Q_OBJECT
public:
    ProjectWizardPage(QWidget *parent);
    bool isComplete() const override;
    int nextId() const override;

private:
    void ui_setup();
    void connect_setup();
    QDoubleSpinBox *x_spin_box, *y_spin_box, *z_spin_box;
    QCheckBox *cryst1_check_box;
    QSpinBox *start_frame_spin_box, *last_frame_spin_box, *stride_spin_box;
    QPushButton *model_file_explorer_button,
                *index_file_explorer_button,
                *trajectory_file_explorer_button;
    QLineEdit *name, *model_path, *index_path, *trajectory_path;
    QString directory;

private slots:
    void load(int type);
    void set_cryst();
    void set_name(const QString model_file_path);
};

#endif // PROJECTWIZARDPAGE_HPP
