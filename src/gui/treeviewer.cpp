#include "treeviewer.h"


LipidSelectionWizardPage::LipidSelectionWizardPage(QWizard *parent)
    : QWizardPage(parent)
{
    this->parent = parent;
    setWindowFlags(Qt::Window);
    setWindowTitle("Lipid Selection");
    moleculeTree = new WizardTreeWidget(parent);
    moleculeTree->setSelectionMode(QAbstractItemView::MultiSelection);
    moleculeTree->clear();
    QStringList headerlist;
    headerlist.append("Molecules");
    headerlist.append("Include");
    moleculeTree->setHeaderLabels(headerlist);
    QTreeWidgetItem *root = new QTreeWidgetItem;
    root->setText(0, tr("Molecule"));
    moleculeTree->addTopLevelItem(root);
    QVBoxLayout *treerun = new QVBoxLayout;
    treerun->addWidget(new QLabel("Select Key-Atoms"));
    treerun->addWidget(moleculeTree);
    QPushButton *auto_select_button = new QPushButton(this);
    auto_select_button->setText("auto select");
    auto_select_button->setFixedWidth(100);
    auto_select_button->setLayoutDirection(Qt::RightToLeft);
    treerun->addWidget(auto_select_button);
    setLayout(treerun);
    registerField("atom_list", moleculeTree, "atoms");
    connect(auto_select_button, SIGNAL(clicked()), this, SLOT(auto_select_key_atoms()));
    // Call the isComplete function when key atom selection changes.
    connect(moleculeTree, SIGNAL(itemChanged(QTreeWidgetItem*, int)), this, SIGNAL(completeChanged()));
    connect(moleculeTree, SIGNAL(itemChanged(QTreeWidgetItem*, int)), this, SLOT(selection_preview(QTreeWidgetItem*, int)));
}


void LipidSelectionWizardPage::FillTree(std::vector<molecule_atoms> atomlist)
{
    /* Fill the tree widget with the residue name at the top level
     * and all atom names as children. Excludes molecules that have
     * previously been selected as proteins.
     */
    QStringList proteins = field("protein_names").toStringList();
    moleculeTree->clear();
    for (unsigned int i = 0 ;i < atomlist.size();i++)
    {
        if (!proteins.contains(atomlist[i].molecule_name))
        {
            QTreeWidgetItem *mol = new QTreeWidgetItem;
            mol->setText(0, atomlist[i].molecule_name);
            mol->setFlags(Qt::ItemIsUserCheckable|Qt::ItemIsEnabled);
            moleculeTree->addTopLevelItem(mol);
            for (int j = 0; j < atomlist[i].atom_names.length(); j++)
            {
                QTreeWidgetItem *atomItem = new QTreeWidgetItem(mol);
                atomItem->setText(0, atomlist[i].atom_names[j]);
                atomItem->setFlags(Qt::ItemIsUserCheckable|Qt::ItemIsEnabled);
                atomItem->setCheckState(1, Qt::Unchecked);
            }
        }
    }
}


bool LipidSelectionWizardPage::isComplete() const
{
    /* Enables the finish button only when at least
     * one key atom is selected.
     */
    if (field("atom_list").toStringList().isEmpty())
        return false;
    else
        return true;
}


bool LipidSelectionWizardPage::validatePage()
{
    /* Collect all data from the project wizard
     * and send it via the project_setup_done signal
     */
    setup_parameters project_setup_data;
    project_setup_data.name = field("name").toString();
    project_setup_data.x_box = field("cryst_x").toDouble();
    project_setup_data.y_box = field("cryst_y").toDouble();
    project_setup_data.z_box = field("cryst_z").toDouble();
    project_setup_data.pbc_cutoff = field("pbc_cutoff").toInt();
    project_setup_data.modus = field("thickness_algo").toInt();
    project_setup_data.leaflet_detection = field("leaflet_detection").toInt();
    project_setup_data.p_degree = field("poly_degree").toInt();
    project_setup_data.first_frame = field("first_frame").toInt();
    project_setup_data.last_frame = field("last_frame").toInt();
    project_setup_data.stride = field("stride").toInt();
    project_setup_data.pdb = field("model_path").toString();
    project_setup_data.trj = field("trajectory_path").toString();
    project_setup_data.index = field("index_path").toString();

    if (parent->hasVisitedPage(protein_selection))
        project_setup_data.protein_names = field("protein_names").toStringList();
    project_setup_data.vdw_check = field("vdw_check").toInt();
    project_setup_data.upright = field("upright").toDouble();
    project_setup_data.key_atoms = field("atom_list").toStringList();
    emit project_setup_done(project_setup_data);
    return true;
}


void LipidSelectionWizardPage::auto_select_key_atoms()
{
    /* For each molecule selects the first phosphorus
     * atom as key atom. This is the most common practise.
     */
    QTreeWidgetItemIterator iterator(moleculeTree);
    QStringList done_residues;
    while (*iterator)
    {
        if((*iterator)->text(0).startsWith("P")
           && (*iterator)->takeChildren().isEmpty()
           && !done_residues.contains((*iterator)->parent()->text(0)))
        {
            done_residues.append((*iterator)->parent()->text(0));
            (*iterator)->setCheckState(1, Qt::Checked);
        }
        iterator++;
    }
}


void LipidSelectionWizardPage::selection_preview(QTreeWidgetItem *item, int column)
{
    /* Display the selected key atoms in each parent node.
     */
    if (item->parent() == nullptr)
        return;

    if (item->checkState(column) == Qt::Checked)
    {
        QString selected_key_atoms = item->parent()->text(1);
        selected_key_atoms += item->text(0) + " ";
        item->parent()->setText(1, selected_key_atoms);
    }
    else
    {
        QString selected_key_atoms = item->parent()->text(1);
        selected_key_atoms.remove(item->text(0) + " ");
        item->parent()->setText(1, selected_key_atoms);
    }
}
