#ifndef ALLPROPERTIES_H
#define ALLPROPERTIES_H
#include <QtGui>
#if QT_VERSION >= 0x050000
#include <QtWidgets>
#endif
#include <QTableView>
#include <QLabel>
#include "coredata/membran.h"


class AllProperties: public QWidget
{
    Q_OBJECT
public:
    AllProperties(QWidget *parent = 0);
    std::vector<Membran> *mems;
    QTableView *UpsideTable;
    QTableView *DownsideTable;
    QLabel *labelUp, *labelDown;
    void setDatas(Membran &mem);
};

#endif // ALLPROPERTIES_H
