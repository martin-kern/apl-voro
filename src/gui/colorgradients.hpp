#ifndef COLORSGRADIENTS_HPP
#define COLORSGRADIENTS_HPP
#include <QtWidgets>

#define LOCS    1
#define GRAY    2
#define HEATED  3
#define RAINBOW 4

/**
 * @brief The ColorGradients class provides a number of color gradients that can be used for visualization.
 */
class ColorGradients
{
public:
    ColorGradients();
    static QString current_style_sheet();
    static QLinearGradient current_gradient();
    static QColor colorAtValue(double value, double minv, double maxv);
    static void setColorScale(int colorScaleID);
    static int getColorScaleID();
    static QLinearGradient gradient(int gradient_id, bool inverse=false);

private:
    static QColor locs[256];
    static QColor linearGray[256];
    static QColor heatedObject[256];
    static QColor rainbow[256];
    static QColor* currentColorList();
    static QColor* getColorList(int colorListID);
    static QString get_gradient_stylesheet(int gradientID);
    static int scaleID;

};
static ColorGradients colorGradients = ColorGradients();
#endif // COLORSGRADIENTS_HPP
