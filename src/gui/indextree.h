#ifndef INDEXTREE_H
#define INDEXTREE_H
#include <QtWidgets>
#include "io/ndx/index_parser.hpp"
#include "wizardtreewidget.hpp"


class ProteinSelectionPage : public QWizardPage
{
    Q_OBJECT

public:
    ProteinSelectionPage(QWidget *parent = nullptr);
    void FillTree(QString _path);

private:
    void fileread();

    void secread(QString V);
    WizardTreeWidget *restree;

    QDoubleSpinBox *upright;
    QComboBox *vdw;
    index_parser *ip;

    QString path;
    QStringList list;
    QStringList selected;
    QHash <QString, QList <int>> *intlist;
    QLabel *label;


public slots:
    void vdwcheck(int vdw_option);
};

#endif
