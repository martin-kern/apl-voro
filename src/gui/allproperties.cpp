//**************************************************************
//
//           Copyright (c) 2012 - 2013 by Gunther Lukat
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************

#include <QtGui>
#include<QHBoxLayout>
#include<QVBoxLayout>
#include "coredata/molecule.h"
#include <string>
#include <stdio.h>
#include "allproperties.h"

AllProperties::AllProperties(QWidget *parent)
    : QWidget(parent)
{
    UpsideTable = new QTableView(this);
    DownsideTable = new QTableView(this);

    labelUp = new QLabel;
    labelUp->setText("Upperside");

    labelDown = new QLabel;
    labelDown->setText("Downside");

    QVBoxLayout *upsidelayout = new QVBoxLayout;
    upsidelayout->addWidget(labelUp);
    upsidelayout->addWidget(UpsideTable);

    QVBoxLayout *downsidelayout = new QVBoxLayout;
    downsidelayout->addWidget(labelDown);
    downsidelayout->addWidget(DownsideTable);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addLayout(upsidelayout);
    layout->addLayout(downsidelayout);
    setLayout(layout);

}

