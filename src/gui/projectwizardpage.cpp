#include "projectwizardpage.hpp"

ProjectWizardPage::ProjectWizardPage(QWidget *parent) : QWizardPage(parent)
{
    directory = QDir::homePath();
    ui_setup();
    connect_setup();
}


bool ProjectWizardPage::isComplete() const
{
    /* Overrides isComplete from QWizardPage. Returns true when the user is
     * ready to go to the next wizard page.
     */

    if (field("cryst_x").toDouble() == 0.0)
        return false;
    if (field("cryst_y").toDouble() == 0.0)
        return false;
    if (field("cryst_z").toDouble() == 0.0)
        return false;
    if (field("last_frame").toInt() != -1 && field("first_frame").toInt() > field("last_frame").toInt())
        return false;
    if (field("model_path").toString().isEmpty())
        return false;
    if (field("name").toString().isEmpty())
        return false;
    return true;
}


int ProjectWizardPage::nextId() const
{
    /* This function determines which wizard page is next, depending
     * on the information provided. If the user selects an index file,
     * the next page will be the protein selection page. Otherwise the
     * next page will be the lipid selection page.
     */
    if (index_path->text().isEmpty())
        return lipid_selection;
    return protein_selection;
}


void ProjectWizardPage::ui_setup()
{
    /* Build the ui.
     */
    setTitle("Base Parameters");

    // Vertical stack of widgets for the base parameters wizard page.
    QVBoxLayout *base_param_vlayout = new QVBoxLayout();

    name = new QLineEdit(this);
    QFormLayout *name_layout = new QFormLayout();
    name_layout->addRow(new QLabel("Name:"), name);
    base_param_vlayout->addLayout(name_layout);

    // Widgets for controlling box dimensions.
    base_param_vlayout->addWidget(new QLabel("Box dimensions in nm:"));
    QHBoxLayout *horizontal_box_param_layout = new QHBoxLayout();
    QFormLayout *x_layout = new QFormLayout();
    x_spin_box = new QDoubleSpinBox(this);
    x_spin_box->setRange(0.0, DBL_MAX);
    x_layout->addRow(new QLabel("X", this),
                     x_spin_box);
    QFormLayout *y_layout = new QFormLayout();
    y_spin_box = new QDoubleSpinBox(this);
    y_spin_box->setRange(0.0, DBL_MAX);
    y_layout->addRow(new QLabel("Y", this),
                     y_spin_box);
    QFormLayout *z_layout = new QFormLayout();
    z_spin_box = new QDoubleSpinBox(this);
    z_spin_box->setRange(0.0, DBL_MAX);
    z_layout->addRow(new QLabel("Z", this),
                     z_spin_box);
    x_spin_box->setDisabled(true);
    y_spin_box->setDisabled(true);
    z_spin_box->setDisabled(true);
    horizontal_box_param_layout->addLayout(x_layout);
    horizontal_box_param_layout->addLayout(y_layout);
    horizontal_box_param_layout->addLayout(z_layout);
    QVBoxLayout *vertical_box_param_layout = new QVBoxLayout();
    cryst1_check_box = new QCheckBox(this);
    cryst1_check_box->setCheckState(Qt::Checked);
    QFormLayout *cryst1_layout = new QFormLayout();
    cryst1_layout->addRow(new QLabel("detect by CRYST1 record"),
                          cryst1_check_box);
    vertical_box_param_layout->addLayout(horizontal_box_param_layout);
    vertical_box_param_layout->addLayout(cryst1_layout);
    QFrame *box_param_frame = new QFrame(this);
    box_param_frame->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
    box_param_frame->setLayout(vertical_box_param_layout);
    base_param_vlayout->addWidget(box_param_frame);


    // Widgets for controlling algorithm options.
    base_param_vlayout->addWidget(new QLabel("Options:"));
    QSpinBox *cutoff_spin_box = new QSpinBox(this);
    cutoff_spin_box->setRange(0, 100);
    cutoff_spin_box->setValue(20);

    QSpinBox *poly_degree = new QSpinBox(this);
    poly_degree->setRange(0, 15);
    poly_degree->setValue(5);

    QComboBox *leaflet_detection_algorithm = new QComboBox(this);
    leaflet_detection_algorithm->addItem("orientation-based");
    leaflet_detection_algorithm->addItem("position-based");

    QComboBox *thickness_algorithm = new QComboBox(this);
    thickness_algorithm->addItem("Vertical neighbor (multi)");
    thickness_algorithm->addItem("Vertical neighbor (single)");
    QFormLayout *options_layout = new QFormLayout();
    options_layout->addRow(new QLabel("PBC cutoff distance in %:"),
                          cutoff_spin_box);
    options_layout->addRow(new QLabel("Polynomial degree for surface fit:"),
                           poly_degree);
    options_layout->addRow(new QLabel("Thickness Algorithm:"),
                           thickness_algorithm);
    options_layout->addRow(new QLabel("Leaflet Detection:"), leaflet_detection_algorithm);
    QFrame *algo_param_frame = new QFrame(this);
    algo_param_frame->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
    algo_param_frame->setLayout(options_layout);
    base_param_vlayout->addWidget(algo_param_frame);

    // Widgets for frame selection.
    base_param_vlayout->addWidget(new QLabel("Frame selection:"));
    start_frame_spin_box = new QSpinBox(this);
    start_frame_spin_box->setRange(0, INT_MAX);
    last_frame_spin_box = new QSpinBox(this);
    last_frame_spin_box->setRange(-1, INT_MAX);
    last_frame_spin_box->setValue(-1);
    stride_spin_box = new QSpinBox(this);
    stride_spin_box->setRange(1, INT_MAX);
    stride_spin_box->setValue(1);
    QFormLayout *frame_selection_layout = new QFormLayout();
    frame_selection_layout->addRow(new QLabel("First frame:"),
                                   start_frame_spin_box);
    frame_selection_layout->addRow(new QLabel("Last frame:"),
                                   last_frame_spin_box);
    frame_selection_layout->addRow(new QLabel("Stride:"),
                                   stride_spin_box);
    QFrame *frame_selection_frame = new QFrame(this);
    frame_selection_frame->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
    frame_selection_frame->setLayout(frame_selection_layout);
    base_param_vlayout->addWidget(frame_selection_frame);

    // Widgets for loading files.
    base_param_vlayout->addWidget(new QLabel("Files:"));
    QGridLayout *file_select_grid = new QGridLayout();
    model_path = new QLineEdit(this);
    model_path->setReadOnly(true);
    model_path->setClearButtonEnabled(true);
    model_file_explorer_button = new QPushButton(this);
    model_file_explorer_button->setText("Model");
    index_path = new QLineEdit(this);
    index_path->setReadOnly(true);
    index_path->setClearButtonEnabled(true);
    index_file_explorer_button = new QPushButton(this);
    index_file_explorer_button->setText("Index");
    trajectory_path = new QLineEdit(this);
    trajectory_path->setReadOnly(true);
    trajectory_path->setClearButtonEnabled(true);
    trajectory_file_explorer_button = new QPushButton(this);
    trajectory_file_explorer_button->setText("Trajectory");
    file_select_grid->addWidget(model_path, 0, 0);
    file_select_grid->addWidget(model_file_explorer_button, 0, 1);
    file_select_grid->addWidget(index_path, 1, 0);
    file_select_grid->addWidget(index_file_explorer_button, 1, 1);
    file_select_grid->addWidget(trajectory_path, 2, 0);
    file_select_grid->addWidget(trajectory_file_explorer_button, 2, 1);
    QFrame *file_select_frame = new QFrame();
    file_select_frame->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
    file_select_frame->setLayout(file_select_grid);
    base_param_vlayout->addWidget(file_select_frame);
    setLayout(base_param_vlayout);

    /* QLineEdit boxes come with an integrated clear button.
     * This button is disabled when QLineEdit is set to read only.
     * The following code is a dirty hack to enable the clear button.
     * But it might not be compatible with future versions of Qt.
     */
    QAction *clear_action;
    clear_action = model_path->findChild<QAction*>();
    if (clear_action)
        clear_action->setEnabled(true);
    clear_action = index_path->findChild<QAction*>();
    if (clear_action)
        clear_action->setEnabled(true);
    clear_action = trajectory_path->findChild<QAction*>();
    if (clear_action)
        clear_action->setEnabled(true);

    registerField("name", name);
    registerField("cryst_x", x_spin_box, "value");
    registerField("cryst_y", y_spin_box, "value");
    registerField("cryst_z", z_spin_box, "value");
    registerField("pbc_cutoff", cutoff_spin_box);
    registerField("poly_degree", poly_degree);
    registerField("thickness_algo", thickness_algorithm);
    registerField("leaflet_detection", leaflet_detection_algorithm);
    registerField("first_frame", start_frame_spin_box);
    registerField("last_frame", last_frame_spin_box);
    registerField("stride", stride_spin_box);
    registerField("model_path", model_path);
    registerField("trajectory_path", trajectory_path);
    registerField("index_path", index_path);
}


void ProjectWizardPage::connect_setup()
{
    /* Connect all signals and slots.
     */

    // All load buttons have a similar function. Using a signal mapper we can
    // pass a parameter to the load function.
    QSignalMapper *button_mapper = new QSignalMapper();
    connect(model_file_explorer_button, SIGNAL(clicked()), button_mapper, SLOT(map()));
    button_mapper->setMapping(model_file_explorer_button, 1);
    connect(index_file_explorer_button, SIGNAL(clicked()), button_mapper, SLOT(map()));
    button_mapper->setMapping(index_file_explorer_button, 2);
    connect(trajectory_file_explorer_button, SIGNAL(clicked()), button_mapper, SLOT(map()));
    button_mapper->setMapping(trajectory_file_explorer_button, 3);
    connect(button_mapper, SIGNAL(mapped(int)), this, SLOT(load(int)));

    // Send a signal every time a field is changed that can lead to the completion
    // of this wizard page. Sending that signal will make Qt call isComplete().
    connect(x_spin_box, SIGNAL(valueChanged(const QString)), this, SIGNAL(completeChanged()));
    connect(y_spin_box, SIGNAL(valueChanged(const QString)), this, SIGNAL(completeChanged()));
    connect(z_spin_box, SIGNAL(valueChanged(const QString)), this, SIGNAL(completeChanged()));
    connect(start_frame_spin_box, SIGNAL(valueChanged(const QString)), this, SIGNAL(completeChanged()));
    connect(last_frame_spin_box, SIGNAL(valueChanged(const QString)), this, SIGNAL(completeChanged()));
    connect(model_path, SIGNAL(textChanged(const QString)), this, SIGNAL(completeChanged()));
    connect(name, SIGNAL(textChanged(const QString)), this, SIGNAL(completeChanged()));

    connect(cryst1_check_box, SIGNAL(stateChanged(int)), this, SLOT(set_cryst()));
    connect(model_path, SIGNAL(textChanged(const QString)), this, SLOT(set_cryst()));
    connect(model_path, SIGNAL(textChanged(const QString)), this, SLOT(set_name(const QString)));

}


void ProjectWizardPage::load(int type)
{
    /* Open a file explorer and write the absolute path of the selected
     * file in the appropriate line.
     */
    QString file_name = "";
    switch (type)
    {
    case 1:
        file_name = QFileDialog::getOpenFileName(this,
                                                 "Open Coordinates",
                                                 directory,
                                                 "Coordinate File (*.pdb)");
        model_path->setText(file_name);
        break;
    case 2:
        file_name = QFileDialog::getOpenFileName(this,
                                                 "Open Index File",
                                                 directory,
                                                 "Index File (*.ndx)");
        index_path->setText(file_name);
        break;
    case 3:
        file_name = QFileDialog::getOpenFileName(this,
                                                 "Open Trajectory",
                                                 directory,
                                                 "Trajectory (*.xtc *.trr)");
        trajectory_path->setText(file_name);
        break;
    }
    directory = file_name.remove(QRegExp("/.*\\..{3}$"));
}


void ProjectWizardPage::set_cryst()
{
    /* Set the value of all spin boxes for defining the box size when
     * cryst1_check_box is checked. Said spin boxes are disabled.
     * If cryst1_check_box is unchecked enable all spin boxes.
     */

    int state = cryst1_check_box->checkState();
    if (state == Qt::Unchecked)
    {
        x_spin_box->setDisabled(false);
        y_spin_box->setDisabled(false);
        z_spin_box->setDisabled(false);
    }
    if (state == Qt::Checked)
    {
        x_spin_box->setDisabled(true);
        y_spin_box->setDisabled(true);
        z_spin_box->setDisabled(true);

        if (!model_path->text().isEmpty())
        {
            Pdbreader *reader = new Pdbreader(model_path->text());
            double x_cryst, y_cryst, z_cryst;
            reader->getBox(x_cryst, y_cryst, z_cryst);
            x_spin_box->setValue(x_cryst);
            y_spin_box->setValue(y_cryst);
            z_spin_box->setValue(z_cryst);
        }
    }
}


void ProjectWizardPage::set_name(const QString model_file_path)
{
    /* Set the project name based on the file name of the model.
     */
    if (field("name").toString().isEmpty())
    {
        QString n = model_file_path.split("/").last();
        name->setText(n.split(".")[0]);
    }
}
