#include "frameprinterwidget.h"

FramePrinterWidget::FramePrinterWidget(QWidget *parent) : QWidget(parent)
{
    list = new QStringList;
    QFrame *frame1 = new QFrame(this);
    frame1->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);

    allframes = new QCheckBox(this);
    allframes->setCheckState(Qt::Unchecked);

    from = new QSpinBox(this);

    till= new QSpinBox(this);

    QFormLayout *formLayout1 = new QFormLayout;
    formLayout1->addRow(tr("&Print current frame:"), allframes);
    formLayout1->addRow(tr("&From frame:"), from);
    formLayout1->addRow(tr("&Till frame:"), till);
    frame1->setLayout(formLayout1);

    QFrame *frame2 = new QFrame(this);
    frame2->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);

    selection = new QComboBox(this);
    QFormLayout *formLayout2 = new QFormLayout;
    formLayout2->addRow(tr("&Selection to print:"), selection);
    frame2->setLayout(formLayout2);


    QFrame *frame3 = new QFrame(this);
    frame3->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);

    browse = new QPushButton(this);
    browse->setText("Browse");

    pathfield = new QLineEdit(this);

    QFormLayout *formLayout3 = new QFormLayout;
    formLayout3->addRow(tr("&Save to file:"),pathfield);
    QHBoxLayout *blayout = new QHBoxLayout;
    blayout->addLayout(formLayout3);
    blayout->addWidget(browse);
    frame3->setLayout(blayout);

    QFrame *frame4 = new QFrame(this);
    frame4->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);

    okay = new QPushButton(this);
    okay->setText("Okay");

    cancel = new QPushButton(this);
    cancel->setText("Cancel");

    QHBoxLayout *buttons = new QHBoxLayout(this);
    buttons->addWidget(okay);
    buttons->addWidget(cancel);

    frame4->setLayout(buttons);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(frame1);
    layout->addWidget(frame2);
    layout->addWidget(frame3);
    layout->addWidget(frame4);

    setLayout(layout);

    //CONNECTIONS
    connect(okay,SIGNAL(clicked()),this,SLOT(okaypressed()));

    connect(browse,SIGNAL(clicked()),this,SLOT(browsepressed()));
    connect(cancel,SIGNAL(clicked()),this,SLOT(cancelpressed()));
    connect(allframes,SIGNAL(stateChanged(int)),this,SLOT(currentselected()));
}


void FramePrinterWidget::populate(QStringList *_list,
                                  int maxframe,
                                  int currentframe,
                                  QHash<QString,Critall > *_criterias,QList<int> *numbers,
                                  core *_vcore,
                                  int _side)
{
    list = _list;
    vcore = _vcore;
    maxframes = maxframe - 1;
    cframe = currentframe;
    criterias = _criterias;
    current = numbers;

    if(side == 0)
        leaflet = "upside";
    else
        leaflet = "downside";
    side = _side;
    from->clear();
    till->clear();
    selection->clear();
    allframes->setCheckState(Qt::Unchecked);

    from->setMinimum(0);
    till->setMinimum(0);
    from->setMaximum(maxframes);
    till->setMaximum(maxframes);
    from->setValue(0);
    till->setValue(maxframes);
    selection->addItem("All");
    selection->addItem("Current");
    for (int i = 0 ; i < list->size(); i++)
        selection->addItem(list->at(i));
}


void FramePrinterWidget::okaypressed()
{
    fromsel = from->value();
    tillsel = till->value();
    if(tillsel < fromsel)
    {
        QMessageBox::warning(this, tr("VLayers"), tr("starting frame must be <= end frame!"));
        return;
    }
    else
    {
        csel = selection->currentIndex();
        if ( !fileName.isEmpty())
        {
            QFileInfo ff (fileName);
            QString file_suffix = ff.suffix();

            if(file_suffix != "xml" && file_suffix != "txt")
            {
                QMessageBox::warning(this, tr("VLayers"), tr("No suffix (.xm/.txt) found for file %1:\n.").arg(fileName));
                return;
            }

            else if(file_suffix == "txt")  // Write txt format
            {
                QFile file(fileName);
                if (!file.open(QFile::WriteOnly | QFile::Text))
                {
                    QMessageBox::warning(this, tr("VLayers"), tr("Cannot write file %1:\n%2.").arg(fileName).arg(file.errorString()));
                    return;
                }

                // File is writable
                QMessageBox msgBox(this);
                msgBox.setText("Saving file, please wait ...");
                msgBox.open();
                QHash<int,Atom>::iterator it;
                QTextStream out(&file);
                out <<"#VLayers"<<"\n";
                out << "#Number  Name  Residue  ResidueID  X  Y  Z  Area  Thick\n";

                if(csel == 0)  // No Selection
                {
                    for(int l = fromsel; l < tillsel; l++)  // For loop for frames
                    {
                        out << "#Framenumber " << l << " Time " << vcore->framemembrans[l].time << "\n";
                        for(it = vcore->framemembrans[l].leaflets[leaflet]->QHside.begin(); it != vcore->framemembrans[l].leaflets[leaflet]->QHside.end(); it++)
                            out <<it.value().getNumber()<<"  "<<it.value().getName()<<"  "<<it.value().getResname()<<"  "<<it.value().getResNumber()<<"  "<<it.value().getX() <<"  "<<it.value().getY() <<"  "<<it.value().getZ() <<"  "<<it.value().getArea()<<"  "<<it.value().getThick()<<"\n";
                    }
                }
                else if(csel == 1)  // Current Selection
                {
                    for(int l = fromsel; l < tillsel;l++)
                    {
                        out << "#Framenumber " << l << " Time " << vcore->framemembrans[l].time << "\n";
                        for(int j = 0; j < current->size(); j++)
                        {
                            it = vcore->framemembrans[l].leaflets[leaflet]->QHside.find(current->at(j));
                            if(it!=vcore->framemembrans[l].leaflets[leaflet]->QHside.end())
                                out <<it.value().getNumber()<<"  "<<it.value().getName()<<"  "<<it.value().getResname()<<"  "<<it.value().getResNumber()<<"  "<<it.value().getX() <<"  "<<it.value().getY() <<"  "<<it.value().getZ() <<"  "<<it.value().getArea()<<"  "<<it.value().getThick()<<"\n";
                        }
                    }
                }
                else if(csel > 1)
                {
                    Critall selected_criterion = criterias->find(selection->currentText()).value();
                    for(int l = fromsel; l < tillsel;l++)
                    {
                        out << "#Framenumber " << l << " Time " << vcore->framemembrans[l].time << "\n";
                        for(it = vcore->framemembrans[l].leaflets[leaflet]->QHside.begin(); it != vcore->framemembrans[l].leaflets[leaflet]->QHside.end(); it++)
                        {
                            if(selected_criterion.and_or)
                            {
                                if(it.value().matchCriterion_list_all(selected_criterion.critlist) )
                                    out <<it.value().getNumber()<<"  "<<it.value().getName()<<"  "<<it.value().getResname()<<"  "<<it.value().getResNumber()<<"  "<<it.value().getX() <<"  "<<it.value().getY() <<"  "<<it.value().getZ() <<"  "<<it.value().getArea()<<"  "<<it.value().getThick()<<"\n";
                            }
                            else
                            {
                                if(it.value().matchCriterion_list_single(selected_criterion.critlist) )
                                    out <<it.value().getNumber()<<"  "<<it.value().getName()<<"  "<<it.value().getResname()<<"  "<<it.value().getResNumber()<<"  "<<it.value().getX() <<"  "<<it.value().getY() <<"  "<<it.value().getZ() <<"  "<<it.value().getArea()<<"  "<<it.value().getThick()<<"\n";
                            }
                        }
                    }
                }
                msgBox.close();
                QMessageBox::information(this, tr("VLayers"), tr("Saved frames to txt file %1:\n.").arg(fileName));
            }

            else if(file_suffix =="xml")
            {
                QHash<int,Atom>::iterator it;
                QFile file(fileName);
                if (!file.open(QFile::WriteOnly | QFile::Text))
                {
                    QMessageBox::warning(this, tr("VLayers"), tr("Cannot write file %1:\n%2.").arg(fileName).arg(file.errorString()));
                    return;
                }

                QMessageBox msgBox(this);
                msgBox.setText("Saving file, please wait ...");
                msgBox.open();
                xmlw.setDevice(&file);
                xmlw.setAutoFormatting(true);
                xmlw.writeStartDocument();
                xmlw.writeStartElement("APL_VORO_EXPORT");

                if(csel == 0)//No Selection
                {
                    for(int l = fromsel; l < tillsel;l++)
                    {
                        xmlw.writeStartElement("Frame");
                        xmlw.writeAttribute("NR",QString::number(l));
                        xmlw.writeAttribute("Time",QString::number(vcore->framemembrans[l].time));
                        xmlw.writeAttribute("Side",QString("UpperLayer"));
                        for(it = vcore->framemembrans[l].leaflets[leaflet]->QHside.begin(); it != vcore->framemembrans[l].leaflets[leaflet]->QHside.end(); it++)
                        {
                            xmlw.writeStartElement("Atom");
                            xmlw.writeAttribute("Number",QString::number((it.value().getNumber())));
                            xmlw.writeAttribute("Atomname",QString((it.value().getName())));
                            xmlw.writeAttribute("Resname",QString(it.value().getResname()));
                            xmlw.writeAttribute("ResNumber",QString::number(it.value().getResNumber()));
                            xmlw.writeTextElement("X",QString::number((it.value().getX())));
                            xmlw.writeTextElement("Y",QString::number(it.value().getY()));
                            xmlw.writeTextElement("Z",QString::number(it.value().getZ()));
                            xmlw.writeTextElement("Area",QString::number(it.value().getArea()));
                            xmlw.writeTextElement("Thick",QString::number(it.value().getThick()));
                            xmlw.writeEndElement();//Atom
                        }
                        xmlw.writeEndElement();//Frame
                    }
                }

                else if(csel == 1)//current selection
                {
                    for(int l = fromsel; l < tillsel;l++)
                    {
                        xmlw.writeStartElement("Frame");
                        xmlw.writeAttribute("NR",QString::number(l));
                        xmlw.writeAttribute("Time",QString::number(vcore->framemembrans[l].time));
                        xmlw.writeAttribute("Side",QString("UpperLayer"));
                        for(int j = 0; j < current->size();j++)
                        {
                            it =vcore->framemembrans[l].leaflets[leaflet]->QHside.find(current->at(j));
                            if(it!=vcore->framemembrans[l].leaflets[leaflet]->QHside.end())
                            {
                                xmlw.writeStartElement("Atom");
                                xmlw.writeAttribute("Number",QString::number((it.value().getNumber())));
                                xmlw.writeAttribute("Atomname",QString((it.value().getName())));
                                xmlw.writeAttribute("Resname",QString(it.value().getResname()));
                                xmlw.writeAttribute("ResNumber",QString::number(it.value().getResNumber()));
                                xmlw.writeTextElement("X",QString::number((it.value().getX())));
                                xmlw.writeTextElement("Y",QString::number(it.value().getY()));
                                xmlw.writeTextElement("Z",QString::number(it.value().getZ()));
                                xmlw.writeTextElement("Area",QString::number(it.value().getArea()));
                                xmlw.writeTextElement("Thick",QString::number(it.value().getThick()));
                                xmlw.writeEndElement();//Atom
                            }
                        }
                        xmlw.writeEndElement();//Frame
                    }
                }

                else if(csel >1)//criteria
                {
                    Critall selected_criterion = criterias->find(selection->currentText()).value();
                    for(int l = fromsel; l < tillsel;l++)
                    {
                        xmlw.writeStartElement("Frame");
                        xmlw.writeAttribute("NR",QString::number(l));
                        xmlw.writeAttribute("Time",QString::number(vcore->framemembrans[l].time));
                        xmlw.writeAttribute("Side",QString("UpperLayer"));
                        for(it = vcore->framemembrans[l].leaflets[leaflet]->QHside.begin(); it != vcore->framemembrans[l].leaflets[leaflet]->QHside.end(); it++)
                        {
                            if(selected_criterion.and_or)
                            {
                                if(it.value().matchCriterion_list_all(selected_criterion.critlist) )
                                {
                                    xmlw.writeStartElement("Atom");
                                    xmlw.writeAttribute("Number",QString::number((it.value().getNumber())));
                                    xmlw.writeAttribute("Atomname",QString((it.value().getName())));
                                    xmlw.writeAttribute("Resname",QString(it.value().getResname()));
                                    xmlw.writeAttribute("ResNumber",QString::number(it.value().getResNumber()));
                                    xmlw.writeTextElement("X",QString::number((it.value().getX())));
                                    xmlw.writeTextElement("Y",QString::number(it.value().getY()));
                                    xmlw.writeTextElement("Z",QString::number(it.value().getZ()));
                                    xmlw.writeTextElement("Area",QString::number(it.value().getArea()));
                                    xmlw.writeTextElement("Thick",QString::number(it.value().getThick()));
                                    xmlw.writeEndElement();//Atom
                                }
                            }
                            else
                            {
                                if(it.value().matchCriterion_list_single(selected_criterion.critlist) )
                                {
                                    xmlw.writeStartElement("Atom");
                                    xmlw.writeAttribute("Number",QString::number((it.value().getNumber())));
                                    xmlw.writeAttribute("Atomname",QString((it.value().getName())));
                                    xmlw.writeAttribute("Resname",QString(it.value().getResname()));
                                    xmlw.writeAttribute("ResNumber",QString::number(it.value().getResNumber()));
                                    xmlw.writeTextElement("X",QString::number((it.value().getX())));
                                    xmlw.writeTextElement("Y",QString::number(it.value().getY()));
                                    xmlw.writeTextElement("Z",QString::number(it.value().getZ()));
                                    xmlw.writeTextElement("Area",QString::number(it.value().getArea()));
                                    xmlw.writeTextElement("Thick",QString::number(it.value().getThick()));
                                    xmlw.writeEndElement();//Atom
                                }
                            }
                        }
                        xmlw.writeEndElement();//Frame
                    }
                }
                xmlw.writeEndElement();//APLVORO
                xmlw.writeEndDocument();

                // Run Stuff
                msgBox.close();
                QMessageBox::information(this, tr("VLayers"), tr("Saved frames to XML file %1:\n.").arg(fileName));
            }  //XML
            close();
        }
        else//filename is empty
        {
            QMessageBox::warning(this, tr("VLayers"), tr("Choose a filename to save the frames!"));
            return;
        }
    }
}


void FramePrinterWidget::browsepressed()
{
    QString fp = QDir::homePath();
    QStringList filter;
    filter += "Txt Documents (*.txt)";
    filter += "XML Documents (*.xml)";
    fileName = QFileDialog::getSaveFileName(this, "Export File Name", fp, filter.join(";;"));
    pathfield->setText(fileName);
}


void FramePrinterWidget::cancelpressed()
{
    close();
}


void FramePrinterWidget::currentselected()
{
    if(allframes->isChecked())
    {
        from->setValue(cframe);
        till->setValue(cframe);
    }
}
