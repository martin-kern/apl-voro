#ifndef PROPERTYWINDOW_H
#define PROPERTYWINDOW_H

#include <QtWidgets>
#include "allproperties.h"
#include "selectedprops.h"
#include "gui/datamodels/allviewproptablesmodell.h"
#include "gui/datamodels/selectedpropsmodell.h"
#include "coredata/core.h"


class PropertyWindow: public QDockWidget
{
    Q_OBJECT
public:
    PropertyWindow(QWidget *parent = nullptr);
    unsigned int mem_id() const {return mem_id_;}
    void updatedat(Membran *mem,
                   QList<int> upsidenumbers,
                   QList<int> downsidenumbers);

private:
    QTabWidget *tables;
    selectedprops *selectedMol;
    AllProperties *allMol;
    QPushButton *actualFrame;
    std::vector<Membran> *mems;
    allViewPropTablesmodell *allupmodell;
    allViewPropTablesmodell *allDownmodell;
    QSortFilterProxyModel *proxyUModel;
    QSortFilterProxyModel *proxyDModel;
    selectedPropsmodell *selectUpmodell;
    selectedPropsmodell *selectDownmodell;
    void closeEvent(QCloseEvent *event);
    unsigned int mem_id_;

public slots:
  void UpdateRequested();

signals:
    void closing();
    void updateRequest();
};

#endif // PROPERTYWINDOW_H
