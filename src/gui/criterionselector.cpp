//**************************************************************
//
//           Copyright (c) 2012 - 2013 by Gunther Lukat
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************

#include "criterionselector.h"

CriterionSelector::CriterionSelector(QStringList wordList, QWidget *parent)
{

    id = 0;
    completer = new QCompleter(wordList, this);
    completer->setCaseSensitivity(Qt::CaseInsensitive);
    prime = new QComboBox();
    prime->addItem("is");
    prime->addItem("is not");

    type = new QComboBox();
    type->addItem("Residue");
    type->addItem("Area >");
    type->addItem("Thickness >");
    type->addItem("Neighbors >");
    type->addItem("Preselected");



    useredit = new QLineEdit();
    useredit->setCompleter(completer);


    plus = new QPushButton();
    plus->setText("+");

    minus = new QPushButton();
    minus->setText("-");
    connect(minus,SIGNAL(clicked()), this, SLOT(willclose()));

    connect(type,SIGNAL(currentIndexChanged(int)),this,SLOT(setvalidators(int)));
    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(prime);
    layout->addWidget(type);
    layout->addWidget(useredit);
    layout->addWidget(plus);
    layout->addWidget(minus);

    setLayout(layout);
}
void CriterionSelector::willclose()
{
    emit onclose(id);
    close();
}

void CriterionSelector::setvalidators(int a)
{

useredit->clear();
    if(a ==0)
          {
        useredit->setInputMask("");
        useredit->setCompleter(completer);
        return;
         }
     if(a ==1)
          {
        useredit->setCompleter(0);
         useredit->setText("000.000");
         useredit->setInputMask("000.000");
         return;
         }
      if(a ==2)
              {
        useredit->setCompleter(0);
        useredit->setText("000.000");
              useredit->setInputMask("000.000");
              return;
                }
     if(a ==3)
         {
        useredit->setCompleter(0);
         useredit->setText("0000");
        useredit->setInputMask("0000");
        return;
         }
     if(a ==4)
         {
        useredit->setCompleter(0);
         useredit->setText("TRUE");
        useredit->setDisabled(true);
        return;
         }
    }


