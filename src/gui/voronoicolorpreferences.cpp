#include "voronoicolorpreferences.hpp"

VoronoiColorPreferences::VoronoiColorPreferences(VoronoiDock *vd, int method, QWidget *parent)
    : QWidget(parent)
{
    int frameStyle = QFrame::Sunken | QFrame::Panel;
    //Labels that display the current color settings.
    backgroundlabel = new QLabel;
    selectionColorlabel = new QLabel;
    linecolorlabel = new QLabel;
    gridcolorlabel = new QLabel;
    colorScalePreview = new QLabel;
    QLabel *gridespacing = new QLabel;

    //Set frame styles for each label
    backgroundlabel->setFrameStyle(frameStyle);
    selectionColorlabel->setFrameStyle(frameStyle);
    linecolorlabel->setFrameStyle(frameStyle);
    gridcolorlabel->setFrameStyle(frameStyle);
    gridespacing->setFrameStyle(frameStyle);
    colorScalePreview->setFrameStyle(frameStyle);

    //Buttons for changing color settings and a spin box to change grid spacing
    QPushButton *getbgcolor = new QPushButton;
    QPushButton *getSelectionColor = new QPushButton;
    QPushButton *getlinecolor = new QPushButton;
    gridselector = new QSpinBox;
    gridselector->setMaximum(10);
    gridselector->setMinimum(1);
    QPushButton *getgridcolor = new QPushButton;

    //Combo box for color scale selection.
    QStringList colorScaleList;
    colorScaleList.append("LOCS");
    colorScaleList.append("Linear Gray");
    colorScaleList.append("Heated Object");
    colorScaleList.append("Rainbow");
    colorScaleCB = new QComboBox();
    colorScaleCB->addItems(colorScaleList);

    //Add text to the buttons
    getbgcolor->setText("BackgroundColor");
    getSelectionColor->setText("SelectionColor");
    getlinecolor->setText("LineColor");
    getgridcolor->setText("Gridcolor");

    //Obtain background colors for labels
    //TODO: Make color scale preview work
    voronoi_dock = vd;
    bgcolor = voronoi_dock->bgcolor;
    linecolor = voronoi_dock->linecolor;
    selectcolor = voronoi_dock->selectcolor;
    gridcolor = voronoi_dock->gridcolor;

    //Set label texts. Color scale doesn't get a text.
    linecolorlabel->setText(linecolor.name());
    gridcolorlabel->setText(gridcolor.name());
    backgroundlabel->setText(bgcolor.name());
    selectionColorlabel->setText(selectcolor.name());
    gridespacing->setText("grid spacing in nm");

    //Set label background colors.
    linecolorlabel->setPalette(QPalette(linecolor));
    gridcolorlabel->setPalette(QPalette(gridcolor));
    backgroundlabel->setPalette(QPalette(bgcolor));
    selectionColorlabel->setPalette(QPalette(selectcolor));

    //Apply background colors.
    linecolorlabel->setAutoFillBackground(true);
    gridcolorlabel->setAutoFillBackground(true);
    backgroundlabel->setAutoFillBackground(true);
    colorScalePreview->setAutoFillBackground(true);
    selectionColorlabel->setAutoFillBackground(true);

    //Vertical alignment for all labels.
    QVBoxLayout *labelslayout = new QVBoxLayout;
    labelslayout->addWidget(backgroundlabel);
    labelslayout->addWidget(selectionColorlabel);
    labelslayout->addWidget(linecolorlabel);
    labelslayout->addWidget(gridcolorlabel);
    labelslayout->addWidget(colorScalePreview);
    labelslayout->addWidget(gridespacing);

    //Vertical alignment for all buttons and the spin box.
    QVBoxLayout *buttonslayout = new QVBoxLayout;
    buttonslayout->addWidget(getbgcolor);
    buttonslayout->addWidget(getSelectionColor);
    buttonslayout->addWidget(getlinecolor);
    buttonslayout->addWidget(getgridcolor);
    buttonslayout->addWidget(colorScaleCB);
    buttonslayout->addWidget(gridselector);

    QHBoxLayout *editlayout = new QHBoxLayout;
    editlayout->addLayout(labelslayout);
    editlayout->addLayout(buttonslayout);

    // Color margins

    QLabel *color_margins = new QLabel("Coloring margins:");
    QButtonGroup *area_grp = new QButtonGroup();
    area_global = new QRadioButton("global area margins ()");
    area_local = new QRadioButton("local area margins");
    area_custom = new QRadioButton("custom area margins");
    area_cmax_sb = new QDoubleSpinBox();
    area_cmax_sb->setRange(0, 1000);
    area_cmax_sb->setValue(voronoi_dock->max_area());
    area_cmax_sb->setDisabled(true);
    area_cmin_sb = new QDoubleSpinBox();
    area_cmin_sb->setRange(0, 1000);
    area_cmin_sb->setDisabled(true);
    QHBoxLayout *area_custom_hbl = new QHBoxLayout();
    area_custom_hbl->addWidget(area_cmin_sb);
    area_custom_hbl->addWidget(area_cmax_sb);
    area_global->setChecked(true);
    area_grp->addButton(area_global);
    area_grp->addButton(area_local);
    area_grp->addButton(area_custom);
    QButtonGroup *thickness_grp = new QButtonGroup();
    thickness_global = new QRadioButton("global thickness margins ()");
    thickness_local = new QRadioButton("local thickness margins");
    thickness_custom = new QRadioButton("custom thickness margins");
    thickness_cmax_sb = new QDoubleSpinBox();
    thickness_cmax_sb->setRange(0, 1000);
    thickness_cmax_sb->setDisabled(true);
    thickness_cmin_sb = new QDoubleSpinBox();
    thickness_cmin_sb->setRange(0, 1000);
    thickness_cmin_sb->setDisabled(true);
    QHBoxLayout *thickness_custom_hbl = new QHBoxLayout();
    thickness_custom_hbl->addWidget(thickness_cmin_sb);
    thickness_custom_hbl->addWidget(thickness_cmax_sb);
    thickness_global->setChecked(true);
    thickness_grp->addButton(thickness_global);
    thickness_grp->addButton(thickness_local);
    thickness_grp->addButton(thickness_custom);
    QVBoxLayout *area_margins_layout = new QVBoxLayout();
    area_margins_layout->addWidget(area_global);
    area_margins_layout->addWidget(area_local);
    area_margins_layout->addWidget(area_custom);
    area_margins_layout->addLayout(area_custom_hbl);
    QVBoxLayout *thickness_margins_layout = new QVBoxLayout();
    thickness_margins_layout->addWidget(thickness_global);
    thickness_margins_layout->addWidget(thickness_local);
    thickness_margins_layout->addWidget(thickness_custom);
    thickness_margins_layout->addLayout(thickness_custom_hbl);
    QHBoxLayout *margins_layout = new QHBoxLayout();
    margins_layout->addLayout(area_margins_layout);
    margins_layout->addLayout(thickness_margins_layout);

    QVBoxLayout *marginsUplayout = new QVBoxLayout;
    marginsUplayout->addWidget(color_margins);
    marginsUplayout->addLayout(margins_layout);

    QFrame *upframe = new QFrame;
    upframe->setFrameStyle(QFrame::StyledPanel);
    upframe->setLayout(marginsUplayout);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addLayout(editlayout);
    layout->addWidget(upframe);

    setLayout(layout);
    coloringmethod = method;
    offset = 0.0;
//    gridselector->setValue(voronoi_dock->gridspace);
}
