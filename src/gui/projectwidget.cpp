#include "gui/projectwidget.h"


Projectwindow::Projectwindow(QWidget *parent)
    :QWizard(parent)
{
    setWindowTitle("New Project");

    project_setup_page = new ProjectWizardPage(this);
    setPage(project_setup, project_setup_page);

    protein_selection_page = new ProteinSelectionPage(this);
    setPage(protein_selection, protein_selection_page);

    molecule_tree = new LipidSelectionWizardPage(this);
    setPage(lipid_selection, molecule_tree);
}


void Projectwindow::initializePage(int id)
{
    /* Fill the protein selection page and the lipid selection page
     * with content.
     */
    Pdbreader *reader = new Pdbreader(field("model_path").toString());
    std::vector<molecule_atoms> mol_atms_list;
    switch (id)
    {
    case lipid_selection:
        reader->get_single_molecules(mol_atms_list);
        molecule_tree->FillTree(mol_atms_list);
        break;
    case protein_selection:
        protein_selection_page->FillTree(field("index_path").toString());
        break;
    }
    delete reader;
    reader = nullptr;
}
