#include "indextree.h"


ProteinSelectionPage::ProteinSelectionPage(QWidget *parent) :
    QWizardPage(parent)
{
    setTitle("Protein Selection");
    restree = new WizardTreeWidget(parent);

    vdw = new QComboBox;
    vdw->insertItem(0, "van der Waals radii");
    vdw->insertItem(1, "user defined");
    vdw->insertItem(2, "absolute");
    vdw->setCurrentIndex(0);

    upright = new QDoubleSpinBox;
    upright->setMinimum(0.0);
    upright->setValue(0.0);
    upright->setDisabled(true);

    QHBoxLayout *toplayout = new QHBoxLayout;
    toplayout->addWidget(new QLabel("TPIM offset method:"));
    toplayout->addWidget(vdw);
    toplayout->addWidget(upright);

    QWidget *mframe = new QWidget;
    mframe->setLayout(toplayout);

    restree->setSelectionMode(QAbstractItemView::MultiSelection);
    QStringList headerlist;
    headerlist.append("Type");
    headerlist.append("Include");
    restree->setHeaderLabels(headerlist);

    QVBoxLayout *treerun = new QVBoxLayout;
    treerun->addWidget(mframe);
    treerun->addWidget(new QLabel("Protein Selection"));
    treerun->addWidget(restree);

    setLayout(treerun);

    connect(vdw, SIGNAL(currentIndexChanged(int)), this, SLOT(vdwcheck(int)));
    registerField("protein_names", restree, "protein_names");
    registerField("vdw_check", vdw);
    registerField("upright", upright);
}


void ProteinSelectionPage::vdwcheck(int vdw_option)
{
    switch (vdw_option)
    {
    case 0:
        upright->setEnabled(false);
        break;
    case 1:
        upright->setEnabled(true);
        break;
    case 2:
        upright->setValue(0.0);
        upright->setEnabled(false);
        break;
    }
}


void ProteinSelectionPage::FillTree(QString path)
{
    this->path = path;
    restree->clear();
    fileread();

    for (int i = 0 ;i < list.size();i++)
    {
        QTreeWidgetItem *mol = new QTreeWidgetItem;
        QString str = list[i];
        mol->setText(0, str);
        mol->setFlags(Qt::ItemIsUserCheckable|Qt::ItemIsEnabled);
        mol->setCheckState(1, Qt::Unchecked);
        restree->addTopLevelItem(mol);
    }
}


void ProteinSelectionPage::fileread()
{
    list.clear();
    QFile file;
    file.setFileName(path);
    ip = new index_parser(file);
    ip->get_content(&list);
    delete(ip);
}


void ProteinSelectionPage::secread(QString V)
{
    QFile file;
    file.setFileName(path);
    ip = new index_parser(file);
    ip->get_content_section(intlist, V);
    delete(ip);
}
