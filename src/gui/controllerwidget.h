#ifndef CONTROLLERWIDGET_H
#define CONTROLLERWIDGET_H
#include <QtGui>
#if QT_VERSION >= 0x050000
#include <QtWidgets>
#endif
#include <QSlider>
#include <QComboBox>
#include <QToolButton>
#include <QGridLayout>


class controllerWidget:public QFrame
{
    Q_OBJECT

public:
    controllerWidget(QWidget *parent = nullptr);
    QSlider *frameSlider, *speedSlider;
    QSpinBox *framechooser;
    QToolButton  *play, *pause;
    QTimer *timer;
    int maxframes, time;
    int getValue();
    void setValue(int a);

signals:
    void needredraw();

private slots:
    void setScene();
    void setChooser();
    void startSlider();
    void stopSlider();
    void SliderUpdate();
};

#endif // CONTROLLERWIDGET_H
