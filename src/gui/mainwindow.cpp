//**************************************************************
//
//           Copyright (c) 2012 - 2013 by Gunther Lukat
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************
#include "gui/mainwindow.h"


MainWindow::MainWindow()
{
    setWindowTitle(QString(APP_NAME) + " Version " + QString(APP_VER));

    framenum = 0;
    next_memID = 0;
    next_voronoi_view_id = 0;

    menuBar = new QMenuBar(this);

    central_dock_area = new QMainWindow();
    central_dock_area->setDockOptions(QMainWindow::AllowNestedDocks|QMainWindow::AllowTabbedDocks|QMainWindow::AnimatedDocks);
    central_dock_area->setCentralWidget(nullptr);
    setCentralWidget(central_dock_area);

    controller= new controllerWidget(nullptr);
    QDockWidget *controller_dock = new QDockWidget(this);
    controller_dock->setFeatures(QDockWidget::NoDockWidgetFeatures);
    controller_dock->setAllowedAreas(Qt::BottomDockWidgetArea);
    controller_dock->setWidget(controller);
    addDockWidget(Qt::BottomDockWidgetArea, controller_dock);

    openMembranesList = new QListWidget();
    QDockWidget *open_mem_dock = new QDockWidget(this);
    open_mem_dock->setFeatures(QDockWidget::NoDockWidgetFeatures);
    open_mem_dock->setAllowedAreas(Qt::LeftDockWidgetArea);
    open_mem_dock->setWidget(openMembranesList);
    addDockWidget(Qt::LeftDockWidgetArea, open_mem_dock);

    setupMenue();
    setupConnections();
}


void MainWindow::setupMenue()
{
    openAct = new QAction(tr("&New"),this);
    openAct->setShortcuts(QKeySequence::New);
    openAct->setStatusTip(tr("Open a Coordinate File"));

    save_as_act = new QAction("Save As", this);
    save_as_act->setShortcut(QKeySequence::Save);
    save_as_act->setStatusTip("Save current session.");

    load_act = new QAction("Open", this);
    load_act->setShortcut(QKeySequence::Open);
    load_act->setStatusTip("Open session.");

    quit = new QAction(tr("&Quit"),this);
    quit->setShortcuts(QKeySequence::Close);
    manualhelp = new QAction(tr("&Manual"),this);
    about = new QAction(tr("&About"),this);

    filemenu = new QMenu("&File",menuBar);
    filemenu->addAction(openAct);
    filemenu->addAction(save_as_act);
    filemenu->addAction(load_act);
    filemenu->addAction(quit);
    menuBar->addMenu(filemenu);

    helpmenu = new QMenu("&Help",menuBar);
    helpmenu->addAction(manualhelp);
    helpmenu->addAction(about);
    menuBar->addMenu(helpmenu);

    setMenuBar(menuBar);
}


void MainWindow::setupConnections()
{
    connect(openAct, SIGNAL(triggered()), this, SLOT(setupProject()));
    connect(save_as_act, SIGNAL(triggered()), this, SLOT(save_session()));
    connect(load_act, SIGNAL(triggered()), this, SLOT(LoadSaved()));
    connect(about, SIGNAL(triggered()), this, SLOT(aboutdialog()));
//    connect(read_selection,SIGNAL(triggered()),this,SLOT(read_selection_file()));
    connect(quit, SIGNAL(triggered()), this, SLOT(close()));
    connect(controller, SIGNAL(needredraw()), this, SLOT(setColoringMethod()));
    connect(manualhelp, SIGNAL(triggered()), this, SLOT(showManual()));
}


void MainWindow::setupProject()
{
    Projectwindow *project_window = new Projectwindow(this);
    project_window->show();
    connect(project_window->page(2), SIGNAL(project_setup_done(setup_parameters)), this, SLOT(create_project(setup_parameters)));
}


void MainWindow::create_project(setup_parameters setup_params)
{
    coreHashList.insert(next_memID, new core(setup_params));
    int size = coreHashList[next_memID]->framemembrans.size();
    framenum = size - 1;


    if(framenum >= 1)
    {
        controller->play->setEnabled(true);
        controller->pause->setEnabled(true);
        controller->frameSlider->setEnabled(true);
        controller->framechooser->setEnabled(true);
    }

    if (size - 1 > controller->maxframes)
        controller->maxframes = size - 1;
    controller->frameSlider->blockSignals(true);
    controller->frameSlider->setMaximum(framenum);
    controller->framechooser->blockSignals(true);
    controller->framechooser->setMaximum(framenum);
    controller->frameSlider->blockSignals(false);
    controller-> framechooser->blockSignals(false);



    QListWidgetItem *new_list_item = new QListWidgetItem(openMembranesList);
    new_list_item->setFlags(Qt::NoItemFlags);
    MembraneHandlerWidget *new_membrane_handler = new MembraneHandlerWidget(next_memID, setup_params.name, coreHashList[next_memID]);
    new_list_item->setSizeHint(new_membrane_handler->sizeHint());
    openMembranesList->addItem(new_list_item);
    openMembranesList->setItemWidget(new_list_item, new_membrane_handler);
    connect(new_membrane_handler, SIGNAL(signal_voronoi_view(unsigned int)), this, SLOT(show_voronoi_view(unsigned int)));
    connect(new_membrane_handler, SIGNAL(signal_averages_view(unsigned int)), this, SLOT(show_averages_table(unsigned int)));
    connect(new_membrane_handler, SIGNAL(signal_close_project(unsigned int, MembraneHandlerWidget*)), this, SLOT(close_project(unsigned int, MembraneHandlerWidget*)));
    connect(new_membrane_handler, SIGNAL(signal_2d_view(unsigned int)), this, SLOT(show_2d_plot()));
    connect(new_membrane_handler, SIGNAL(signal_properties_view(unsigned int)), this, SLOT(show_properties(unsigned int)));
    connect(new_membrane_handler, SIGNAL(update_controller()), this, SLOT(controller_span_update()));
    next_memID++;
}


void MainWindow::showManual()
{
    // Open manual in your default webbrowser.
    QDesktopServices::openUrl(QUrl("file:///usr/share/aplvoro/manual/manual/APL_VORO_Manual.html", QUrl::TolerantMode));
}


void MainWindow::clear()
{
    // Close all docks and clear the lists where docks are managed.
    for (int i = 0; i < voro_dock_list.length(); i++)
    {
        voro_dock_list[i]->close();
        delete voro_dock_list[i];
    }
    voro_dock_list.clear();
    for (int i = 0; i < TableList.length(); i++)
    {
        TableList[i]->close();
        delete TableList[i];
    }
    TableList.clear();
    for (int i = 0; i < PlotList.length(); i++)
    {
        PlotList[i]->close();
        delete PlotList[i];
    }
    PlotList.clear();
    for (int i = 0; i < properties_list.length(); i++)
    {
        properties_list[i]->close();
        delete properties_list[i];
    }
    properties_list.clear();
    QHash<unsigned int, core*>::iterator it;
    for (it = coreHashList.begin(); it != coreHashList.end(); it++)
        delete it.value();
    coreHashList.clear();
    openMembranesList->clear();

    controller->play->setDisabled(true);
    controller->pause->setDisabled(true);
    controller->frameSlider->setDisabled(true);
    controller->framechooser->setDisabled(true);
    framenum = 0;
}


void MainWindow::setColoringMethod()
{
    for (int i = 0; i < voro_dock_list.length(); i++)
    {
//        VoronoiList[i]->update(controller->getValue(), controller->maxframes);
        voro_dock_list[i]->update(controller->getValue(), controller->maxframes);
    }
    for (int i = 0; i < PlotList.length(); i++)
        PlotList[i]->set_marker(controller->getValue());
}


void MainWindow::save_session()
{
    QString filename = QFileDialog::getSaveFileName(this, "Save File", "untitled.apl");
    if (filename.isEmpty())
        return;
    if (!filename.endsWith(".apl"))
        filename += ".apl";
    AplReadWrite::write(filename, &coreHashList);
}


void MainWindow::LoadSaved()
{
    if (!coreHashList.keys().isEmpty())
    {
        QMessageBox::StandardButton ret;
        ret = QMessageBox::warning(this,
                                   "Application",
                                   "Opening a saved session will close the current session.",
                                   QMessageBox::Ok | QMessageBox::Cancel);
        if (ret == QMessageBox::Cancel)
            return;
    }
    QString file_name = QFileDialog::getOpenFileName(this, "Open APL file", QDir::homePath(), "APL (*.apl)");
    if (file_name.isEmpty())
        return;
    clear();
    QList<setup_parameters> setup_list = AplReadWrite::read(file_name);
    for (int i = 0; i < setup_list.length(); i++)
        create_project(setup_list[i]);
}


void MainWindow::closeEvent(QCloseEvent *event)
{
     if (maybeSave())
         event->accept();
     else
         event->ignore();
}


bool MainWindow::maybeSave()
{
    // This bit is broken for the time being
//    if (mycore->framemembrans.size()>0)
//    {
//        QMessageBox::StandardButton ret;
//        ret = QMessageBox::warning(this, tr("Application"),
//                  tr("You are about to close this application.\n"
//                     "Do you want to close?"),
//                   QMessageBox::Ok | QMessageBox::Cancel);
//        if (ret == QMessageBox::Ok)
//            return true;
//        else if (ret == QMessageBox::Cancel)
//            return false;
//    }
    return true;
}


bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{
    if (obj == controller->frameSlider)
    {
        if (event->type() == QEvent::KeyPress)
        {
            QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);
            if(keyEvent->key()== Qt::Key_Left)
            {
                if(controller->frameSlider->value()< framenum)
                {
                    controller->frameSlider->setValue(controller->frameSlider->value()+1);
                    controller-> frameSlider->setFocus();
                    return true;
                }
            }
            else if(keyEvent->key()== Qt::Key_Right)
            {
                if(controller->frameSlider->value() > 0)
                {
                    controller-> frameSlider->setValue(controller->frameSlider->value()-1);
                    controller-> frameSlider->setFocus();
                    return true;
                }
            }
        }
    }
    else if (obj == controller->speedSlider)
    {
        if (event->type() == QEvent::KeyPress)
        {
            QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);

            if(keyEvent->key()== Qt::Key_Left)
            {
                controller->speedSlider->setValue(controller->speedSlider->value()+100);
                controller->speedSlider->setFocus();
                return true;
            }
            else if(keyEvent->key()== Qt::Key_Right)
            {
               controller->speedSlider->setValue(controller->speedSlider->value()-100);
               controller->speedSlider->setFocus();
               return true;
            }
        }
    }
    else
        return QMainWindow::eventFilter(obj, event);

    return QMainWindow::eventFilter(obj, event);
}


void MainWindow::aboutdialog()
{
    QMessageBox msgBox;
    msgBox.setText("About APL@Voro");
    msgBox.setInformativeText("Copyright (c) Gunther Lukat 2013.\n"
                              "This program is free software: you can redistribute it and/or modify"
                              "it under the terms of the GNU General Public License as published by"
                              " the Free Software Foundation, either version 3 of the License, or"
                              "(at your option) any later version.\n"
                                 "This program is distributed in the hope that it will be useful, "
                                 "but WITHOUT ANY WARRANTY; without even the implied warranty of"
                                 " MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the"
                                 " GNU General Public License for more details.\n "

                                 "You should have received a copy of the GNU General Public License"
                                 "along with this program.  If not, see <http://www.gnu.org/licenses/>."
                              "\n"
                              "Third party libraries are: \n"
                              "\n"
                              "xdrlib (1.1.1) Copyright (c) Erik Lindahl, David van der Spoel 2003,2004. Coordinate compression (c) by Frans van Hoesel. GPLv3"
                              "\n"
                              "APL@voro is based in part on the work of the Qwt project (http://qwt.sf.net)"
                              "\n"
                              "APL@voro is based in part on the work of the QwPlot3D project.(The OSX Version of APL@Voro includes the static qwtplot3d library with slight modifications to match the OSX OpenGL headers.)");
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.setDefaultButton(QMessageBox::Ok);
    msgBox.setIconPixmap(QPixmap(":pixmaps/64x64/aplvoro.png"));
    msgBox.exec();
}


void MainWindow::show_voronoi_view(unsigned int memID)
{
    VoronoiDock *new_voronoi_view = new VoronoiDock(memID,
                                                    &coreHashList,
                                                    "Voronoi-View " + coreHashList[memID]->name(),
                                                    controller->getValue(),
                                                    controller->maxframes);
    selections[next_voronoi_view_id] = new selection();
    selections[next_voronoi_view_id]->mem_id = memID;
    selections[next_voronoi_view_id]->view_id = next_voronoi_view_id;
    selections[next_voronoi_view_id]->leaflet = new_voronoi_view->active_leaflet();
    central_dock_area->addDockWidget(Qt::TopDockWidgetArea, new_voronoi_view);
    voro_dock_list.append(new_voronoi_view);
    next_voronoi_view_id++;
}


void MainWindow::close_project(unsigned int mem_id, MembraneHandlerWidget *mhw)
{
    for (int i = 0; i < voro_dock_list.length(); i++)
    {
        if (voro_dock_list[i]->mem_id() == mem_id)
        {
            delete voro_dock_list[i];
            voro_dock_list.removeAt(i);
        }
    }
    for (int i = 0; i < TableList.length(); i++)
    {
        if (TableList[i]->mem_id() == mem_id)
        {
            delete TableList[i];
            TableList.removeAt(i);
        }
    }
    for (int i = 0; i < properties_list.length(); i++)
    {
        if (properties_list[i]->mem_id() == mem_id)
        {
            delete properties_list[i];
            PlotList.removeAt(i);
        }
    }
    for (int i = 0; i < openMembranesList->count(); i++)
    {
        if (openMembranesList->itemWidget(openMembranesList->item(i)) == mhw)
        {
            delete mhw;
            delete openMembranesList->item(i);
        }
    }
    delete coreHashList[mem_id];
    coreHashList.remove(mem_id);
}


void MainWindow::show_averages_table(unsigned int memID)
{
    Tableviewer *new_tableViewer = new Tableviewer(memID, coreHashList[memID]);
    central_dock_area->addDockWidget(Qt::TopDockWidgetArea, new_tableViewer);
    TableList.append(new_tableViewer);
}


void MainWindow::show_2d_plot()
{
    Plot2DWindow *new_plot = new Plot2DWindow(&coreHashList,
                                              controller->getValue(),
                                              this);
    central_dock_area->addDockWidget(Qt::TopDockWidgetArea, new_plot);
    PlotList.append(new_plot);
}


void MainWindow::show_properties(unsigned int memID)
{
    PropertyWindow *new_property_view = new PropertyWindow(this);
    QList<int> empty;
    new_property_view->updatedat(&coreHashList[memID]->framemembrans[0], empty, empty);
    properties_list.append(new_property_view);
    central_dock_area->addDockWidget(Qt::TopDockWidgetArea, new_property_view);
}


void MainWindow::controller_span_update()
{
    int max_span = 0;
    for (int i = 0; i < openMembranesList->count(); i++)
    {
        QListWidgetItem *a = openMembranesList->item(i);
        MembraneHandlerWidget *b = static_cast<MembraneHandlerWidget*>(openMembranesList->itemWidget(a));
        if (max_span < b->span())
            max_span = b->span();
    }
    controller->frameSlider->setMaximum(max_span);
    controller->framechooser->setMaximum(max_span);
    controller->maxframes = max_span;
    this->setColoringMethod();
}
