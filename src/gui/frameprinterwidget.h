#ifndef FRAMEPRINTERWIDGET_H
#define FRAMEPRINTERWIDGET_H
#include <QtGui>
#if QT_VERSION >= 0x050000
#include <QtWidgets>
#endif
#include <QXmlStreamWriter>
#include "coredata/core.h"

class FramePrinterWidget :public QWidget
{
    Q_OBJECT
public:
    FramePrinterWidget(QWidget *parent = nullptr);
    void populate(QStringList *_list,int maxframe, int currentframe, QHash<QString,Critall > *_criterias,QList<int> *numbers, core *_vcore,int side);
private:
    QCheckBox *allframes;
    QSpinBox *from, *till;
    QComboBox *selection;
    QPushButton *browse,*okay,*cancel;
    QLineEdit *pathfield;
    QStringList *list;
    QString fileName;
    core *vcore;
    int fromsel,tillsel,maxframes,cframe,csel,side;
    QList<int> *current;
    QHash<QString,Critall > *criterias;
    QXmlStreamWriter xmlw;

    QString leaflet;
private slots:
    void okaypressed();
    void browsepressed();
    void cancelpressed();
    void currentselected();
};

#endif // FRAMEPRINTERWIDGET_H
