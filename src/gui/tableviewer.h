#ifndef TABLEVIEWER_H
#define TABLEVIEWER_H

#include <QtWidgets>
#include "coredata/core.h"
#include "gui/datamodels/averageareasmodelldown.h"
#include "gui/datamodels/averageareasmodellup.h"


class Tableviewer : public QDockWidget
{
    Q_OBJECT
public:
    Tableviewer(unsigned int m, core *c, QWidget *parent = nullptr);
    void update();
    unsigned int mem_id() const {return mem_id_;}

private:
    core *table_core;
    QTabWidget *tablesUp;
    QTableView *Upside, *Downside;
    AverageAreasmodellUp *avgareamodelup;
    AverageAreasmodellDown *avgareasmodeldown;
    void closeEvent(QCloseEvent *event);
    unsigned int mem_id_;

signals:
    void closing();
};
#endif
