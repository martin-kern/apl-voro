#ifndef PLOTTINGMENUEWINDOW_H
#define PLOTTINGMENUEWINDOW_H
#include <QWidget>
#include <QComboBox>
#include <QLabel>
#include <QPushButton>
#include <QTreeWidgetItem>
#include <QLineEdit>
#include <QFormLayout>
#include <QVBoxLayout>
#include <QMessageBox>
#include <QColorDialog>
#include "coredata/core.h"
#include "qwt/qwt_plot_curve.h"
#include "qwt/qwt_plot.h"


class PlottingMenueWindow : public QWidget
{
    /* This widget will pop up when you want to add a new curve to a 2d view.
     * It defines what data will be displayed.
     */

    Q_OBJECT

public:
    PlottingMenueWindow(QHash<unsigned int, core*> *core_hash_list, QWidget *parent = nullptr);
    QHash<QString, QPair <QwtPlotCurve*, QTreeWidgetItem*> >
        *d_curvesAreaUPSIDE,
        *d_curvesThickUPSIDE,
        *d_curvesNBUPSIDE,
        *d_curvesSumAreaUPSIDE,
        *d_curvesAreaDOWNSIDE,
        *d_curvesThickDOWNSIDE,
        *d_curvesNBDOWNSIDE,
        *d_curvesSumAreaDOWNSIDE;
    int mode;
    int side;
    QColor mycolor;
    QStringList UpsideResidues, DownsideResidues;
    QHash<QString,Critall> *upcritlist;
    QHash<QString,Critall> *downcritlist;
//    void createTimePlot();
//    void setTimePlotAreaData(int Y_content);
    void setAxes( QList<QHash<QString, QPair <QwtPlotCurve*, QTreeWidgetItem*> >* > &plots);

private:
    QHash<unsigned int, core*> *core_hash_list;
    QPushButton *ChooseUpsideFile;
    QPushButton *closeB;
    QList<int> *Upsidenumbers, *Downsidenumbers;  // selection
    QComboBox *project;
    QComboBox *leaflet;
    QComboBox *Y_Axis;
    QComboBox *Databox;
    QLineEdit *thename;
    QLabel *color;

private slots:
    void setCOLOR();
    void setName();
    void chooseDir();
    void update_selection_cb();

signals:
    void plotcreated(int side);
    void getSelection(int side);
    void create_plot(unsigned int mem_id, QString leaflet, QString plot_name, QColor graph_color, int data, QString selection);
};


#endif // PLOTTINGMENUEWINDOW_H
