#include "tree2dgraphs.h"


tree2dgraphs::tree2dgraphs(QWidget *parent) : QTreeWidget(parent){}


void tree2dgraphs::mousePressEvent(QMouseEvent *event)
{
    QTreeWidgetItem *item = itemAt(event->pos());
    if(!item)
        setSelection(frameGeometry(),QItemSelectionModel::Clear);
    QTreeWidget::mousePressEvent(event);
}
