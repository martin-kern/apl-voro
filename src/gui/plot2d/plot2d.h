#ifndef _PLOT2D_H_
#define _PLOT2D_H_
#include <qmath.h>
#include "qwt/qwt_math.h"
#include "qwt/qwt_scale_engine.h"
#include "qwt/qwt_symbol.h"
#include "qwt/qwt_plot_grid.h"
#include "qwt/qwt_plot_marker.h"
#include "qwt/qwt_plot_curve.h"
#include "qwt/qwt_legend.h"
#include "qwt/qwt_text.h"
#include "qwt/qwt_plot.h"
#include "qwt/qwt_plot_grid.h"

class QwtPlotCurve;

class Plot2D: public QwtPlot
{
    Q_OBJECT

public:
    Plot2D(QWidget *parent);
    void setMarkerPos(double x);
    QwtPlotMarker *d_marker;
    QwtPlotGrid *grid;
};

#endif
