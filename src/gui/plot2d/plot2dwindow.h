#ifndef PLOT2DWINDOW_H
#define PLOT2DWINDOW_H

#include <QtWidgets>


#include "qwt/qwt_counter.h"
#include "qwt/qwt_plot_curve.h"
#include "qwt/qwt_legend.h"
#include "qwt/qwt_picker_machine.h"
#include "qwt/qwt_plot_zoomer.h"
#include "qwt/qwt_plot_panner.h"
#include "qwt/qwt_plot_renderer.h"
#include "qwt/qwt_text.h"
#include "qwt/qwt_math.h"
#include "qwt/qwt_plot_canvas.h"

#include "coredata/membran.h"
#include "gui/plot2d/plot2d.h"
#include "coredata/core.h"
#include "gui/plot2d/plot2d.h"
#include "gui/plot2d/coloring2d.h"
#include "gui/plot2d/tree2dgraphs.h"
#include "gui/plot2d/plottingmenuewindow.h"

class QwtPlotZoomer;
class QwtPlotPicker;
class QwtPlotPanner;
class Plot2D;
class QPolygon;


class CurveTreeWidgetItem : public QTreeWidgetItem
{
public:
    CurveTreeWidgetItem(QwtPlotCurve *curve)
    {
        this->curve = curve;
    }

    QwtPlotCurve* get_curve() {return curve;}

private:
    QwtPlotCurve *curve;
};


class Plot2DWindow : public QDockWidget
{
    Q_OBJECT

public:
    Plot2DWindow(QHash<unsigned int, core*> *core_hash_list,
                 int current_frame,
                 QWidget *parent);
    void set_marker(int frame);
    tree2dgraphs *residues;

    QToolBar *toolBar;
    QString currentDir;
    QHash<QString, QPair <QwtPlotCurve*, QTreeWidgetItem*> > *d_curvesArea, *d_curvesThick, *d_curvesSumArea;

    void setAxis(double maxX, double miny1, double maxy1, double miny2, double maxy2);

private:
    Plot2D *d_plot;
    double minxBottom,   maxXBottom,   minyleft,   maxyleft,  minyright,   maxyright;
    QTreeWidgetItem *actualitem;
    QTreeWidgetItem *topareaitem, *topthickitem, *topSumItem;
    QwtPlotZoomer *d_zoomer[2];
    QwtPlotPicker *d_picker;
    QwtPlotPanner *d_panner;
    QHash<unsigned int, core*> *core_hash_list;
    int frame_number;
//    QList<CurveTreeWidgetItem*> curve_items;

public slots:
    void remSelectedGraph();

private slots:
    void new_plot(unsigned int mem_id, QString leaflet, QString plot_name, QColor graph_color, int data, QString selection);
    void setPlots();

private Q_SLOTS:
    void requestColor(QTreeWidgetItem *item, int);
    void displayMarker(bool);
    void setBGColor();
    void exportDocument();
    void enableZoomMode(bool);
    void SelectedGraph(QTreeWidgetItem *item, int);
    void resetZoom();
    bool exportGraphs();
    void add_plot();

signals:
    void addnewPlot(int side);
};

#endif // PLOT2DWINDOW_H
