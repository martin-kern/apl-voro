#include "gui/plot2d/plot2d.h"


Plot2D::Plot2D(QWidget *parent):
    QwtPlot(parent)
{
    setAutoReplot(false);
    setCanvasBackground(QColor(Qt::white));

    // grid
    grid = new QwtPlotGrid();
    grid->enableXMin(true);
    grid->setMajorPen(QPen(Qt::gray, 0, Qt::DotLine));
    grid->setMinorPen(QPen(Qt::gray, 0 , Qt::DotLine));
    grid->attach(this);

    // axes
    enableAxis(QwtPlot::yRight);
    d_marker = new QwtPlotMarker();
    d_marker->setLineStyle(QwtPlotMarker::VLine);
    d_marker->setLabelAlignment(Qt::AlignRight | Qt::AlignBottom);
    d_marker->setLinePen(QPen(Qt::green, 3, Qt::DashDotLine));
    d_marker->attach(this);
    d_marker->setVisible(false);
    setAutoReplot(true);
}


void Plot2D::setMarkerPos(double x)
{
   d_marker->setValue(x, 0.0);
   replot();
}
