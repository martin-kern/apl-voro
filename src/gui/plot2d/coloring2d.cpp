#include "gui/plot2d/coloring2d.h"



Coloring2D::Coloring2D(QColor bgcolorin,QColor gridcolorin,QColor markercolorin,Plot2D *aplot,QWidget *parent)
    :QWidget(parent)
{
    int frameStyle = QFrame::Sunken | QFrame::Panel;
    bgcolor = bgcolorin;
    gridcolor = gridcolorin;
    markercolor = markercolorin;
    m_plot = aplot;

    backgroundlabel = new QLabel;
    backgroundlabel->setFrameStyle(frameStyle);
    backgroundlabel->setText(bgcolor.name());
    backgroundlabel->setPalette(QPalette(bgcolor));
    backgroundlabel->setAutoFillBackground(true);
    getbgcolor = new QPushButton;
    getbgcolor->setText("BackgroundColor");



    markercolorlabel = new QLabel;
    markercolorlabel->setFrameStyle(frameStyle);
    markercolorlabel->setText(markercolor.name());
    markercolorlabel->setPalette(QPalette(markercolor));
    markercolorlabel->setAutoFillBackground(true);
    getMarkerColor = new QPushButton;
    getMarkerColor->setText("MarkerColor");



    gridcolorlabel = new QLabel;
    gridcolorlabel->setFrameStyle(frameStyle);
    gridcolorlabel->setText(gridcolor.name());
    gridcolorlabel->setPalette(QPalette(gridcolor));
    gridcolorlabel->setAutoFillBackground(true);
    getgridcolor = new QPushButton;
    getgridcolor->setText("Gridcolor");


    QVBoxLayout *labelslayout = new QVBoxLayout;
    labelslayout->addWidget(backgroundlabel);
    labelslayout->addWidget(markercolorlabel);
    labelslayout->addWidget(gridcolorlabel);



    QVBoxLayout *buttonslayout = new QVBoxLayout;
    buttonslayout->addWidget(getbgcolor);
    buttonslayout->addWidget(getMarkerColor);
    buttonslayout->addWidget(getgridcolor);





    QHBoxLayout *editlayout = new QHBoxLayout;
    editlayout->addLayout(labelslayout);
    editlayout->addLayout(buttonslayout);



    QVBoxLayout *layout = new QVBoxLayout;
    layout->addLayout(editlayout);





    setLayout(layout);


    connect(getMarkerColor,SIGNAL(clicked()),this,SLOT(changemarkercolor()));
    connect(getbgcolor,SIGNAL(clicked()),this,SLOT(changebgcolor()));
    connect(getgridcolor,SIGNAL(clicked()),this,SLOT(changegridcolor()));



}




void Coloring2D::changebgcolor()
{


             bgcolor = QColorDialog::getColor(Qt::white, this);

         if (bgcolor.isValid()) {
             backgroundlabel->setText(bgcolor.name());
             backgroundlabel->setPalette(QPalette(bgcolor));
             backgroundlabel->setAutoFillBackground(true);
             m_plot->setCanvasBackground(bgcolor);
              m_plot->replot();

         }

}
void Coloring2D::changemarkercolor()
{


             markercolor = QColorDialog::getColor(Qt::black, this);

         if (markercolor.isValid()) {
             markercolorlabel->setText(markercolor.name());
             markercolorlabel->setPalette(QPalette(markercolor));
             markercolorlabel->setAutoFillBackground(true);
             m_plot->d_marker->setLinePen(QPen(markercolor, 3, Qt::DashDotLine));
             m_plot->replot();

         }

}

void Coloring2D::changegridcolor()
{


             gridcolor = QColorDialog::getColor(Qt::black, this);

         if (gridcolor.isValid())
         {
             gridcolorlabel->setText(gridcolor.name());
             gridcolorlabel->setPalette(QPalette(gridcolor));
             gridcolorlabel->setAutoFillBackground(true);

             m_plot->grid->setMajorPen(QPen(gridcolor, 0, Qt::DotLine));
             m_plot->grid->setMinorPen(QPen(gridcolor, 0 , Qt::DotLine));

              m_plot->replot();

         }

}






