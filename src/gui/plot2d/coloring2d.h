#ifndef COLORING2D_H
#define COLORING2D_H
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>
#include <QDoubleSpinBox>
#include <QColorDialog>
#include "gui/plot2d/plot2d.h"



/*!
 \brief

*/
class Coloring2D :public QWidget
{
        Q_OBJECT

public:

/*!
 \brief

 \param bgcolorin
 \param gridcolorin
 \param markercolorin
 \param aplot
 \param parent
*/
    Coloring2D(QColor bgcolorin,QColor gridcolorin,QColor markercolorin,Plot2D *aplot,QWidget *parent = 0);


    QColor bgcolor,gridcolor,markercolor; /**< TODO */ /*!< TODO */

private slots:

    /*!
     \brief

    */
    void changebgcolor();
    /**
     * @brief
     *
     */
    /*!
     \brief

    */
    void changemarkercolor();

    /*!
     \brief

    */
    void changegridcolor();



private:
    QLabel  *backgroundlabel, *markercolorlabel,*gridcolorlabel; /**< TODO */ /*!< TODO */
    QPushButton *getMarkerColor, *getbgcolor, *getgridcolor; /**< TODO */ /*!< TODO */
    Plot2D *m_plot; /**< TODO */ /*!< TODO */
    /**
     * @brief
     *
     */
    /*!
     \brief

    */
    void reselect();
};

#endif
