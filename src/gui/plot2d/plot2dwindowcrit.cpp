#include "gui/plot2d/plot2dwindowcrit.h"






class Zoomer: public QwtPlotZoomer
{
public:
    Zoomer(int xAxis, int yAxis, QwtPlotCanvas *canvas):QwtPlotZoomer(xAxis, yAxis, canvas)
    {
        setTrackerMode(QwtPicker::AlwaysOff);
        setRubberBand(QwtPicker::NoRubberBand);

        // RightButton: zoom out by 1
        // Ctrl+RightButton: zoom out to full size

        setMousePattern(QwtEventPattern::MouseSelect2,
                        Qt::RightButton,
                        Qt::ControlModifier);
        setMousePattern(QwtEventPattern::MouseSelect3,
                        Qt::RightButton);
    }
};


Plot2DWindowCrit::Plot2DWindowCrit(QWidget *parent):QMainWindow(parent)
{
    residues = new tree2dgraphs(this);
    percentitem = new QTreeWidgetItem;
    percentitem->setText(0, "Percent");
    percentitem->setBackground(0,Qt::white);
    residues->addTopLevelItem(percentitem);

    numitem = new QTreeWidgetItem;
    numitem->setText(0, "Count");
    numitem->setBackground(0,Qt::white);
    residues->addTopLevelItem(numitem);

    residues->setSelectionMode(QAbstractItemView::SingleSelection);
    connect(residues,SIGNAL(itemChanged(QTreeWidgetItem*,int)),this,SLOT(SelectedGraph()));
    connect(residues,SIGNAL(itemDoubleClicked(QTreeWidgetItem*,int)),this,SLOT(requestColor(QTreeWidgetItem*)));

    QStringList headerlist;
    headerlist.append("Name");
    headerlist.append("Plot");

    residues->setHeaderLabels(headerlist);
    residues->resizeColumnToContents(1);

    d_plot = new Plot2D(this);

    const int margin = 5;
    d_plot->setContentsMargins(margin, margin, margin, 0 );

    setContextMenuPolicy(Qt::NoContextMenu);

    d_zoomer[0] = new Zoomer(QwtPlot::xBottom, QwtPlot::yLeft, (QwtPlotCanvas*)d_plot->canvas());
    d_zoomer[0]->setRubberBand(QwtPicker::RectRubberBand);
    d_zoomer[0]->setRubberBandPen(QColor(Qt::green));
    d_zoomer[0]->setTrackerMode(QwtPicker::ActiveOnly);
    d_zoomer[0]->setTrackerPen(QColor(Qt::black));

    d_zoomer[1] = new Zoomer(QwtPlot::xBottom, QwtPlot::yRight, (QwtPlotCanvas*)d_plot->canvas());


    d_panner = new QwtPlotPanner(d_plot->canvas());
    d_panner->setMouseButton(Qt::MidButton);

    d_picker = new QwtPlotPicker(QwtPlot::xBottom,
                                 QwtPlot::yLeft,
                                 QwtPlotPicker::CrossRubberBand,
                                 QwtPicker::AlwaysOn,
                                 d_plot->canvas());
    d_picker->setStateMachine(new QwtPickerDragPointMachine());
    d_picker->setRubberBandPen(QColor(Qt::green));
    d_picker->setRubberBand(QwtPicker::CrossRubberBand);
    d_picker->setTrackerPen(QColor(Qt::black));


    toolBar = new QToolBar(this);
    toolBar->setFloatable(false);

    btnZoom = new QToolButton(toolBar);
    btnZoom->setObjectName(QString::fromUtf8("Zoom"));
    btnZoom->setIcon(QIcon(QPixmap(":img/lupe.png")));
    btnZoom->setCheckable(true);
    btnZoom->setText("Zoom");
    btnZoom->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    toolBar->addWidget(btnZoom);
    connect(btnZoom, SIGNAL(toggled(bool)), SLOT(enableZoomMode(bool)));

    btnResetZoom = new QToolButton(toolBar);
    btnResetZoom->setObjectName(QString::fromUtf8("Reet"));
    btnResetZoom->setIcon(QIcon(QPixmap(":img/lupecross.png")));

    btnResetZoom->setText("Reset Zoom");
    btnResetZoom->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    toolBar->addWidget(btnResetZoom);
    connect(btnResetZoom, SIGNAL(clicked()), SLOT(resetZoom()));

    btnBgCOLOR = new QToolButton(toolBar);
    btnBgCOLOR->setObjectName(QString::fromUtf8("Color"));
    btnBgCOLOR->setIcon(QIcon(QPixmap(":img/ColorOption.png")));
    btnBgCOLOR->setText("Coloring");
    btnBgCOLOR->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    toolBar->addWidget(btnBgCOLOR);
    connect(btnBgCOLOR, SIGNAL(clicked()), SLOT(setBGColor()));

    marker = new QToolButton(toolBar);
    marker->setObjectName(QString::fromUtf8("Marker"));
    marker->setIcon(QIcon(QPixmap(":img/Marke.png")));
    marker->setCheckable(true);
    marker->setText("Marker");
    marker->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    toolBar->addWidget(marker);
    connect(marker, SIGNAL(toggled(bool)), SLOT(displayMarker(bool)));

    graphexp = new QToolButton(toolBar);
    graphexp->setObjectName(QString::fromUtf8("graphexp"));
    graphexp->setIcon(QIcon(QPixmap(":img/graphexport.png")));
    graphexp->setText("Export");
    graphexp->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    toolBar->addWidget(graphexp);
    connect(graphexp,SIGNAL(clicked()),this,SLOT(exportGraph()));

    leftside = new QWidget(this);

    QVBoxLayout *treeButtons = new QVBoxLayout;
    treeButtons->addWidget(residues);

    leftside->setLayout(treeButtons);
    leftside->setMaximumWidth(180);

    QSplitter *sp = new QSplitter(this);
    sp->addWidget(leftside);
    sp->addWidget(d_plot);

    setCentralWidget(sp);

    btnExport = new QToolButton(toolBar);
    btnExport->setObjectName(QString::fromUtf8("Export"));
    btnExport->setIcon(QIcon(QPixmap(":img/imagexport.png")));
    btnExport->setText("Image");
    btnExport->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    toolBar->addWidget(btnExport);
    connect(btnExport, SIGNAL(clicked()), SLOT(exportDocument()));

    addToolBar(toolBar);
#ifndef QT_NO_STATUSBAR
    (void)statusBar();
#endif

    enableZoomMode(false);
}


bool Plot2DWindowCrit::exportGraph()
{
    QList<QTreeWidgetItem*>selected =  residues->selectedItems();
    if(selected.size() == 0)
    {
        QMessageBox msgBox;
        msgBox.setText("No selected Graph....");
        msgBox.exec();
    }
    else if(selected[0]->parent() == nullptr)
    {
        QMessageBox msgBox;
        msgBox.setText("No selected Graph....");
        msgBox.exec();
    }
    else
    {
        QwtPlotCurve *mc = nullptr;
        QTreeWidgetItem *item = selected[0];
        if(item->parent()->text(0) == "Percent" && d_curvespercent.find(item->text(0))!= d_curvespercent.end())
            mc = d_curvespercent.find(item->text(0)).value().first;
        if(item->parent()->text(0) == "Count" && d_curves.find(item->text(0))!=d_curves.end())
            mc =  d_curves.find(item->text(0)).value().first;

        QString fp = QDir::homePath();
        fp += "/graph.xvg";
        QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
                                                        fp,
                                                        tr("Grace plots (*.xvg)"));
        if ( !fileName.isEmpty())
        {
            QFile file(fileName);
            if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
                return false;

            QTextStream out(&file);
            out << "@type xy " <<"\n";

            for (unsigned int j = 0; j < mc->data()->size(); j++)
                out << mc->sample(j).x() << "   " << mc->sample(j).y() << "\n";

            file.close();
        }
    }
    return true;
}


void Plot2DWindowCrit::exportDocument()
{
    QString fileName = "graph.png";

    const QList<QByteArray> imageFormats = QImageWriter::supportedImageFormats();

    QStringList filter;
    filter += "PDF Documents (*.pdf)";

    if ( imageFormats.size() > 0 )
    {
        QString imageFilter("Images (");
        for ( int i = 0; i < imageFormats.size(); i++ )
        {
            if ( i > 0 )
                imageFilter += " ";
            imageFilter += "*.";
            imageFilter += imageFormats[i];
        }
        imageFilter += ")";
        filter += imageFilter;
    }
    fileName = QFileDialog::getSaveFileName(this,
                                            "Export File Name",
                                            fileName,
                                            filter.join(";;"),
                                            nullptr,
                                            QFileDialog::DontConfirmOverwrite);


    if ( !fileName.isEmpty() )
    {
        QwtPlotRenderer renderer;
        // flags to make the document look like the widget
        renderer.setDiscardFlag(QwtPlotRenderer::DiscardBackground, false);
        renderer.renderDocument(d_plot, fileName, QSizeF(300, 200), 85);
    }
}


void Plot2DWindowCrit::resetZoom()
{
    d_plot->setAxisScale(QwtPlot::xBottom ,minxBottom, maxXBottom);
    d_plot->setAxisScale(QwtPlot::yLeft ,minyleft, maxyleft);
    d_plot->setAxisScale(QwtPlot::yRight ,minyright, maxyright);
    d_plot->setAxisAutoScale(QwtPlot::yLeft,true);
    d_plot->setAxisAutoScale(QwtPlot::yRight,true);
    d_plot->setAxisAutoScale(QwtPlot::xBottom,true);
    d_plot->replot();
    d_zoomer[0]->setZoomBase();
    d_zoomer[1]->setZoomBase();
}


void Plot2DWindowCrit::enableZoomMode(bool on)
{
    // get axis min and max values to calculate a rectangle for initialize the zoomer
    double xLowerBound,xUpperBound,yLowerBound,yUpperBound;


    xLowerBound = d_plot->axisScaleDiv(QwtPlot::xBottom).lowerBound();
    xUpperBound = d_plot->axisScaleDiv(QwtPlot::xBottom).upperBound();
    yLowerBound = d_plot->axisScaleDiv(QwtPlot::yLeft).lowerBound();
    yUpperBound = d_plot->axisScaleDiv(QwtPlot::yLeft).upperBound();

    // initialize the zoomer with auto scale values
    d_zoomer[0]->setZoomBase(QRectF(xLowerBound, yUpperBound, yUpperBound-yLowerBound, xUpperBound-xLowerBound));
    d_zoomer[0]->setZoomBase(false);
    d_panner->setEnabled(on);

    d_zoomer[0]->setEnabled(on);
    d_zoomer[0]->zoom(0);
    double xLowerBound1,xUpperBound1,yLowerBound1,yUpperBound1;

    xLowerBound1 = d_plot->axisScaleDiv(QwtPlot::xBottom).lowerBound();
    xUpperBound1 = d_plot->axisScaleDiv(QwtPlot::xBottom).upperBound();
    yLowerBound1 = d_plot->axisScaleDiv(QwtPlot::yLeft).lowerBound();
    yUpperBound1 = d_plot->axisScaleDiv(QwtPlot::yLeft).upperBound();

    // initialize the zoomer with auto scale values
    d_zoomer[1]->setZoomBase(QRectF(xLowerBound1, yUpperBound1, yUpperBound1-yLowerBound1, xUpperBound1-xLowerBound1));
    // reset zoom level to 0
    d_zoomer[1]->setZoomBase(false);
    d_zoomer[1]->setEnabled(on);
    d_zoomer[1]->zoom(0);

    d_picker->setEnabled(!on);
}


void Plot2DWindowCrit::setPlots()
{
    residues->clear();
    QHash<QString, QPair <QwtPlotCurve*, QTreeWidgetItem*> >::Iterator it;
    for(it = d_curves.begin(); it != d_curves.end(); it++)
        percentitem->addChild(it.value().second);

    for(it = d_curvespercent.begin(); it != d_curvespercent.end(); it++)
        numitem->addChild(it.value().second);

    residues->resizeColumnToContents(1);
}


void Plot2DWindowCrit::SelectedGraph()
{
    QHash<QString, QPair <QwtPlotCurve*, QTreeWidgetItem*> >::Iterator it;
    for(it = d_curves.begin(); it != d_curves.end(); it++)
    {
        if(it.value().second->checkState(1)==Qt::Checked)
            it.value().first->attach(d_plot);
        else
            it.value().first->detach();
    }
    for(it = d_curvespercent.begin(); it != d_curvespercent.end(); it++)
    {
        if(it.value().second->checkState(1)==Qt::Checked)
           it.value().first->attach(d_plot);
        else
            it.value().first->detach();
    }
    d_plot->replot();
}


void Plot2DWindowCrit::setAxis()
{
    d_plot->enableAxis(QwtPlot::xTop,false);
    QString x_label("Time (Frames)");
    QString y_labelleft("Count");
    QString y_labelright("Percent");

    d_plot->setAxisTitle(QwtPlot::xBottom, x_label);
    d_plot->setAxisTitle(QwtPlot::yLeft, y_labelleft);
    d_plot->setAxisTitle(QwtPlot::yRight, y_labelright);

    d_plot->setAxisAutoScale(QwtPlot::yLeft,true);
    d_plot->setAxisAutoScale(QwtPlot::yRight,true);
    d_plot->setAxisAutoScale(QwtPlot::xBottom,true);

    d_plot->replot();

    d_zoomer[0]->setZoomBase();
    d_zoomer[1]->setZoomBase();
}


void Plot2DWindowCrit::requestColor(QTreeWidgetItem *item)
{
    if(item->parent()->text(0) == "Count" && d_curves.find(item->text(0))!= d_curves.end())
    {
        QColor mycolor = Qt::white;
        mycolor = QColorDialog::getColor(Qt::white, this );
        d_curves.find(item->text(0)).value().first->setPen(QPen(mycolor));
        item->setBackgroundColor(0,mycolor);
        SelectedGraph();
    }
    if(item->parent()->text(0) == "Percent" && d_curvespercent.find(item->text(0))!=d_curvespercent.end())
    {
        QColor mycolor = Qt::white;
        mycolor = QColorDialog::getColor(Qt::white, this );
        d_curvespercent.find(item->text(0)).value().first->setPen(QPen(mycolor));
        item->setBackgroundColor(0,mycolor);
        SelectedGraph();
    }
}


void Plot2DWindowCrit::setBGColor()
{
    Coloring2D *dcol;
    dcol = new Coloring2D(d_plot->canvasBackground().color(),d_plot->grid->majorPen().color(),d_plot->d_marker->linePen().color(),d_plot,this);
    dcol->setWindowFlags(Qt::Window);
    dcol->show();
}


void Plot2DWindowCrit::displayMarker(bool a)
{
    d_plot->d_marker->setVisible(a);
}


void Plot2DWindowCrit::setCriteriaData(int side)
{
    minxBottom = 0;
    maxXBottom = mems->size();
    minyleft = 0;
    maxyleft = (*mems)[0].leaflets["upside"]->QHside.size() + 100;
    minyright = 0;
    maxyright = 100;
    setAxis();


    QHash<QString, QPair <QwtPlotCurve*, QTreeWidgetItem*> >::Iterator it;
    for(it = d_curves.begin(); it != d_curves.end(); it++)
    {
        it.value().first->detach();
        d_plot->replot();
        numitem->removeChild(it.value().second);
        d_curves.remove(it.value().second->text(0));
    }
    for(it = d_curvespercent.begin(); it != d_curvespercent.end(); it++)
    {
        it.value().first->detach();
        d_plot->replot();
        percentitem->removeChild(it.value().second);
        d_curvespercent.remove(it.value().second->text(0));
    }
    for(QHash<QString,Critall > ::Iterator it = criteria->begin(); it != criteria->end(); it++)
    {
        const int ArraySize = int(maxXBottom);

        double X_VAL[ArraySize];
        double Y_VAL[ArraySize];
        double YVALPERC[ArraySize];
        for( int j=0; j<ArraySize; j++ )
        {
            double val ;
            double full;
            double G;
            QList<int> nums;

            if (side == 1)
            {
                (*mems)[j].leaflets["upside"]->matchCriterion_List(it.value().critlist, nums, it.value().and_or);
                full = (*mems)[j].leaflets["upside"]->QHside.size();
                G= nums.size();
                val = G/full;
            }
            else
            {
                (*mems)[j].leaflets["downside"]->matchCriterion_List(it.value().critlist, nums, it.value().and_or);
                full = (*mems)[j].leaflets["downside"]->QHside.size();
                G= nums.size();
                val = G/full;
            }
            X_VAL[j] = j;
            Y_VAL[j] = nums.size();
            YVALPERC[j]= val*100;
        }

        QString graphname = it.key();
        QwtPlotCurve *d_curve2 = new QwtPlotCurve(graphname);
        d_curve2->setRenderHint(QwtPlotItem::RenderAntialiased);
        d_curve2->setPen(QPen(Qt::green));
        d_curve2->setYAxis(QwtPlot::yLeft);
        d_curve2->setSamples(X_VAL, Y_VAL, ArraySize);


        QTreeWidgetItem *topx = new QTreeWidgetItem;
        topx->setFlags(Qt::ItemIsEnabled|Qt::ItemIsUserCheckable|Qt::ItemIsSelectable);
        topx->setCheckState(1, Qt::Unchecked);
        topx->setText(0, d_curve2->title().text());
        topx->setBackground(0, d_curve2->pen().color());
        numitem->addChild(topx);

        d_curves.insert(graphname,qMakePair(d_curve2,topx));

        QwtPlotCurve *d_curve = new QwtPlotCurve(graphname);
        d_curve->setRenderHint(QwtPlotItem::RenderAntialiased);
        d_curve->setPen(QPen(Qt::blue));
        d_curve->setYAxis(QwtPlot::yRight);
        d_curve->setSamples(X_VAL, YVALPERC, ArraySize);

        QTreeWidgetItem *topx2 = new QTreeWidgetItem;
        topx2->setFlags(Qt::ItemIsEnabled|Qt::ItemIsUserCheckable|Qt::ItemIsSelectable);
        topx2->setCheckState(1, Qt::Unchecked);
        topx2->setText(0, d_curve->title().text());
        topx2->setBackground(0, d_curve->pen().color());
        percentitem->addChild(topx2);
        d_curvespercent.insert(graphname,qMakePair(d_curve,topx2));
    }
}
