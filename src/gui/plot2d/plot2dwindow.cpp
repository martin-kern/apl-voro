#include "gui/plot2d/plot2dwindow.h"


class Zoomer: public QwtPlotZoomer
{
public:
    Zoomer(int xAxis, int yAxis, QwtPlotCanvas *canvas):
        QwtPlotZoomer(xAxis, yAxis, canvas)
    {
        setTrackerMode(QwtPicker::AlwaysOff);
        setRubberBand(QwtPicker::NoRubberBand);

        // RightButton: zoom out by 1
        // Ctrl+RightButton: zoom out to full size

        setMousePattern(QwtEventPattern::MouseSelect2,
            Qt::RightButton, Qt::ControlModifier);
        setMousePattern(QwtEventPattern::MouseSelect3,
            Qt::RightButton);
    }
};



Plot2DWindow::Plot2DWindow(QHash<unsigned int, core*> *core_hash_list,
                           int current_frame,
                           QWidget *parent):
    QDockWidget(parent)
{
    this->core_hash_list = core_hash_list;
    currentDir = QDir::homePath();
    frame_number = current_frame;


    setContextMenuPolicy(Qt::NoContextMenu);
    residues = new tree2dgraphs(this);
    residues->setSelectionMode(QAbstractItemView::SingleSelection);
    residues->setHeaderLabels({"Name", "Plot"});
    residues->resizeColumnToContents(1);
    topareaitem = new QTreeWidgetItem;
    topareaitem->setText(0, "Area");
    topareaitem->setBackground(0,Qt::white);
    residues->addTopLevelItem(topareaitem);
    topthickitem = new QTreeWidgetItem;
    topthickitem->setText(0, "Thickness");
    topthickitem->setBackground(0,Qt::white);
    residues->addTopLevelItem(topthickitem);
    topSumItem = new QTreeWidgetItem;
    topSumItem->setText(0, "SumArea");
    topSumItem->setBackground(0,Qt::white);
    residues->addTopLevelItem(topSumItem);


    connect(residues, &tree2dgraphs::itemChanged, this, &Plot2DWindow::SelectedGraph);
    connect(residues, &tree2dgraphs::itemDoubleClicked, this, &Plot2DWindow::requestColor);

    d_plot = new Plot2D(this);
    d_plot->setContentsMargins(5, 5, 5, 0 );
    d_plot->setAxisTitle(QwtPlot::xBottom, "Time (Frames)");
    d_plot->setAxisTitle(QwtPlot::yLeft, "Area ( nm² )");
    d_plot->setAxisTitle(QwtPlot::yRight, "Thickness ( nm )");
    d_plot->setAxisAutoScale(QwtPlot::yLeft, true);
    d_plot->setAxisAutoScale(QwtPlot::yRight, true);
    d_plot->setAxisAutoScale(QwtPlot::xBottom, true);

    d_zoomer[0] = new Zoomer(QwtPlot::xBottom, QwtPlot::yLeft, static_cast<QwtPlotCanvas*>(d_plot->canvas()));
    d_zoomer[0]->setRubberBand(QwtPicker::RectRubberBand);
    d_zoomer[0]->setRubberBandPen(QColor(Qt::green));
    d_zoomer[0]->setTrackerMode(QwtPicker::ActiveOnly);
    d_zoomer[0]->setTrackerPen(QColor(Qt::black));
    d_zoomer[1] = new Zoomer(QwtPlot::xBottom, QwtPlot::yRight, static_cast<QwtPlotCanvas*>(d_plot->canvas()));
    d_panner = new QwtPlotPanner(d_plot->canvas());
    d_panner->setMouseButton(Qt::MidButton);

    d_picker = new QwtPlotPicker(QwtPlot::xBottom,
                                 QwtPlot::yLeft,
                                 QwtPlotPicker::CrossRubberBand,
                                 QwtPicker::AlwaysOn,
                                 d_plot->canvas());
    d_picker->setStateMachine(new QwtPickerDragPointMachine());
    d_picker->setRubberBandPen(QColor(Qt::green));
    d_picker->setRubberBand(QwtPicker::CrossRubberBand);
    d_picker->setTrackerPen(QColor(Qt::black));

    // Tool bar widgets.
    toolBar = new QToolBar(this);
    toolBar->setFloatable(false);
    QToolButton *btnZoom = new QToolButton(toolBar);
    btnZoom->setObjectName(QString::fromUtf8("Zoom"));
    btnZoom->setIcon(QIcon(QPixmap(":img/lupe.png")));
    btnZoom->setCheckable(true);
    btnZoom->setText("Zoom");
    btnZoom->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    toolBar->addWidget(btnZoom);
    QToolButton *btnResetZoom = new QToolButton(toolBar);
    btnResetZoom->setObjectName(QString::fromUtf8("Reet"));
    btnResetZoom->setIcon(QIcon(QPixmap(":img/lupecross.png")));
    btnResetZoom->setText("Reset zoom");
    btnResetZoom->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    toolBar->addWidget(btnResetZoom);
    QToolButton *btnBgCOLOR = new QToolButton(toolBar);
    btnBgCOLOR->setObjectName(QString::fromUtf8("Color"));
    btnBgCOLOR->setIcon(QIcon(QPixmap(":img/ColorOption.png")));
    btnBgCOLOR->setText("Coloring");
    btnBgCOLOR->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    toolBar->addWidget(btnBgCOLOR);
    QToolButton *marker = new QToolButton(toolBar);
    marker->setObjectName(QString::fromUtf8("Marker"));
    marker->setIcon(QIcon(QPixmap(":img/Marke.png")));
    marker->setCheckable(true);
    marker->setText("Marker");
    marker->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    toolBar->addWidget(marker);
    QToolButton *graphexp = new QToolButton(toolBar);
    graphexp->setObjectName(QString::fromUtf8("graphexp"));
    graphexp->setIcon(QIcon(QPixmap(":img/graphexport.png")));
    graphexp->setText("Export");
    graphexp->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    toolBar->addWidget(graphexp);
    QToolButton *btnExport = new QToolButton(toolBar);
    btnExport->setObjectName(QString::fromUtf8("Export"));
    btnExport->setIcon(QIcon(QPixmap(":img/imagexport.png")));
    btnExport->setText("Image");
    btnExport->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    toolBar->addWidget(btnExport);


    QPushButton  *addGraph = new QPushButton(this);
    addGraph->setObjectName(QString::fromUtf8("AddGraph"));
    addGraph->setIcon(QIcon(QPixmap(":img/Button-Add-icon.png")));
    addGraph->setIconSize(QSize(30, 30));
    addGraph->setFixedSize(32, 32);
    addGraph->setStyleSheet("QPushButton { border: none }");
    QPushButton  *remGraph = new QPushButton(this);
    remGraph->setObjectName(QString::fromUtf8("RemoveGraph"));
    remGraph->setIcon(QIcon(QPixmap(":img/minus.png")));
    remGraph->setIconSize(QSize(30, 30));
    remGraph->setFixedSize(32, 32);
    remGraph->setStyleSheet("QPushButton { border: none }");

    connect(btnZoom, SIGNAL(toggled(bool)), SLOT(enableZoomMode(bool)));
    connect(btnResetZoom, SIGNAL(clicked()), SLOT(resetZoom()));
    connect(btnBgCOLOR, SIGNAL(clicked()), this, SLOT(setBGColor()));
    connect(marker, SIGNAL(toggled(bool)),this, SLOT(displayMarker(bool)));
    connect(graphexp, SIGNAL(clicked()), this, SLOT(exportGraphs()));
    connect(addGraph, SIGNAL(clicked()), this, SLOT(add_plot()));
    connect(remGraph, SIGNAL(clicked()), this, SLOT(remSelectedGraph()));

    QWidget *leftside = new QWidget(this);
    QHBoxLayout *Buttons = new QHBoxLayout;
    Buttons->addWidget(addGraph);
    Buttons->addWidget(remGraph);

    QVBoxLayout *treeButtons = new QVBoxLayout;
    treeButtons->addWidget(residues);
    treeButtons->addLayout(Buttons);
    leftside->setLayout(treeButtons);
    leftside->setMaximumWidth(180);

    QSplitter *sp = new QSplitter(this);
    sp->addWidget(leftside);
    sp->addWidget(d_plot);

    QMainWindow *main_window = new QMainWindow(parent);
    main_window->setCentralWidget(sp);


    connect(btnExport, SIGNAL(clicked()), SLOT(exportDocument()));

    main_window->addToolBar(toolBar);
    setWidget(main_window);
    enableZoomMode(false);
}


void Plot2DWindow::set_marker(int frame)
{
    this->frame_number = frame;
    d_plot->setMarkerPos(frame);
}


void Plot2DWindow::add_plot()
{
    PlottingMenueWindow *pw = new PlottingMenueWindow(core_hash_list, this);
    connect(pw, &PlottingMenueWindow::create_plot, this, &Plot2DWindow::new_plot);
    pw->show();
}


void Plot2DWindow::new_plot(unsigned int mem_id, QString leaflet, QString plot_name, QColor graph_color, int data, QString sel)
{
    /* Add a new curve to the plot.
     */

    int array_size = (*core_hash_list)[mem_id]->framemembrans.size();
    double *x_values = static_cast<double*>(malloc(unsigned(long(array_size)) * sizeof (double)));
    double *y_values = static_cast<double*>(malloc(unsigned(long(array_size)) * sizeof (double)));
    if (sel == "All residues")
    {
        QList<int> nums;
        for (int i = 0; i < array_size; i++)
        {
            nums = (*core_hash_list)[mem_id]->framemembrans[i].leaflets[leaflet]->QHside.keys();
            x_values[i] = double(i);
            if (data == 0)
                y_values[i] = (*core_hash_list)[mem_id]->framemembrans[i].leaflets[leaflet]->getAreaForList(nums);
            if (data == 1)
                y_values[i] = (*core_hash_list)[mem_id]->framemembrans[i].leaflets[leaflet]->getThickForList(nums);
            if (data == 2)
                y_values[i] = (*core_hash_list)[mem_id]->framemembrans[i].leaflets[leaflet]->getSumAreaForList(nums);
        }
    }
    else
    {
        Critall crit_all = (*core_hash_list)[mem_id]->get_criteria(sel);
        for (int i = 0; i < array_size; i++)
        {
            x_values[i] = double(i);
            // Average apl
            if (data == 0)
                y_values[i] = (*core_hash_list)[mem_id]->framemembrans[i].leaflets[leaflet]->area_for_selection(crit_all);
            // Average thickness
            if (data == 1)
                y_values[i] = (*core_hash_list)[mem_id]->framemembrans[i].leaflets[leaflet]->thickness_for_selection(crit_all);
            // Sum apl
            if (data == 2)
                y_values[i] = (*core_hash_list)[mem_id]->framemembrans[i].leaflets[leaflet]->sum_area_for_selection(crit_all);
        }
    }


    QwtPlotCurve *d_curve = new QwtPlotCurve(plot_name);
    d_curve->setRenderHint(QwtPlotItem::RenderAntialiased);
    d_curve->setPen(QPen(graph_color));
    if (data == 1)
        d_curve->setYAxis(QwtPlot::yRight);
    else
        d_curve->setYAxis(QwtPlot::yLeft);
    d_curve->setSamples(x_values, y_values, array_size);
    free(x_values);
    free(y_values);
    d_curve->attach(d_plot);

    CurveTreeWidgetItem *plot_item = new CurveTreeWidgetItem(d_curve);
    plot_item->setText(0, plot_name);
    plot_item->setCheckState(1, Qt::Checked);
    plot_item->setBackground(0, QBrush(graph_color));
    if (data == 0)
        topareaitem->addChild(plot_item);
    if (data == 1)
        topthickitem->addChild(plot_item);
    if (data == 2)
        topSumItem->addChild(plot_item);

    d_plot->replot();
}


bool Plot2DWindow::exportGraphs()
{
    CurveTreeWidgetItem *selection = nullptr;
    QTreeWidgetItemIterator it(residues);
    while (*it)
    {
        if ((*it)->isSelected() && (*it)->parent())
        {
            selection = static_cast<CurveTreeWidgetItem*>(*it);
            break;
        }
        it++;
    }
    if (selection == nullptr)
    {
        QMessageBox msgBox;
        msgBox.setText("No selected Graph....");
        msgBox.exec();
    }
    else
    {
        QwtPlotCurve *curve = selection->get_curve();
        QString fp = currentDir;
        fp += "/graph.xvg";

        QString fileName = QFileDialog::getSaveFileName(this,
                                                        "Save File",
                                                        fp,
                                                        "Grace plots (*.xvg)");
        if ( !fileName.isEmpty())
        {
            QDir *dir = new QDir(currentDir);
            currentDir = dir->absoluteFilePath(fileName);
            QFile file(fileName);
            if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
                return false;

            QTextStream out(&file);
            out << "@type xy " <<"\n";

            for (int j = 0; j < signed(curve->data()->size()); j++)
                out << curve->sample(j).x()<<"   "<< curve->sample(j).y()<<"\n";

            file.close();
        }
    }
    return true;
}


void Plot2DWindow::exportDocument()
{
    QString fileName = currentDir;
    const QList<QByteArray> imageFormats = QImageWriter::supportedImageFormats();

    QStringList filter;
    filter += "PDF Documents (*.pdf)";

    if ( imageFormats.size() > 0 )
    {
        QString imageFilter("Images (");
        for ( int i = 0; i < imageFormats.size(); i++ )
        {
            if ( i > 0 )
                imageFilter += " ";
            imageFilter += "*.";
            imageFilter += imageFormats[i];
        }
        imageFilter += ")";
        filter += imageFilter;
    }

    fileName = QFileDialog::getSaveFileName(this,
                                            tr("Export File Name"),
                                            fileName,
                                            filter.join(";;"));
    if ( !fileName.isEmpty() )
    {
        QDir *dir = new QDir(currentDir);
        currentDir = dir->absoluteFilePath(fileName);
        QwtPlotRenderer renderer;

        // flags to make the document look like the widget
        renderer.setDiscardFlag(QwtPlotRenderer::DiscardBackground, false);
        renderer.renderDocument(d_plot, fileName, QSizeF(300, 200), 85);
    }
}


void Plot2DWindow::resetZoom()
{
    d_plot->setAxisScale(QwtPlot::xBottom ,minxBottom, maxXBottom);
    d_plot->setAxisScale(QwtPlot::yLeft ,minyleft, maxyleft);
    d_plot->setAxisScale(QwtPlot::yRight ,minyright, maxyright);
    d_plot->setAxisAutoScale(QwtPlot::yLeft,true);
    d_plot->setAxisAutoScale(QwtPlot::yRight,true);
    d_plot->setAxisAutoScale(QwtPlot::xBottom,true);
    d_plot->replot();
    d_zoomer[0]->setZoomBase();
    d_zoomer[1]->setZoomBase();
}


void Plot2DWindow::enableZoomMode(bool on)
{
    // get axis min and max values to calculate a rectangle for initialize the zoomer
    double xLowerBound,xUpperBound,yLowerBound,yUpperBound;
    xLowerBound = d_plot->axisScaleDiv(QwtPlot::xBottom).lowerBound();
    xUpperBound = d_plot->axisScaleDiv(QwtPlot::xBottom).upperBound();
    yLowerBound = d_plot->axisScaleDiv(QwtPlot::yLeft).lowerBound();
    yUpperBound = d_plot->axisScaleDiv(QwtPlot::yLeft).upperBound();

    // initialize the zoomer with auto scale values
    d_zoomer[0]->setZoomBase(QRectF(xLowerBound, yUpperBound, yUpperBound-yLowerBound, xUpperBound-xLowerBound));
    d_zoomer[0]->setZoomBase(false);
    d_panner->setEnabled(on);
    d_zoomer[0]->setEnabled(on);
    d_zoomer[0]->zoom(0);
    double xLowerBound1,xUpperBound1,yLowerBound1,yUpperBound1;
    xLowerBound1 = d_plot->axisScaleDiv(QwtPlot::xBottom).lowerBound();
    xUpperBound1 = d_plot->axisScaleDiv(QwtPlot::xBottom).upperBound();
    yLowerBound1 = d_plot->axisScaleDiv(QwtPlot::yLeft).lowerBound();
    yUpperBound1 = d_plot->axisScaleDiv(QwtPlot::yLeft).upperBound();

    // initialize the zoomer with auto scale values
    d_zoomer[1]->setZoomBase(QRectF(xLowerBound1, yUpperBound1, yUpperBound1-yLowerBound1, xUpperBound1-xLowerBound1) );
    // reset zoom level to 0
    d_zoomer[1]->setZoomBase(false);
    // d_zoomer[1]->setAxis(d_plot->xTop, d_plot->yRight);
    d_zoomer[1]->setEnabled(on);
    d_zoomer[1]->zoom(0);
    d_picker->setEnabled(!on);
}


void Plot2DWindow::setPlots()
{
    QList<QString> toparechildren;
    QList<QString> topthickchildren;
    QList<QString> topSumchildren;
    for(int i = 0 ;i < topareaitem->childCount(); i++)
        toparechildren.push_back(topareaitem->child(i)->text(0));
    for(int i = 0 ;i < topthickitem->childCount(); i++)
        topthickchildren.push_back(topthickitem->child(i)->text(0));
    for(int i = 0 ;i < topSumItem->childCount(); i++)
        topSumchildren.push_back(topSumItem->child(i)->text(0));

    QHash<QString, QPair <QwtPlotCurve*, QTreeWidgetItem*> >::Iterator it;
    for(it = d_curvesArea->begin(); it != d_curvesArea->end(); it++)
    {
        if(!toparechildren.contains(it.value().second->text(0)))
            topareaitem->addChild(it.value().second);
    }
    for(it = d_curvesThick->begin(); it != d_curvesThick->end(); it++)
    {
        if(!topthickchildren.contains(it.value().second->text(0)))
            topthickitem->addChild(it.value().second);
    }
    for(it = d_curvesSumArea->begin(); it != d_curvesSumArea->end(); it++)
    {
        if(!topSumchildren.contains(it.value().second->text(0)))
            topSumItem->addChild(it.value().second);
    }
    residues->resizeColumnToContents(1);
}


void Plot2DWindow::SelectedGraph(QTreeWidgetItem *item, int)
{
    CurveTreeWidgetItem *curve_item = static_cast<CurveTreeWidgetItem*>(item);
    if(curve_item->parent())
    {
        if(curve_item->checkState(1) == Qt::Checked)
            curve_item->get_curve()->attach(d_plot);
        else
            curve_item->get_curve()->detach();
        d_plot->replot();
    }
}


void Plot2DWindow::setAxis(double maxX, double miny1, double maxy1, double miny2, double maxy2)
{
    d_plot->enableAxis(QwtPlot::xTop,false);

    minxBottom = 0;
    maxXBottom = maxX;
    minyleft = miny1;
    maxyleft = maxy1;
    minyright = miny2;
    maxyright = maxy2;

    QString x_label("Time (Frames)");
    QString y_labelleft("Area ( ");
    y_labelleft += "nm";
    y_labelleft +=  QChar(178);
    y_labelleft += QString(" )");

    QString y_labelright("Thickness (nm)");
    d_plot->setAxisTitle(QwtPlot::xBottom, x_label);
    d_plot->setAxisTitle(QwtPlot::yLeft, y_labelleft);
    d_plot->setAxisTitle(QwtPlot::yRight, y_labelright);
    d_plot->setAxisScale(QwtPlot::xBottom ,0, maxX);
    d_plot->setAxisScale(QwtPlot::yLeft ,miny1, maxy1);
    d_plot->setAxisScale(QwtPlot::yRight ,miny2, maxy2);
    d_plot->setAxisAutoScale(QwtPlot::yLeft,true);
    d_plot->setAxisAutoScale(QwtPlot::yRight,true);
    d_plot->setAxisAutoScale(QwtPlot::xBottom,true);
    d_plot->replot();
    d_zoomer[0]->setZoomBase();
    d_zoomer[1]->setZoomBase();
}


void Plot2DWindow::requestColor(QTreeWidgetItem *item, int)
{
    if(item->parent())
    {
        QColor new_color = QColorDialog::getColor(item->background(0).color(), this );
        if (new_color.isValid()) {
            item->setBackground(0, QBrush(new_color));
            static_cast<CurveTreeWidgetItem*>(item)->get_curve()->setPen(QPen(new_color));
            d_plot->replot();
        }
    }
}


void Plot2DWindow::setBGColor()
{
    Coloring2D *dcol;
    dcol = new Coloring2D(d_plot->canvasBackground().color(),
                          d_plot->grid->majorPen().color(),
                          d_plot->d_marker->linePen().color(),
                          d_plot,
                          this);
    dcol->setWindowFlags(Qt::Window);
    dcol->show();
}


void Plot2DWindow::displayMarker(bool a)
{
    d_plot->d_marker->setVisible(a);
    d_plot->setMarkerPos(frame_number);
}


void Plot2DWindow::remSelectedGraph()
{
    QTreeWidgetItemIterator it(residues);
    while (*it)
    {
        if ((*it)->isSelected() && (*it)->parent())
        {
            static_cast<CurveTreeWidgetItem*>(*it)->get_curve()->detach();
            (*it)->parent()->removeChild(*it);
        }
        it++;
    }

    d_plot->replot();
}
