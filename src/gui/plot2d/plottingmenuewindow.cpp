#include "gui/plot2d/plottingmenuewindow.h"
#include <QDebug>

PlottingMenueWindow::PlottingMenueWindow(QHash<unsigned int, core*> *core_hash_list, QWidget *parent)
    : QWidget(parent)
{
    this->core_hash_list = core_hash_list;
    setWindowFlag(Qt::Window);
    setWindowTitle("New Plot..");
    project = new QComboBox(this);
    for (QHash<unsigned int, core*>::iterator i = core_hash_list->begin(); i != core_hash_list->end(); i++)
        project->addItem(i.value()->name(), i.key());

    leaflet = new QComboBox(this);
    leaflet->addItem("upside");
    leaflet->addItem("downside");

    Y_Axis = new QComboBox(this);
    Y_Axis->addItem("Avg. Area");
    Y_Axis->addItem("Avg. Thickness");
    Y_Axis->addItem("Sum. Area");

    Databox = new QComboBox(this);
    Databox->addItem("All residues", -1);
    update_selection_cb();

    thename = new QLineEdit(this);

    color = new QLabel(this);
    mycolor = Qt::red;
    color->setText(mycolor.name());
    color->setPalette(QPalette(mycolor));
    color->setAutoFillBackground(true);
    QPushButton *colorchoose = new QPushButton(this);
    colorchoose->setText("Color");
    connect(colorchoose, SIGNAL(clicked()), this, SLOT(setCOLOR()));

    QFormLayout *formLayout = new QFormLayout;

    formLayout->addRow("Project:", project);
    formLayout->addRow("Leaflet:", leaflet);
    formLayout->addRow(tr("Y-Axis:"), Y_Axis);
    formLayout->addRow(tr("Data:"), Databox);
    formLayout->addRow(color, colorchoose);
    formLayout->addRow(tr("Name"),thename);

    ChooseUpsideFile = new QPushButton(this);
    ChooseUpsideFile->setText("Save");
    closeB = new QPushButton(this);
    closeB->setText("Close");

    QHBoxLayout *buttonField = new QHBoxLayout;
    buttonField->addWidget(closeB);
    buttonField->addWidget(ChooseUpsideFile);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addLayout(formLayout);
    layout->addLayout(buttonField);

    setLayout(layout);

    connect(ChooseUpsideFile, SIGNAL(clicked()), this, SLOT(chooseDir()));
    connect(closeB, SIGNAL(clicked()), this, SLOT(close()));
    connect(project, SIGNAL(currentIndexChanged(int)), this, SLOT(update_selection_cb()));
}


void PlottingMenueWindow::update_selection_cb()
{
    Databox->clear();
    Databox->addItem("All residues", -1);
    int current_project_id = project->currentData().toUInt();
    QList<QString> crit_list = (*core_hash_list)[current_project_id]->get_crit_list()->keys();
    for (int i = 0; i < crit_list.length(); i++)
        Databox->addItem(crit_list[i]);
}


void PlottingMenueWindow::chooseDir()
{
    emit create_plot(project->currentData().toUInt(),
                     leaflet->currentText(),
                     thename->text(),
                     mycolor,
                     Y_Axis->currentIndex(),
                     Databox->currentText());
    close();

}


void PlottingMenueWindow::setAxes(QList<QHash<QString, QPair <QwtPlotCurve*, QTreeWidgetItem*> >* > &plots)
{
    d_curvesAreaUPSIDE      = plots[0];
    d_curvesThickUPSIDE     = plots[1];
    d_curvesNBUPSIDE        = plots[2];
    d_curvesSumAreaUPSIDE   = plots[3];
    d_curvesAreaDOWNSIDE    = plots[4];
    d_curvesThickDOWNSIDE   = plots[5];
    d_curvesNBDOWNSIDE      = plots[6];
    d_curvesSumAreaDOWNSIDE = plots[7];
}


void PlottingMenueWindow::setCOLOR()
{
    mycolor = QColorDialog::getColor(Qt::red, this);

    if (mycolor.isValid())
    {
        color->setText(mycolor.name());
        color->setPalette(QPalette(mycolor));
        color->setAutoFillBackground(true);
    }
}


void PlottingMenueWindow::setName()
{
    thename->setText(Databox->currentText());
}
