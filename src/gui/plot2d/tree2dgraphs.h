#ifndef TREE2DGRAPHS_H
#define TREE2DGRAPHS_H
#include <QWidget>
#include <QTreeWidget>
#include <QMouseEvent>


class tree2dgraphs: public QTreeWidget
{
     Q_OBJECT

public:
    tree2dgraphs(QWidget*);
    void mousePressEvent(QMouseEvent *event);
};

#endif // TREE2DGRAPHS_H
