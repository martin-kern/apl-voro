#ifndef PLOT2DWINDOWCRIT_H
#define PLOT2DWINDOWCRIT_H

#include <qmainwindow.h>
#include <QSplitter>
#include <QMessageBox>
#include <QMenu>
#include <QPushButton>
#include <qregexp.h>
#include <qtoolbar.h>
#include <qtoolbutton.h>
#include <qlabel.h>
#include <qlayout.h>
#include <qstatusbar.h>
#include <qprinter.h>
#include <qpicture.h>
#include <qpainter.h>
#include <qfiledialog.h>
#include <qimagewriter.h>
#include <qprintdialog.h>
#include <qfileinfo.h>
#include "qwt/qwt_counter.h"
#include "qwt/qwt_plot_curve.h"
#include "qwt/qwt_legend.h"
#include "qwt/qwt_picker_machine.h"
#include "qwt/qwt_plot_zoomer.h"
#include "qwt/qwt_plot_panner.h"
#include "qwt/qwt_plot_renderer.h"
#include "qwt/qwt_text.h"
#include "qwt/qwt_plot_canvas.h"
#include "qwt/qwt_math.h"
#include "coredata/membran.h"
#include "gui/plot2d/plot2d.h"
#include "coredata/core.h"
#include "gui/plot2d/plot2d.h"
#include "gui/plot2d/coloring2d.h"
#include "gui/plot2d/tree2dgraphs.h"

class QwtPlotZoomer;
class QwtPlotPicker;
class QwtPlotPanner;
class Plot2D;
class QPolygon;


/**
 * @brief
 *
 */
/*!
 \brief

*/
class Plot2DWindowCrit : public QMainWindow
{
    Q_OBJECT

public:
/**
 * @brief
 *
 * @param parent
 */
/*!
 \brief

 \param parent
*/
    Plot2DWindowCrit(QWidget *parent);
    QMenu *menu; /**< TODO */ /*!< TODO */
    QVector<Membran> *framemems; /**< TODO */ /*!< TODO */
    tree2dgraphs *residues; /**< TODO */ /*!< TODO */
    QWidget *leftside;  /*!< TODO */
    Plot2D *d_plot; /**< TODO */ /*!< TODO */
    QHash<int,Membran> *mems; /*!< TODO */

    QToolButton *btnExport, *btnZoom, * btnBgCOLOR, *marker,*btnResetZoom, *graphexp; /**< TODO */ /*!< TODO */
    QToolBar *toolBar; /**< TODO */ /*!< TODO */
    QHash<QString, QPair<QwtPlotCurve*,QTreeWidgetItem*> > d_curves, d_curvespercent; /**< TODO */ /*!< TODO */
    QHash<QString,Critall > *criteria; /**< TODO */ /*!< TODO */

    /**
     * @brief
     *
     */
    /*!
     \brief

    */
    void setPlots();
    /**
     * @brief
     *
     */
    /*!
     \brief

    */
    void setAxis();
    /**
     * @brief
     *
     * @param side
     */
    /*!
     \brief

     \param side
    */
    void setCriteriaData(int side);
private:
   double minxBottom,   maxXBottom,   minyleft,   maxyleft,  minyright,   maxyright; /**< TODO */ /*!< TODO */
   QTreeWidgetItem *percentitem, *numitem; /**< TODO */ /*!< TODO */

public slots:



private Q_SLOTS:

    /**
     * @brief
     *
     * @param item
     */
    /*!
     \brief

     \param item
    */
    void requestColor(QTreeWidgetItem *item);
    /**
     * @brief
     *
     * @param bool
     */
    /*!
     \brief

     \param bool
    */
    void displayMarker(bool);
    /**
     * @brief
     *
     */
    /*!
     \brief

    */
    void setBGColor();
    /**
     * @brief
     *
     */
    /*!
     \brief

    */
    void exportDocument();
    /**
     * @brief
     *
     * @param bool
     */
    /*!
     \brief

     \param bool
    */
    void enableZoomMode(bool);
    /**
     * @brief
     *
     */
    /*!
     \brief

    */
    void SelectedGraph();
    /**
     * @brief
     *
     */
    /*!
     \brief

    */
    void resetZoom();
    /**
     * @brief
     *
     * @return bool
     */
    /*!
     \brief

     \return bool
    */
    bool exportGraph();



private:



    QwtPlotZoomer *d_zoomer[2]; /**< TODO */ /*!< TODO */
    QwtPlotPicker *d_picker; /**< TODO */ /*!< TODO */
    QwtPlotPanner *d_panner; /**< TODO */ /*!< TODO */

};

#endif // PLOT2DWINDOW_H
