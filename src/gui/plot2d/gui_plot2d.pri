SOURCES += gui/plot2d/plot2d.cpp \
           gui/plot2d/plot2dwindow.cpp \
           gui/plot2d/tree2dgraphs.cpp \
           gui/plot2d/coloring2d.cpp \
           gui/plot2d/plottingmenuewindow.cpp \
    gui/plot2d/plot2dwindowcrit.cpp


HEADERS += 		   gui/plot2d/plot2d.h \
                           gui/plot2d/plot2dwindow.h \
                           gui/plot2d/plottingmenuewindow.h \
                           gui/plot2d/tree2dgraphs.h \
                           gui/plot2d/coloring2d.h \
    gui/plot2d/plot2dwindowcrit.h
