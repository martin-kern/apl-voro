#ifndef PROJECTWIDGET_H
#define PROJECTWIDGET_H

#include <QtWidgets>
#include "treeviewer.h"
#include "projectwizardpage.hpp"
#include "indextree.h"


class Projectwindow : public QWizard
{
    Q_OBJECT
public:
    Projectwindow(QWidget *parent = nullptr);

private:
    void connections_setup();
    void initializePage(int id) override;
    LipidSelectionWizardPage *molecule_tree;
    ProjectWizardPage *project_setup_page;
    ProteinSelectionPage *protein_selection_page;
};
#endif // PROJECTWINDOW_H
