#ifndef CRITERIONSELECTOR_H
#define CRITERIONSELECTOR_H
#include <QWidget>
#include <QtGui>
#if QT_VERSION >= 0x050000
#include <QtWidgets>
#endif

/*!
 \brief

*/
class CriterionSelector: public QWidget
{
     Q_OBJECT
public:

   /*!
    \brief

    \param wordlist
    \param parent
   */
   explicit CriterionSelector(QStringList wordlist,QWidget *parent = 0);
    QComboBox *prime, *type; /**< TODO */ /*!< TODO */
    QCompleter *completer; /**< TODO */ /*!< TODO */
    QPushButton *plus, *minus; /**< TODO */ /*!< TODO */
    QLineEdit *useredit; /**< TODO */ /*!< TODO */
    int id; /**< TODO */ /*!< TODO */

signals:

    /*!
     \brief

     \param x
    */
    void onclose(int x);

public slots:

    /*!
     \brief

    */
    void willclose();

    /*!
     \brief

     \param a
    */
    void setvalidators(int a);

};

#endif // CRITERIONSELECTOR_H
