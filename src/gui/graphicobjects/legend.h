#ifndef LEGEND_H
#define LEGEND_H
#include <QtWidgets>
#include "gui/colorgradients.hpp"


class Legend : public QGraphicsItem
{
public:
    Legend(double low_bound, double upper_bound);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *widget) override;
    QRectF boundingRect() const override;
    void set_labels(double min , double max);
    void set_gradient();

private:
    QString low_bound, upper_bound;
    QRectF bounding_rect;
    QLinearGradient _legend_gradient;

};

#endif // LEGEND_H
