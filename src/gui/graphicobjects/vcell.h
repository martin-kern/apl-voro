#ifndef VCELL_H
#define VCELL_H

#include <QColor>
#include <QBrush>
#include <QPainter>
#include <QStyle>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsItem>
#include "coredata/atom.h"



class Vcell : public QGraphicsPolygonItem
{
public:
    enum { Type = UserType + 1 };
    int type() const override{return Type;}
    Vcell(const QColor &pcolor,const QColor &color,const QColor &ccolor, Atom *cell);
    QColor brushcolor,pencolor,cellcolor;
    QPainterPath shape() const override;
    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *item, QWidget *widget) override;
    int selectionflag;
    QPolygonF stuff;
    void changedat(Atom *cell);
    Atom *a;

private:
    double x, y;


};

#endif
