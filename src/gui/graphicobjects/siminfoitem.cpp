#include "siminfoitem.hpp"

SimInfoItem::SimInfoItem(QString name) : QGraphicsSimpleTextItem()
{
    this->name = name;
    setZValue(3);
    scale_factor = 1;
    leaflet = "upside";
    time = 0;
    average_apl = 0;
    average_thickness = 0;
    frame = 0;
    zoom = 1;
    setTransform(QTransform(scale_factor, 0, 0, -scale_factor, 0, 0));
    QString voro_info = QString("%1\nLeaflet: %2\nZoom: %3\nTime: %4 ns\nFrame: %5\nAverage APL: %6\nAverage Thickness: %7")
            .arg(name, leaflet).arg(zoom).arg(time / 1000).arg(frame).arg(average_apl, 0, 'f', 3).arg(average_thickness, 0, 'f', 3);
    setText(voro_info);
}


void SimInfoItem::paint(QPainter *painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
    painter->setPen(QPen(QColor(255, 255, 255, 200)));
    painter->setBrush(QBrush(QColor(255, 255, 255, 200)));
    painter->drawRect(boundingRect());
    painter->setPen(QPen(Qt::black));
    QGraphicsSimpleTextItem::paint(painter, option, widget);
}


void SimInfoItem::info_update(QString leaflet, float time, int frame, int zoom, double average_apl, double average_thickness)
{
    this->leaflet = leaflet;
    this->time = time;
    this->frame = frame;
    this->zoom = zoom;
    this->average_apl = average_apl;
    this->average_thickness = average_thickness;
    QString voro_info = QString("%1\nLeaflet: %2\nZoom: %3\nTime: %4 ns\nFrame: %5\nAverage APL: %6\nAverage Thickness: %7")
            .arg(name, leaflet).arg(zoom).arg(time / 1000).arg(frame).arg(average_apl, 0, 'f', 3).arg(average_thickness, 0, 'f', 3);
    setText(voro_info);
}


void SimInfoItem::zoom_update(int zoom)
{
    this->zoom = zoom;
    QString voro_info = QString("%1\nLeaflet: %2\nZoom: %3\nTime: %4 ns\nFrame: %5\nAverage APL: %6\nAverage Thickness: %7")
            .arg(name, leaflet).arg(zoom).arg(time / 1000).arg(frame).arg(average_apl, 0, 'f', 3).arg(average_thickness, 0, 'f', 3);
    setText(voro_info);
}
