#include "gui/graphicobjects/vcell.h"
#include "coredata/atom.h"



Vcell::Vcell(const QColor &pencolor,const QColor &color,const QColor &ccolor, Atom *cell)
{
    a = cell;
    this->x = cell->getX();
    this->y = cell->getY();
    this->brushcolor = color;
    this->pencolor = pencolor;
    this->cellcolor= ccolor;

    selectionflag = 0;
    setFlag(QGraphicsItem::ItemIsSelectable);
    cell->makeQpolygon(stuff);
}


void Vcell::changedat(Atom *cell)
{
    a = cell;
    prepareGeometryChange();
    this->x = cell->getX();
    this->y = cell->getY();
    stuff.clear();
    cell->makeQpolygon(stuff);
}


QRectF Vcell::boundingRect() const
{
    return stuff.boundingRect();
}


QPainterPath Vcell::shape() const
{
    QPainterPath path;
    QGraphicsPolygonItem item(stuff);
    path =  item.shape();

    return path;
}


void Vcell::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget* /*unused*/)
{
    QBrush b;
    painter->setPen(QPen(QBrush(pencolor), 0));
    painter->setBrush(b);
    if(option->state & QStyle::State_Selected)
    {
        painter->setBrush(brushcolor);
        painter->drawPolygon(stuff,Qt::FillRule(0));
    }
    else
    {
        QColor color = this->data(1).value<QColor>();
        painter->setBrush(color);
        painter->drawPolygon(stuff,Qt::FillRule(0));
    }
}



