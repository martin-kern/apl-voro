#include "legend.h"

Legend::Legend(double low_bound, double upper_bound)
{
    bounding_rect = QRectF(0, 0, 80, 250);
    setTransform(QTransform(1, 0, 0, -1, 0, 0));
    this->upper_bound.setNum(upper_bound);
    this->low_bound.setNum(low_bound);
    _legend_gradient = ColorGradients::current_gradient();
    setZValue(3);
}

void Legend::paint(QPainter *painter, const QStyleOptionGraphicsItem* /*unused*/, QWidget* /*unused*/)
{
    // Background
    painter->setPen(QPen(QColor(255, 255, 255, 200), 0));
    painter->setBrush(QBrush(QColor(255, 255, 255, 200)));
    painter->drawRect(QRectF(0, 0, 80, 250));

    // Gradient
    QRectF gradient_rect = QRectF(20, 25, 20, 200);
    painter->setPen(QPen(Qt::black, 0));
    _legend_gradient.setStart(gradient_rect.bottomLeft());
    _legend_gradient.setFinalStop(gradient_rect.topLeft());
    painter->setBrush(_legend_gradient);
    painter->drawRect(gradient_rect);

    // scale marks
    painter->drawLine(QLineF(20, 45, 25, 45));
    painter->drawLine(QLineF(20, 65, 25, 65));
    painter->drawLine(QLineF(20, 85, 25, 85));
    painter->drawLine(QLineF(20, 105, 25, 105));
    painter->drawLine(QLineF(20, 125, 30, 125));
    painter->drawLine(QLineF(20, 145, 25, 145));
    painter->drawLine(QLineF(20, 165, 25, 165));
    painter->drawLine(QLineF(20, 185, 25, 185));
    painter->drawLine(QLineF(20, 205, 25, 205));

    // Labels
    painter->drawText(gradient_rect.topLeft() - QPointF(0, 5), upper_bound);
    painter->drawText(gradient_rect.bottomLeft() + QPointF(0, 15), low_bound);
}

void Legend::set_labels(double min , double max)
{
    low_bound.setNum(min, 'g', 2);
    upper_bound.setNum(max, 'g', 2);
    update();
}


void Legend::set_gradient()
{
    _legend_gradient = ColorGradients::current_gradient();
    update();
}


QRectF Legend::boundingRect() const
{
    return bounding_rect;
}
