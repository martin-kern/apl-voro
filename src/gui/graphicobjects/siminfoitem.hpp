#ifndef SIMINFOITEM_HPP
#define SIMINFOITEM_HPP


#include <QtWidgets>

class SimInfoItem : public QGraphicsSimpleTextItem
{
public:
    SimInfoItem(QString name);

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override;
    void info_update(QString leaflet,
                     float time,
                     int frame,
                     int zoom,
                     double aveage_apl,
                     double average_thickness);
    void zoom_update(int zoom);

private:
    double scale_factor;
    QString name;
    QString leaflet;
    double time;
    double average_apl;
    double average_thickness;
    int frame;
    int zoom;
};

#endif // SIMINFOITEM_HPP
