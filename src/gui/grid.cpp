#include "grid.hpp"

Grid::Grid(QColor color, QGraphicsItem *parent) : QGraphicsItem(parent)
{
    setZValue(1);  // Draw grid on top of the Voronoi diagram
    _bounding_rect = QRectF(0, 0, 0, 0);
    this->_color = color;
    _grid_on = true;
    _spacing = 10;
    scale_factor = 1;
}


QRectF Grid::boundingRect() const
{
    return _bounding_rect;
}

void Grid::set_color(QColor color)
{
    this->_color = color;
    update();
}

void Grid::set_grid_spacing(int spacing)
{
    this->_spacing = spacing;
    update();
}


void Grid::paint(QPainter *painter, const QStyleOptionGraphicsItem* /*option*/, QWidget* /*widget*/)
{
//    painter->setBrush(QBrush(_color));
    painter->setPen(QPen(_color, 0));
    double edge_distance = 40 / scale_factor;
    double vertical_line_pos = _bounding_rect.y() + edge_distance - fmod((_bounding_rect.y() + edge_distance), _spacing);
    double horizontal_line_pos = _bounding_rect.x() + edge_distance - fmod((_bounding_rect.x() + edge_distance), _spacing);
    double x_axis_pos = _bounding_rect.y() + edge_distance;

    if (_grid_on)
    {

        // horizontal lines
        for (int i = 0; i < int((_bounding_rect.height()) / _spacing) + 1; i++)
        {
            painter->drawLine(QLineF(_bounding_rect.x() + edge_distance,
                                    vertical_line_pos,
                                    _bounding_rect.width() + _bounding_rect.x(),
                                    vertical_line_pos));
            vertical_line_pos = vertical_line_pos + _spacing;
        }
        // vertical lines
        for (int i = 0; i < int(_bounding_rect.width() / _spacing) + 1; i++)
        {
            painter->drawLine(QLineF(horizontal_line_pos,
                                    x_axis_pos,
                                    horizontal_line_pos,
                                    _bounding_rect.height() + _bounding_rect.y() + edge_distance));
            horizontal_line_pos = horizontal_line_pos + _spacing;
        }
    }

    painter->setPen(QPen(Qt::black, 0));
    // x-axis
    painter->drawLine(QLineF(_bounding_rect.x() + edge_distance,
                            x_axis_pos,
                            _bounding_rect.width() + _bounding_rect.x(),
                            x_axis_pos));
    // 0 mark
    painter->drawLine(QLineF(0,
                            x_axis_pos,
                            0,
                            x_axis_pos + 7 / scale_factor));



    // y-axis
    double y_axis_pos = _bounding_rect.x() + edge_distance;
    painter->drawLine(QLineF(y_axis_pos,
                             x_axis_pos,
                             y_axis_pos,
                             _bounding_rect.height() + _bounding_rect.y() + edge_distance));
    // 0 mark
    painter->drawLine(QLineF(y_axis_pos,
                             0,
                             y_axis_pos + 7 / scale_factor,
                             0));

    // axis labels
    painter->scale(1 / scale_factor, -1 / scale_factor);
    painter->drawText(QPointF(-2, (-x_axis_pos * scale_factor + 15)), "0");
    painter->drawText(QPointF((y_axis_pos * scale_factor - 15), 2), "0");
}


void Grid::toggle_grid()
{
    _grid_on = !_grid_on;
}


void Grid::set_bounds(double x, double y, double w, double h)
{
    _bounding_rect = QRectF(x, y, w, h);
}


void Grid::set_bounds(QRect viewport)
{
    _bounding_rect = viewport;
}


int Grid::_start(int x, int spacing)
{
    if (x < 0)
        return x + (-x % spacing);
    else
        return x - (x % spacing);
}
