#ifndef TREEVIEWER_H
#define TREEVIEWER_H

#include <QtWidgets>
#include "io/pdb/pdbreader.h"
#include "wizardtreewidget.hpp"
#include "data/setup_parameters.hpp"
#include "data/constants.hpp"


class LipidSelectionWizardPage : public QWizardPage
{
    Q_OBJECT
public:
    LipidSelectionWizardPage(QWizard *parent = nullptr);
    void FillTree(std::vector<molecule_atoms> atomlist);
    bool isComplete() const override;

private:
    bool validatePage() override;
    WizardTreeWidget *moleculeTree;
    QWizard *parent;

private slots:
    void auto_select_key_atoms();
    void selection_preview(QTreeWidgetItem *item, int column);

signals:
    void project_setup_done(setup_parameters data);
};
#endif


