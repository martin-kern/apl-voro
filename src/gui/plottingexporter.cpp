#include "gui/plottingexporter.h"


bool PairsSorting(const QPair<double, double> &p1, const QPair<double, double> &p2)
{
    return p1.first < p2.first;
}

Plottingexporter::Plottingexporter(QWidget *parent)
    : QWidget(parent)
{
    setWindowTitle("Plot..");
    ChooseUpsideFile = new QPushButton(this);
    ChooseUpsideFile->setText("Save");
    closeB = new QPushButton(this);
    closeB->setText("Close");
    checksidelabel= new QLabel;
    checksidelabel->setText("Side");

    typelabel=new QLabel;
    typelabel->setText("Type:");

    contentlabel=new QLabel;
    contentlabel->setText("Content:");

    selectframeslabel=new QLabel;
    selectframeslabel->setText("Frames:");

    chooseFrameslabel=new QLabel;
    chooseFrameslabel->setText("Range:");

    checkarealabel= new QLabel;
    checkarealabel->setText("Area");
    checkArea= new QCheckBox(this);

    checkthickabel= new QLabel;
    checkthickabel->setText("Thickness");
    checkThick= new QCheckBox(this);

    QHBoxLayout *areaandThicklayout = new QHBoxLayout;
    areaandThicklayout->addWidget(checkarealabel);
    areaandThicklayout->addWidget(checkArea);
    areaandThicklayout->addWidget(checkthickabel);
    areaandThicklayout->addWidget(checkThick);

    checkalllabel = new QLabel;
    checkalllabel->setText("All");
    checkallMol= new QCheckBox(this);

    checkselectlabel= new QLabel;
    checkselectlabel->setText("Selection");
    checkselectMol= new QCheckBox(this);

    QHBoxLayout *checkallANDselectlayout = new QHBoxLayout;
    checkallANDselectlayout->addWidget(checkalllabel);
    checkallANDselectlayout->addWidget(checkallMol);
    checkallANDselectlayout->addWidget(checkselectlabel);
    checkallANDselectlayout->addWidget(checkselectMol);

    checkallFrameslabel= new QLabel;
    checkallFrameslabel->setText("All Frames");
    allFrames= new QCheckBox(this);

    checkSelectFrameslabel= new QLabel;
    checkSelectFrameslabel->setText("Select range");
    selectFrames= new QCheckBox(this);

    QHBoxLayout *checkallFrameslayout = new QHBoxLayout;
    checkallFrameslayout ->addWidget(checkallFrameslabel);
    checkallFrameslayout ->addWidget(allFrames);
    checkallFrameslayout ->addWidget(checkSelectFrameslabel);
    checkallFrameslayout ->addWidget(selectFrames);

    side= new QComboBox(this);
    side->addItem("Upside");
    side->addItem("Downside");

    checkbeginlabel= new QLabel;
    checkbeginlabel->setText("Begin");
    beginframe= new QComboBox(this);
    beginframe->setEnabled(false);

    checkendlabel= new QLabel;
    checkendlabel->setText("End");
    endframe= new QComboBox(this);
    endframe->setEnabled(false);

    QHBoxLayout *checkbeginANDendlayout = new QHBoxLayout;
    checkbeginANDendlayout->addWidget(checkbeginlabel);
    checkbeginANDendlayout->addWidget(beginframe);
    checkbeginANDendlayout->addWidget(checkendlabel);
    checkbeginANDendlayout->addWidget(endframe);

    QVBoxLayout *labelleft = new QVBoxLayout;
    labelleft->addWidget(checksidelabel);
    labelleft->addWidget(typelabel);
    labelleft->addWidget(contentlabel);
    labelleft->addWidget(selectframeslabel);
    labelleft->addWidget(chooseFrameslabel);

    QVBoxLayout *righcheckboxLayout = new QVBoxLayout;
    righcheckboxLayout->addWidget(side);
    righcheckboxLayout->addLayout(areaandThicklayout);
    righcheckboxLayout->addLayout(checkallANDselectlayout);
    righcheckboxLayout->addLayout(checkallFrameslayout);
    righcheckboxLayout->addLayout(checkbeginANDendlayout);

    QHBoxLayout *leftTOrightlayout = new QHBoxLayout;
    leftTOrightlayout->addLayout(labelleft);
    leftTOrightlayout->addLayout(righcheckboxLayout);

    QHBoxLayout *buttonField = new QHBoxLayout;
    buttonField->addWidget(closeB);
    buttonField->addWidget(ChooseUpsideFile);

    SingleArea = new QLabel;
    SingleArea->setText("Options for plotting a single frame: ");
    SingleX = new QLabel;
    SingleX->setText("Plot by X");
    SingleAreaX=new QCheckBox(this);
    SingleY = new QLabel;
    SingleY->setText("Plot by Y");
    SingleAreaY=new QCheckBox(this);
    SingleXY = new QLabel;
    SingleXY->setText("Plot 3D");
    SingleAreaXY=new QCheckBox(this);
    QHBoxLayout *SingleXYCHECK = new QHBoxLayout;
    SingleXYCHECK ->addWidget(SingleX);
    SingleXYCHECK ->addWidget(SingleAreaX);
    SingleXYCHECK ->addWidget(SingleY);
    SingleXYCHECK ->addWidget(SingleAreaY);
    SingleXYCHECK ->addWidget(SingleXY);
    SingleXYCHECK ->addWidget(SingleAreaXY);

    QVBoxLayout *AreaOptionsLayout = new QVBoxLayout;
    AreaOptionsLayout->addWidget(SingleArea);
    AreaOptionsLayout->addLayout(SingleXYCHECK);

    QFrame *singleoptFrame = new QFrame;
    singleoptFrame->setFrameStyle(QFrame::Box);
    singleoptFrame->setLayout(AreaOptionsLayout);

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addLayout(leftTOrightlayout);
    layout->addWidget(singleoptFrame);
    layout->addLayout(buttonField);
    setLayout(layout);

    connect(ChooseUpsideFile,SIGNAL(clicked()),this,SLOT(chooseDir()));
    connect(closeB,SIGNAL(clicked()),this,SLOT(closeit()));
    connect(SingleAreaX,SIGNAL(toggled(bool)),this,SLOT(singleAreaXToggled()));
    connect(SingleAreaY,SIGNAL(toggled(bool)),this,SLOT(singleAreaYToggled()));
    connect(SingleAreaXY,SIGNAL(toggled(bool)),this,SLOT(singleAreaXYToggled()));
    connect(checkallMol,SIGNAL(toggled(bool)),this,SLOT(checkallToggled()));
    connect(checkselectMol,SIGNAL(toggled(bool)),this,SLOT(checkselectToggled()));
    connect(checkArea,SIGNAL(toggled(bool)),this,SLOT(checkAreaToggled()));
    connect(checkThick,SIGNAL(toggled(bool)),this,SLOT(checkThickToggled()));
    connect(allFrames,SIGNAL(toggled(bool)),this,SLOT(allFramesToggled()));
    connect(beginframe,SIGNAL(currentIndexChanged(int)),this,SLOT(beginFrameChanged()));
    connect(endframe,SIGNAL(currentIndexChanged(int)),this,SLOT(endFrameChanged()));
    connect(selectFrames,SIGNAL(toggled(bool)),this,SLOT(selectFramesToggled()));
}


void Plottingexporter::chooseDir()
{
    QList<int> numbers;
    if(side->currentIndex() == 0)  // UP
    {
        if(!checkArea->isChecked() && !checkThick->isChecked())
        {
            QMessageBox::StandardButton ret;
            ret = QMessageBox::warning(this, tr("Application"), tr("Please select a type!"), QMessageBox::Ok);
            if (ret == QMessageBox::Ok)
                return;
        }

        if(!checkallMol->isChecked() && !checkselectMol->isChecked())
        {
            QMessageBox::StandardButton ret;
            ret = QMessageBox::warning(this, tr("Application"), tr("Please select a content!"), QMessageBox::Ok);
            if (ret == QMessageBox::Ok)
                return;
        }

        if(checkselectMol->isChecked() && Upsidenumbers.size() == 0)
        {
            QMessageBox::StandardButton ret;
            ret = QMessageBox::warning(this, tr("Application"), tr("There is no active selection!"), QMessageBox::Ok);
            if (ret == QMessageBox::Ok)
                return;
        }

        if(checkselectMol->isChecked() && Upsidenumbers.size() > 0)
            numbers = Upsidenumbers.toList();

        if(!allFrames->isChecked() && !selectFrames->isChecked())
        {
            QMessageBox::StandardButton ret;
            ret = QMessageBox::warning(this, tr("Application"), tr("Please select a set of frames!"), QMessageBox::Ok);
            if (ret == QMessageBox::Ok)
                return;
        }

        if(selectFrames->isChecked() && !SingleAreaX->isChecked() && !SingleAreaY->isChecked() && !SingleAreaXY->isChecked()&& (beginframe->currentIndex()==endframe->currentIndex()))
        {
            QMessageBox::StandardButton ret;
            ret = QMessageBox::warning(this, tr("Application"), tr("Please select options for single frame plotting!"), QMessageBox::Ok);
            if (ret == QMessageBox::Ok)
                return;
        }
    }

    if(side->currentIndex()==1)  // Down
    {
        if(!checkArea->isChecked() && !checkThick->isChecked())
        {
            QMessageBox::StandardButton ret;
            ret = QMessageBox::warning(this, tr("Application"), tr("Please select a type!"), QMessageBox::Ok);
            if (ret == QMessageBox::Ok)
                return;
        }
        if(!checkallMol->isChecked() && !checkselectMol->isChecked())
        {
            QMessageBox::StandardButton ret;
            ret = QMessageBox::warning(this, tr("Application"), tr("Please select a content!"), QMessageBox::Ok);
            if (ret == QMessageBox::Ok)
                return;
        }
        if(checkselectMol->isChecked() && Downsidenumbers.size() == 0)
        {
            QMessageBox::StandardButton ret;
            ret = QMessageBox::warning(this, tr("Application"), tr("There is no active selection!"), QMessageBox::Ok);
            if (ret == QMessageBox::Ok)
                return;
        }
        if(checkselectMol->isChecked() && Downsidenumbers.size() > 0)
            numbers = Downsidenumbers.toList();

        if(!allFrames->isChecked() && !selectFrames->isChecked())
        {
            QMessageBox::StandardButton ret;
            ret = QMessageBox::warning(this, tr("Application"), tr("Please select a set of frames!"), QMessageBox::Ok);
            if (ret == QMessageBox::Ok)
                return;
        }
        if(selectFrames->isChecked() && !SingleAreaX->isChecked() && !SingleAreaY->isChecked() && !SingleAreaXY->isChecked() && (beginframe->currentIndex()==endframe->currentIndex()))
        {
            QMessageBox::StandardButton ret;
            ret = QMessageBox::warning(this, tr("Application"), tr("Please select options for single frame plotting!"), QMessageBox::Ok);
            if (ret == QMessageBox::Ok)
                return;
        }
    }

    if (plotALL(numbers))
    {
        QMessageBox msgBox;
        msgBox.setText("Saved! \n Do you want to continue ?");
        QPushButton *yesButton = msgBox.addButton(tr("Yes"), QMessageBox::ActionRole);
        QPushButton *nolButton = msgBox.addButton(tr("No"), QMessageBox::ActionRole);
        msgBox.exec();
        if (msgBox.clickedButton() == yesButton)
            msgBox.close();
        else if (msgBox.clickedButton() == nolButton)
        {
            msgBox.close();
            closeit();
        }
    }
    else
    {
        QMessageBox::StandardButton ret;
        ret = QMessageBox::critical(this, tr("Application"), tr("An error occured while saving!"), QMessageBox::Ok);
        if (ret == QMessageBox::Ok)
            return;
    }
}


bool Plottingexporter::plotALL(QList<int> numbers)
{
    if(checkArea->isChecked())
    {
        if(checkallMol->isChecked())
            return plotArea(numbers);
        if(checkselectMol->isChecked())
            return plotArea(numbers);
    }
    else if(checkThick->isChecked())
    {
        if(checkallMol->isChecked())
            return  plotThick(numbers);
        if(checkselectMol->isChecked())
            return plotThick(numbers);
    }
    return true;
}



bool Plottingexporter::plotArea(QList<int> nums)
{
    QList<int> numbers = nums;
    QString fp = QDir::homePath();
    fp += "/untitled.xvg";
    if(allFrames->isChecked())
    {
        fileName = QFileDialog::getSaveFileName(this, tr("Save File"), fp, tr("gracefiles (*.xvg)"));
        QFile file(fileName);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
            return false;

         QTextStream out(&file);
         out << "@type xy " <<"\n";
         out << "@xaxis  label \"Time (ps)\""<<"\n";
         out << "@yaxis  label \"Area \\c\\E\\C\\S2\""<<"\n";

         for (int i = 0 ; i < mycore->framemembrans.size(); i++)
         {
             if(side->currentIndex() == 0) // UP
             {
                 if(checkallMol->isChecked())
                     numbers = mycore->framemembrans[i].leaflets["upside"]->QHside.keys();
                 int div = numbers.size();
                 double myarea =0;
                 double time = mycore->framemembrans[i].time;
                 for (int j = 0 ; j < div; j++)
                 {
                     if(mycore->framemembrans[i].leaflets["upside"]->QHside.find(numbers[j]) != mycore->framemembrans[i].leaflets["upside"]->QHside.end())
                         myarea += mycore->framemembrans[i].leaflets["upside"]->QHside[numbers[j]].getArea();
                 }
                 double val = myarea/div;
                 out << time <<"   "<<val<<"\n";
             }
             if(side->currentIndex() == 1)  // down
             {
                 if(checkallMol->isChecked())
                     numbers = mycore->framemembrans[i].leaflets["downside"]->QHside.keys();
                 int div = numbers.size();
                 double myarea =0;
                 double time = mycore->framemembrans[i].time;
                 for (int j = 0 ; j < div; j++)
                 {
                     if(mycore->framemembrans[i].leaflets["downside"]->QHside.find(numbers[j]) != mycore->framemembrans[i].leaflets["downside"]->QHside.end())
                         myarea += mycore->framemembrans[i].leaflets["downside"]->QHside[numbers[j]].getArea();
                 }
                 out << time << "   " << myarea/div << "\n";
             }
         }
         file.close();
         return true;
    }
    else if (selectFrames->isChecked())
    {
        if(beginframe->currentIndex() == endframe->currentIndex())
        {
            if(SingleAreaX->isChecked())
            {
                QList< QPair<double, double> >XAREA;
                if(side->currentIndex()==0)  // UP
                {
                    QString fp = QDir::homePath();
                    fp += "/untitled.xvg";
                    fileName = QFileDialog::getSaveFileName(this, tr("Save File"), fp, tr("gracefiles (*.xvg)"));
                    if(checkallMol->isChecked())
                        numbers = mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside.keys();

                    for (int i = 0 ;i < numbers.size(); i++)
                    {
                        if(mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside.find(numbers[i]) != mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside.end())
                        {
                            QPair<double, double> pair;
                            pair.first= mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside[numbers[i]].getX();
                            pair.second= mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside[numbers[i]].getArea();
                            XAREA.push_back(pair);
                        }
                    }
                }
                if(side->currentIndex()==1)//down
                {
                    if(checkallMol->isChecked())
                        numbers = mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside.keys();

                    for (int i = 0 ;i < numbers.size(); i++)
                    {
                        if(mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside.find(numbers[i]) != mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside.end())
                        {
                            QPair<double, double> pair;
                            pair.first= mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside[numbers[i]].getX();
                            pair.second= mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside[numbers[i]].getArea();
                            XAREA.push_back(pair);
                        }
                    }
                }
                qSort(XAREA.begin(),XAREA.end(),PairsSorting);
                QFile file(fileName);
                if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
                    return false;

                QTextStream out(&file);
                out << "@type xy " <<"\n";
                out << "@xaxis  label \"X(\\c\\E\\C)\""<<"\n";
                out << "@yaxis  label \"Area \\c\\E\\C\\S 2\""<<"\n";
                for(int i = 0 ; i < XAREA.size(); i++)
                    out<< XAREA[i].first<<"   "<<XAREA[i].second<<"\n";
                file.close();
                return true;
            }
            else if(SingleAreaY->isChecked())
            {
                QString fp = QDir::homePath();
                fp += "/untitled.xvg";
                QList< QPair<double, double> >YAREA;
                if(side->currentIndex()==0) //UP
                {
                    fileName = QFileDialog::getSaveFileName(this, tr("Save File"), fp, tr("gracefiles (*.xvg)"));
                    if(checkallMol->isChecked())
                        numbers = mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside.keys();
                    for (int i = 0 ;i < numbers.size(); i++)
                    {
                        QPair<double, double> pair;
                        if(mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside.find(numbers[i]) != mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside.end())
                        {
                            pair.first= mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside[numbers[i]].getY();
                            pair.second= mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside[numbers[i]].getArea();
                            YAREA.push_back(pair);
                        }
                    }
                }
            if(side->currentIndex()==1) //down
            {
                QString fp = QDir::homePath();
                fp += "/untitled.xvg";
                fileName = QFileDialog::getSaveFileName(this, tr("Save File"), fp, tr("gracefiles (*.xvg)"));
                if(checkallMol->isChecked())
                    numbers = mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside.keys();
                for (int i = 0 ;i < numbers.size(); i++)
                {
                    if(mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside.find(numbers[i]) != mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside.end())
                    {
                        QPair<double, double> pair;
                        pair.first= mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside[numbers[i]].getY();
                        pair.second= mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside[numbers[i]].getArea();
                        YAREA.push_back(pair);
                    }
                }
            }
            qSort(YAREA.begin(),YAREA.end(),PairsSorting);
            QFile file(fileName);
            if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
                return false;
            QTextStream out(&file);
            out << "@type xy " <<"\n";
            out << "@xaxis  label \"Y(\\c\\E\\C)\""<<"\n";
            out << "@yaxis  label \"Area \\c\\E\\C\\S 2\""<<"\n";
            for(int i = 0 ; i < YAREA.size(); i++)
                out<< YAREA[i].first<<"   "<<YAREA[i].second<<"\n";
            file.close();
            return true;
            }
            else if(SingleAreaXY->isChecked())
            {
                if(side->currentIndex()==0)  // UP
                {
                    QString fp = QDir::homePath();
                    fp += "/untitled.dat";
                    fileName = QFileDialog::getSaveFileName(this, tr("Save File"), fp, tr("gnuplot (*.dat)"));
                    QFile file(fileName);
                    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
                        return false;

                    QTextStream out(&file);
                    out<<"# this data was written with APL@VORO"<<"\n";
                    out<<"# example commands for visualizing with gnuplot:"<<"\n";
                    out<<"# >plot set style data lines"<<"\n";
                    out<<"# >plot set contour base"<<"\n";
                    out<<"# >plot set surface"<<"\n";
                    out<<"# >plot set dgrid3d 60,50,1"<<"\n";
                    out<<"# >plot show contour"<<"\n";
                    out<<"# >plot set surface"<<"\n";
                    out<<"# >plot set pm3d"<<"\n";
                    out<<"# X"<<"   "<<"Y"<<"   "<<"Z"<<"\n";
                    if(checkallMol->isChecked())
                        numbers = mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside.keys();
                    for (int i = 0 ;i < numbers.size(); i++)
                    {
                        if(mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside.find(numbers[i]) != mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside.end())
                        {
                            double x = mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside[numbers[i]].getX();
                            double y= mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside[numbers[i]].getY();
                            double area = mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside[numbers[i]].getArea();
                            out << x << "   " << y << "   " << area << "\n";
                        }
                    }
                    file.close();
                    return true;
                }
                if(side->currentIndex()==1) //down
                {
                    QString fp = QDir::homePath();
                    fp += "/untitled.dat";
                    fileName = QFileDialog::getSaveFileName(this, tr("Save File"), fp, tr("gnuplot (*.dat)"));
                    QFile file(fileName);
                    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
                        return false;

                    QTextStream out(&file);
                    out<<"# this data was written with APL@VORO"<<"\n";
                    out<<"# example commands for visualizing with gnuplot:"<<"\n";
                    out<<"# >plot set style data lines"<<"\n";
                    out<<"# >plot set contour base"<<"\n";
                    out<<"# >plot set surface"<<"\n";
                    out<<"# >plot set dgrid3d 60,50,1"<<"\n";
                    out<<"# >plot show contour"<<"\n";
                    out<<"# >plot set surface"<<"\n";
                    out<<"# >plot set pm3d"<<"\n";
                    out<<"# X"<<"   "<<"Y"<<"   "<<"Z"<<"\n";
                    if(checkallMol->isChecked())
                        numbers = mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside.keys();
                    for (int i = 0 ;i < numbers.size(); i++)
                    {
                        if(mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside.find(numbers[i]) != mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside.end())
                        {
                            double X= mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside[numbers[i]].getX();
                            double Y= mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside[numbers[i]].getY();
                            double area= mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside[numbers[i]].getArea();
                            out << X << "   " << Y << "   " << area << "\n";
                        }
                    }
                    file.close();
                    return true;
                }
            }
        }
        else if(beginframe->currentIndex() < endframe->currentIndex())
        {
            QString fp = QDir::homePath();
            fp += "/untitled.xvg";
            fileName = QFileDialog::getSaveFileName(this, tr("Save File"), fp, tr("gracefiles (*.xvg)"));
            QFile file(fileName);
            if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
                return false;

            QTextStream out(&file);
            out << "@type xy " <<"\n";
            out << "@xaxis  label \"Time (ps)\""<<"\n";
            out << "@yaxis  label \"Area \\c\\E\\C\\S 2\""<<"\n";
            for (int i = beginframe->currentIndex() ; i < endframe->currentIndex(); i++)
            {
                if(side->currentIndex()==0) //UP
                {
                    if(checkallMol->isChecked())
                        numbers = mycore->framemembrans[i].leaflets["upside"]->QHside.keys();
                    int div = numbers.size();
                    double myarea =0;
                    double time = mycore->framemembrans[i].time;
                    for (int j = 0 ; j < div; j++)
                    {
                        if(mycore->framemembrans[i].leaflets["upside"]->QHside.find(numbers[j]) != mycore->framemembrans[i].leaflets["upside"]->QHside.end())
                            myarea += mycore->framemembrans[i].leaflets["upside"]->QHside[numbers[j]].getArea();
                    }
                    out << time << "   " << myarea/div << "\n";
                }
                if(side->currentIndex()==1) //down
                {
                    if(checkallMol->isChecked())
                        numbers = mycore->framemembrans[i].leaflets["downside"]->QHside.keys();
                    int div = numbers.size();
                    double myarea =0;
                    double time = mycore->framemembrans[i].time;
                    for (int j = 0 ; j < div; j++)
                    {
                        if(mycore->framemembrans[i].leaflets["downside"]->QHside.find(numbers[j]) != mycore->framemembrans[i].leaflets["downside"]->QHside.end())
                            myarea += mycore->framemembrans[i].leaflets["downside"]->QHside[numbers[j]].getArea();

                    }
                    out <<time <<"   "<<myarea/div<<"\n";
                }
            }
            file.close();
            return true;
        }
    }
    return true;
}


bool Plottingexporter::plotThick(QList<int> nums)
{
    QList<int>numbers = nums;
    if(allFrames->isChecked())
    {
        QString fp = QDir::homePath();
        fp += "/untitled.xvg";
        fileName = QFileDialog::getSaveFileName(this, tr("Save File"), fp, tr("gracefiles (*.xvg)"));
        QFile file(fileName);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
            return false;

        QTextStream out(&file);
        out << "@type xy " <<"\n";
        out << "@xaxis  label \"Time (ps)\""<<"\n";
        out << "@yaxis  label \"(\\c\\E\\C)\""<<"\n";

        for (int i = 0 ; i < mycore->framemembrans.size(); i++)
        {
            if(side->currentIndex() ==0) //UP
            {
                if(checkallMol->isChecked())
                    numbers = mycore->framemembrans[i].leaflets["upside"]->QHside.keys();
                int div = numbers.size();
                double mythick =0;
                double time = mycore->framemembrans[i].time;
                for (int j = 0 ; j < div; j++)
                {
                    if(mycore->framemembrans[i].leaflets["upside"]->QHside.find(numbers[j]) != mycore->framemembrans[i].leaflets["upside"]->QHside.end())
                        mythick += mycore->framemembrans[i].leaflets["upside"]->QHside[numbers[j]].getThick();
                }
                out <<time<<"   "<<mythick/div<<"\n";
            }
            if(side->currentIndex() ==1) //DOWN
            {
                if(checkallMol->isChecked())
                    numbers = mycore->framemembrans[i].leaflets["downside"]->QHside.keys();
                int div = numbers.size();
                double mythick =0;
                double time = mycore->framemembrans[i].time;
                for (int j = 0 ; j < div; j++)
                {
                    if(mycore->framemembrans[i].leaflets["downside"]->QHside.find(numbers[j]) != mycore->framemembrans[i].leaflets["downside"]->QHside.end())
                        mythick += mycore->framemembrans[i].leaflets["downside"]->QHside[numbers[j]].getThick();

                }
                out <<time<<"   "<<mythick/div<<"\n";
            }
        }
        file.close();
        return true;
    }
    else if(selectFrames->isChecked())
    {
        if(beginframe->currentIndex() == endframe->currentIndex())
        {
            if(!SingleAreaX->isChecked() && !SingleAreaY->isChecked() && !SingleAreaXY->isChecked())
            {
                QMessageBox::StandardButton ret;
                ret = QMessageBox::warning(this, tr("Application"), tr("Please select a type!"), QMessageBox::Ok);
                if (ret == QMessageBox::Ok)
                    return false;
            }
            else if(SingleAreaX->isChecked())
            {
                QString fp = QDir::homePath();
                fp += "/untitled.xvg";
                fileName = QFileDialog::getSaveFileName(this, tr("Save File"), fp, tr("gracefiles (*.xvg)"));
                QList< QPair<double, double> >XTHICK;
                if(side->currentIndex()==0)//UP
                {
                    if(checkallMol->isChecked())
                        numbers = mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside.keys();
                    for (int i = 0 ;i < numbers.size(); i++)
                    {
                        if(mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside.find(numbers[i]) != mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside.end())
                        {
                            QPair<double, double> pair;
                            pair.first= mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside[numbers[i]].getX();
                            pair.second= mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside[numbers[i]].getThick();
                            XTHICK.push_back(pair);
                        }
                    }
                }
                if(side->currentIndex()==1)//DOWN
                {
                    if(checkallMol->isChecked())
                        numbers = mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside.keys();
                    for (int i = 0 ;i < numbers.size(); i++)
                    {
                        if(mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside.find(numbers[i]) != mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside.end())
                        {
                            QPair<double, double> pair;
                            pair.first= mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside[numbers[i]].getX();
                            pair.second= mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside[numbers[i]].getThick();
                            XTHICK.push_back(pair);
                        }
                    }
                }
                qSort(XTHICK.begin(),XTHICK.end(),PairsSorting);
                QFile file(fileName);
                if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
                    return false;

                 QTextStream out(&file);
                 out << "@type xy " <<"\n";
                 out << "@xaxis  label \"X (\\c\\E\\C)\""<<"\n";
                 out << "@yaxis  label \"Distance (\\c\\E\\C)\""<<"\n";
                 for(int i = 0 ; i < XTHICK.size(); i++)
                     out<< XTHICK[i].first<<"   "<<XTHICK[i].second<<"\n";
                 file.close();
                 return true;
            }

            else if(SingleAreaY->isChecked())
            {
                QString fp = QDir::homePath();
                fp += "/untitled.xvg";
                fileName = QFileDialog::getSaveFileName(this, tr("Save File"), fp, tr("gracefiles (*.xvg)"));
                QList< QPair<double, double> >THICK;
                if(side->currentIndex()==0)//UP
                {
                    if(checkallMol->isChecked())
                        numbers = mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside.keys();
                    for (int i = 0 ;i < numbers.size(); i++)
                    {
                        if(mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside.find(numbers[i]) != mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside.end())
                        {
                            QPair<double, double> pair;
                            pair.first= mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside[numbers[i]].getY();
                            pair.second= mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside[numbers[i]].getThick();
                            THICK.push_back(pair);
                        }
                    }
                }
                if(side->currentIndex()==1)//DOWN
                {
                    if(checkallMol->isChecked())
                        numbers = mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside.keys();
                    for (int i = 0 ;i < numbers.size(); i++)
                    {
                        if(mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside.find(numbers[i]) != mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside.end())
                        {
                            QPair<double, double> pair;
                            pair.first= mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside[numbers[i]].getY();
                            pair.second= mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside[numbers[i]].getThick();
                            THICK.push_back(pair);
                        }
                    }
                }
                qSort(THICK.begin(),THICK.end(),PairsSorting);
                QFile file(fileName);
                if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
                    return false;

                QTextStream out(&file);
                out << "@type xy " <<"\n";
                out << "@xaxis  label \"Y (\\c\\E\\C)\""<<"\n";
                out << "@yaxis  label \"Distance (\\c\\E\\C)\""<<"\n";
                for(int i = 0 ; i < THICK.size(); i++)
                    out<< THICK[i].first<<"   "<<THICK[i].second<<"\n";
                file.close();
                return true;
            }
            else if(SingleAreaXY->isChecked())
            {
                QString fp = QDir::homePath();
                fp += "/untitled.dat";
                fileName = QFileDialog::getSaveFileName(this, tr("Save File"), fp, tr("gnuplot (*.dat)"));

                if(side->currentIndex()==0)//UP
                {
                    QFile file(fileName);
                    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
                        return false;
                    QTextStream out(&file);
                    out<<"# this data was written with APL@VORO"<<"\n";
                    out<<"# example commands for visualizing with gnuplot:"<<"\n";
                    out<<"# >plot set style data lines"<<"\n";
                    out<<"# >plot set contour base"<<"\n";
                    out<<"# >plot set surface"<<"\n";
                    out<<"# >plot set dgrid3d 60,50,1"<<"\n";
                    out<<"# >plot show contour"<<"\n";
                    out<<"# >plot set surface"<<"\n";
                    out<<"# >plot set pm3d"<<"\n";
                    out<<"# X"<<"   "<<"Y"<<"   "<<"Z"<<"\n";

                    if(checkallMol->isChecked())
                        numbers = mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside.keys();

                    for (int i = 0 ;i < numbers.size(); i++)
                    {
                        if(mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside.find(numbers[i]) != mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside.end())
                        {
                            double x = mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside[numbers[i]].getX();
                            double y= mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside[numbers[i]].getY();
                            double area = mycore->framemembrans[beginframe->currentIndex()].leaflets["upside"]->QHside[numbers[i]].getThick();
                            out<<x<<"   "<<y<<"   "<<area<<"\n";
                        }
                    }
                    file.close();
                    return true;
                }
                if(side->currentIndex()==1)//DOWN
                {
                    QFile file(fileName);
                    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
                        return false;

                    QTextStream out(&file);
                    out<<"# this data was written with APL@VORO"<<"\n";
                    out<<"# example commands for visualizing with gnuplot:"<<"\n";
                    out<<"# >plot set style data lines"<<"\n";
                    out<<"# >plot set contour base"<<"\n";
                    out<<"# >plot set surface"<<"\n";
                    out<<"# >plot set dgrid3d 60,50,1"<<"\n";
                    out<<"# >plot show contour"<<"\n";
                    out<<"# >plot set surface"<<"\n";
                    out<<"# >plot set pm3d"<<"\n";
                    out<<"# X"<<"   "<<"Y"<<"   "<<"Z"<<"\n";
                    if(checkallMol->isChecked())
                        numbers = mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside.keys();
                    for (int i = 0 ;i < numbers.size(); i++)
                    {
                        if(mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside.find(numbers[i]) != mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside.end())
                        {
                            double X= mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside[numbers[i]].getX();
                            double Y= mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside[numbers[i]].getY();
                            double area= mycore->framemembrans[beginframe->currentIndex()].leaflets["downside"]->QHside[numbers[i]].getThick();
                            out<<X<<"   "<<Y<<"   "<<area<<"\n";
                        }
                    }
                    file.close();
                    return true;
                }
            }
        }
        else if(beginframe->currentIndex() < endframe->currentIndex())
        {
            QString fp = QDir::homePath();
            fp += "/untitled.xvg";
            fileName = QFileDialog::getSaveFileName(this, tr("Save File"), fp, tr("gracefiles (*.xvg)"));
            QFile file(fileName);
            if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
                return false;

            QTextStream out(&file);
            out << "@type xy " <<"\n";
            out << "@xaxis  label \"Time (ps)\""<<"\n";
            out << "@yaxis  label \"Distance (\\c\\E\\C)\""<<"\n";
            for (int i = beginframe->currentIndex() ; i < endframe->currentIndex(); i++)
            {
                if(side->currentIndex()==0)//UP
                {
                    if(checkallMol->isChecked())
                        numbers = mycore->framemembrans[i].leaflets["upside"]->QHside.keys();
                    int div = numbers.size();
                    double myarea =0;
                    double time = mycore->framemembrans[i].time;
                    for (int j = 0 ; j < div; j++)
                    {
                        if(mycore->framemembrans[i].leaflets["upside"]->QHside.find(numbers[j]) != mycore->framemembrans[i].leaflets["upside"]->QHside.end())
                            myarea += mycore->framemembrans[i].leaflets["upside"]->QHside[numbers[j]].getThick();
                    }
                    out <<time <<"   "<<myarea/div<<"\n";
                }
                if(side->currentIndex()==1)//DOWN
                {
                    if(checkallMol->isChecked())
                        numbers = mycore->framemembrans[i].leaflets["downside"]->QHside.keys();
                    int div = numbers.size();
                    double myarea =0;
                    double time = mycore->framemembrans[i].time;
                    for (int j = 0 ; j < div; j++)
                    {
                        if(mycore->framemembrans[i].leaflets["downside"]->QHside.find(numbers[j]) != mycore->framemembrans[i].leaflets["downside"]->QHside.end())
                            myarea += mycore->framemembrans[i].leaflets["downside"]->QHside[numbers[j]].getThick();
                    }
                    out <<time<<"   "<<myarea/div<<"\n";
                }
            }
            file.close();
            return true;
        }
    }
    return true;
}


void Plottingexporter::closeit()
{
   close();
}


void Plottingexporter::setFrames(int current, int max)
{
    MaxFramenum = max;
    Framenum =current;

    for (int i = 0 ; i <= max; i++)
    {
        QString aframe;
        aframe.setNum(i+1);
        beginframe->addItem(aframe);
        endframe->addItem(aframe);
    }
    beginframe->setCurrentIndex(0);
    endframe->setCurrentIndex(MaxFramenum);
    if(MaxFramenum == 0)
    {
        selectFrames->setCheckState(Qt::Checked);
        allFrames->setDisabled(true);
        beginframe->setDisabled(true);
        endframe->setDisabled(true);
        SingleAreaX->setEnabled(true);
        SingleAreaY->setEnabled(true);
        SingleAreaXY->setEnabled(true);

    }
    else
    {
        SingleAreaX->setDisabled(true);
        SingleAreaY->setDisabled(true);
        SingleAreaXY->setDisabled(true);
    }
}


void Plottingexporter::singleAreaXToggled()
{
    SingleAreaY->blockSignals(true);
    SingleAreaXY->blockSignals(true);
    if(SingleAreaX->isChecked())
    {
        SingleAreaY->setCheckState(Qt::Unchecked);
        SingleAreaXY->setCheckState(Qt::Unchecked);
    }
    SingleAreaY->blockSignals(false);
    SingleAreaXY->blockSignals(false);
}


void Plottingexporter::singleAreaYToggled()
{
    SingleAreaX->blockSignals(true);
    SingleAreaXY->blockSignals(true);
    if(SingleAreaY->isChecked())
    {
        SingleAreaX->setCheckState(Qt::Unchecked);
        SingleAreaXY->setCheckState(Qt::Unchecked);
    }
    SingleAreaX->blockSignals(false);
    SingleAreaXY->blockSignals(false);
}


void Plottingexporter::singleAreaXYToggled()
{
    SingleAreaX->blockSignals(true);
    SingleAreaY->blockSignals(true);
    if(SingleAreaXY->isChecked())
    {
        SingleAreaY->setCheckState(Qt::Unchecked);
        SingleAreaX->setCheckState(Qt::Unchecked);
    }
    SingleAreaY->blockSignals(false);
    SingleAreaX->blockSignals(false);
}


void Plottingexporter::checkallToggled()
{
    checkselectMol->blockSignals(true);
    if(checkallMol->isChecked())
        checkselectMol->setCheckState(Qt::Unchecked);
     checkselectMol->blockSignals(false);
}


void Plottingexporter::checkselectToggled()
{
    checkallMol->blockSignals(true);
    if(checkselectMol->isChecked())
        checkallMol->setCheckState(Qt::Unchecked);
     checkallMol->blockSignals(false);
}


void Plottingexporter::checkAreaToggled()
{
    checkThick->blockSignals(true);
    if(checkArea->isChecked())
        checkThick->setCheckState(Qt::Unchecked);
     checkThick->blockSignals(false);
}


void Plottingexporter::checkThickToggled()
{
    checkArea->blockSignals(true);
    if(checkThick->isChecked())
        checkArea->setCheckState(Qt::Unchecked);
    checkArea->blockSignals(false);
}


void Plottingexporter::allFramesToggled()
{

    selectFrames->blockSignals(true);
    if(allFrames->isChecked())
    {   selectFrames->setCheckState(Qt::Unchecked);
        beginframe->setDisabled(true);
        endframe->setDisabled(true);

    }
    else
    {
        selectFrames->setCheckState(Qt::Checked);
        beginframe->setDisabled(false);
        endframe->setDisabled(false);
    }
    selectFrames->blockSignals(false);
}


void Plottingexporter::selectFramesToggled()
{
    allFrames->blockSignals(true);
    if(selectFrames->isChecked())
    {
        allFrames->setCheckState(Qt::Unchecked);
        beginframe->setDisabled(false);
        endframe->setDisabled(false);
    }
    else
    {
        allFrames->setCheckState(Qt::Checked);
        beginframe->setDisabled(true);
        endframe->setDisabled(true);
    }
    allFrames->blockSignals(false);
}


void Plottingexporter::beginFrameChanged()
{
    if(beginframe->currentIndex() > endframe->currentIndex())
    {
        endframe->blockSignals(true);
        endframe->setCurrentIndex(beginframe->currentIndex());
        endframe->blockSignals(false);
        SingleAreaX->setDisabled(false);
        SingleAreaY->setDisabled(false);
        SingleAreaXY->setDisabled(false);
    }
    if(beginframe->currentIndex() == endframe->currentIndex())
    {
        SingleAreaX->blockSignals(true);
        SingleAreaY->blockSignals(true);
        SingleAreaXY->blockSignals(true);

        SingleAreaX->setCheckState(Qt::Unchecked);
        SingleAreaY->setCheckState(Qt::Unchecked);
        SingleAreaXY->setCheckState(Qt::Unchecked);

        SingleAreaX->setDisabled(false);
        SingleAreaY->setDisabled(false);
        SingleAreaXY->setDisabled(false);

        SingleAreaX->blockSignals(false);
        SingleAreaY->blockSignals(false);
        SingleAreaXY->blockSignals(false);
    }
    if(beginframe->currentIndex() < endframe->currentIndex())
    {
        SingleAreaX->blockSignals(true);
        SingleAreaY->blockSignals(true);
        SingleAreaXY->blockSignals(true);

        SingleAreaX->setCheckState(Qt::Unchecked);
        SingleAreaY->setCheckState(Qt::Unchecked);
        SingleAreaXY->setCheckState(Qt::Unchecked);

        SingleAreaX->setDisabled(true);
        SingleAreaY->setDisabled(true);
        SingleAreaXY->setDisabled(true);

        SingleAreaX->blockSignals(false);
        SingleAreaY->blockSignals(false);
        SingleAreaXY->blockSignals(false);
    }
}


void Plottingexporter::endFrameChanged()
{
    if(beginframe->currentIndex() > endframe->currentIndex())
    {
        beginframe->blockSignals(true);
        beginframe->setCurrentIndex(endframe->currentIndex());
        beginframe->blockSignals(false);
        SingleAreaX->setDisabled(false);
        SingleAreaY->setDisabled(false);
        SingleAreaXY->setDisabled(false);
    }
    if(beginframe->currentIndex() == endframe->currentIndex())
    {
        SingleAreaX->blockSignals(true);
        SingleAreaY->blockSignals(true);
        SingleAreaXY->blockSignals(true);

        SingleAreaX->setCheckState(Qt::Unchecked);
        SingleAreaY->setCheckState(Qt::Unchecked);
        SingleAreaXY->setCheckState(Qt::Unchecked);

        SingleAreaX->setDisabled(false);
        SingleAreaY->setDisabled(false);
        SingleAreaXY->setDisabled(false);

        SingleAreaX->blockSignals(false);
        SingleAreaY->blockSignals(false);
        SingleAreaXY->blockSignals(false);
    }
    if(beginframe->currentIndex() < endframe->currentIndex())
    {
        SingleAreaX->blockSignals(true);
        SingleAreaY->blockSignals(true);
        SingleAreaXY->blockSignals(true);

        SingleAreaX->setCheckState(Qt::Unchecked);
        SingleAreaY->setCheckState(Qt::Unchecked);
        SingleAreaXY->setCheckState(Qt::Unchecked);

        SingleAreaX->setDisabled(true);
        SingleAreaY->setDisabled(true);
        SingleAreaXY->setDisabled(true);

        SingleAreaX->blockSignals(false);
        SingleAreaY->blockSignals(false);
        SingleAreaXY->blockSignals(false);
    }
}

