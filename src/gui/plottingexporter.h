#ifndef PLOTTINGEXPORTER_H
#define PLOTTINGEXPORTER_H

#ifndef Plottingexporter_H
#define Plottingexporter_H
#include <QtGui>
#if QT_VERSION >= 0x050000
#include <QtWidgets>
#endif
#include "coredata/core.h"

class Plottingexporter:public QWidget
{
        Q_OBJECT
public:
    Plottingexporter(QWidget *parent = 0);
    QVector<int> Upsidenumbers, Downsidenumbers;//selection /**< TODO */ /*!< TODO */
    int mode;//0 = printGNUSelectedThickOneFrame, 1 = printGraceSelectedThick,2=printGraceAllThick,3=printGraceSelectedArea,4=printGraceAllArea /**< TODO */ /*!< TODO */
    int MaxFramenum;
    int Framenum;
    core *mycore;

    void setFrames(int current, int max);
    QString fileName;

private:
    bool plotALL(QList<int> numbers);
    bool plotArea(QList<int> numbers );
    bool plotThick(QList<int> numbers );

    QPushButton *ChooseUpsideFile;
    QPushButton *closeB;
    QLabel *checksidelabel; /**< TODO */ /*!< TODO */
    QLabel *typelabel; /**< TODO */ /*!< TODO */
    QLabel *contentlabel; /**< TODO */ /*!< TODO */
    QLabel *selectframeslabel; /**< TODO */ /*!< TODO */
    QLabel *chooseFrameslabel; /**< TODO */ /*!< TODO */

    QLabel *checkalllabel; /**< TODO */ /*!< TODO */
    QLabel *SingleArea; /**< TODO */ /*!< TODO */
    QLabel *SingleX; /**< TODO */ /*!< TODO */
    QLabel *SingleY; /**< TODO */ /*!< TODO */
    QLabel *SingleXY; /**< TODO */ /*!< TODO */
    QCheckBox *SingleAreaX; /**< TODO */ /*!< TODO */
    QCheckBox *SingleAreaY; /**< TODO */ /*!< TODO */
    QCheckBox *SingleAreaXY; /**< TODO */ /*!< TODO */
    QCheckBox *checkallMol; /**< TODO */ /*!< TODO */
    QLabel *checkselectlabel; /**< TODO */ /*!< TODO */
    QCheckBox *checkselectMol; /**< TODO */ /*!< TODO */
    QLabel *checkarealabel; /**< TODO */ /*!< TODO */
    QCheckBox *checkArea; /**< TODO */ /*!< TODO */
    QLabel *checkthickabel; /**< TODO */ /*!< TODO */
    QCheckBox *checkThick; /**< TODO */ /*!< TODO */
    QLabel *checkallFrameslabel; /**< TODO */ /*!< TODO */
    QCheckBox *allFrames; /**< TODO */ /*!< TODO */
    QLabel *checkSelectFrameslabel; /**< TODO */ /*!< TODO */
     QCheckBox *selectFrames; /**< TODO */ /*!< TODO */
    QLabel *checkbeginlabel; /**< TODO */ /*!< TODO */
    QComboBox *beginframe; /**< TODO */ /*!< TODO */
    QLabel *checkendlabel; /**< TODO */ /*!< TODO */
    QComboBox *endframe; /**< TODO */ /*!< TODO */
    QComboBox *side; /**< TODO */ /*!< TODO */

private slots:
    void closeit();
    void singleAreaXToggled();
    void singleAreaYToggled();
    void singleAreaXYToggled();
    void checkallToggled();
    void checkselectToggled();
    void checkAreaToggled();
    void checkThickToggled();
    void allFramesToggled();
    void beginFrameChanged();
    void endFrameChanged();
    void chooseDir();
    void selectFramesToggled();
};


#endif // Plottingexporter_H


#endif // PLOTTINGEXPORTER_H
