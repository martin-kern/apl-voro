//**************************************************************
//
//           Copyright (c) 2012 - 2013 by Gunther Lukat
//
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
//    USA or visit their web site at www.gnu.org .
//**************************************************************

#include "criterionwidget.h"

CriterionWidget::CriterionWidget(QStringList wordlist,
                                 core *related_core,
                                 QSet<int> *preselection,
                                 QWidget *parent) : QWidget(parent)
{
    this->related_core = related_core;
    this->preselection = preselection;
    this->setWindowTitle("New Selection Model");
    counter = 1;
    criterionName = new QLineEdit;
    residues = wordlist;
    QWidget *w0 = new QWidget(this);
    w0->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
    descr1 = new QLabel(this);
    descr1->setText("Selection will match ");

    and_or = new QComboBox(this);

    and_or->addItem("one of the");
    and_or->addItem("all");

    and_or->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);

    descr2 = new QLabel(this);
    descr2->setText("criteria.");

    layouttop = new QHBoxLayout;
    layouttop->addWidget(descr1);
    layouttop->addWidget(and_or);
    layouttop->addWidget(descr2);

    QVBoxLayout *topal = new QVBoxLayout;
    topal->addWidget(criterionName);
    topal->addLayout(layouttop);

    w0->setLayout(topal);
    choose1 = new CriterionSelector(residues,this);
    choose1->minus->hide();
    wlist.insert(counter,choose1);
    connect(choose1->plus, SIGNAL(clicked()), this, SLOT(insertchooser()));

    ok = new QPushButton(this);
    ok->setText("okay");
    ok->adjustSize();
    connect(ok, SIGNAL(clicked()), this, SLOT(getCriterias()));

    QWidget *w1 = new QWidget(this);
    layout = new QVBoxLayout;
    layout->addWidget(w0);
    layout->addWidget(choose1);

    w1->setLayout(layout);

    centrallayout = new QVBoxLayout;
    centrallayout->addWidget(w1);
    centrallayout->addWidget(ok);

    setLayout(centrallayout);
}



void CriterionWidget::insertchooser()
{
    counter++;
    CriterionSelector *choosex = new CriterionSelector(residues,this);
    layout->addWidget(choosex);
    wlist.insert(counter,choosex);
    connect(choosex->plus,SIGNAL(clicked()),this, SLOT(insertchooser()));
    connect(choosex,SIGNAL(onclose(int)),this,SLOT(remchooser(int)));
}


void CriterionWidget::remchooser(int a)
{
    wlist.remove(a);
    layout->activate();
    centrallayout->activate();
    adjustSize();
}


void CriterionWidget::getCriterias()
{
    QList<Criterion> critlist;
    bool and_or_bool = and_or->currentIndex();
    QString listname = criterionName->text();
    Critall mycriteriafirst;
    QHash<QString,Critall> aQhash;
    aQhash.insert(listname,mycriteriafirst);

    for (QHash<int, CriterionSelector*>::iterator s = wlist.begin(); s != wlist.end(); s++)
    {
        Criterion crit;
        bool valt = s.value()->prime->currentIndex();
        crit.isnot =  valt;

        int type =  s.value()->type->currentIndex();
        if(type == 0)
        {
            crit.name = true;
            crit.resname=  s.value()->useredit->text();
        }
        if(type == 1 )
        {
            crit.area = true;
            crit.d_val =  s.value()->useredit->text().toDouble();
        }
        if( type == 2)
        {
            crit.thick = true;
            crit.d_val =  s.value()->useredit->text().toDouble();
        }
        if(type == 3)
        {
            crit.neighb = true;
            crit.intval =  s.value()->useredit->text().toInt();
        }
        if(type == 4)
        {
            crit.select = true;
            crit.preselection = *(this->preselection);
            crit.boolval = true;
        }
        critlist.push_back(crit);
    }
    Critall mycriteria;
    mycriteria.and_or = and_or_bool;
    mycriteria.critlist = critlist;
    related_core->add_selection(listname, mycriteria);

//    criterialist->insert(listname,mycriteria);
    emit criteriacreated(listname);
    close();
}
