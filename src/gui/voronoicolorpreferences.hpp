#ifndef VORONOICOLORPREFERENCES_HPP
#define VORONOICOLORPREFERENCES_HPP
#include <QtWidgets>
#include "gui/voronoidock.hpp"

class VoronoiDock;

class VoronoiColorPreferences : public QWidget
{
    Q_OBJECT
public:
    VoronoiColorPreferences(VoronoiDock *vd, int method, QWidget *parent = nullptr);
    QSpinBox *gridselector;
    QComboBox *Upminthick, *Upmaxthick, *Upminarea, *Upmaxarea, *colorScaleCB;
    QColor linecolor, bgcolor, selectcolor, gridcolor;
    int coloringmethod;
    double offset;
    int linewidth;
    int gridsize;
    int start;
    void custom_area_margins(double &, double &);
    void custom_thickness_margins(double &, double &);

private:
    VoronoiDock *voronoi_dock;
    QLabel *backgroundlabel, *linecolorlabel, *selectionColorlabel, *gridcolorlabel, *colorScalePreview;
    QDoubleSpinBox *area_cmax_sb, *area_cmin_sb, *thickness_cmax_sb, *thickness_cmin_sb;
    QRadioButton *area_global, *area_local, *area_custom, *thickness_global, *thickness_local, *thickness_custom;
};

#endif // VORONOICOLORPREFERENCES_HPP
