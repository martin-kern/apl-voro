#include "membranehandlerwidget.hpp"

MembraneHandlerWidget::MembraneHandlerWidget(unsigned int memID, QString name, core *core_ptr, QWidget *parent):QFrame(parent)
{
    this->memID = memID;
    this->core_ptr = core_ptr;
    setFrameStyle(QFrame::Panel);

    // Setup widget label
    QLabel *membrane_name = new QLabel(name, this);
    QString mem_ID_string;

    // Setup for buttons.
    QToolButton *voronoi_view_button = new QToolButton(this);
    voronoi_view_button->setIconSize(QSize(30, 30));
    voronoi_view_button->setFixedSize(32, 32);
    voronoi_view_button->setFocusPolicy(Qt::StrongFocus);
    voronoi_view_button->setStyleSheet("QToolButton { border: none }");
    voronoi_view_button->setIcon(QIcon(QPixmap(":img/Voro_Icon.png")));
    voronoi_view_button->setToolTip("new Voronoi diagram");
    QToolButton *plot2d_button = new QToolButton(this);
    plot2d_button->setIconSize(QSize(30, 30));
    plot2d_button->setFixedSize(32, 32);
    plot2d_button->setStyleSheet("QToolButton { border: none }");
    plot2d_button->setIcon(QIcon(QPixmap(":img/Graph_Icon.png")));
    plot2d_button->setToolTip("new 2d graph");
    QToolButton *averages_button = new QToolButton(this);
    averages_button->setIconSize(QSize(30, 30));
    averages_button->setFixedSize(32, 32);
    averages_button->setStyleSheet("QToolButton { border: none }");
    averages_button->setIcon(QIcon(QPixmap(":img/Average_Icon.png")));
    averages_button->setToolTip("new averages table");
//    QToolButton *properties_button = new QToolButton(this);
//    properties_button->setIconSize(QSize(30, 30));
//    properties_button->setFixedSize(32, 32);
//    properties_button->setStyleSheet("QToolButton { border: none }");
//    properties_button->setIcon(QIcon(QPixmap(":img/Properties_Icon.png")));
//    properties_button->setToolTip("new properties table");
    QToolButton *close_button = new QToolButton(this);
    close_button->setIconSize(QSize(15, 15));
    close_button->setFixedSize(17, 17);
    close_button->setStyleSheet("QToolButton { border: none }");
    close_button->setIcon(QIcon(QPixmap(":img/Close.png")));
    close_button->setToolTip("close");

    // Spin boxes for synchronization frames
    sb_sync_frame_1 = new QSpinBox;
    sb_sync_frame_1->setRange(0, core_ptr->framemembrans.size() - 1);
    sb_sync_frame_1->setValue(0);

    sb_sync_frame_2 = new QSpinBox;
    sb_sync_frame_2->setRange(0, core_ptr->framemembrans.size() - 1);
    sb_sync_frame_2->setValue(core_ptr->framemembrans.size() - 1);

    // put all UI elements in the right place
    QBoxLayout *top_layout = new QBoxLayout(QBoxLayout::TopToBottom, this);
    QSplitter *buttons_splitter = new QSplitter(Qt::Horizontal);
    buttons_splitter->addWidget(voronoi_view_button);
    buttons_splitter->addWidget(plot2d_button);
    buttons_splitter->addWidget(averages_button);
//    buttons_splitter->addWidget(properties_button);
    QHBoxLayout *name_and_close = new QHBoxLayout();
    name_and_close->addWidget(membrane_name, Qt::LeftEdge);
    name_and_close->addWidget(close_button, Qt::RightEdge);
    top_layout->addLayout(name_and_close);
    top_layout->addWidget(buttons_splitter);
    top_layout->addWidget(sb_sync_frame_1);
    top_layout->addWidget(sb_sync_frame_2);

    setLayout(top_layout);

    connect(voronoi_view_button, SIGNAL(clicked()), this, SLOT(create_voronoi_view()));
    connect(plot2d_button, SIGNAL(clicked()), this, SLOT(create_2d_view()));
    connect(averages_button, SIGNAL(clicked()), this, SLOT(create_averages_view()));
//    connect(properties_button, SIGNAL(clicked()), this, SLOT(create_properties_view()));
    connect(close_button, SIGNAL(clicked()), this, SLOT(close_project()));
    connect(sb_sync_frame_1, SIGNAL(valueChanged(int)), this, SLOT(update_sync_frame_1(int)));
    connect(sb_sync_frame_2, SIGNAL(valueChanged(int)), this, SLOT(update_sync_frame_2(int)));
}


void MembraneHandlerWidget::create_voronoi_view()
{
    emit signal_voronoi_view(memID);
}


void MembraneHandlerWidget::create_2d_view()
{
    emit signal_2d_view(memID);
}


void MembraneHandlerWidget::create_averages_view()
{
    emit signal_averages_view(memID);
}


void MembraneHandlerWidget::create_properties_view()
{
    emit signal_properties_view(memID);
}


void MembraneHandlerWidget::close_project()
{
    emit signal_close_project(memID, this);
}


void MembraneHandlerWidget::update_sync_frame_1(int frame)
{
    sb_sync_frame_2->setMinimum(frame);
    core_ptr->set_sync_frame_1(unsigned(frame));
    emit update_controller();
}


void MembraneHandlerWidget::update_sync_frame_2(int frame)
{
    sb_sync_frame_1->setMaximum(frame);
    core_ptr->set_sync_frame_2(unsigned(frame));
    emit update_controller();
}
