#ifndef VORONOIDOCK_HPP
#define VORONOIDOCK_HPP

#include <QtWidgets>
#include <math.h>
#include <algorithm>
#include "coredata/core.h"
#include "coredata/vt.h"
#include "gui/graphicobjects/siminfoitem.hpp"
#include "gui/graphicobjects/legend.h"
#include "gui/criterionwidget.h"
#include "frameprinterwidget.h"
#include "gui/grid.hpp"
#include "gui/graphicobjects/legend.h"
#include "colorgradients.hpp"


class VoronoiDock;

class VoronoiGraphicsView : public QGraphicsView
{
    Q_OBJECT
public:
    VoronoiGraphicsView(VoronoiDock *parent);
    void info_update(QString leaflet, float time, int frame, double area, double thickness);
    void set_line_color(QColor lc);
    void set_selection_color(QColor sc);
protected:
    void resizeEvent(QResizeEvent *event) override;
    void wheelEvent(QWheelEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent* event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
private:
    int zoom_level;
    double scale_factor;
    bool _drag;
    VoronoiDock *_parent;
    Grid *_grid;
    QMenu *_context_menu;
    SimInfoItem *_sim_info_item;
    Legend *_legend;
    QLabel *_hover_label;
    void _update_grid();
    void _update_voro_info();
    void _update_legend();
    void _center_voronoi();
    void read_selection_file();

    friend class VoronoiDock;
    friend class ViewPreferences;
private slots:
    void autocomputeSceneSize(const QList<QRectF>& region);
    void toggle_grid();
    void toggle_legend();
    void toggle_info();
};


class ViewPreferences :public QWidget
{
    Q_OBJECT

public:
    ViewPreferences(VoronoiGraphicsView &view_1, int method, QWidget *parent = nullptr);
    void custom_area_margins(double &, double &);
    void custom_thickness_margins(double &, double &);
    int coloringmethod;

private slots:
    void changelinecolor();
    void changebgcolor();
    void changeselectcolor();
    void changegridcolor();
    void changegridsize();
    void changeColorScale();
    void change_area_margins(int);
    void change_thickness_margins(int);
    void update_margins(double);


private:
    QSpinBox *gridselector;
    QComboBox *colorScaleCB;
    QColor linecolor, bgcolor, selectcolor, gridcolor;
    QLabel *backgroundlabel, *linecolorlabel, *selectionColorlabel, *gridcolorlabel;

    VoronoiGraphicsView *voronoi_view;
    QDoubleSpinBox *area_cmax_sb, *area_cmin_sb, *thickness_cmax_sb, *thickness_cmin_sb;
    QRadioButton *area_global, *area_local, *area_custom, *thickness_global, *thickness_local, *thickness_custom;
    friend class VoronoiGraphicsView;
    friend class VoronoiDock;
};


class VoronoiDock : public QDockWidget
{
    Q_OBJECT

public:
    // Voronoiview
    VoronoiDock(unsigned int memID,
                QHash<unsigned int, core*> *cores,
                QString window_title,
                int controller_frame,
                int max_frame,
                QWidget *parent = nullptr);
    void update(int controller_frame, int max_frame);
    unsigned int mem_id(){return memID;}
    void UpdateTreeandView();
    QString active_leaflet(){return active_leaflet_;}
    void set_thickness_mode(int m){thickness_mode = m; UpdateTreeandView();}
    void set_area_mode(int m){area_mode = m; UpdateTreeandView();}
    double max_area(){return globalmaxarea;}
    double min_area(){return globalminarea;}
    void setcoloringMethod(int coloring_id);
    void set_leaflet(QString leaflet);

    QColor linecolor, bgcolor, selectcolor, gridcolor;    


protected:
    bool eventFilter(QObject *obj, QEvent *event);


private:
    void setCellColorsByType();
    void get_tree_selections();
    void update_globals();
    void set_margins(double min_area, double max_area, double min_thickness, double max_thickness);
    void makeParents(QStringList *Lipidlist, QStringList *chainnames);
    void fill_averages();
    void UpdateView(int coloring_id);
    void setColors(int method);
    QList<QTreeWidgetItem*> toplist;
    QComboBox *criteriaselection;
    int side;
    unsigned int memID;
    QHash<unsigned int, core*> *vcores;
    QString active_leaflet_;
    QHash<int, VT> cell_tree_map;
    QList<QPair<QString,QColor> > RCList;

    QHash<QString, Critall> *criterias;  // TODO: use criteria from core
    ViewPreferences *view_preferences;
    QList<int> numbers;  // TODO: replace with selection_ids
    QSet<int> selection_ids;
    int currentframe;


    double maxarea;
    double minarea;
    double globalmaxarea;
    double globalminarea;
    double maxthick;
    double minthick;
    double globalmaxthick;
    double globalminthick;
    int area_mode, thickness_mode;
    core *simulation;
    QTableWidget *averages_table;
    QStringList residues;
    VoronoiGraphicsView *voronoi_view;
    int metric;  // 0: none, 1: residue, 2: Area, 3: neighbors, 4: thickness
    QTreeWidget *lipid_properties;
    friend class VoronoiGraphicsView;

private slots:
    void addCriteria(QString a);
    void setCheck();
    void TreeSelectionsUpdate(QTreeWidgetItem *item, int);
    void setViewSelect();
    void exportImage();
    void show_color_options();
    void exportDat();
    void selectCriteria();
    void showCriteriamaker();
};


#endif // VORONOIDOCK_HPP
