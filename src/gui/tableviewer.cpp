#include "gui/tableviewer.h"


Tableviewer::Tableviewer(unsigned int m, core *c, QWidget *parent)
    :QDockWidget(parent)
{
    table_core = c;
    mem_id_ = m;
    avgareasmodeldown = new AverageAreasmodellDown();
    avgareamodelup = new AverageAreasmodellUp();

    Upside = new QTableView(this);
    Upside->setSelectionBehavior(QAbstractItemView::SelectRows);

    Downside = new QTableView(this);
    Downside->setSelectionBehavior(QAbstractItemView::SelectRows);

    tablesUp = new QTabWidget(this);
    tablesUp->addTab(Upside,"Upside");
    tablesUp->addTab(Downside,"Downside");
    setWidget(tablesUp);
    update();
}


void Tableviewer::closeEvent(QCloseEvent *event)
{
    emit closing();
    event->accept();
}


void Tableviewer::update()
{
    avgareamodelup->mems = &table_core->framemembrans;
    avgareamodelup->Upresidues = &table_core->residues["upside"];
    avgareamodelup->numbers = &table_core->ALLupsideAnumbers;
    Upside->setModel(this->avgareamodelup);

    avgareasmodeldown->mems = &table_core->framemembrans;
    avgareasmodeldown->Downresidues = &table_core->residues["downside"];
    avgareasmodeldown->numbers = &table_core->ALLdownsideAnumbers;
    Downside->setModel(this->avgareasmodeldown);
}
