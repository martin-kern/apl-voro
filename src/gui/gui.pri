SOURCES += \
    $$PWD/grid.cpp \
    $$PWD/membranehandlerwidget.cpp \
    $$PWD/projectwizardpage.cpp \
    $$PWD/voronoicolorpreferences.cpp \
    $$PWD/voronoidock.cpp \
    $$PWD/wizardtreewidget.cpp \
           gui/propertywindow.cpp \
           gui/allproperties.cpp \
           gui/selectedprops.cpp \
           gui/controllerwidget.cpp \
           gui/tableviewer.cpp \
           gui/mainwindow.cpp \
           gui/treeviewer.cpp \
           gui/plottingexporter.cpp \
           gui/projectwidget.cpp \
           gui/indextree.cpp \
    gui/criterionwidget.cpp \
    gui/criterionselector.cpp \
    gui/frameprinterwidget.cpp \
    gui/colorgradients.cpp


HEADERS += \
    $$PWD/grid.hpp \
    $$PWD/membranehandlerwidget.hpp \
    $$PWD/projectwizardpage.hpp \
    $$PWD/voronoicolorpreferences.hpp \
    $$PWD/voronoidock.hpp \
    $$PWD/wizardtreewidget.hpp \
           gui/propertywindow.h \
           gui/allproperties.h \
           gui/selectedprops.h \
           gui/controllerwidget.h \
           gui/tableviewer.h \
           gui/mainwindow.h \
           gui/treeviewer.h \
           gui/plottingexporter.h \
           gui/projectwidget.h \
           gui/indextree.h \
    gui/criterionwidget.h \
    gui/criterionselector.h \
    gui/frameprinterwidget.h \
    gui/colorgradients.hpp
