#ifndef GRID_HPP
#define GRID_HPP


#include <QtWidgets>

class Grid : public QGraphicsItem
{
public:
    Grid(QColor color, QGraphicsItem *parent=nullptr);

    QRectF boundingRect() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override;
    void set_bounds(double x, double y, double w, double h);
    void set_bounds(QRect viewport);
    void toggle_grid();
    void set_color(QColor color);
    void set_grid_spacing(int spacing);
    double scale_factor;

private:
    QRectF _bounding_rect;
    QColor _color;
    int _spacing;
    bool _grid_on;
    int _start(int x, int spacing);
//    int dynamic_grid_spacing(double scaling_factor);

};

#endif // GRID_HPP
