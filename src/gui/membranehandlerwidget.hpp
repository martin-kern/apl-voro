#ifndef MEMBRANEHANDLERWIDGET_HPP
#define MEMBRANEHANDLERWIDGET_HPP
#include <QtWidgets>
#include "coredata/core.h"

/**
 * @brief The MembraneHandlerWidget class will provide a small ui for each loaded membrane.
 * From the MembraneHandlerWidget you can open views for each respective membrane or close it.
 */

class MembraneHandlerWidget : public QFrame
{
    Q_OBJECT
public:
    MembraneHandlerWidget(unsigned int memID, QString name, core *core_ptr, QWidget *parent=nullptr);
    unsigned int mem_id() const {return memID;}
    int span() {return sb_sync_frame_2->value() - sb_sync_frame_1->value();}

private:
    // An ID to reference the membrane data.
    unsigned int memID;
    core *core_ptr;
    QSpinBox *sb_sync_frame_1, *sb_sync_frame_2;

private slots:
    void create_voronoi_view();
    void create_2d_view();
    void create_averages_view();
    void create_properties_view();
    void close_project();
    void update_sync_frame_1(int sync_frame);
    void update_sync_frame_2(int sync_frame);

signals:
    void signal_voronoi_view(unsigned int memID);
    void signal_2d_view(unsigned int memID);
    void signal_averages_view(unsigned int memID);
    void signal_properties_view(unsigned int memID);
    void signal_close_project(unsigned int memID, MembraneHandlerWidget *self);
    void update_controller();
};

#endif // MEMBRANEHANDLERWIDGET_HPP
