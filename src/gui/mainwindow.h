#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include <QtWidgets>
#include "data/constants.hpp"
#include "coredata/core.h"
#include "gui/plot2d/plottingmenuewindow.h"
#include "gui/plottingexporter.h"
#include "gui/tableviewer.h"
#include "gui/treeviewer.h"
#include "gui/propertywindow.h"

#include "gui/projectwidget.h"
#include "gui/plot2d/plot2dwindow.h"
#include "gui/voronoidock.hpp"
#include "gui/controllerwidget.h"
#include "data/setup_parameters.hpp"
#include "data/selection.hpp"

#include "io/apl/aplreadwrite.hpp"
#include "gui/indextree.h"
#include "gui/membranehandlerwidget.hpp"


QT_FORWARD_DECLARE_CLASS(QGraphicsView)
QT_FORWARD_DECLARE_CLASS(QGraphicsScene)
QT_FORWARD_DECLARE_CLASS(QSplitter)
QT_FORWARD_DECLARE_CLASS(QMenuBar)
QT_FORWARD_DECLARE_CLASS(QMenu)
QT_FORWARD_DECLARE_CLASS(QAction)
QT_FORWARD_DECLARE_CLASS(QPushButton)
QT_FORWARD_DECLARE_CLASS(QSlider)
QT_FORWARD_DECLARE_CLASS(QComboBox)
QT_FORWARD_DECLARE_CLASS(QTreeWidgetItem)


class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow();

private:
    void clear();
    void setupMenue();
    void setupConnections();
    void setCellColorsByType();
    void setCellColorsByArea(Membran *a, double offset);
    void setCellColorsByThickness(Membran *a,double offset);
    bool eventFilter(QObject *obj, QEvent *event);
    void closeEvent(QCloseEvent *event);
    bool maybeSave();

    QWidget *mytable, *mytree, *resultTreewidget;
    QMenuBar *menuBar;
    QMenu *filemenu,
          *helpmenu;
    QAction  *openAct,
             *quit,
             *about,
             *manualhelp,
             *save_as_act,
             *load_act;

    // A list view containing an overview of opened membranes.
    QListWidget *openMembranesList;
    QMainWindow *central_dock_area;
    QHash<unsigned int, core*> coreHashList;

    QList<VoronoiDock*> voro_dock_list;
    QList<Tableviewer*> TableList;
    QList<Plot2DWindow*> PlotList;
    QList<PropertyWindow*> properties_list;

    controllerWidget *controller; // Play the simulation or select specific frames
    int framenum;
    // ID for each loaded membrane. Increases every time a new membrane is loaded.
    unsigned int next_memID, next_voronoi_view_id;
    QHash<unsigned int, selection*> selections;

public slots:
    void create_project(setup_parameters setup_params);

private slots:
    void show_voronoi_view(unsigned int memID);
    void show_averages_table(unsigned int memID);
    void show_2d_plot();
    void show_properties(unsigned int memID);
    void close_project(unsigned int mem_id, MembraneHandlerWidget *mhw);
    void aboutdialog();
    void setupProject();
    void LoadSaved();
    void save_session();
    void showManual();
    void setColoringMethod();
    void controller_span_update();
};
#endif
