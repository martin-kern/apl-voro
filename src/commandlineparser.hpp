#ifndef COMMANDLINEPARSER_HPP
#define COMMANDLINEPARSER_HPP

#include <QStringList>
#include <QFile>
#include <QFileInfo>
#include <QTextStream>
#include "data/setup_parameters.hpp"
#include "io/pdb/pdbreader.h"
#include "io/apl/conf_parser.hpp"

using namespace std;

class CommandLineParser
{
public:
    CommandLineParser();
    ~CommandLineParser();
    bool parse(QStringList args);
    bool no_args_given() const {return no_params;}
    bool with_gui() const {return g_checked;}
    setup_parameters setup_params() const {return setup_params_;}
    QString selection_index() const {return selection_index_;}
    QString area_xvg() const {return area_xvg_;}
    QString thickness_xvg() const {return thickness_xvg_;}
    QString frames_txt() const {return frames_txt_;}
    QString sel_frames_txt() const {return sel_frames_txt_;}
    QStringList area_lipids() const {return area_lipids_;}
    QStringList thickness_lipids() const {return thickness_lipids_;}

private:

    void print_help();
    bool file_exists(QString path);
    bool directory_exists(QString path);

    setup_parameters setup_params_;
    QString selection_index_;
    QString area_xvg_;
    QString thickness_xvg_;
    QString frames_txt_;
    QString sel_frames_txt_;
    QStringList area_lipids_;
    QStringList thickness_lipids_;

    bool g_checked;
    bool no_params;
};

#endif // COMMANDLINEPARSER_HPP
