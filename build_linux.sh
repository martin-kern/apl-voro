#! /bin/bash
# Builds APL@Voro on linux and greates a debian and rpm package
# Requires static Qt libraries including xcb, and the rpm package

if test $# -ne 2
then
echo "This script needs exactly 2 arguments: The location of the src folder and the directory where APL@Voro will be built and packaged.";
exit 0;
fi

src_dir=$(realpath $1);   # Location of the src folder
build_dir=$(realpath $2); # Location where APL@Voro will be built and where the packages will end up
wd=$(pwd);
version='3.3.3';


# build aplvoro and move binary file
cp -r ${src_dir} ${build_dir}/aplvoro_build;
cd ${build_dir}/aplvoro_build;
# qmake with static libraries
/usr/local/Qt-5.15.3/bin/qmake -config release aplvoro.pro;
make;
cd ${wd};


# Build directory structures and copy files
# Debian
cp -r ${src_dir}/../aplvoro_deb  ${build_dir}/aplvoro_${version}_x64;
mkdir -p ${build_dir}/aplvoro_${version}_x64/usr/share/man/man1 ${build_dir}/aplvoro_${version}_x64/usr/share/pixmaps;

cp      ${build_dir}/aplvoro_build/aplvoro     ${build_dir}/aplvoro_${version}_x64/usr/bin/;                         # Executable binary
cp -r   ${src_dir}/img                         ${build_dir}/aplvoro_${version}_x64/usr/share/;                       # Images
gzip -c ${src_dir}/manpage/aplvoro.1 >         ${build_dir}/aplvoro_${version}_x64/usr/share/man/man1/aplvoro.1.gz;  # Manpage
cp -r   ${src_dir}/manual                      ${build_dir}/aplvoro_${version}_x64/usr/share/manual/;                # Manual
cp      ${src_dir}/pixmaps/256x256/aplvoro.png ${build_dir}/aplvoro_${version}_x64/usr/share/pixmaps/;               # Icon
dpkg-deb --build ${build_dir}/aplvoro_${version}_x64;

# rpm
mkdir ${build_dir}/rpmbuild;
mkdir ${build_dir}/rpmbuild/BUILD;
mkdir ${build_dir}/rpmbuild/RPMS;
mkdir ${build_dir}/rpmbuild/SOURCES;
mkdir ${build_dir}/rpmbuild/SPECS;
mkdir ${build_dir}/rpmbuild/SRPMS;
mkdir -p ${build_dir}/aplvoro-${version}/share;

cp    ${build_dir}/aplvoro_build/aplvoro                                  ${build_dir}/aplvoro-${version}/;  # Executable binary
cp -r ${src_dir}/img                                                      ${build_dir}/aplvoro-${version}/;  # Images
cp    ${build_dir}/aplvoro_${version}_x64/usr/share/man/man1/aplvoro.1.gz ${build_dir}/aplvoro-${version}/;  # Manpage
cp -r ${src_dir}/manual                                                   ${build_dir}/aplvoro-${version}/;  # Manual
cp    ${src_dir}/pixmaps/256x256/aplvoro.png                              ${build_dir}/aplvoro-${version}/;  # Icon
cp    ${src_dir}/../aplvoro_deb/usr/share/applications/aplvoro.desktop    ${build_dir}/aplvoro-${version}/;  # Menu entry
cp    ${src_dir}/../rpm/aplvoro.spec            ${build_dir}/rpmbuild/SPECS/;      # Specs

cd ${build_dir};
tar --create --file aplvoro-${version}.tar.gz aplvoro-${version};
cd ${wd};

mv ${build_dir}/aplvoro-${version}.tar.gz ${build_dir}/rpmbuild/SOURCES/;


rpmbuild --define "_topdir ${build_dir}/rpmbuild" -bb ${build_dir}/rpmbuild/SPECS/aplvoro.spec;
mv ${build_dir}/rpmbuild/RPMS/x86_64/* ${build_dir}/;


# Cleanup
rm -r ${build_dir}/aplvoro_build;
rm -r ${build_dir}/aplvoro_${version}_x64;
rm -r ${build_dir}/aplvoro-${version};
rm -r ${build_dir}/rpmbuild;


