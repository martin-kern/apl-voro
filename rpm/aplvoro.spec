Name:           aplvoro
Version:        3.3.3
Release:        1%{?dist}
Summary:        Interactive visualization of cell membrane simulaions


License:        GPL 3.0
# https://bitbucket.org/martin-kern/apl-voro/src/master/
Source0:        %{name}-%{version}.tar.gz

%description

%global debug_package %{nil}

%prep
%setup -q

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_bindir}
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/aplvoro
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/icons/hicolor/256x256/apps
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/man/man1
mkdir $RPM_BUILD_ROOT/%{_datadir}/applications

cp aplvoro $RPM_BUILD_ROOT/%{_bindir}
cp -r img $RPM_BUILD_ROOT/%{_datadir}/aplvoro/
cp -r manual $RPM_BUILD_ROOT/%{_datadir}/aplvoro/
cp aplvoro.1.gz $RPM_BUILD_ROOT/%{_datadir}/man/man1/
cp aplvoro.png $RPM_BUILD_ROOT/%{_datadir}/icons/hicolor/256x256/apps/
cp aplvoro.desktop $RPM_BUILD_ROOT/%{_datadir}/applications/



%files
%{_bindir}/aplvoro
%{_datadir}/aplvoro/img/3d_Icon.png
%{_datadir}/aplvoro/img/Average_Icon.png
%{_datadir}/aplvoro/img/Button-Add-icon.png
%{_datadir}/aplvoro/img/Button-Delete-icon.png
%{_datadir}/aplvoro/img/Close.png
%{_datadir}/aplvoro/img/ColorOption.png
%{_datadir}/aplvoro/img/Graph_Icon.png
%{_datadir}/aplvoro/img/Marke.png
%{_datadir}/aplvoro/img/Properties_Icon.png
%{_datadir}/aplvoro/img/Voro_Icon.png
%{_datadir}/aplvoro/img/graph.png
%{_datadir}/aplvoro/img/graphexport.png
%{_datadir}/aplvoro/img/imagexport.png
%{_datadir}/aplvoro/img/legende.png
%{_datadir}/aplvoro/img/lupe.png
%{_datadir}/aplvoro/img/lupecross.png
%{_datadir}/aplvoro/img/minus.png
%{_datadir}/aplvoro/img/pause.png
%{_datadir}/aplvoro/img/play.png
%{_datadir}/aplvoro/img/raster.png
%{_datadir}/aplvoro/img/scale_markings.png
%{_datadir}/aplvoro/img/xmltxt.png
%{_datadir}/aplvoro/manual/manual/APL_VORO_Manual.html
%{_datadir}/aplvoro/manual/manual/css/blue.css
%{_datadir}/aplvoro/manual/manual/css/prettyPhoto.css
%{_datadir}/aplvoro/manual/manual/images/icon-next.png
%{_datadir}/aplvoro/manual/manual/images/icon-previous.png
%{_datadir}/aplvoro/manual/manual/images/icon-zoom.png
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/dark_rounded/btnNext.png
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/dark_rounded/btnPrevious.png
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/dark_rounded/contentPattern.png
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/dark_rounded/default_thumbnail.gif
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/dark_rounded/loader.gif
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/dark_rounded/sprite.png
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/dark_square/btnNext.png
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/dark_square/btnPrevious.png
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/dark_square/contentPattern.png
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/dark_square/default_thumbnail.gif
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/dark_square/loader.gif
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/dark_square/sprite.png
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/facebook/btnNext.png
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/facebook/btnPrevious.png
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/facebook/contentPatternBottom.png
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/facebook/contentPatternLeft.png
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/facebook/contentPatternRight.png
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/facebook/contentPatternTop.png
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/facebook/default_thumbnail.gif
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/facebook/loader.gif
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/facebook/sprite.png
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/light_rounded/btnNext.png
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/light_rounded/btnPrevious.png
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/light_rounded/default_thumbnail.gif
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/light_rounded/loader.gif
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/light_rounded/sprite.png
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/light_square/btnNext.png
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/light_square/btnPrevious.png
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/light_square/default_thumbnail.gif
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/light_square/loader.gif
%{_datadir}/aplvoro/manual/manual/images/prettyPhoto/light_square/sprite.png
%{_datadir}/aplvoro/manual/manual/js/jquery-1.3.2.min.js
%{_datadir}/aplvoro/manual/manual/js/jquery.prettyPhoto.js
%{_datadir}/aplvoro/manual/manual/lessons/2d_plots.html
%{_datadir}/aplvoro/manual/manual/lessons/about.html
%{_datadir}/aplvoro/manual/manual/lessons/averages.html
%{_datadir}/aplvoro/manual/manual/lessons/images/2d_plots/new_plot.png
%{_datadir}/aplvoro/manual/manual/lessons/images/2d_plots/plot.png
%{_datadir}/aplvoro/manual/manual/lessons/images/averages/avg_table.png
%{_datadir}/aplvoro/manual/manual/lessons/images/mainwindow/mainwindow.png
%{_datadir}/aplvoro/manual/manual/lessons/images/mainwindow/sim_handler.png
%{_datadir}/aplvoro/manual/manual/lessons/images/new/new.png
%{_datadir}/aplvoro/manual/manual/lessons/images/new/setup1.png
%{_datadir}/aplvoro/manual/manual/lessons/images/new/setup2.png
%{_datadir}/aplvoro/manual/manual/lessons/images/new/setup3.png
%{_datadir}/aplvoro/manual/manual/lessons/images/voronoi_view/color_settings.png
%{_datadir}/aplvoro/manual/manual/lessons/images/voronoi_view/context_menu.png
%{_datadir}/aplvoro/manual/manual/lessons/images/voronoi_view/lipid_details.png
%{_datadir}/aplvoro/manual/manual/lessons/images/voronoi_view/rendering_area.png
%{_datadir}/aplvoro/manual/manual/lessons/images/voronoi_view/sel_model.png
%{_datadir}/aplvoro/manual/manual/lessons/images/voronoi_view/voronoi_view.png
%{_datadir}/aplvoro/manual/manual/lessons/installation.html
%{_datadir}/aplvoro/manual/manual/lessons/mainwindow.html
%{_datadir}/aplvoro/manual/manual/lessons/new.html
%{_datadir}/aplvoro/manual/manual/lessons/voronoi_view.html

%{_datadir}/icons/hicolor/256x256/apps/aplvoro.png
%{_datadir}/man/man1/aplvoro.1.gz
%{_datadir}/applications/aplvoro.desktop


%changelog
* Mon Jan 10 2022 Martin Kern
<martin.kern@uni-konstanz.de> - 3.3.3
